//
//  AddEcditFuelViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 19/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "AddEcditFuelViewController.h"
#import "CarUtilAppDelegate.h"
#import "FuelDetailViewController.h"
#import "MileageViewController.h"

@interface AddEcditFuelViewController ()

@end

@implementation AddEcditFuelViewController
@synthesize headerImageView;
@synthesize fuellbl;
@synthesize tankFullBtnlbl;
@synthesize costlbl;
@synthesize odometerText;
@synthesize fuelText;
@synthesize tankFullBtn;
@synthesize costText;
@synthesize fueldatepicker;
@synthesize tankFullHelpBtn;
@synthesize dateBtn;
@synthesize scrollView;
@synthesize headerImageType;
@synthesize dbowID;
@synthesize carId;
@synthesize minodometerValue;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.navigationItem.title = NSLocalizedString(@"Add Fuel Details", @"Add Fuel Details");
    }
    return self;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    NSInteger nextTag = textField.tag + 1;
//    UIResponder *next=[textField.superview viewWithTag:nextTag];
//    if (next)
//    {
//        [next becomeFirstResponder];
//    }
//    else
//    {
//        [textField resignFirstResponder];
//    }
//    return NO;

    [textField resignFirstResponder];
   	return YES;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    fuelText.hidden         = NO;
    costText.hidden         = NO;
    tankFullBtn.hidden      = NO;
    fuellbl.hidden          = NO;
    costlbl.hidden          = NO;
    tankFullBtnlbl.hidden   = NO;
    tankFullHelpBtn.hidden  = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveBarButtonPressed)];
    self.navigationItem.rightBarButtonItem = saveButton;
    [saveButton release];
    
    [self addHeaderImage];
    [self addPickerWithDoneButton];
    
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.scrollView  addGestureRecognizer:viewTap];
    [viewTap release];
    
    odometerText.keyboardType = UIKeyboardTypeDecimalPad;
    fuelText.keyboardType = UIKeyboardTypeDecimalPad;
    costText.keyboardType = UIKeyboardTypeDecimalPad;

    [odometerText becomeFirstResponder];
    
    scrollView.contentSize = CGSizeMake(320, 400);
    isPartial = 0;
}
- (void)viewDidUnload
{
    [self setHeaderImageView:nil];
    [self setOdometerText:nil];
    [self setFuelText:nil];
    [self setTankFullBtn:nil];
    [self setCostText:nil];
    [self setFueldatepicker:nil];
    [self setScrollView:nil];
    [self setDateBtn:nil];
    [self setFuellbl:nil];
    [self setTankFullBtnlbl:nil];
    [self setCostlbl:nil];
    [self setTankFullHelpBtn:nil];
    [super viewDidUnload];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)dealloc {
    [headerImageView release];
    [odometerText release];
    [fuelText release];
    [tankFullBtn release];
    [costText release];
    [fueldatepicker release];
    [scrollView release];
    [dateBtn release];
    [actionSheet release];
    [fuellbl release];
    [tankFullBtnlbl release];
    [costlbl release];
    [tankFullHelpBtn release];
    [super dealloc];
}
- (IBAction)tapDetected:(UIGestureRecognizer *)sender {
	// Code to respond to gesture her
//    if(sender.view.tag == 33)
//    {
//        [self showDatePicker];
//    }
//    else
    {
        DebugLog(@"you touched scrollview");
        [self.view endEditing:YES];
        [self scrollTobottom];
    }
}

-(void)saveBarButtonPressed
{
    DebugLog(@"saveBarButtonPressed");
    DebugLog(@"%d",headerImageType );
    if([costText.text doubleValue] <= 0 && headerImageType != END) {
        UIAlertView *message = [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                           message:@"Cost should be greater than zero."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil] autorelease];
        
        [message show];
        return;
    } else if([fuelText.text doubleValue] <= 0 && headerImageType != END) {
        UIAlertView *message = [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                           message:@"Fuel should be greater than zero."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil] autorelease];
        
        [message show];
        return;
    }
    else if([odometerText.text isEqualToString:@""] && headerImageType == END)
    {
        UIAlertView *message = [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                           message:@"Please fill the mandatory fields marked with star (*)"
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil] autorelease];
        
        [message show];
        return;
        
    }
    else if(([odometerText.text isEqualToString:@""] || [fuelText.text isEqualToString:@""] || [costText.text isEqualToString:@""]) && headerImageType != END)
    {
        UIAlertView *message = [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                           message:@"Please fill the mandatory fields marked with star (*)"
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil] autorelease];
        
        [message show];
        return;

    }
    else if (headerImageType != START && minodometerValue >= [odometerText.text doubleValue])
    {
        UIAlertView *message = [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                           message:[NSString stringWithFormat:@"Odometer reading should be greater than %.1f",minodometerValue]
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil] autorelease];
        
        [message show];
        return;
    }
    if (headerImageType == START)
    {
        [Flurry logEvent:@"Mileage_Reading_Added_Event"];
        [self AddMilegaeReading];
    }
    else if (headerImageType == END) {
        [Flurry logEvent:@"Mileage_Reading_Completed_Event"];
        [self UpdateMilegaeReading];
    } else {
        [Flurry logEvent:@"Mileage_Entry_Added_Event"];
    }

    [self AddMilegaeBreakReading];
    DebugLog(@"%@",self.navigationController.viewControllers);
    DebugLog(@"parent %@",[self.navigationController.viewControllers objectAtIndex:2]);
    if (headerImageType == START)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        FuelDetailViewController *parent = [self.navigationController.viewControllers objectAtIndex:2];
        if(parent != nil)
        {
            [parent reloadFuelDetails];
        }
        parent = nil;
        [self.navigationController popViewControllerAnimated:YES];
    }
}
//Internal functions ------------
-(void)addHeaderImage
{
    switch (self.headerImageType) {
        case START:
        {
            headerImageView.image = [UIImage imageNamed:@"start_flag.png"];
        }
            break;
        case INTERMEDIATE:
        {
            headerImageView.image = [UIImage imageNamed:@"milestone.png"];
            odometerText.text = [NSString stringWithFormat:@"%.1f",minodometerValue];
        }
            break;
        case END:
        {
            headerImageView.image = [UIImage imageNamed:@"end_flag.png"];
            odometerText.text = [NSString stringWithFormat:@"%.1f",minodometerValue];
            fuelText.hidden         = YES;
            costText.hidden         = YES;
            tankFullBtn.hidden      = YES;
            fuellbl.hidden          = YES;
            costlbl.hidden          = YES;
            tankFullBtnlbl.hidden   = YES;
            tankFullHelpBtn.hidden  = YES;
        }
            break;
        default:
            break;
    }
}
-(void) addPickerWithDoneButton
{
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:nil
                                     cancelButtonTitle:nil
                                destructiveButtonTitle:nil
                                     otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    fueldatepicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 40, 0, 0)];
    fueldatepicker.datePickerMode = UIDatePickerModeDate;
   // fueldatepicker.timeZone = [NSTimeZone localTimeZone];
   // fueldatepicker.maximumDate = [NSDate date];
   // [fueldatepicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:fueldatepicker];
    
    UISegmentedControl *doneButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
    doneButton.momentary = YES;
    doneButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    doneButton.segmentedControlStyle = UISegmentedControlStyleBar;
    doneButton.tintColor = [UIColor colorWithRed:0.945 green:0.580 blue:0.113 alpha:1.0];
    [doneButton addTarget:self action:@selector(donePickerBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:doneButton];
    [doneButton release];

    UISegmentedControl *cancelButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Cancel"]];
    cancelButton.momentary = YES;
    cancelButton.frame = CGRectMake(10, 7.0f, 60.0f, 30.0f);
    cancelButton.segmentedControlStyle = UISegmentedControlStyleBar;
    cancelButton.tintColor = [UIColor blackColor];
    [cancelButton addTarget:self action:@selector(cancelPickerBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:cancelButton];
    [cancelButton release];
    
    [fueldatepicker setDate:[NSDate date] animated:YES];
    datedoubleval  = [[fueldatepicker date] timeIntervalSinceReferenceDate];
    [self donePickerBtnClicked:nil];
    // [self dateChanged:nil];
}
-(void)showDatePicker
{
 //   [fueldatepicker setHidden:NO];
    [actionSheet showInView:self.view];
    [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
}
//- (void) dateChanged:(id)sender{
//    // handle date changes
//    NSDate *pickerDate = [fueldatepicker date];
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"dd MMMM yyyy"];
//    NSString *dateString = [dateFormat stringFromDate:pickerDate];
//    [dateFormat release];
//}
- (void) donePickerBtnClicked:(id)sender
{
    DebugLog(@"donePickerBtnClicked");
    NSDate *pickerDate = [fueldatepicker date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMMM yyyy"];
    [dateBtn setTitle:[dateFormat stringFromDate:pickerDate] forState:UIControlStateNormal];
    DebugLog(@"%@",dateBtn.titleLabel.text);
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    datedoubleval  = [[fueldatepicker date] timeIntervalSinceReferenceDate];
    [dateFormat release];
}
- (void) cancelPickerBtnClicked:(id)sender
{
    DebugLog(@"cancelPickerBtnClicked");
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];

}
- (IBAction)dateBtnPressed:(id)sender {
    [self showDatePicker];
}

- (IBAction)tankFullBtnPressed:(id)sender {
    if ([tankFullBtn.titleLabel.text isEqualToString:@"untick"]) {
        [tankFullBtn setImage:[UIImage imageNamed:@"tick_box.png"] forState:UIControlStateNormal];
        [tankFullBtn setTitle:@"tick" forState:UIControlStateNormal];
        isPartial = 1;
    }
    else
    {
        [tankFullBtn setImage:[UIImage imageNamed:@"untick_box.png"] forState:UIControlStateNormal];
        [tankFullBtn setTitle:@"untick" forState:UIControlStateNormal];
        isPartial = 0;
    }
}

- (IBAction)tankFullHelpBtnPressed:(id)sender {
    UIAlertView *message = [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                       message:@"Check \"Partial Fillup\" if you don't fill the tank completely. On partial fill-ups it is not possible to calculate an accurate Mileage for this specific trip. But the amount of fuel is summed up to the next full fill-up, so the calculations are still accurate.\n\nThe more full fill-ups you have,the better, because more detailed data is calculated. If your first fill-up is a partial one, the mileage calculation of the first full fill-up shown in the list may not be totally accurate. It is better to start with a full fill-up."
                                                      delegate:nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil] autorelease];
    
    [message show];
}

- (IBAction)textEditingChanged:(id)sender {
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    // do not allow . at the beggining
    if (range.location == 0 && [string isEqualToString:@"."]) {
        return NO;
    }
    NSRange temprange = [textField.text rangeOfString:@"."];
    if ((temprange.location != NSNotFound) && [string isEqualToString:@"."])
    {
        return NO;
    }
    
    if (textField == odometerText)
    {
        return !([odometerText.text length]> 7 && [string length] > range.length);
    }
    else if (textField == fuelText)
    {
        return !([fuelText.text length]> 4 && [string length] > range.length);
    }
    else if (textField == costText)
    {
        return !([costText.text length]> 11 && [string length] > range.length);
    }
    else
    {
        return YES;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == fuelText) {
        [self scrollTotop:90];
	}
    else if (textField == costText) {
        [self scrollTotop:140];
	}
}
-(void)scrollTobottom
{
    CGPoint bottomOffset = CGPointMake(0, 0);
    [scrollView setContentOffset:bottomOffset animated:YES];
}

-(void)scrollTotop : (int) value
{
    CGPoint topOffset = CGPointMake(0, value);
    [scrollView setContentOffset:topOffset animated:YES];
}
- (void) AddMilegaeBreakReading
{
    DebugLog(@"inserting data");
    //MILEAGE_BREAKS(ID INTEGER PRIMARY KEY AUTOINCREMENT, MILEAGEID INTEGER,ODOMETER_READING REAL, DATE REAL, FUEL REAL,ISPARTIAL INTEGER, COST REAL,FLAGTYPE INTEGER, CARID INTEGER)
    sqlite3_stmt    *statement;
    int personID;

    DebugLog(@"Mileage id %d %f %f %f %f [milestone type %d]",dbowID,[odometerText.text doubleValue], datedoubleval, [fuelText.text doubleValue],[costText.text doubleValue],headerImageType);

    if (sqlite3_open([[NEONAppDelegate databasePath] UTF8String], &expenseDB) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:
                               @"INSERT INTO MILEAGE_BREAKS(MILEAGEID,ODOMETER_READING, DATE,FUEL,ISPARTIAL, COST,FLAGTYPE,CARID) VALUES (%d, %.1f, %f, %.1f , %d, %.1f,%d,%d)",dbowID,
                               [odometerText.text doubleValue], datedoubleval, [fuelText.text doubleValue],isPartial,[costText.text doubleValue],headerImageType,self.carId];
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            personID = sqlite3_last_insert_rowid(expenseDB);
            DebugLog(@"MILEAGE_BREAKS Inserted %d",personID);
        } else {
            DebugLog(@"FAILED");
        }
        sqlite3_finalize(statement);
        
    }
    sqlite3_close(expenseDB);
}
- (void) UpdateMilegaeReading
{
    sqlite3_stmt  *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        //double totalkm;
//        if([startkmlbl.text doubleValue] < [endkmlbl.text doubleValue]) {
//            totalkm = [endkmlbl.text doubleValue] - [startkmlbl.text doubleValue];
//        } else {
//            totalkm = 0;
//            avglbl.text = @"0";
//        }
        DebugLog(@"UpdateMilegaeReading carid %d ",self.carId);

        NSString *updateSQL = [NSString stringWithFormat:@"update MILEAGE set ENDDATE=\"%f\" , ENDKM=\"%f\" , AVGMILEAGE=\"%f\" , COST=\"%f\" ,  CARID=\"%d\"  , FLAGTYPE=\"%d\" where id=\"%d\"", datedoubleval, [odometerText.text  doubleValue], 0.0 ,0.0,self.carId,headerImageType,self.dbowID ];
        
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            DebugLog(@"Mileage updated");
        } else {
            DebugLog(@"Mileage update failed");
        }
        sqlite3_finalize(statement);
    }
    sqlite3_close(expenseDB);
}

- (void) AddMilegaeReading
{
    
    // MILEAGE(ID INTEGER PRIMARY KEY AUTOINCREMENT,STARTDATE REAL, STARTKM REAL, ENDDATE REAL, ENDKM REAL,FUEL REAL, COST REAL, AVERAGE REAL, SELECTED TEXT, CARID INTEGER , TOTALKM REAL,FLAGTYPE INTEGER)
    sqlite3_stmt    *statement;
    int personID;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    //DebugLog(@"-- %f --,-- %f --,-- %f --,-- %f --,-- %f --,-- %f --,-- %f --,-- %@ --",datedoubleval , startKMdoubleval, enddatedoubleval , endKMdoubleval, [fuellbl.text doubleValue], [costlbl.text doubleValue], [avglbl.text doubleValue], @"0");
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
//        double totalkm;
//        if([startkmlbl.text doubleValue] < [endkmlbl.text doubleValue]) {
//            totalkm = [endkmlbl.text doubleValue] - [startkmlbl.text doubleValue];
//        } else {
//            totalkm = 0;
//        }
        DebugLog(@"date %f",datedoubleval);
        NSString *insertSQL = [NSString stringWithFormat:
                               @"INSERT INTO MILEAGE (STARTDATE , STARTKM, ENDDATE, ENDKM, AVGMILEAGE, COST, AVERAGE, SELECTED, TOTALKM, CARID,FLAGTYPE) VALUES (\"%f\", \"%f\", \"%f\" , \"%f\", \"%f\", \"%f\" , \"%f\" , \"%@\" , \"%f\", \"%d\", \"%d\")",
                               datedoubleval , [odometerText.text doubleValue], 0.0 ,0.0, 0.0, 0.0, 0.0, @"1" , 0.0, self.carId,headerImageType];
        
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            personID = sqlite3_last_insert_rowid(expenseDB);
            self.dbowID = personID;
            DebugLog(@"Mileage %d",personID);
        } else
        {
            DebugLog(@"Mileage FAILED");
            personID = -1;
            DebugLog(@"personID %d",personID);
        }
        sqlite3_finalize(statement);
        
    }
    sqlite3_close(expenseDB);
}

@end
