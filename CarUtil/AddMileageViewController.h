//
//  AddMileageViewController.h
//  CarUtil
//
//  Created by Sagar Mody on 13/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import <QuartzCore/QuartzCore.h>

@interface AddMileageViewController : UIViewController {
    //NSString *databasePath;
    sqlite3 *expenseDB;
    double startdatedoubleval;
    double enddatedoubleval;
    double startKMdoubleval;
    double endKMdoubleval;
    NSInteger cardId;
    NSInteger rowID;
    NSInteger dbowID;
    int savePressed;

}
@property (retain, nonatomic) IBOutlet UILabel *startkmlbl;
@property (retain, nonatomic) IBOutlet UILabel *endkmlbl;
@property (retain, nonatomic) IBOutlet UIButton *startdatebtn;
@property (retain, nonatomic) IBOutlet UIButton *enddatebtn;
@property (retain, nonatomic) IBOutlet UILabel *fuellbl;
@property (retain, nonatomic) IBOutlet UILabel *costlbl;
@property (retain, nonatomic) IBOutlet UILabel *avglbl;
@property (retain, nonatomic) IBOutlet UIButton *addfuelbtn;
@property (readwrite, nonatomic) NSInteger cardId;
@property (readwrite, nonatomic) NSInteger rowID;
@property (readwrite, nonatomic) NSInteger dbowID;
@property(nonatomic,readwrite)  int savePressed;
@property (retain, nonatomic) IBOutlet UIButton *helpbtn;
@property (retain, nonatomic) IBOutlet UIImageView *helpImage;
@property (retain, nonatomic) IBOutlet UIButton *neonbtn1;
@property (retain, nonatomic) IBOutlet UIButton *neonbtn2;
@property (retain, nonatomic) IBOutlet UIButton *neonbtn3;
@property (retain, nonatomic) IBOutlet UIButton *neonbtn4;

- (IBAction)startBtnPressed:(id)sender;
- (IBAction)endBtnPressed:(id)sender;
- (IBAction)addfuelPressed:(id)sender;
- (IBAction)helpPressed:(id)sender;
- (IBAction)neonbtnpressed:(id)sender;

- (void)setstartOdometeValue:(NSString *)value ;
- (void)setendOdometeValue:(NSString *)value ;
- (void)setAvgValues:(NSString *)lvalue dateDouble:(double)ldt fuel:(NSString *)lfuel cost:(NSString *)lcost;
-(void) UpdateMileageBreakTotalReading ;

@end
