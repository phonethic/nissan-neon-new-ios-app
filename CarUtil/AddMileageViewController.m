//
//  AddMileageViewController.m
//  CarUtil
//
//  Created by Sagar Mody on 13/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "AddMileageViewController.h"
#import "OdometerPickerViewController.h"
#import "DatePickerViewController.h"
#import "FuelDetailViewController.h"
#import "AddEcditFuelViewController.h"
#import "MileageViewController.h"
#import "CarUtilAppDelegate.h"


@interface AddMileageViewController ()

@end

@implementation AddMileageViewController
@synthesize startkmlbl;
@synthesize endkmlbl;
@synthesize startdatebtn;
@synthesize enddatebtn;
@synthesize fuellbl;
@synthesize costlbl;
@synthesize avglbl;
@synthesize addfuelbtn;
@synthesize dbowID;
@synthesize rowID;
@synthesize savePressed;
@synthesize helpbtn;
@synthesize helpImage;
@synthesize neonbtn1;
@synthesize neonbtn2;
@synthesize neonbtn3;
@synthesize neonbtn4;
@synthesize cardId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    UITouch *touch = [touches anyObject];

    if(self.navigationItem.rightBarButtonItem.tag == 2 || self.navigationItem.rightBarButtonItem.tag == 1) 
    {
        if(touch.view.tag == 1) {
            [self openstartReadingPressed];
        } else if(touch.view.tag == 2) {
            [self openendReadingPressed];
        } else if(touch.view.tag == 3) {
            [self openaddeditfuelPressed];
        } 
    }
    if(touch.view.tag > 10 && touch.view.tag <= 20)
    {
        CATransition *animation = [CATransition animation];
        animation.duration = 1.0;
        animation.type = kCATransitionFade;
        [self.helpImage.layer addAnimation:animation forKey:@"imageFade"];
        if(self.helpImage.tag == 17)
        {
            self.helpImage.tag = 10;
            self.helpImage.hidden = TRUE;
        } else {
            self.helpImage.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%di", self.helpImage.tag] ofType:@"png" ]];
            self.helpImage.tag = self.helpImage.tag + 1;
        }
        
    }
    if(touch.view.tag == 50) {
        CATransition *animation = [CATransition animation];
        animation.duration = 1.0;
        animation.type = kCATransitionFade;
        [self.helpImage.layer addAnimation:animation forKey:@"imageFade"];
        self.helpImage.hidden = TRUE;
        self.helpImage.tag = 10;
    }   

         
}

- (void)setstartOdometeValue:(NSString *)value {
    DebugLog(@"start val == %@",value);
    [self labelTextAnimation:startkmlbl];
    startKMdoubleval = [[value stringByReplacingOccurrencesOfString:@"," withString:@""] doubleValue];
    if(value.length > 1)
    startkmlbl.text = [[value substringToIndex:value.length-2] stringByReplacingOccurrencesOfString:@"," withString:@""];
}

- (void)setendOdometeValue:(NSString *)value {
      DebugLog(@"end val == %@",value);
    [self labelTextAnimation:endkmlbl];
    endKMdoubleval = [[value stringByReplacingOccurrencesOfString:@"," withString:@""] doubleValue];
    if(value.length > 1)
    endkmlbl.text = [[value substringToIndex:value.length-2] stringByReplacingOccurrencesOfString:@"," withString:@""];
    if(([startkmlbl.text intValue] >= [endkmlbl.text intValue]))
    {
        double avg = 0.000000;
        avglbl.text = [NSString stringWithFormat:@"%.2f", avg];
    } else {
        double avg = (([endkmlbl.text doubleValue] - [startkmlbl.text doubleValue]) / [fuellbl.text doubleValue]);
        avglbl.text = [NSString stringWithFormat:@"%.2f", avg];
    }
}

-(void)labelTextAnimation:(UILabel *)llabel
{
    CATransition *animation = [CATransition animation];
    animation.duration = 2.0;
    animation.type = kCATransitionReveal;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    [llabel.layer addAnimation:animation forKey:@"changeTextTransition"];
}

- (void)openstartReadingPressed {
    OdometerPickerViewController *odometerpickerController = [[OdometerPickerViewController alloc] initWithNibName:@"OdometerPickerViewController" bundle:nil] ;
    if(self.dbowID >= 0)
        odometerpickerController.title = @"Edit Start Reading";
    else 
        odometerpickerController.title = @"Start Reading";
    odometerpickerController.parentControllerType = 3;
    DebugLog(@"val --> %@",startkmlbl.text);
    odometerpickerController.setvalue = [NSString stringWithFormat:@"%.1f", startKMdoubleval];  
    odometerpickerController.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:odometerpickerController animated:YES];
    [odometerpickerController release];
}
- (void)openendReadingPressed {
    OdometerPickerViewController *odometerpickerController = [[OdometerPickerViewController alloc] initWithNibName:@"OdometerPickerViewController" bundle:nil] ;
    if(self.dbowID >= 0)
        odometerpickerController.title = @"Edit End Reading";
    else 
        odometerpickerController.title = @"End Reading";
    
    odometerpickerController.parentControllerType = 4;
    odometerpickerController.setvalue = [NSString stringWithFormat:@"%.1f", endKMdoubleval];
    odometerpickerController.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:odometerpickerController animated:YES];
    [odometerpickerController release];
}
- (void)openaddeditfuelPressed {
    FuelDetailViewController *fueldetailController = [[FuelDetailViewController alloc] initWithNibName:@"FuelDetailViewController" bundle:nil] ;
    fueldetailController.title = @"Fuel Details";
    //to push the UIView.
    fueldetailController.dbowID = self.dbowID;
    fueldetailController.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:fueldetailController animated:YES];
    [fueldetailController release];
   
}
- (IBAction)startBtnPressed:(id)sender {
    DatePickerViewController *datepickerController = [[DatePickerViewController alloc] initWithNibName:@"DatePickerViewController" bundle:nil] ;
    if(self.dbowID >= 0)
        datepickerController.title = @"Edit Start Date";
    else 
        datepickerController.title = @"Start Date";
    //to push the UIView.
    datepickerController.hidesBottomBarWhenPushed = NO;
    datepickerController.datePickType = 2;
    datepickerController.datedoubleval = startdatedoubleval;
    [self.navigationController pushViewController:datepickerController animated:YES];
    [datepickerController release];
}

- (IBAction)endBtnPressed:(id)sender {
    DatePickerViewController *datepickerController = [[DatePickerViewController alloc] initWithNibName:@"DatePickerViewController" bundle:nil] ;
    if(self.dbowID >= 0)
        datepickerController.title = @"Edit End Date";
    else 
        datepickerController.title = @"End Date";
    //to push the UIView.
    datepickerController.hidesBottomBarWhenPushed = NO;
    datepickerController.datePickType = 3;
    datepickerController.datedoubleval = enddatedoubleval;
    [self.navigationController pushViewController:datepickerController animated:YES];
    [datepickerController release];
}

-(void)setDateFieldValue:(NSString *) value doubleval:(double)val type:(int)ltype
{
    if(ltype==2) {
        [startdatebtn setTitle:value forState:UIControlStateNormal];
        startdatedoubleval = val;
    } else if(ltype==3) {
        [enddatebtn setTitle:value forState:UIControlStateNormal];
        enddatedoubleval = val;
    }
}

- (IBAction)addfuelPressed:(id)sender {
    AddEcditFuelViewController *addfueldetailsController = [[AddEcditFuelViewController alloc] initWithNibName:@"AddEcditFuelViewController" bundle:nil] ;
    if(self.dbowID==-1)
    {
      //  addfueldetailsController.dbowID = -1;
        addfueldetailsController.title = @"Add Fuel Details";
    } else {
      //  addfueldetailsController.dbowID = self.dbowID;
        addfueldetailsController.title = @"Edit Fuel Details";
    }
    addfueldetailsController.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:addfueldetailsController animated:YES];
    [addfueldetailsController release];
}

- (IBAction)helpPressed:(id)sender {
    CATransition *animation = [CATransition animation];
    animation.duration = 1.0;
    animation.type = kCATransitionFade;
    [self.helpImage.layer addAnimation:animation forKey:@"imageFade"];
    if(self.helpImage.tag == 10) {
        self.helpImage.hidden = FALSE;
        self.helpImage.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%di", self.helpImage.tag] ofType:@"png" ]];
        self.helpImage.tag = self.helpImage.tag + 1;
    } else {
        self.helpImage.hidden = TRUE;
        self.helpImage.tag = 10;
    }

}

- (IBAction)neonbtnpressed:(id)sender {
    CATransition *animation = [CATransition animation];
    animation.duration = 1.0;
    animation.type = kCATransitionFade;
    [self.helpImage.layer addAnimation:animation forKey:@"imageFade"];
    //if(self.neonbtn1.tag == 50) {
        self.helpImage.hidden = FALSE;
        self.helpImage.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"indicators" ofType:@"png" ]];
    self.helpImage.tag = self.neonbtn1.tag;
    //} else {
    //    self.helpImage.hidden = TRUE;
    //    self.helpImage.tag = 10;
    //}
}

- (void) UpdateMilegaeReading
{   
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];

    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        double totalkm;
        if([startkmlbl.text doubleValue] < [endkmlbl.text doubleValue]) {
            totalkm = [endkmlbl.text doubleValue] - [startkmlbl.text doubleValue];
        } else {
            totalkm = 0;
            avglbl.text = @"0";
        }
        NSString *updateSQL = [NSString stringWithFormat:@"update MILEAGE set STARTDATE=\"%f\" , STARTKM=\"%f\", ENDDATE=\"%f\" , ENDKM=\"%f\" , FUEL=\"%f\" , COST=\"%f\" ,  AVERAGE=\"%f\"  , TOTALKM=\"%f\" where id=\"%d\"", startdatedoubleval, startKMdoubleval , enddatedoubleval, endKMdoubleval , [fuellbl.text doubleValue], [costlbl.text doubleValue] , [avglbl.text doubleValue] , totalkm ,self.dbowID ];
        
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            //[self showMessage:@"Contact updated"];
        } else {
            //[self showMessage:@"Contact updatation failed"];
        }
        sqlite3_finalize(statement);
    }         
    sqlite3_close(expenseDB);
}

- (void) AddMilegaeReading
{   
    sqlite3_stmt    *statement;
    int personID;    
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    DebugLog(@"-- %f --,-- %f --,-- %f --,-- %f --,-- %f --,-- %f --,-- %f --,-- %@ --",startdatedoubleval , startKMdoubleval, enddatedoubleval , endKMdoubleval, [fuellbl.text doubleValue], [costlbl.text doubleValue], [avglbl.text doubleValue], @"0");
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        double totalkm;
        if([startkmlbl.text doubleValue] < [endkmlbl.text doubleValue]) {
            totalkm = [endkmlbl.text doubleValue] - [startkmlbl.text doubleValue];
        } else {
            totalkm = 0;
        }
        NSString *insertSQL = [NSString stringWithFormat: 
                               @"INSERT INTO MILEAGE (STARTDATE , STARTKM, ENDDATE, ENDKM, FUEL, COST, AVERAGE, SELECTED, TOTALKM, CARID) VALUES (\"%f\", \"%f\", \"%f\" , \"%f\", \"%f\", \"%f\" , \"%f\" , \"%@\" , \"%f\", \"%d\")", 
                               startdatedoubleval , startKMdoubleval, enddatedoubleval ,endKMdoubleval, [fuellbl.text doubleValue], [costlbl.text doubleValue], [avglbl.text doubleValue], @"0" , totalkm, self.cardId];
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            personID = sqlite3_last_insert_rowid(expenseDB);
            self.dbowID = personID;
            DebugLog(@"%d",personID);
            //[self showMessage:@"Contact added"];
        } else {
             DebugLog(@"FAILED");
            personID = -1;
            //[self showMessage:@"Failed to add contact"];
        }
        sqlite3_finalize(statement);
        
    } 
    sqlite3_close(expenseDB);

//    NSNumber *myDoubleNumber = [NSNumber numberWithDouble:datedoubleval];
    [self UpdateMilegaeBreakReading];
    MileageViewController *parent = [self.navigationController.viewControllers objectAtIndex:1];
    //call the method on the parent view controller
    if(parent != nil) 
        [parent adddataInMileageArray:personID startdate:startdatedoubleval enddate:enddatedoubleval startreading:[startkmlbl.text doubleValue] endreading:[endkmlbl.text doubleValue] fuel:[fuellbl.text doubleValue] average:[avglbl.text doubleValue] cost:[costlbl.text doubleValue] selected:@"0"];
    parent = nil;
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)setAvgValues:(NSString *)lvalue dateDouble:(double)ldt fuel:(NSString *)lfuel cost:(NSString *)lcost
{
    costlbl.text = lcost;
    fuellbl.text = lfuel;
    
//    NSNumber *avg = [NSNumber numberWithDouble:(([endkmlbl.text doubleValue] - [startkmlbl.text doubleValue]) / [fuellbl.text doubleValue])] ; 
//    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
//    [formatter setMaximumFractionDigits:2];
//    [formatter setMinimumFractionDigits:2];
//    avglbl.text = [NSString stringWithFormat:@"%@", [formatter stringFromNumber:avg]];
//    [formatter release];
    if(([startkmlbl.text intValue] >= [endkmlbl.text intValue]) || [endkmlbl.text intValue] == 0)
    {
        double avg = 0.000000;
        avglbl.text = [NSString stringWithFormat:@"%.2f", avg];
    } else {
        double avg = (([endkmlbl.text doubleValue] - [startkmlbl.text doubleValue]) / [fuellbl.text doubleValue]);
        avglbl.text = [NSString stringWithFormat:@"%.2f", avg];
    }
}

- (void)viewDidLoad
{
#ifdef ENABLE_BACK_BUTTON
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [backButton release]; 
    
#endif
#ifdef GOOGLE_ANALYTICS
    if (![[GANTracker sharedTracker] trackEvent:@"Nissan CarUitl Application iOS"
                                         action:@"Launch iOS"
                                          label:@"Example iOS"
                                          value:-1
                                      withError:NULL]) {
        DebugLog(@"error in trackEvent");
    }
#endif 
    self.helpImage.hidden = TRUE;
    if(self.dbowID >= 0) {
        [self getMileageObject];
        UIBarButtonItem *editmileageButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editMileageValues:)];
        self.navigationItem.rightBarButtonItem = editmileageButton;
        self.navigationItem.rightBarButtonItem.tag = 0;
        [editmileageButton release];
        [self disableAll];
    } else {
        UIBarButtonItem *savemileageButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveMileageValues:)];
        self.navigationItem.rightBarButtonItem = savemileageButton;
        self.navigationItem.rightBarButtonItem.tag = 1;
        [savemileageButton release];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd MMM yyyy"];
        NSString *dateString = [dateFormat stringFromDate:[NSDate date]];  
        [dateFormat release];
        [startdatebtn setTitle:dateString forState:UIControlStateNormal];
        startdatedoubleval = [[NSDate date] timeIntervalSinceReferenceDate];
        self.savePressed = 0;

    }
    
    //r=100, g=33, b=1
    [super viewDidLoad];        

    // Do any additional setup after loading the view from its nib.
}

-(void) enabledAll {
    //startkmlbl.enabled = TRUE;
    //endkmlbl.enabled = TRUE;
    startdatebtn.enabled = TRUE ;
    enddatebtn.enabled = TRUE;
    //fuellbl.enabled = TRUE;
    addfuelbtn.enabled = TRUE;
}
-(void) disableAll {
    //startkmlbl.enabled = FALSE;
    //endkmlbl.enabled = FALSE;
    startdatebtn.enabled = FALSE ;
    enddatebtn.enabled = FALSE;
    //fuellbl.enabled = FALSE;
    addfuelbtn.enabled = FALSE;
}
- (void) editMileageValues: (id) sender {
	UIBarButtonItem *stopeditButton = [[ UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemDone target:self action: @selector(stopEditingMileageValues:)];
    self.navigationItem.rightBarButtonItem = stopeditButton;
    self.navigationItem.rightBarButtonItem.tag = 2;
    [stopeditButton release];
    [self enabledAll];
}

- (void) stopEditingMileageValues: (id) sender {
	UIBarButtonItem *editButton = [[ UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemEdit target: self  action: @selector(editMileageValues:)];
    self.navigationItem.rightBarButtonItem = editButton;
    self.navigationItem.rightBarButtonItem.tag = 0;
    [editButton release];
    [self UpdateMilegaeReading];
    [self disableAll];
    MileageViewController *parent = [self.navigationController.viewControllers objectAtIndex:1];
    if(parent != nil) 
        [parent reloadAll];
    parent = nil;

}


- (void) getMileageObject
{
    sqlite3_stmt    *statement;
    
     const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"SELECT * FROM MILEAGE WHERE id=\"%d\"", self.dbowID];
        
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(expenseDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                //NSInteger primaryKey = sqlite3_column_int(compiledStatement, 0);
				double startdateCol = sqlite3_column_double(statement, 1);
                startdatedoubleval = startdateCol;
                NSDate *startDate = [NSDate dateWithTimeIntervalSinceReferenceDate:startdateCol]; 
                NSDateFormatter *sdateFormat = [[NSDateFormatter alloc] init];
                [sdateFormat setDateFormat:@"dd MMM yyyy"];
                NSString *sdateString = [sdateFormat stringFromDate:startDate];  
                [startdatebtn setTitle:sdateString forState:UIControlStateNormal];
                [sdateFormat release];
                
				double startreadingCol = sqlite3_column_double(statement, 2);
                startKMdoubleval = startreadingCol;
                DebugLog(@"----> start --->%f",startKMdoubleval);
                startkmlbl.text = [[NSNumber numberWithInt:startreadingCol] stringValue];  
                
				double enddateCol = sqlite3_column_double(statement, 3);
                enddatedoubleval = enddateCol;
                if(0 == (int)enddateCol) {
                    [enddatebtn setTitle:@"" forState:UIControlStateNormal];
                } else {
                    NSDate *endDate = [NSDate dateWithTimeIntervalSinceReferenceDate:enddateCol]; 
                    NSDateFormatter *edateFormat = [[NSDateFormatter alloc] init];
                    [edateFormat setDateFormat:@"dd MMM yyyy"];
                    NSString *edateString = [edateFormat stringFromDate:endDate];  
                    [enddatebtn setTitle:edateString forState:UIControlStateNormal];
                    [edateFormat release];
                }
                
                double endreadingCol = sqlite3_column_double(statement, 4);
                endKMdoubleval = endreadingCol;
                DebugLog(@"----> end--->%f",endKMdoubleval);
                endkmlbl.text = [[NSNumber numberWithInt:endreadingCol] stringValue];  
                
                double fuelCol = sqlite3_column_double(statement, 5);
                fuellbl.text = [[NSNumber numberWithDouble:fuelCol] stringValue];  
                
                double costCol = sqlite3_column_double(statement, 6);
                costlbl.text = [[NSNumber numberWithDouble:costCol] stringValue];  
                
                double avgCol = sqlite3_column_double(statement, 7);
                avglbl.text = [[NSNumber numberWithDouble:avgCol] stringValue];  
                
            } else {
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(expenseDB);
    }

}

- (void) saveMileageValues: (id) sender
{
    #ifdef TESTING
    [TestFlight passCheckpoint:@"New Mileage Reading Added"];
    #endif
#ifdef GOOGLE_ANALYTICS
    if (![[GANTracker sharedTracker] trackEvent:@"Nissan CarUitl Application iOS"
                                         action:@"Add New Mileage Pressed"
                                          label:@""
                                          value:-1
                                      withError:NULL]) {
        DebugLog(@"error in trackEvent");
    }
#endif
    self.savePressed = 1;
    [self AddMilegaeReading];
}


- (void)UpdateMilegaeBreakReading
{   
    sqlite3_stmt    *statement;
    int personID;
    
     const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"update  MILEAGE_BREAKS  set MILEAGEID=\"%d\" where MILEAGEID=\"%d\"", self.dbowID, -1];
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            personID = sqlite3_last_insert_rowid(expenseDB);
            DebugLog(@"%d",personID);
        } else {
            DebugLog(@"FAILED");
        }
        sqlite3_finalize(statement);
        
    } 
    sqlite3_close(expenseDB);
}

-(void) UpdateMileageBreakTotalReading {
    sqlite3_stmt    *fuelstatement;
    sqlite3_stmt    *coststatement;
    sqlite3_stmt    *updatetatement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *fuelquerySQL = [NSString stringWithFormat:@"SELECT sum(FUEL) as TOTAL FROM MILEAGE_BREAKS where MILEAGEID=\"%d\"", self.dbowID];
        
        const char *query_stmt_fuel = [fuelquerySQL UTF8String];
        
        if (sqlite3_prepare_v2(expenseDB, query_stmt_fuel, -1, &fuelstatement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(fuelstatement) == SQLITE_ROW)
            {
                int field1 =  sqlite3_column_double(fuelstatement, 0);
                fuellbl.text = [NSString stringWithFormat:@"%d", field1];
                DebugLog(@"fuel = %d",field1);
                
            } else {
                //Failed
            }
            sqlite3_finalize(fuelstatement);
        }
        
        NSString *querySQL = [NSString stringWithFormat:@"SELECT sum(COST) as TOTAL FROM MILEAGE_BREAKS where MILEAGEID=\"%d\"", self.dbowID];
        
        const char *query_stmt_cost = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(expenseDB, query_stmt_cost, -1, &coststatement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(coststatement) == SQLITE_ROW)
            {
                int field1 =  sqlite3_column_double(coststatement, 0);
                costlbl.text = [NSString stringWithFormat:@"%d", field1];
                DebugLog(@"cost = %d",field1);
                
            } else {
                //Failed
            }
            sqlite3_finalize(coststatement);
        }
        
         NSString *updateSQL = [NSString stringWithFormat:@"update MILEAGE set FUEL=\"%f\" , COST=\"%f\"  where id=\"%d\"", [fuellbl.text doubleValue], [costlbl.text doubleValue] , self.dbowID ];
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, update_stmt, -1, &updatetatement, NULL);
        if (sqlite3_step(updatetatement) == SQLITE_DONE)
        {
            //personID = sqlite3_last_insert_rowid(expenseDB);
            //DebugLog(@"%d",personID);
        } else {
            DebugLog(@"FAILED");
        }
        sqlite3_finalize(updatetatement);
    }
    sqlite3_close(expenseDB);
}

- (void) DeleteMilegaeBreakReading
{   
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM MILEAGE_BREAKS WHERE MILEAGEID=\"%d\"", -1];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            //[self showMessage:@"Contact deleted"];
            //[self clearData:nil];
        } else {
            //[self showMessage:@"Failed to delete contact"];
        }
        sqlite3_finalize(statement);
    }         
    sqlite3_close(expenseDB);    
}

- (void)viewDidUnload
{
    [self setStartkmlbl:nil];
    [self setEndkmlbl:nil];
    [self setStartdatebtn:nil];
    [self setEnddatebtn:nil];
    [self setFuellbl:nil];
    [self setCostlbl:nil];
    [self setAvglbl:nil];
    [self setAddfuelbtn:nil];
    [self setHelpbtn:nil];
    [self setHelpImage:nil];
    [self setNeonbtn1:nil];
    [self setNeonbtn2:nil];
    [self setNeonbtn3:nil];
    [self setNeonbtn4:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    if(self.navigationItem.rightBarButtonItem.tag == 1) {
        if(self.savePressed == 0 && self.dbowID == -1) 
            [self DeleteMilegaeBreakReading];
    }
    [startkmlbl release];
    [endkmlbl release];
    [startdatebtn release];
    [enddatebtn release];
    [fuellbl release];
    [costlbl release];
    [avglbl release];
    [addfuelbtn release];
    [helpbtn release];
    [helpImage release];
    [neonbtn1 release];
    [neonbtn2 release];
    [neonbtn3 release];
    [neonbtn4 release];
    [super dealloc];
}

@end
