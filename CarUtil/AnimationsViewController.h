//
//  AnimationsViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 03/07/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface AnimationsViewController : UIViewController {
    MPMoviePlayerController *moviePlayer;
}

@end
