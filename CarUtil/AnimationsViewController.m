//
//  AnimationsViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 03/07/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "AnimationsViewController.h"
#import "CarUtilAppDelegate.h"

@interface AnimationsViewController ()

@end

@implementation AnimationsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"logoanimation" ofType:@"mp4"]];
    moviePlayer =  [[MPMoviePlayerController alloc] initWithContentURL:url];
    moviePlayer.view.frame = [[UIScreen mainScreen] applicationFrame];
    moviePlayer.scalingMode = MPMovieScalingModeAspectFill;
    moviePlayer.movieSourceType = MPMovieSourceTypeFile;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:moviePlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(moviePlayBackDidFinish:) 
                                                 name:MPMoviePlayerDidExitFullscreenNotification object:moviePlayer];
    
    
    moviePlayer.controlStyle = MPMovieControlStyleNone;
    moviePlayer.shouldAutoplay = YES;
    [moviePlayer setFullscreen:YES animated:YES];
    [moviePlayer prepareToPlay];
    [self.view addSubview:moviePlayer.view ];
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    
    MPMoviePlayerController *player = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerDidExitFullscreenNotification 
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self      
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:player];
    [moviePlayer stop];
    [moviePlayer.view removeFromSuperview];
    [moviePlayer release];
    
    [(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] animationStop];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(moviePlayer != nil)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMoviePlayerDidExitFullscreenNotification
                                                      object:moviePlayer];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:moviePlayer];
        [moviePlayer stop];
        [moviePlayer.view removeFromSuperview];
        [moviePlayer release];
        
        [(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] animationStop];

    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc 
{
    [super dealloc];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
