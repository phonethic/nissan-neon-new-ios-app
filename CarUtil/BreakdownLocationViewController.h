//
//  BreakdownLocationViewController.h
//  CarUtil
//
//  Created by Kirti Nikam on 06/02/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MessageUI/MFMailComposeViewController.h>

#define REVERSE_API(LAT,LON) [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/xml?latlng=%f,%f&sensor=false",LAT,LON]
#define MAP_URL(LAT,LON) [NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%f,%f",LAT,LON]

#define BREAKDOWN_EMAILID @"customercare.nissan@hai.net.in"

@interface BreakdownLocationViewController : UIViewController <CLLocationManagerDelegate,MKMapViewDelegate,NSXMLParserDelegate,UITextFieldDelegate,MFMailComposeViewControllerDelegate>
{
    CLLocationManager *locationManager;
    NSMutableData *webData;
    NSXMLParser *xmlParser;
    BOOL elementFound;
}
@property (copy,nonatomic) NSString *reverseAddress;
@property (readwrite,nonatomic) double currentLatitude;
@property (readwrite,nonatomic) double currentLongitude;

@property (retain, nonatomic) IBOutlet MKMapView *mapView;
@property (retain, nonatomic) IBOutlet UILabel *lbl;

@property (retain, nonatomic) IBOutlet UIView *proceedView;
@property (retain, nonatomic) IBOutlet UITextField *phonefield;
@property (retain, nonatomic) IBOutlet UIButton *proceedBtn;

- (IBAction)proceedBtnPressed:(id)sender;

@end
