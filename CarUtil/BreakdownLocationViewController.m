//
//  BreakdownLocationViewController.m
//  CarUtil
//
//  Created by Kirti Nikam on 06/02/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "BreakdownLocationViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "MBProgressHUD.h"

@interface BreakdownLocationViewController ()
@property (nonatomic, retain) MBProgressHUD *progressHUD;
@end

@implementation BreakdownLocationViewController
@synthesize mapView;
@synthesize proceedView;
@synthesize phonefield;
@synthesize proceedBtn;
@synthesize currentLatitude,currentLongitude;
@synthesize reverseAddress;
@synthesize progressHUD;
@synthesize lbl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [locationManager stopUpdatingLocation];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    lbl.font = HELVETICA_FONT(16);
    phonefield.font = HELVETICA_FONT(16);
    [self changeButtonFontAndTextColor:proceedBtn];
    
    mapView.mapType = MKMapTypeStandard;
    mapView.zoomEnabled = YES;
    mapView.scrollEnabled = YES;
    mapView.delegate = self;
    mapView.showsUserLocation = YES;
    [self.view addSubview:mapView];
    
    [self showProgressHUDWithMessage:@"Fetching your location"];
    
    proceedView.layer.cornerRadius = 10;
    
    if ([CLLocationManager locationServicesEnabled])
    {
        DebugLog(@"Location services are enabled.");
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        [locationManager startUpdatingLocation];
        [self performSelector:@selector(stopUpdatingCoreLocation:) withObject:@"Timed Out" afterDelay:10];
    }
    else
    {
        DebugLog(@"Location services not enabled.");
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"You currently have all location services for this device disabled. If you proceed, you will be asked to confirm whether location services should be reenabled." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [servicesDisabledAlert show];
        [servicesDisabledAlert release];
    }
}
-(void)changeButtonFontAndTextColor:(UIButton *)btn
{
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:TABLE_SELECTION_COLOR forState:UIControlStateHighlighted];
    [btn setBackgroundColor:[UIColor grayColor]];
    btn.titleLabel.font = HELVETICA_FONT(15);
    btn.layer.cornerRadius = 5;
    btn.layer.masksToBounds = YES;
    btn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    btn.layer.borderWidth = 1.0;
}

- (void)showProgressHUDWithMessage:(NSString *)message {
    self.progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.progressHUD.labelText = message;
    self.progressHUD.labelFont = HELVETICA_FONT(15);
    self.progressHUD.mode = MBProgressHUDModeIndeterminate;
    [self.view bringSubviewToFront:self.progressHUD];
    [self.progressHUD show:YES];
}
-(void)hideProgressHUD:(BOOL)animated
{
    [self.progressHUD hide:animated];
}
- (void) locationManager:(CLLocationManager *) manager didFailWithError:(NSError *) error
{
    [locationManager stopUpdatingLocation];
}

- (void)stopUpdatingCoreLocation:(NSString *)state
{
    [locationManager stopUpdatingLocation];
    [self saveCurrentocation:[locationManager location]];
}

-(void)saveCurrentocation:(CLLocation *)lLocation
{
    // If it's not possible to get a location, then return.
	if (!lLocation) {
        UIAlertView *errorAlert = [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                             message:@"Failed to get your current location. Please check your internet connection and try again."
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil] autorelease];
        [errorAlert show];
        [self hideProgressHUD:YES];
		return;
	}
    // Configure the new event with information from the location.
    [self.mapView setRegion:MKCoordinateRegionMake([lLocation coordinate], MKCoordinateSpanMake(0.2, 0.2))];
    [self hideProgressHUD:YES];
    currentLatitude  = lLocation.coordinate.latitude;
    currentLongitude = lLocation.coordinate.longitude;
    DebugLog(@"%f --- %f",currentLatitude,currentLongitude);
    [self getReverseAddress];
    [self showAlertMessage];
}

-(void)getReverseAddress
{
    DebugLog(@"getReverseAddress");
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:REVERSE_API(currentLatitude,currentLongitude)] cachePolicy:YES timeoutInterval:20.0];
	NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    [theConnection autorelease];
}

-(void)showAlertMessage
{
    proceedView.hidden = FALSE;
    [phonefield becomeFirstResponder];
    [self.view bringSubviewToFront:proceedView];
}


#pragma mark textField Delegate Methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return !([textField.text length]>= 13 && [string length] > range.length);
}

#pragma mark mailComposeController Delegate Methods

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
     if (result == MFMailComposeResultSent)
     {
         [Flurry logEvent:@"Breakdown_Location_Event"];
     }
    [controller dismissModalViewControllerAnimated:YES];
     proceedView.hidden = TRUE;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Connection Delegate Methods

-(void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *) response
{

}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *) data
{
    if(webData==nil)
	{
		webData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[webData appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *) error
{
    DebugLog(@"didFailWithError error %@",error);
    [self hideProgressHUD:YES];

}

-(void) connectionDidFinishLoading:(NSURLConnection *) connection
{
    DebugLog(@"connectionDidFinishLoading");
    if (webData)
    {
        NSString *result = [[NSString alloc] initWithData:webData encoding:NSASCIIStringEncoding];
		DebugLog(@"\n result:%@\n\n", result);
        
        NSString *xmlDataFromChannelSchemes = [[NSString alloc] initWithString:result];
		NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
		xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
		[xmlParser setDelegate:self];
		[xmlParser parse];
        [xmlParser release];
		[xmlDataFromChannelSchemes release];
		[result release];
		[webData release];
		webData = nil;
    }
    else
    {
        DebugLog(@"webdata = %@",webData);
    }
}

#pragma mark XML Parser Delegate Methods

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"GeocodeResponse"])
    {
    }
    else if ([elementName isEqualToString:@"result"])
    {
    }
    else if ([elementName isEqualToString:@"formatted_address"])
    {
      //  reverseAddress = [[NSString alloc] init];
        elementFound = YES;
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    
    if (elementFound)
    {
        reverseAddress = [[NSString alloc] initWithString:string];
        DebugLog(@"-- %@ ---",reverseAddress);
    }
    elementFound = FALSE;
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
}

-(void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError{
}

-(void)parserDidEndDocument:(NSXMLParser *)parser{
    
}

- (void)viewDidUnload
{
    [self setMapView:nil];
    [self setReverseAddress:nil];
    [self setProceedView:nil];
    [self setPhonefield:nil];
    [self setProceedBtn:nil];
    [self setLbl:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)dealloc {
    [mapView release];
    [reverseAddress release];
    reverseAddress = nil;
    [proceedView release];
    [phonefield release];
    [proceedBtn release];
    [lbl release];
    [super dealloc];
}
- (IBAction)proceedBtnPressed:(id)sender {
    @try{
        MFMailComposeViewController *picker=[[[MFMailComposeViewController alloc]init] autorelease];
        picker.mailComposeDelegate=self;
        NSString *msgBody;
        if (reverseAddress == nil)
        {
            msgBody = [NSString stringWithFormat:@"Address URL:  %@",MAP_URL(currentLatitude,currentLongitude)];
        }
        else
        {
            msgBody=[NSString stringWithFormat:@"Address:  %@\n\nAddress URL:  %@",reverseAddress,MAP_URL(currentLatitude,currentLongitude)];
        }
        picker.navigationBar.tintColor = DEFAULT_COLOR;
        [picker setToRecipients:[NSArray arrayWithObject:BREAKDOWN_EMAILID]];
        [picker setSubject:[NSString stringWithFormat:@"Breakdown %@",phonefield.text]];
        [picker setMessageBody:msgBody isHTML:NO];
        [self presentModalViewController:picker animated:YES];
    }
    @catch (NSException *exception)
    {
        //DebugLog(@"Exception %@",exception.reason);
    }
}
@end
