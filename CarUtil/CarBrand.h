//
//  CarBrand.h
//  CarUtil
//
//  Created by Rishi on 05/02/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarBrand : NSObject {
    
}
@property (nonatomic, readwrite) int carmodelid;
@property (nonatomic, copy) NSString *carmodel;

-(id)initWithID:(int)lcarmodelid image:(NSString *)lcarmodel;
@end
