//
//  CarUtilAppDelegate.h
//  CarUtil
//
//  Created by Rishi Saxena on 29/03/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "Reachability.h"
#import <sqlite3.h>
#import <FacebookSDK/FacebookSDK.h>
//#import "Crittercism.h"

#define ENABLE_BACK_BUTTON
#define NO_STARTING_ANIMATION

#define NEONAppDelegate (CarUtilAppDelegate*)[[UIApplication sharedApplication] delegate] 

extern NSString *const SCSessionStateChangedNotification;

@class  animationsTestViewController;
@class  AnimationsViewController;
@class MKNumberBadgeView;

@interface CarUtilAppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate> {
    UINavigationController *myCarNavigationController;   //MyCar Tab
    UINavigationController *tipsNavigationController;    //Tips Tab
	UINavigationController *toolsNavigationController;   //Tools Tab
	UINavigationController *nissanNavigationController;  //Nissan Tab
    UINavigationController *contactNavigationController; //Contact Tab
    sqlite3 *expenseDB;
    NSString *databasePath;
    //Reachability* hostReach;
    Reachability* internetReach;
    Boolean networkavailable;
    
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UITabBarController *tabBarController;
@property (strong, nonatomic) AnimationsViewController *animationviewController;
@property(nonatomic) Boolean networkavailable;


@property (nonatomic, assign)  UINavigationController *tipsNavigationController;
@property (nonatomic, assign)  UINavigationController *toolsNavigationController;
@property (nonatomic, assign)  UINavigationController *nissanNavigationController;
@property (nonatomic, assign)  UINavigationController *contactNavigationController;
@property (nonatomic, assign)  UINavigationController *myCarNavigationController;
@property (nonatomic, retain)  NSString *databasePath;
@property (readwrite, nonatomic)  int badgeValue1;
@property (readwrite, nonatomic)  int badgeValue2;
@property (strong, nonatomic)  MKNumberBadgeView* badgeOne;
@property (strong, nonatomic)  MKNumberBadgeView* badgeTwo;
@property (copy, nonatomic)  NSString* tabType;

- (NSString *) md5:(NSString *) input;
-(void) writeToTextFile:(NSString *)lcontent name:(NSString *)lname;
-(NSString *) getTextFromFile:(NSString *)lname;
-(void)animationStop;
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;
@end
