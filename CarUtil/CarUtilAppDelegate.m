//
//  CarUtilAppDelegate.m
//  CarUtil
//
//  Created by Rishi Saxena on 29/03/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import<MapKit/MapKit.h>  
#import<CoreLocation/CoreLocation.h>
#import "CarUtilAppDelegate.h"
#import "CarUtilFirstViewController.h"
#import "CarUtilSecondViewController.h"
#import "CarUtilThirrdViewController.h"
#import "CarUtilFourthViewController.h"
#import "ContactViewController.h"
#import "MyCarViewController.h"
#import "ImageGalleryViewController.h"
#import "AnimationsViewController.h"
#import "MKNumberBadgeView.h"
#import <Parse/Parse.h>
#import "Appirater.h"
#import "GalleryGridViewController.h"
#import <CommonCrypto/CommonDigest.h>

#import "MFSideMenu.h"
#import "SideMenuViewController.h"

//#define FLURRY_APPID @"94NVYQDDMKGJ3FW5RSCF" //Main Flurry ID
#define FLURRY_APPID @"HRWPHQW8NZVW5YF4T5GN" //TEST APP Flurry ID
#define APPIRATER_APPID @"557559431"

NSString *const SCSessionStateChangedNotification = @"com.facebook.Neon:MSessionStateChangedNotification";

// Dispatch period in seconds
static const NSInteger kGANDispatchPeriodSec = 20;
//Dispatch App ID
static NSString* const kAnalyticsAccountId = @"UA-15726015-30";

@implementation CarUtilAppDelegate
@synthesize databasePath;
@synthesize networkavailable;
@synthesize window = _window;
@synthesize tabBarController = _tabBarController;
@synthesize animationviewController;
@synthesize tipsNavigationController,toolsNavigationController,nissanNavigationController,myCarNavigationController,contactNavigationController;
@synthesize badgeOne;
@synthesize badgeTwo;
@synthesize badgeValue1;
@synthesize badgeValue2;
@synthesize tabType;

//- (GalleryGridViewController *)homeController {
//    return [[GalleryGridViewController alloc] initWithNibName:@"GalleryGridViewController" bundle:nil];
//}
//
//- (UINavigationController *)navigationController {
//    return [[UINavigationController alloc]
//            initWithRootViewController:[self homeController]];
//}
//
//- (MFSideMenu *)sideMenu
//{
//    SideMenuViewController *rightSideMenuController = [[SideMenuViewController alloc] init];
//    UINavigationController *navigationController = [self navigationController];
//    
//    MFSideMenu *sideMenu = [MFSideMenu menuWithNavigationController:navigationController
//                                             leftSideMenuController:nil
//                                            rightSideMenuController:rightSideMenuController];
//    
//    rightSideMenuController.sideMenu = sideMenu;
//    return sideMenu;
//}

- (void) setupNavigationControllerApp {
    // self.sideMenu.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    
    UIViewController *homeController = [[GalleryGridViewController alloc] initWithNibName:@"GalleryGridViewController" bundle:nil];
    nissanNavigationController = [ [ UINavigationController alloc ] initWithRootViewController:homeController];
    nissanNavigationController.navigationBar.tintColor = DEFAULT_COLOR;
    SideMenuViewController *rightSideMenuController = [[SideMenuViewController alloc] init];
    UINavigationController *navigationController = nissanNavigationController;//[self navigationController];
    
    MFSideMenu *sideMenu = [MFSideMenu menuWithNavigationController:navigationController
                                             leftSideMenuController:nil
                                            rightSideMenuController:rightSideMenuController];
    
    rightSideMenuController.sideMenu = sideMenu;
    
    self.window.rootViewController = sideMenu.navigationController;
    [self.window makeKeyAndVisible];
    
    [self.animationviewController release];
    [homeController release];
    [nissanNavigationController release];
    [rightSideMenuController release];
}
/* with tab
- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    NSUInteger orientations = UIInterfaceOrientationMaskAll;
    DebugLog(@"self.window.rootViewController %@",self.window.rootViewController);
    if (self.window.rootViewController) {
        UINavigationController * presentedNav = (UINavigationController *)[(UITabBarController *)self.tabBarController selectedViewController];
        
        if (presentedNav != nil) {
            UIViewController* presented = [[presentedNav viewControllers] lastObject];
            DebugLog(@"presented %@",presented);
            orientations = [presented supportedInterfaceOrientations];
        }
        
    }
    return orientations;
}*/
- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    NSUInteger orientations = UIInterfaceOrientationMaskAll;
    
    if (self.window.rootViewController) {
            if ([self.window.rootViewController isKindOfClass:[AnimationsViewController class]]) {
                orientations = [self.window.rootViewController supportedInterfaceOrientations];
            }
            else{
                UIViewController* presented = [[(UINavigationController *)self.window.rootViewController viewControllers] lastObject];
                orientations = [presented supportedInterfaceOrientations];
            }
    }
    return orientations;
}
- (void)dealloc
{
    DebugLog(@"app dealloc");
    [databasePath release];
    [self stopCheckNetwork];
    //[[GANTracker sharedTracker] stopTracker];
    [_window release];
    [_tabBarController release];
    [super dealloc];
}

-(void)animationStop
{
    //[animationviewController.view removeFromSuperview];
    [UIView  transitionWithView:self.window duration:0.4  options:UIViewAnimationOptionTransitionCrossDissolve
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         [self setupNavigationControllerApp];
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
     DebugLog(@"info didFinishLaunchingWithOptions");
    //[UIApplication sharedApplication].applicationIconBadgeNumber = 0;
#ifdef TESTING
    [TestFlight takeOff:TESTFLIGHT_TEAM_TOKEN];
    

    [TestFlight setDeviceIdentifier:[[UIDevice currentDevice] uniqueIdentifier]];

    
    [TestFlight passCheckpoint:@"App Started"];
#endif    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    
    // Create Database
    [self checkAndCreateDatabase];
    //Start for checking rechability
    [self startCheckNetwork];
    [self setAppRateAlert];
    [Flurry startSession:FLURRY_APPID];
    [Flurry logEvent:@"NEON App Started"];
    [Appirater appLaunched:YES];
    
    //Add SlideMenu
    //[self setupNavigationControllerApp];
    
    
    animationviewController = [[AnimationsViewController alloc] initWithNibName:@"AnimationsViewController" bundle:nil];
    //[self.window addSubview:animationviewController.view];
    self.window.rootViewController = animationviewController;
    [self.window makeKeyAndVisible];
    
//    [Parse setApplicationId:@"xVf594RDAOScmU75UHoRXP07fpXqXhsl34rrB5ek"
//                  clientKey:@"cFjC1Hq1aGuzrzdzo2FZaVCc5NIdw4SpSbJyzMev"];
    /*
     { "type": "tips", "action": "com.phonethics.neon.MyCustomReceiver", "alert": "Notification", "title": "Neon", "badge": "2" }
     { "type": "gallery", "action": "com.phonethics.neon.MyCustomReceiver", "alert": "Notification", "title": "Neon", "badge": "2" }
    */
    
    [Parse setApplicationId:@"pxAyG1oQ4nYkoLQlnLbmMimLQiAnUuWaGt6vwywh"
                  clientKey:@"yQRxNvNQmOoBC3CYr5Gn16ZSmijpRpOm1J3UWJHH"];
    
    [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound];
    
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                UITextAttributeTextColor: [UIColor whiteColor],
                          UITextAttributeTextShadowColor: [UIColor blackColor],
                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)],
                                     UITextAttributeFont: HELVETICA_FONT(17.0)
     }];
    
//    [[UIBarButtonItem appearance] setTitleTextAttributes: @{
//                                UITextAttributeTextColor: [UIColor whiteColor],
//                          UITextAttributeTextShadowColor: [UIColor blackColor],
//                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)],
//                                     UITextAttributeFont: HELVETICA_FONT(15.0)
//     } forState:UIControlStateNormal];
//    [[UIBarButtonItem appearance] setTitleTextAttributes: @{
//                                UITextAttributeTextColor: [UIColor whiteColor],
//                          UITextAttributeTextShadowColor: [UIColor blackColor],
//                         UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)],
//                                     UITextAttributeFont: HELVETICA_FONT(15.0)
//     } forState:UIControlStateHighlighted];
    
    /*
    UIViewController *viewController1 = [[MyCarViewController alloc] initWithNibName:@"MyCarViewController" bundle:nil] ;
    
    UIViewController *viewController2 = [[CarUtilSecondViewController alloc] initWithNibName:@"CarUtilSecondViewController" bundle:nil] ;

    UIViewController *viewController3 = [[CarUtilThirrdViewController alloc] initWithNibName:@"CarUtilThirrdViewController" bundle:nil] ;
    
    UIViewController *viewController4 = [[GalleryGridViewController alloc] initWithNibName:@"GalleryGridViewController" bundle:nil];
    
    UIViewController *viewController5 = [[ContactViewController alloc] initWithNibName:@"ContactViewController" bundle:nil];
   
    
    myCarNavigationController = [ [ UINavigationController alloc ] initWithRootViewController: viewController1 ];
    myCarNavigationController.navigationBar.tintColor = [UIColor blackColor];
    myCarNavigationController.navigationBar.tag = 1;
	[viewController1 release]; 
    
    toolsNavigationController = [ [ UINavigationController alloc ] initWithRootViewController: viewController2 ];
     toolsNavigationController.navigationBar.tintColor = [UIColor blackColor];
     toolsNavigationController.navigationBar.tag = 2;
	[viewController2 release]; 
    
    tipsNavigationController = [ [ UINavigationController alloc ] initWithRootViewController: viewController3 ];
    tipsNavigationController.navigationBar.tintColor = [UIColor blackColor];
    tipsNavigationController.navigationBar.tag = 3;
	[viewController3 release]; 
    
    nissanNavigationController = [ [ UINavigationController alloc ] initWithRootViewController: viewController4 ];
    nissanNavigationController.navigationBar.tintColor = [UIColor blackColor];
    nissanNavigationController.navigationBar.tag = 4;
    //[nissanNavigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"list-bg-40.png"] forBarMetrics:UIBarMetricsDefault];
	[viewController4 release]; 
    
    contactNavigationController = [ [ UINavigationController alloc ] initWithRootViewController: viewController5 ];
    contactNavigationController.navigationBar.tintColor = [UIColor blackColor];
    contactNavigationController.navigationBar.tag = 5;
	[viewController5 release]; 
    
    NSMutableArray *viewControllers;
    viewControllers = [[NSMutableArray alloc] init];
    
    //Tab 1
    [viewControllers addObject:tipsNavigationController];
	[tipsNavigationController release];
    //Tab 2
    [viewControllers addObject:toolsNavigationController];
	[toolsNavigationController release];
    //Tab 3
    [viewControllers addObject:nissanNavigationController];
	[nissanNavigationController release];
    //Tab 4
    [viewControllers addObject:myCarNavigationController];
	[myCarNavigationController release];
    //Tab 5
    [viewControllers addObject:contactNavigationController];
	[contactNavigationController release];
    
    self.tabBarController = [[[UITabBarController alloc] init] autorelease];
    
    self.tabBarController.viewControllers = viewControllers;
	
    [viewControllers release];
    
    self.tabBarController.delegate = self; 
    
    [self addCustomTabBarIcons];
    
    AnimationsViewController *tempviewController = [[AnimationsViewController alloc] initWithNibName:@"AnimationsViewController" bundle:nil];
    self.animationviewController = tempviewController;
    [tempviewController release];

#ifdef NO_STARTING_ANIMATION
    self.window.rootViewController = self.tabBarController;
    self.tabBarController.selectedIndex = 2;
#else
        [self.window addSubview:animationviewController.view];
#endif
    // Create Database
    [self checkAndCreateDatabase];
    //Start for checking rechability
    [self startCheckNetwork];
#ifdef GOOGLE_ANALYTICS
    //Add Google analytics
    [self addAnalytics];
#endif
    [self setAppRateAlert];
    [Flurry startSession:FLURRY_APPID];
    [Flurry logEvent:@"NEON App Started"];
    [Appirater appLaunched:YES];
    [self.window makeKeyAndVisible];
*/    
   /* [Crittercism initWithAppID: @"5016211eeeaf416d39000002"
                        andKey:@"c6xga0z2mfyuz4rjco5jlek3gwbk"
                     andSecret:@"4gcei7virlhtswfmjo15zjxera9nkdpq"];
   */
    return YES;
}

-(void) setAppRateAlert
{
    [Appirater setAppId:APPIRATER_APPID];
    [Appirater setDaysUntilPrompt:2];
    [Appirater setUsesUntilPrompt:2];
    [Appirater setSignificantEventsUntilPrompt:-1];
    [Appirater setTimeBeforeReminding:3];
    //[Appirater setDebug:YES];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    //DebugLog(@"didRegisterForRemoteNotificationsWithDeviceToken : devicetoken %@",deviceToken);
    [PFPush storeDeviceToken:deviceToken];
    [PFPush subscribeToChannelInBackground:@"" block:^(BOOL succeeded, NSError *error) {
        if (succeeded)
            DebugLog(@"Successfully subscribed to broadcast channel!");
        else
            DebugLog(@"Failed to subscribe to broadcast channel; Error: %@",error);
    }];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    DebugLog(@"didFailToRegisterForRemoteNotificationsWithError");
    if (error.code == 3010) {
        DebugLog(@"Push notifications are not supported in the iOS Simulator.");
    } else {
        // show some alert or otherwise handle the failure to register.
        DebugLog(@"application:didFailToRegisterForRemoteNotificationsWithError: %@", error);
	}
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    DebugLog(@"didReceiveRemoteNotification  : userInfo %@",userInfo);
    [PFPush handlePush:userInfo];
//	for (NSString *key in [userInfo allKeys]) {
//		DebugLog(@"%@ is %@",key, [userInfo objectForKey:key]);
//	}
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateInactive) {
        DebugLog(@"UIApplicationStateInactive");
    } else if (state == UIApplicationStateBackground){
        DebugLog(@"UIApplicationStateBackground");
    } else if (state == UIApplicationStateActive){
        DebugLog(@"UIApplicationStateActive");
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:BADGE_NOTIFICATIONAME object:nil userInfo:userInfo];

//    NSDictionary * mainDict = [userInfo objectForKey:@"aps"];
//    //DebugLog(@"badge = %d",[[mainDict objectForKey:@"badge"] integerValue]);
//    //DebugLog(@"type = %@",[userInfo objectForKey:@"title"]);
//    tabType = [userInfo objectForKey:@"type"];
//    if(tabType != nil && [tabType isEqualToString:@"tips"])
//    {
//        self.tabBarController.selectedIndex = 0;
//        [self selectparticularTab];
//        MKNumberBadgeView *tempbadge = (MKNumberBadgeView *)[self.tabBarController.tabBar viewWithTag:20];
//        tempbadge.value = [[mainDict objectForKey:@"badge"] integerValue];
//        badgeValue1 = [[mainDict objectForKey:@"badge"] integerValue];
//    } else if(tabType != nil && [tabType isEqualToString:@"gallery"]) {
//        self.tabBarController.selectedIndex = 2;
//        [self selectparticularTab];
//        MKNumberBadgeView *tempbadge = (MKNumberBadgeView *)[self.tabBarController.tabBar viewWithTag:30];
//        tempbadge.value = [[mainDict objectForKey:@"badge"] integerValue];
//        badgeValue2 = [[mainDict objectForKey:@"badge"] integerValue];
//    }
}
#pragma MD5 callbacks
- (NSString *) md5:(NSString *) input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
    
}
#pragma File handling callbacks
//Method writes a string to a text file
-(void) writeToTextFile:(NSString *)lcontent name:(NSString *)lname
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fileName = [NSString stringWithFormat:@"%@.txt",lname];
    //DebugLog(@"filename = %@" , fileName);
    NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    // DebugLog(@"fullpath = %@" , fullPath);
    
    NSError* error;
    if ([fileManager fileExistsAtPath:fullPath])
    {
        [self removeFile:fileName];
        [lcontent writeToFile:fullPath atomically:NO encoding:NSUTF8StringEncoding error:&error];
    } else {
        [lcontent writeToFile:fullPath atomically:NO encoding:NSUTF8StringEncoding error:&error];
    }
//    if(error != nil)
//        DebugLog(@"write error %@", error);
//    else {
//        DebugLog(@"file created %@", fileName);
//    }
    
	
}

-(NSString *) getTextFromFile:(NSString *)lname
{
	NSFileManager *fileManager = [NSFileManager defaultManager];
    //get the documents directory:
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	
	//make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@.txt",lname];
    //DebugLog(@"filename = %@" , fileName);
    NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    DebugLog(@"fullpath = %@" , fullPath);
    NSError* error;
    if ([fileManager fileExistsAtPath:fullPath])
    {
        NSString *data = [NSString stringWithContentsOfFile:fullPath encoding:NSUTF8StringEncoding error:&error];
//        if(error != nil)
//        {
//            DebugLog(@"read error %@", error);
//            return @"";
//        }
        return data;
    } else {
        return @"";
    }
}

- (void)removeFile:(NSString*)lname {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:lname];
    BOOL success = [fileManager removeItemAtPath: fullPath error:NULL];
    if(success == YES)
        DebugLog(@" %@ removed",lname);
    else
        DebugLog(@" %@ NOT removed",lname);
}

/*
-(void) addAnalytics
{
    DebugLog(@"addAnalytics trackEvent");
    [[GANTracker sharedTracker] startTrackerWithAccountID:kAnalyticsAccountId
                                           dispatchPeriod:kGANDispatchPeriodSec
                                                 delegate:NULL];
    //[[GANTracker sharedTracker] setSampleRate:50];

    NSError *error;
    
    if (![[GANTracker sharedTracker] trackEvent:@"Nissan CarUitl Application iOS"
                                         action:@"Launch iOS"
                                          label:@""
                                          value:-1
                                      withError:NULL]) {
        DebugLog(@"error in trackEvent");
    }
    
    if (![[GANTracker sharedTracker] trackPageview:@"/app_entry_point"
                                         withError:&error]) {
        DebugLog(@"error in trackPageview");
    }

}
*/
-(void) startCheckNetwork
{

	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];	
	
    internetReach = [[Reachability reachabilityForInternetConnection] retain];
	[internetReach startNotifer];
    [self updateReachabitlityFlag:internetReach];

}

-(void) stopCheckNetwork
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    [internetReach release];
	internetReach = nil;
}

- (void)reachabilityChanged:(NSNotification *)note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateReachabilityStatus: curReach];
}

- (void) updateReachabilityStatus: (Reachability*) curReach
{
	if(curReach == internetReach)
	{
		[self updateReachabitlityFlag: curReach];
    }
}

- (void) updateReachabitlityFlag: (Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    BOOL connectionRequired=[curReach connectionRequired];

    switch (netStatus)
    {
        case NotReachable:
        {
            networkavailable =  NO;
            break;
        }		
            
        case ReachableViaWWAN:
        {
			if(connectionRequired==NO)
			{	 
                networkavailable = YES;
			}
			else
			{
				//this is for invoking internet library
				NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:@"maps.google.com"] cachePolicy:NO timeoutInterval:15.0] ;
				
				NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
				if (theConnection) {
					connectionRequired	= NO;	
				}
				else {
				}
				
				[theConnection autorelease];
				
			}
			break;
        }
        case ReachableViaWiFi:
        {
            if(connectionRequired==NO)
            {	 
                networkavailable = YES;
            }	 
            break;
		}
    }
    if(connectionRequired)
    {
		networkavailable = NO;
    }
    DebugLog(@"network status = %d",networkavailable);
    [self sendNetworkNotification];
}
#pragma Wifi Notification CallBack
-(void)sendNetworkNotification
{
    if (networkavailable) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NETWORK_NOTIFICATION object:nil];
    }
}
-(void) addCustomTabBarIcons
{
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] > 4.9) {
        //iOS 5
        for(int iLoop = 0; iLoop < [self.tabBarController.viewControllers count]; iLoop++)
        {
            UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"tabIconiPhone_%i_gray.png",iLoop + 1]]];        
            [imgView setFrame:CGRectMake(iLoop * 64, 0, 64, 49)];
            imgView.autoresizingMask =  UIViewAutoresizingFlexibleLeftMargin   |
            UIViewAutoresizingFlexibleWidth        |
            UIViewAutoresizingFlexibleRightMargin  |
            UIViewAutoresizingFlexibleTopMargin    |
            UIViewAutoresizingFlexibleHeight       |
            UIViewAutoresizingFlexibleBottomMargin ;
            [self.tabBarController.tabBar insertSubview:imgView atIndex:1]; 
            imgView.tag = 1000;
            [imgView release];
        }
    }
    else {
        //iOS 4.whatever and below
        for(int iLoop = 0; iLoop < [self.tabBarController.viewControllers count]; iLoop++)
        {
            UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"tabIconiPhone_%i_gray.png",iLoop + 1]]];        
            [imgView setFrame:CGRectMake(iLoop * 64, 0, 64, 49)];
            imgView.autoresizingMask =  UIViewAutoresizingFlexibleLeftMargin   |
            UIViewAutoresizingFlexibleWidth        |
            UIViewAutoresizingFlexibleRightMargin  |
            UIViewAutoresizingFlexibleTopMargin    |
            UIViewAutoresizingFlexibleHeight       |
            UIViewAutoresizingFlexibleBottomMargin ;
            [self.tabBarController.tabBar insertSubview:imgView atIndex:0]; 
            imgView.tag = 1000;
			
            [imgView release];
        }
    }
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"tabIconiPhone_%i.png",3]]];
	
	[imgView setFrame:CGRectMake(64*2, 0, 64, 49)];
	[self.tabBarController.tabBar addSubview:imgView]; 
	[imgView release];
    
    badgeOne = [[MKNumberBadgeView alloc] initWithFrame:CGRectMake(64*0+30, -20, 40, 40)];
    badgeOne.hideWhenZero = YES;
    badgeOne.fillColor = DEFAULT_COLOR;
    badgeOne.strokeColor = [UIColor blackColor];
    badgeOne.textColor = [UIColor blackColor];
    badgeOne.backgroundColor = [UIColor clearColor];
    badgeOne.tag = 20;
    self.badgeOne.value = 0;
    [self.tabBarController.tabBar addSubview:badgeOne];
    [badgeOne release];
    
    badgeTwo = [[MKNumberBadgeView alloc] initWithFrame:CGRectMake(64*2+30, -20, 40, 40)];
    badgeTwo.hideWhenZero = YES;
    badgeTwo.fillColor = DEFAULT_COLOR;
    badgeTwo.strokeColor = [UIColor blackColor];
    badgeTwo.textColor = [UIColor blackColor];
    badgeTwo.backgroundColor = [UIColor clearColor];
    badgeTwo.tag = 30;
    self.badgeTwo.value = 0;
    [self.tabBarController.tabBar addSubview:badgeTwo];
    [badgeTwo release];

}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification 
{
      NSLog(@"didReceiveLocalNotification");
    UIApplicationState state = [application applicationState];
    DebugLog(@"state %d",state);
    
    if (state == UIApplicationStateInactive) {
        DebugLog(@"UIApplicationStateInactive");
        [[NSNotificationCenter defaultCenter] postNotificationName:BADGE_NOTIFICATIONAME object:nil userInfo:notification.userInfo];
    } else if (state == UIApplicationStateBackground){
        DebugLog(@"UIApplicationStateBackground");
    } else if (state == UIApplicationStateActive){
        DebugLog(@"UIApplicationStateActive");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:notification.alertBody 
                                                       delegate:self cancelButtonTitle:@"Ok" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        //DebugLog(@"Notification Body: %@", notification.alertBody);
        //DebugLog(@"info %@", notification.userInfo);
       // [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
       // [[NSNotificationCenter defaultCenter] postNotificationName:BADGE_NOTIFICATIONAME object:nil userInfo:notification.userInfo];
    }
}

-(void) checkAndCreateDatabase {
    
    NSString *docsDir;
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    // Build the path to the database file
    NSString *databasepath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"nissan.db"]];
    self.databasePath = databasepath;
    [databasepath release];
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        DebugLog(@" new path = %@",databasePath);
        
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
        {
            DebugLog(@"Database opened successfully");
            char *errMsg;
            const char *sql_stmt_exp = "CREATE TABLE IF NOT EXISTS EXPENSES(ID INTEGER PRIMARY KEY AUTOINCREMENT,DATE REAL,COST TEXT, TYPE TEXT, NOTES TEXT, READING TEXT, SELECTED TEXT, CARID INTEGER)";
            
            if (sqlite3_exec(expenseDB, sql_stmt_exp, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                DebugLog(@"Failed to create table");
            }
            
            const char *sql_stmt_mycar = "CREATE TABLE IF NOT EXISTS MYCAR(ID INTEGER PRIMARY KEY AUTOINCREMENT, IMAGE TEXT, BRAND TEXT, MODEL TEXT, CARNUMBER TEXT, MFGDATE REAL, RCNO TEXT, INSURER TEXT, POLICY TEXT, POLICYDATE REAL, CHASSIS TEXT, BUYDATE REAL, SELECTED TEXT)";
            
            if (sqlite3_exec(expenseDB, sql_stmt_mycar, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                DebugLog(@"Failed to create table");
            }
            
//            const char *sql_stmt_mileage = "CREATE TABLE IF NOT EXISTS MILEAGE(ID INTEGER PRIMARY KEY AUTOINCREMENT,STARTDATE REAL, STARTKM REAL, ENDDATE REAL, ENDKM REAL, FUEL REAL, COST REAL, AVERAGE REAL, SELECTED TEXT, CARID INTEGER , TOTALKM REAL)";
//            
//            if (sqlite3_exec(expenseDB, sql_stmt_mileage, NULL, NULL, &errMsg) != SQLITE_OK)
//            {
//                DebugLog(@"Failed to create table");
//            }
            
//            const char *sql_stmt_mileage_breakpoints = "CREATE TABLE IF NOT EXISTS MILEAGE_BREAKS(ID INTEGER PRIMARY KEY AUTOINCREMENT, MILEAGEID INTEGER, DATE REAL, FUEL REAL, COST REAL, CARID INTEGER)";
//            
//            if (sqlite3_exec(expenseDB, sql_stmt_mileage_breakpoints, NULL, NULL, &errMsg) != SQLITE_OK)
//            {
//                DebugLog(@"Failed to create table");
//            }
            
            //------
            const char *sql_stmt_mileage = "CREATE TABLE IF NOT EXISTS MILEAGE(ID INTEGER PRIMARY KEY AUTOINCREMENT,STARTDATE REAL, STARTKM REAL, ENDDATE REAL, ENDKM REAL,AVGMILEAGE REAL, COST REAL, AVERAGE REAL, SELECTED TEXT, CARID INTEGER , TOTALKM REAL,FLAGTYPE INTEGER)";
            
            if (sqlite3_exec(expenseDB, sql_stmt_mileage, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                DebugLog(@"Failed to create table");
            }
            
            const char *sql_stmt_mileage_breakpoints1 = "CREATE TABLE IF NOT EXISTS MILEAGE_BREAKS(ID INTEGER PRIMARY KEY AUTOINCREMENT, MILEAGEID INTEGER,ODOMETER_READING REAL, DATE REAL, FUEL REAL,ISPARTIAL INTEGER, COST REAL,FLAGTYPE INTEGER, CARID INTEGER)";
            
            if (sqlite3_exec(expenseDB, sql_stmt_mileage_breakpoints1, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                DebugLog(@"Failed to create MILEAGE_BREAKS1 table");
            }
            //----------
            
            const char *sql_stmt_reminder = "CREATE TABLE IF NOT EXISTS REMINDERS(ID INTEGER PRIMARY KEY AUTOINCREMENT, REMINDERONOFF INTEGER, MESSAGE TEXT, DATE REAL, COMPLETED INTEGER, CARID INTEGER , PREVALUE INTEGER, PRETYPE TEXT)";
            
            if (sqlite3_exec(expenseDB, sql_stmt_reminder, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                DebugLog(@"Failed to create table");
            }
            
            const char *sql_stmt_exytype = "CREATE TABLE IF NOT EXISTS EXPTYPE(ID INTEGER PRIMARY KEY AUTOINCREMENT, TYPE TEXT, CARID INTEGER)";
            
            if (sqlite3_exec(expenseDB, sql_stmt_exytype, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                DebugLog(@"Failed to create table");
            }
            for(int i=0;i<=2;i++) {
                sqlite3_stmt    *statement;
                NSString *insertSQL;
                switch (i) {
                    case 0:
                        insertSQL = [NSString stringWithFormat:@"INSERT INTO EXPTYPE (TYPE) VALUES (\"%@\")", @"Repair"];
                        break;
                    case 1:
                        insertSQL = [NSString stringWithFormat:@"INSERT INTO EXPTYPE (TYPE) VALUES (\"%@\")", @"Oil Change"];
                        break;
                    case 2:
                        insertSQL = [NSString stringWithFormat:@"INSERT INTO EXPTYPE (TYPE) VALUES (\"%@\")", @"Wash"];
                        break;
                        break;
                        
                    default:
                        insertSQL = insertSQL = [NSString stringWithFormat:@"INSERT INTO EXPTYPE (TYPE) VALUES (\"%@\")", @""];;
                        break;
                }
                const char *insert_stmt = [insertSQL UTF8String];
                sqlite3_prepare_v2(expenseDB, insert_stmt, -1, &statement, NULL);
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                } else {
                }
                sqlite3_finalize(statement);
            }
            
        } else {
            DebugLog(@"Failed to open/create database");
        }
        
        sqlite3_close(expenseDB);
        
    } else {
        DebugLog(@" old path = %@",databasePath);
    }
    
    //[filemgr release];
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    DebugLog(@"applicationWillResignActive");
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    #ifdef TESTING
    [TestFlight passCheckpoint:@"App Closed"];
    #endif
    DebugLog(@"applicationDidEnterBackground");
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    DebugLog(@"applicationWillEnterForeground");
    [Appirater appEnteredForeground:YES];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were     (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    DebugLog(@"applicationDidBecomeActive");
    //DebugLog(@"==%d==",badgeValue);
    if(badgeValue1 != 0)
    {
        MKNumberBadgeView *tempbadge1 = (MKNumberBadgeView *)[self.tabBarController.tabBar viewWithTag:20];
        tempbadge1.value = badgeValue1;
    }
    if(badgeValue2 != 0)
    {
        MKNumberBadgeView *tempbadge2 = (MKNumberBadgeView *)[self.tabBarController.tabBar viewWithTag:30];
        tempbadge2.value = badgeValue2;

    }
    //[UIApplication sharedApplication].applicationIconBadgeNumber = 0;

}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
     DebugLog(@"applicationWillTerminate");
     [FBSession.activeSession close];
}

// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
   // [self.tabBarController.navigationController popToRootViewControllerAnimated:YES];
    for (UIView *view in tabBarController.tabBar.subviews){
		
		if([view isKindOfClass:[UIImageView class]])
		{
			if(view.tag != 1000 && view.tag != 20)
				[view removeFromSuperview];
		}
	}
    MKNumberBadgeView *tempbadge1 = (MKNumberBadgeView *)[self.tabBarController.tabBar viewWithTag:20];
    MKNumberBadgeView *tempbadge2 = (MKNumberBadgeView *)[self.tabBarController.tabBar viewWithTag:30];
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"tabIconiPhone_%i.png",self.tabBarController.selectedIndex+1]]];
	[self.tabBarController.tabBar setBackgroundColor:[UIColor blackColor]];
    [imgView setFrame:CGRectMake(self.tabBarController.selectedIndex * 64, 0, 64, 49)];
    imgView.autoresizingMask =  UIViewAutoresizingFlexibleLeftMargin   |
    UIViewAutoresizingFlexibleWidth        |
    UIViewAutoresizingFlexibleRightMargin  |
    UIViewAutoresizingFlexibleTopMargin    |
    UIViewAutoresizingFlexibleHeight       |
    UIViewAutoresizingFlexibleBottomMargin ;
    if(tabBarController.selectedIndex == 0) {
        [self.tabBarController.tabBar insertSubview:imgView belowSubview:tempbadge1];
    } else if(tabBarController.selectedIndex == 2) {
        [self.tabBarController.tabBar insertSubview:imgView belowSubview:tempbadge2];
    } else {
        [self.tabBarController.tabBar addSubview:imgView];
    }
    [imgView release];

    //self.badgeOne.value = 0;
    //badgeOne.tag = 20;
    //[self.tabBarController.tabBar addSubview:badgeOne];

}

-(void)selectparticularTab
{
    for (UIView *view in self.tabBarController.tabBar.subviews){
		
		if([view isKindOfClass:[UIImageView class]])
		{
			if(view.tag != 1000 && view.tag != 20)
				[view removeFromSuperview];
		}
	}
    MKNumberBadgeView *tempbadge1 = (MKNumberBadgeView *)[self.tabBarController.tabBar viewWithTag:20];
    MKNumberBadgeView *tempbadge2 = (MKNumberBadgeView *)[self.tabBarController.tabBar viewWithTag:30];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"tabIconiPhone_%i.png",self.tabBarController.selectedIndex+1]]];
	[self.tabBarController.tabBar setBackgroundColor:[UIColor blackColor]];
    [imgView setFrame:CGRectMake(self.tabBarController.selectedIndex * 64, 0, 64, 49)];
    imgView.autoresizingMask =  UIViewAutoresizingFlexibleLeftMargin   |
    UIViewAutoresizingFlexibleWidth        |
    UIViewAutoresizingFlexibleRightMargin  |
    UIViewAutoresizingFlexibleTopMargin    |
    UIViewAutoresizingFlexibleHeight       |
    UIViewAutoresizingFlexibleBottomMargin ;
    if(self.tabBarController.selectedIndex == 0) {
        [self.tabBarController.tabBar insertSubview:imgView belowSubview:tempbadge1];
    } else if(self.tabBarController.selectedIndex == 2) {
        [self.tabBarController.tabBar insertSubview:imgView belowSubview:tempbadge2];
    } else {
        [self.tabBarController.tabBar addSubview:imgView];
    }
    [imgView release];

}


// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
{
    DebugLog(@"didEndCustomizingViewControllers");
}

- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState)state
                      error:(NSError *)error
{
    // FBSample logic
    // Any time the session is closed, we want to display the login controller (the user
    // cannot use the application unless they are logged in to Facebook). When the session
    // is opened successfully, hide the login controller and show the main UI.
    switch (state) {
        case FBSessionStateOpen: {
            DebugLog(@"mytoken=%@",[FBSession.activeSession accessToken]);
        }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
        {
            // FBSample logic
            // Once the user has logged in, we want them to be looking at the root view.
            //[self.navController popToRootViewControllerAnimated:NO];
            
            [FBSession.activeSession closeAndClearTokenInformation];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:@"" forKey:@"userId"];
            [defaults setObject:@"" forKey:@"accessToken"];
            [defaults synchronize];
            //[self showLoginView];
        }
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SCSessionStateChangedNotification
                                                        object:session];
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:error.localizedDescription
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
    NSArray *permissions = [NSArray arrayWithObjects:@"publish_actions", @"user_photos", nil];
    return [FBSession openActiveSessionWithPermissions:permissions
                                          allowLoginUI:allowLoginUI
                                     completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                         [self sessionStateChanged:session state:state error:error];
                                     }];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // FBSample logic
    // We need to handle URLs by passing them to FBSession in order for SSO authentication
    // to work.
    return [FBSession.activeSession handleOpenURL:url];
}




@end
