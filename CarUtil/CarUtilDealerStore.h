//
//  CarUtilDealerStore.h
//  CarUtil
//
//  Created by Kirti Nikam on 15/02/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarUtilDealerStore : NSObject
@property (nonatomic, readwrite) int storeId;
@property (nonatomic, copy) NSString *storeName;
@property (nonatomic, copy) NSString *storeAddress;
@property (nonatomic, copy) NSString *storePhone1;
@property (nonatomic, copy) NSString *storePhone2;
@property (nonatomic, copy) NSString *storeMobile1;
@property (nonatomic, copy) NSString *storeMobile2;
@property (nonatomic, copy) NSString *storeFax;
@property (nonatomic, copy) NSString *storeEmail;
@property (nonatomic, copy) NSString *storeWebsite;
@property (nonatomic, retain) NSMutableArray *storePhotos;
//@property (nonatomic, copy) NSString *storePhoto2;
@property (nonatomic, copy) NSString *storeDescription;
@property (nonatomic, readwrite) double storeLatitude;
@property (nonatomic, readwrite) double storeLongitute;
@end
