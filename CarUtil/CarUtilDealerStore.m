//
//  CarUtilDealerStore.m
//  CarUtil
//
//  Created by Kirti Nikam on 15/02/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "CarUtilDealerStore.h"

@implementation CarUtilDealerStore
@synthesize storeId,storeName,storeAddress,storeEmail,storeWebsite,storePhone1,storePhone2,storeFax,storeMobile1,storeMobile2,storePhotos,storeDescription,storeLatitude,storeLongitute;

-(id)init
{
    self = [super init];
    
    if(self) {
        self.storePhotos = [[[NSMutableArray alloc] init] autorelease];
    }
	return self;
}
-(void)dealloc{
    DebugLog(@"CarUtilDealerStore dealloc");
    self.storeName = nil;
    self.storeAddress = nil;
    self.storeEmail = nil;
    self.storeWebsite = nil;
    self.storePhone1= nil;
    self.storePhone2= nil;
    self.storeFax= nil;
    self.storeMobile1= nil;
    self.storeMobile2= nil;
    //[storePhotos release];
    [self.storePhotos removeAllObjects];
    self.storePhotos  = nil;
    self.storeDescription = nil;
	[super dealloc];
}
@end
