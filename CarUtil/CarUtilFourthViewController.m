//
//  CarUtilFourthViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 29/03/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "CarUtilFourthViewController.h"
#import "CarUtilAppDelegate.h"
#import "MyCarViewController.h"
#import "CarUtilGallery.h"
#import "MKNumberBadgeView.h"

#define NEON_GALLERY_LINK @"http://stage.phonethics.in/proj/neon/neon_gallery.php"
//#define NEON_GALLERY_LINK @"http://localhost/carutil/neon_gallery.xml"

@interface CarUtilFourthViewController ()

@end

@implementation CarUtilFourthViewController
@synthesize imgscrollView;
@synthesize view1;
@synthesize img2;
@synthesize but2;
@synthesize audioPlayer;
@synthesize audioPlayer1;
@synthesize audioPlayer2;
@synthesize photos = _photos;
@synthesize galleryArray;
@synthesize socialLink;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.navigationItem.title = NSLocalizedString(@"Nissan", @"Nissan");
        //self.tabBarItem.image = [UIImage imageNamed:@"nissantab"];
    }
    return self;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    if(toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        //[imgscrollView setContentSize:fcreat];

    }
    else if(toInterfaceOrientation == UIInterfaceOrientationLandscapeRight || toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft){
        //[imgscrollView setContentSize:CGSizeMake(self.view.frame.size.width, imageView.frame.size.height)];
//        CGRect frame = self.imgscrollView.frame;
//        frame.origin.x = 0;
//        frame.origin.y = 0;
//        [self.imgscrollView scrollRectToVisible:frame animated:YES];
        [imgscrollView bringSubviewToFront:view1];
        [imgscrollView bringSubviewToFront:img2];
        [imgscrollView bringSubviewToFront:but2];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
//    CGRect frame = CGRectMake(0.0, 0.0, self.view.bounds.size.width, 48);
//    UIView *v = [[UIView alloc] initWithFrame:frame];
//    [v setBackgroundColor:[UIColor colorWithRed:0.1 green:0.2 blue:0.6 alpha:0.8]];
//    [v setAlpha:0.5];
//    [[self.tabBarController tabBar] insertSubview:v atIndex:0];
//    [v release];
    #ifdef TESTING
    [TestFlight passCheckpoint:@"Nissan Home Tab Pressed"];
    #endif 
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    DebugLog(@"Nissan Home Tab nissan viewWillDisappear");
//    MKNumberBadgeView *tempbadge = (MKNumberBadgeView *)[self.tabBarController.tabBar viewWithTag:30];
//    tempbadge.value = 0;
//    [NEONAppDelegate setBadgeValue2:0];
//    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    DebugLog(@"Nissan Home Tab nissan viewDidDisappear");
}
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    //DebugLog(@"scroll x= %f",sender.contentOffset.x);
   // DebugLog(@"scroll y= %f",sender.contentOffset.y);
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    

}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //UITouch *touch =[touches anyObject]; 
    //CGPoint location =[touch locationInView:self.view];
    //DebugLog(@"Touched view  %@",[touch.view class] );
    
    //DebugLog(@"%f %f",touch.view.frame.origin.x,touch.view.bounds.origin.y);
    //DebugLog(@"%f %f",touch.view.bounds.origin.x,touch.view.bounds.origin.y);
    //DebugLog(@"%f %f",touch.view.bounds.size.width,touch.view.bounds.size.height);
    
    NSArray *allTouches = [touches allObjects];
    for (UITouch *touch in allTouches)
    {
        DebugLog(@"%d",touch.view.tag);
    }
    
//    if([[touch view] isKindOfClass:[UIView class]])
//    {
//        if(CGRectContainsPoint([self.xtrailBody frame], location))
//        {
//            NSInteger imgTag = [touch view].tag;
//            DebugLog(@"%d",imgTag);
//        } else if(CGRectContainsPoint([self.sunnyBody frame], location)) {
//            NSInteger imgTag = [touch view].tag;
//            DebugLog(@"%d",imgTag);
//        } else if(CGRectContainsPoint([self.micraBody frame], location)) {
//            NSInteger imgTag = [touch view].tag;
//            DebugLog(@"%d",imgTag);
//        }
//    }
    
//    if (touch.view == self.xtrailBody){
//    } else if (touch.view == self.sunnyBody) {
//    } else if (touch.view == self.micraBody) {
//    }
}

- (void)viewDidLoad
{
//    UIBarButtonItem *mycarButton = [[UIBarButtonItem alloc] initWithTitle:@"My Car" style:UIBarButtonItemStyleBordered target:self action:@selector(addMyCar:)];
//    self.navigationItem.rightBarButtonItem = mycarButton;
//    [mycarButton release]; 

//#ifdef TESTING
//    UIBarButtonItem *feedbackButton = [[UIBarButtonItem alloc] initWithTitle:@"Testflight" style:UIBarButtonItemStyleBordered target:self action:@selector(launchFeedback:)];
//    self.navigationItem.leftBarButtonItem = feedbackButton;
//    [feedbackButton release]; 
//#endif

    
#ifdef ENABLE_BACK_BUTTON
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [backButton release]; 
    
#endif
    NSError *error;
    NSURL *shutterUrl = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                         pathForResource:@"shutter"
                                         ofType:@"mp3"]];
    NSURL *engineUrl = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                         pathForResource:@"engine_rev_once"
                                         ofType:@"mp3"]];
    NSURL *grillUrl = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                               pathForResource:@"home_grill_slide"
                                               ofType:@"mp3"]];
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:shutterUrl error:&error];
    
    if (error)
    {
        DebugLog(@"Error in audioPlayer: %@", [error localizedDescription]);
    } else {
        audioPlayer.delegate = self;
        [audioPlayer prepareToPlay];
    }
    audioPlayer1 = [[AVAudioPlayer alloc] initWithContentsOfURL:engineUrl error:&error];
    
    if (error)
    {
        DebugLog(@"Error in audioPlayer: %@", [error localizedDescription]);
    } else {
        audioPlayer1.delegate = self;
        [audioPlayer1 prepareToPlay];
    }
    
    audioPlayer2 = [[AVAudioPlayer alloc] initWithContentsOfURL:grillUrl error:&error];
    
    if (error)
    {
        DebugLog(@"Error in audioPlayer: %@", [error localizedDescription]);
    } else {
        audioPlayer2.delegate = self;
        [audioPlayer2 prepareToPlay];
    }
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    imgscrollView.scrollEnabled = YES;
    imgscrollView.clipsToBounds = YES;
    imgscrollView.showsHorizontalScrollIndicator = NO;
    imgscrollView.showsVerticalScrollIndicator = NO;
    imgscrollView.scrollsToTop = YES;
    imgscrollView.delegate = self;
    imgscrollView.bounces = NO;
    
    noOfgrills = MIN_GRILL_COUNT;
    //imgscrollView.directionalLockEnabled = YES;
    //imageView1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"About_1.jpg"]];
    //imageView1.frame = CGRectMake(0, 0, imageView1.frame.size.width, imageView1.frame.size.height);
    
    UIImage *buttonImage1 = [UIImage imageNamed:@"About_1.png"];
    UIImage *backImage2 = [UIImage imageNamed:@"About_2.png"];
    UIImage *buttonbackImage1 = [UIImage imageNamed:@"1_grill.png"];
    UIImage *buttonImage3 = [UIImage imageNamed:@"About_3.png"];
    UIImage *textImage4 = [UIImage imageNamed:@"topshutter.png"];
    UIImage *buttonImage5a = [UIImage imageNamed:@"About_5_unpressed.jpg"];
    UIImage *buttonImage5b = [UIImage imageNamed:@"About_5_pressed.jpg"];

    abtbut1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [abtbut1 setImage:buttonImage1 forState:UIControlStateNormal];
    [abtbut1 setFrame:CGRectMake(0, 0, buttonImage1.size.width, buttonImage1.size.height)];
    [abtbut1 setTitle:@"" forState:UIControlStateNormal];
    [abtbut1 addTarget:self action:@selector(buttonPressed1) forControlEvents:UIControlEventTouchUpInside];
    [imgscrollView addSubview:abtbut1];
    
    imageView2 = [[UIImageView alloc] initWithImage:backImage2];
    imageView2.frame = CGRectMake(0, abtbut1.frame.size.height, backImage2.size.width, backImage2.size.height);
    [imgscrollView addSubview:imageView2];
    [imageView2 release];
    
    abtbut2 = [UIButton buttonWithType:UIButtonTypeCustom];
    abtbut2.tag = 10;
    [abtbut2 setImage:buttonbackImage1 forState:UIControlStateNormal];
    //[abtbut2 setImage:buttonbackImage1 forState:UIControlStateSelected];
    [abtbut2 setImage:buttonbackImage1 forState:UIControlStateHighlighted];
    [abtbut2 setFrame:CGRectMake(0, abtbut1.frame.size.height, backImage2.size.width, backImage2.size.height)];
    [abtbut2 setTitle:@"" forState:UIControlStateNormal];
    [abtbut2 addTarget:self action:@selector(buttonPressed2) forControlEvents:UIControlEventTouchUpInside];
    [imgscrollView addSubview:abtbut2];
  
    abtbut3 = [UIButton buttonWithType:UIButtonTypeCustom];
    [abtbut3 setImage:buttonImage3 forState:UIControlStateNormal];
    [abtbut3 setFrame:CGRectMake(0, abtbut1.frame.size.height + abtbut2.frame.size.height, buttonImage3.size.width, buttonImage3.size.height)];
    [abtbut3 setTitle:@"" forState:UIControlStateNormal];
    [abtbut3 addTarget:self action:@selector(buttonPressed3) forControlEvents:UIControlEventTouchUpInside];
    [imgscrollView addSubview:abtbut3];
    
    imageView4 = [[UIImageView alloc] initWithImage:textImage4];
    imageView4.frame = CGRectMake(0, abtbut1.frame.size.height + abtbut2.frame.size.height + abtbut3.frame.size.height , imageView4.frame.size.width, imageView4.frame.size.height);
    [imgscrollView addSubview:imageView4];
    [imageView4 release];

    abtbut5 = [UIButton buttonWithType:UIButtonTypeCustom];
    [abtbut5 setImage:buttonImage5a forState:UIControlStateNormal];
    [abtbut5 setImage:buttonImage5b forState:UIControlStateHighlighted];
    [abtbut5 setFrame:CGRectMake(0, abtbut1.frame.size.height + abtbut2.frame.size.height + abtbut3.frame.size.height + imageView4.frame.size.height, buttonImage5a.size.width, buttonImage5a.size.height)];
    [abtbut5 setTitle:@"" forState:UIControlStateNormal];
    [abtbut5 addTarget:self action:@selector(buttonPressed5) forControlEvents:UIControlEventTouchUpInside];
    [imgscrollView addSubview:abtbut5];
   
    imgscrollView.contentMode = UIViewContentModeTop;
	[imgscrollView setContentSize:CGSizeMake(self.view.frame.size.width, abtbut1.frame.size.height + abtbut2.frame.size.height + abtbut3.frame.size.height + imageView4.frame.size.height + abtbut5.frame.size.height + 2 )];
    
    view1.frame = CGRectMake(0, abtbut1.frame.size.height + abtbut2.frame.size.height + abtbut3.frame.size.height + imageView4.frame.size.height, 320, 160 );

    img2.userInteractionEnabled = FALSE;
    //img2.frame = CGRectMake(0, 0, 320, 160);
    img2.autoresizingMask = UIViewAutoresizingNone;
    but2.frame = CGRectMake(85, 1, 150, 160);
    [but2 addTarget:self action:@selector(testPressed) forControlEvents:UIControlEventTouchUpInside];
    but2.tag = 1001;
    view1.exclusiveTouch = true;
    img2.exclusiveTouch = true;
    [imgscrollView insertSubview:view1 aboveSubview:abtbut5];
   
    [imgscrollView setScrollEnabled:YES];
    imgscrollView.delaysContentTouches = NO;
        
    UISwipeGestureRecognizer *rightswipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self 
                                                                                               action:@selector(swipePressed2:)];
    rightswipeRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [abtbut2 addGestureRecognizer:rightswipeRecognizer];
    [rightswipeRecognizer release];
    
    UISwipeGestureRecognizer *leftswipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self 
                                                                                              action:@selector(swipePressed2:)];
    leftswipeRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [abtbut2 addGestureRecognizer:leftswipeRecognizer];
    [leftswipeRecognizer release];
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                          action:@selector(handleSingleTap:)];
    [singleTapGestureRecognizer setNumberOfTapsRequired:1];
    [abtbut2 addGestureRecognizer:singleTapGestureRecognizer];
    [singleTapGestureRecognizer release];
    
    //[self carModelsListAsynchronousCall];
}

- (void) addMyCar: (id) sender
{   
    MyCarViewController *mycarController = [[MyCarViewController alloc] initWithNibName:@"MyCarViewController" bundle:nil] ;
    mycarController.title = @"MY CAR";
    //to push the UIView.
    mycarController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:mycarController animated:YES];
    [mycarController release];
}

#ifdef TESTING
- (void) launchFeedback: (id) sender 
{
    [TestFlight openFeedbackView];
}
#endif 

- (IBAction)swipeDetected:(UISwipeGestureRecognizer *)recognizer {
    
	//CGPoint location = [recognizer locationInView:self.view];
	//[self showImageWithText:@"swipe" atPoint:location];
	DebugLog(@"scroll x= %f",[[recognizer view] center].x);
    DebugLog(@"scroll y= %f",[[recognizer view] center].y);
    if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft) {
        DebugLog(@"UISwipeGestureRecognizerDirectionLeft");
    } else if (recognizer.direction == UISwipeGestureRecognizerDirectionRight) {
        DebugLog(@"UISwipeGestureRecognizerDirectionRight");
    } else if (recognizer.direction == UISwipeGestureRecognizerDirectionUp) {
        DebugLog(@"UISwipeGestureRecognizerDirectionUp");
    } else if (recognizer.direction == UISwipeGestureRecognizerDirectionDown) {
        DebugLog(@"UISwipeGestureRecognizerDirectionDown");
    }
    
}

-(void)audioPlayerDidFinishPlaying:
(AVAudioPlayer *)player successfully:(BOOL)flag
{
}

-(void)audioPlayerDecodeErrorDidOccur:
(AVAudioPlayer *)player error:(NSError *)error
{
}

-(void)audioPlayerBeginInterruption:(AVAudioPlayer *)player
{
}

-(void)audioPlayerEndInterruption:(AVAudioPlayer *)player
{
}

-(void)buttonPressed1 {
    [self scrollTobottom];
}

-(void)buttonPressed2 {
    DebugLog(@"Pressed butoon 2");
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [Flurry logEvent:@"Swipe_Grills_Event"];

    // set up an animation for the transition between the views
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.5];
    [animation setType:kCATransitionPush];
    //if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft) {
        [animation setSubtype:kCATransitionFromRight];
        DebugLog(@" left start grill = %d",noOfgrills);
        if(noOfgrills == MAX_GRILL_COUNT)
            noOfgrills = MIN_GRILL_COUNT;
        else
            noOfgrills++;
        [abtbut2 setImage: [UIImage imageNamed:[NSString stringWithFormat:@"%d_grill.png", noOfgrills]] forState:UIControlStateNormal];
        [abtbut2 setImage: [UIImage imageNamed:[NSString stringWithFormat:@"%d_grill.png", noOfgrills]] forState:UIControlStateHighlighted];
        DebugLog(@"left end grill  = %d",noOfgrills);
    //}
    
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
    
    [[abtbut2 layer] addAnimation:animation forKey:@"SwitchToView"];
    [audioPlayer2 play];

}

-(void)swipePressed2:(UISwipeGestureRecognizer *)recognizer {
    [Flurry logEvent:@"Swipe_Grills_Event"];
#ifdef GOOGLE_ANALYTICS
    if (![[GANTracker sharedTracker] trackEvent:@"Nissan CarUitl Application iOS"
                                         action:@"Swipe Grills"
                                          label:@""
                                          value:-1
                                      withError:NULL]) {
        DebugLog(@"error in trackEvent");
    }
#endif 
    // set up an animation for the transition between the views
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.5];
    [animation setType:kCATransitionPush];
    if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft) {
        [animation setSubtype:kCATransitionFromRight];
        DebugLog(@" left start grill = %d",noOfgrills);
        if(noOfgrills == MAX_GRILL_COUNT)
            noOfgrills = MIN_GRILL_COUNT;
        else 
            noOfgrills++;
        [abtbut2 setImage: [UIImage imageNamed:[NSString stringWithFormat:@"%d_grill.png", noOfgrills]] forState:UIControlStateNormal];
        [abtbut2 setImage: [UIImage imageNamed:[NSString stringWithFormat:@"%d_grill.png", noOfgrills]] forState:UIControlStateHighlighted];
        DebugLog(@"left end grill  = %d",noOfgrills);
    } else if (recognizer.direction == UISwipeGestureRecognizerDirectionRight) {
        [animation setSubtype:kCATransitionFromLeft];
        DebugLog(@"right start grill  = %d",noOfgrills);
        if(noOfgrills == MIN_GRILL_COUNT )
            noOfgrills = MAX_GRILL_COUNT;
        else 
            noOfgrills--;
        [abtbut2 setImage: [UIImage imageNamed:[NSString stringWithFormat:@"%d_grill.png", noOfgrills]] forState:UIControlStateNormal];
        [abtbut2 setImage: [UIImage imageNamed:[NSString stringWithFormat:@"%d_grill.png", noOfgrills]] forState:UIControlStateHighlighted];
        DebugLog(@"right end grill  = %d",noOfgrills);
    }

    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
    
    [[abtbut2 layer] addAnimation:animation forKey:@"SwitchToView"];
    [audioPlayer2 play];
}

-(void)buttonPressed3 {
    [self scrollTobottom];
}

-(void)buttonPressed5 {
    [audioPlayer1 play];
     [self carModelsListAsynchronousCall];
}

-(void)testPressed {
    if(but2.tag == 1001) {
        self.but2.userInteractionEnabled = FALSE;
        abtbut5.userInteractionEnabled = FALSE;
        but2.tag = 1002;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:1.5];
        [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:view1 cache:YES];
        view1.frame = CGRectMake(0, abtbut1.frame.size.height + abtbut2.frame.size.height + abtbut3.frame.size.height + imageView4.frame.size.height,320,  35);
        img2.frame = CGRectMake(0, -125, 320, 160);
        but2.frame = CGRectMake(85, -125, 150, 160);
        [UIView commitAnimations];
    } else if (but2.tag == 1002) {
        self.but2.userInteractionEnabled = FALSE;
        abtbut5.userInteractionEnabled = FALSE;
        but2.tag = 1001;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:1.5];
        [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:view1 cache:YES];
        view1.frame = CGRectMake(0, abtbut1.frame.size.height + abtbut2.frame.size.height + abtbut3.frame.size.height + imageView4.frame.size.height,320,  160);
        img2.frame = CGRectMake(0, 0, 320, 160);
        but2.frame = CGRectMake(85, 1, 150, 160);
        [UIView commitAnimations];
    }
    if(audioPlayer.isPlaying==TRUE) {
        [audioPlayer stop];
    }
    [audioPlayer play];

}

- (void)transitionDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    self.but2.userInteractionEnabled = TRUE;
    abtbut5.userInteractionEnabled = TRUE;
}


-(void)carModelsListAsynchronousCall
{
	/****************Asynchronous Request**********************/
    
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:NEON_GALLERY_LINK] cachePolicy:YES timeoutInterval:5.0];
    
	// Note: An NSOperation creates an autorelease pool, but doesn't schedule a run loop
	// Create the connection and schedule it on a run loop under our namespaced run mode
	NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    [theConnection autorelease];
	
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	[self connectionDidFinishLoading:nil];
    //[self parseFromFile];
   DebugLog(@"Error: %@", [error localizedDescription]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	
	NSString *xmlDataFromChannelSchemes;
    
	if(responseAsyncData)
	{
		NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
		//DebugLog(@"\n result:%@\n\n", result);
        [NEONAppDelegate writeToTextFile:result name:@"gallery"];
		xmlDataFromChannelSchemes = [[NSString alloc] initWithString:result];
		NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
		xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
		[xmlParser setDelegate:self];
		[xmlParser parse];
        [xmlParser release];
		[xmlDataFromChannelSchemes release];
		[result release];
		[responseAsyncData release];
		responseAsyncData = nil;
	}
    else {
        [self parseFromFile];
    }

}

-(void) parseFromFile
{
    NSString *xmlDataFromChannelSchemes;
    NSString *data;
    data = [NEONAppDelegate getTextFromFile:@"gallery"];
    DebugLog(@"\n data:%@\n\n", data);
    if(data != nil && ![data isEqualToString:@""])
    {
        xmlDataFromChannelSchemes = [[NSString alloc] initWithString:data];
        NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
        [xmlParser setDelegate:self];
        [xmlParser parse];
        [xmlParser release];
        [xmlDataFromChannelSchemes release];
    }
    else
    {
        UIAlertView *errorView = [[[UIAlertView alloc]
                                   initWithTitle:ALERT_TITLE
                                   message:@"Please check your internet connection and try again."
                                   delegate:self
                                   cancelButtonTitle:@"OK"
                                   otherButtonTitles:nil] autorelease];
        [errorView show];
    }
}

#pragma mark xmlParser methods
/* Called when the parser runs into an open tag (<tag>) */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName 	attributes:(NSDictionary *)attributeDict
{
	if([elementName isEqualToString:@"neongallery"])
	{
		galleryArray = [[NSMutableArray alloc] init];
        socialLink = [attributeDict objectForKey:@"sociallink"];
	} else if([elementName isEqualToString:@"photo"]) {
        galleryObj = [[CarUtilGallery alloc] init];
        galleryObj.ilink = [attributeDict objectForKey:@"ilink"];
        galleryObj.vlink = [attributeDict objectForKey:@"vlink"];
        galleryObj.caption = [attributeDict objectForKey:@"caption"];
        galleryObj.slink = [NSString stringWithFormat:@"%@%@/",socialLink,[attributeDict objectForKey:@"name"]];
    } else if([elementName isEqualToString:@"description"]) {
        elementFound = YES;
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
     //DebugLog(@"Processing Value: %@", string);
    //[ElementValue appendString:string];
    if (elementFound)
    {
        galleryObj.description = string;
    }

}

/* Called when the parser runs into a close tag (</tag>). */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"neongallery"]) {
        DebugLog(@"%d",[galleryArray count]);
        [self pushGalleryController];

    } else if([elementName isEqualToString:@"photo"]) {
        elementFound = NO;
        DebugLog(@" -%@- -%@- -%@- -%@- ", galleryObj.ilink,galleryObj.vlink,galleryObj.caption,galleryObj.description);
        if(galleryObj.description == nil)
        {
            galleryObj.description = @"";
        }
        [galleryArray addObject:galleryObj];
        [galleryObj release];
         galleryObj = nil;
    }
}



- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    DebugLog(@"Error: %@", [parseError localizedDescription]);
}

- (void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError
{
    DebugLog(@"Error: %@", [validationError localizedDescription]);
}

-(void)pushGalleryController {
    [Flurry logEvent:@"Gallery_Opened_Event" withParameters:nil timed:YES];
#ifdef TESTING
    [TestFlight passCheckpoint:@"Nissan Gallery Opened"];
#endif
#ifdef GOOGLE_ANALYTICS
    if (![[GANTracker sharedTracker] trackEvent:@"Nissan CarUitl Application iOS"
                                         action:@"Nissan Gallery Opened"
                                          label:@""
                                          value:-1
                                      withError:NULL]) {
        DebugLog(@"error in trackEvent");
    }
#endif 
    NSMutableArray *photos = [[NSMutableArray alloc] init];
    MWPhoto *photo;

    for(int i = 0; i < [galleryArray count]; i++)
    {
        CarUtilGallery *tempgalleryObj = (CarUtilGallery *)[galleryArray objectAtIndex:i];
        photo = [MWPhoto photoWithURL:[NSURL URLWithString:tempgalleryObj.ilink]];
//        photo.slink = tempgalleryObj.slink;
//        photo.vlink = tempgalleryObj.vlink;
        photo.caption = [NSString stringWithFormat:@"%@\n%@",tempgalleryObj.caption,tempgalleryObj.description];
        [photos addObject:photo];
    }

    self.photos = photos;
    
    // Create browser
	MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = YES;
    browser.wantsFullScreenLayout = YES;
    [browser setInitialPageIndex:0];
    [self.navigationController pushViewController:browser animated:YES];
    // Release
	[browser release];
	[photos release];

}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _photos.count;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}

//- (NSString *)photoBrowser:(MWPhotoBrowser *)photoBrowser slinkAtIndex:(int)index {
//    if (index < _photos.count)
//    {
//        MWPhoto *tempPhotoObj = (MWPhoto *)[_photos objectAtIndex:index];
//        return tempPhotoObj.slink;
//    }
//    return nil;
//}
//
//- (NSString *)photoBrowser:(MWPhotoBrowser *)photoBrowser vlinkAtIndex:(int)index {
//    if (index < _photos.count)
//    {
//        MWPhoto *tempPhotoObj = (MWPhoto *)[_photos objectAtIndex:index];
//        return tempPhotoObj.vlink;
//    }
//    return @"";
//}

-(void)scrollTotext
{
    CGPoint bottomOffset = CGPointMake(0,  abtbut1.frame.size.height + abtbut2.frame.size.height + abtbut3.frame.size.height);
    [imgscrollView setContentOffset:bottomOffset animated:YES];
}

-(void)scrollTobottom
{
    //DebugLog(@"contentsize = %f bound = %f",imgscrollView.contentSize.height,self.imgscrollView.bounds.size.height);
    CGPoint bottomOffset = CGPointMake(0, imgscrollView.contentSize.height - self.imgscrollView.bounds.size.height);
    [imgscrollView setContentOffset:bottomOffset animated:YES];
}

-(void)scrollTotop
{
    CGPoint bottomOffset = CGPointMake(0, 0);
    [imgscrollView setContentOffset:bottomOffset animated:YES];
}

- (void)viewDidUnload
{
    imgscrollView = nil;
    view1 = nil;
    img2 = nil;
    but2 = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    //[_photos release];
}

- (void)dealloc {
    DebugLog(@"dealloc");
    [_photos release];
    [galleryArray release];
    [audioPlayer release];
    [audioPlayer1 release];
    [audioPlayer2 release];
    [imgscrollView release];
    [view1 release];
    [img2 release];
    [but2 release];
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


@end
