//
//  CarUtilGallery.h
//  CarUtil
//
//  Created by Rishi on 22/10/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarUtilGallery : NSObject {
    
}
@property (nonatomic, copy) NSString *galleryid;
@property (nonatomic, copy) NSString *ilink;
@property (nonatomic, copy) NSString *vlink;
@property (nonatomic, copy) NSString *caption;
@property (nonatomic, copy) NSString *description;
@property (nonatomic, copy) NSString *slink;
@property (nonatomic, copy) NSString *thumbnail;

-(id)initWithID:(NSString *)lgalleryid image:(NSString *)lilink  video:(NSString *)lvlink caption:(NSString *)lcaption description:(NSString *)ldescription share:(NSString *)lsharelink;

@end
