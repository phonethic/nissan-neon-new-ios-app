//
//  CarUtilGallery.m
//  CarUtil
//
//  Created by Rishi on 22/10/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "CarUtilGallery.h"

@implementation CarUtilGallery
@synthesize galleryid,ilink,vlink,caption,description,slink,thumbnail;

-(id)initWithID:(NSString *)lgalleryid image:(NSString *)lilink  video:(NSString *)lvlink caption:(NSString *)lcaption description:(NSString *)ldescription share:(NSString *)lsharelink
{
    self = [super init];
    
    if(self) {
        self.galleryid = lgalleryid;
        self.ilink = lilink;
        self.vlink = lvlink;
        self.caption = lcaption;
        self.description = ldescription;
        self.slink = lsharelink;
    }
	return self;
}

-(void)dealloc
{
    self.thumbnail = nil;
    self.galleryid = nil;
    self.ilink = nil;
    self.vlink = nil;
    self.caption = nil;
    self.description = nil;
    self.slink = nil;
    [super dealloc];
}

@end
