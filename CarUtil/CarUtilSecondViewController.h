//
//  CarUtilSecondViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 29/03/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "MWPhotoBrowser.h"

@interface CarUtilSecondViewController : UIViewController <AVAudioPlayerDelegate>{
    //NSArray *_photos;
    AVAudioPlayer *audioPlayer;
}
//@property (nonatomic, retain) NSArray *photos;
//@property (nonatomic, retain) AVAudioPlayer *audioPlayer;
@property(nonatomic,retain) IBOutlet UIButton *button3;
@property(nonatomic,retain) IBOutlet UIButton *button4;
@property(nonatomic,retain) IBOutlet UIButton *button5;
@property(nonatomic,retain) IBOutlet UIButton *button6;
@property (retain, nonatomic) IBOutlet UIButton *button7;

@property (nonatomic, strong) NSTimer *glowTimer;
-(IBAction)ThirdButton:(id)sender;
-(IBAction)ForthButton:(id)sender;
-(IBAction)FifthButton:(id)sender;
-(IBAction)sixthButton:(id)sender;
- (IBAction)seventhButton:(id)sender;
@end
