//
//  CarUtilSecondViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 29/03/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "CarUtilSecondViewController.h"
#import "CarUtilAppDelegate.h"
#import "MapKitDisplayViewController.h"
#import "ExpenseViewController.h"
#import "ReminderViewController.h"
#import "TrafficRulesViewController.h"
#import "MenuViewController.h"
#import "MileageViewController.h"
#import "ReminderDetailsViewController.h"
#import "CarUtilTrafficSignalsViewController.h"

@interface CarUtilSecondViewController ()

@end


@implementation CarUtilSecondViewController
@synthesize button7;
//@synthesize audioPlayer;
@synthesize button3,button4,button5,button6;
//@synthesize photos = _photos;
@synthesize glowTimer;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.navigationItem.title = NSLocalizedString(@"Tools", @"Tools");
        //self.tabBarItem.image = [UIImage imageNamed:@"tools"];
        //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background-img.png"]];
    }   
    return self;
}


//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    UITouch *touch =[touches anyObject]; 
//    
//    if (touch.view == self.firstpart){
//        int val = self.firstpart.frame.origin.x;
//        if(val == 0) {
//            [self moveForward:self.firstpart];
//        } else {
//            [self moveBackward:self.firstpart];
//        }
//    } else if (touch.view == self.secondpart) {
//        int val = self.secondpart.frame.origin.x;
//        if(val == 0) {
//            [self moveForward:self.secondpart];
//        } else {
//            [self moveBackward:self.secondpart];
//        }
//    } else if (touch.view == self.thirdpart) {
//        int val = self.thirdpart.frame.origin.x;
//        if(val == 0) {
//            [self moveForward:self.thirdpart];
//        } else {
//            [self moveBackward:self.thirdpart];
//        }
//    } else if (touch.view == self.fourthpart) {
//        int val = self.fourthpart.frame.origin.x;
//        if(val == 0) {
//            [self moveForward:self.fourthpart];
//        } else {
//            [self moveBackward:self.fourthpart];
//        }
//    } else if (touch.view == self.fifthpart) {
//        int val = self.fifthpart.frame.origin.x;
//        if(val == 0) {
//            [self moveForward:self.fifthpart];
//        } else {
//            [self moveBackward:self.fifthpart];
//        }
//    }
//
//
//}

//-(void) enableAll {
//    self.firstpart.userInteractionEnabled = TRUE;
//    self.secondpart.userInteractionEnabled = TRUE;
//    self.thirdpart.userInteractionEnabled = TRUE;
//    self.fourthpart.userInteractionEnabled = TRUE;
//    self.fifthpart.userInteractionEnabled = TRUE;
//    self.button3.userInteractionEnabled = TRUE;
//    self.button4.userInteractionEnabled = TRUE;
//    self.button5.userInteractionEnabled = TRUE;
//    self.button6.userInteractionEnabled = TRUE;
//    self.button7.userInteractionEnabled = TRUE;
//
//}
//-(void) disableAll {
//    self.firstpart.userInteractionEnabled = TRUE;
//    self.secondpart.userInteractionEnabled = TRUE;
//    self.thirdpart.userInteractionEnabled = TRUE;
//    self.fourthpart.userInteractionEnabled = TRUE;
//    self.fifthpart.userInteractionEnabled = TRUE;
//    self.button3.userInteractionEnabled = FALSE;
//    self.button4.userInteractionEnabled = FALSE;
//    self.button5.userInteractionEnabled = FALSE;
//    self.button6.userInteractionEnabled = FALSE;
//    self.button7.userInteractionEnabled = FALSE;
//}
//-(void)moveForward:(UIImageView *)lview
//{
//    [self disableAll];
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:1.5];
//    [UIView setAnimationDelegate:self];
//    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:lview cache:YES];
//    [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
//    [lview setFrame:CGRectMake(self.view.bounds.size.width * 0.9,lview.frame.origin.y,lview.frame.size.width,lview.frame.size.height)];
//    [UIView commitAnimations];
//    [audioPlayer play];
//}
//
//-(void)moveBackward:(UIImageView *)lview
//{
//    [self disableAll];
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:1.5];
//    [UIView setAnimationDelegate:self];
//    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:lview cache:YES];
//    [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
//    [lview setFrame:CGRectMake(0.0f,lview.frame.origin.y,lview.bounds.size.width,lview.bounds.size.height)];
//    [UIView commitAnimations];
//     [audioPlayer play];
//}

//- (void)transitionDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
//{
//    [self enableAll];
//}
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
//    if(toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown){
//        [UIView beginAnimations:nil context:nil];
//        [UIView setAnimationDuration:1.0];
//        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:button5 cache:YES];
//        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:button6 cache:YES];
//        [button3 setFrame:CGRectMake(button3.bounds.origin.x + 20 ,button3.bounds.origin.y + 50,button3.frame.size.width,button3.frame.size.height)];
//        [button4 setFrame:CGRectMake(button4.bounds.origin.x + 170,button4.bounds.origin.y + 50,button4.frame.size.width,button4.frame.size.height)];
//        [button5 setFrame:CGRectMake(button5.bounds.origin.x + 20,button5.bounds.origin.y + 220 ,button5.frame.size.width,button5.frame.size.height)];
//        [button6 setFrame:CGRectMake(button6.bounds.origin.x + 170,button6.bounds.origin.y + 220,button6.frame.size.width,button6.frame.size.height)];
//        [UIView commitAnimations];
//
//    }
//    else if(toInterfaceOrientation == UIInterfaceOrientationLandscapeRight || toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft){
//        [UIView beginAnimations:nil context:nil];
//        [UIView setAnimationDuration:1.0];
//        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:button5 cache:YES];
//        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:button6 cache:YES];
//        [button3 setFrame:CGRectMake(button3.bounds.origin.x + 50,button3.bounds.origin.y + 5,button3.frame.size.width,button3.frame.size.height)];
//        [button4 setFrame:CGRectMake(button4.bounds.origin.x + 300,button4.bounds.origin.y + 5,button4.frame.size.width,button4.frame.size.height)];
//        [button5 setFrame:CGRectMake(button5.bounds.origin.x + 50,button5.bounds.origin.y + 111,button5.frame.size.width,button5.frame.size.height)];
//        [button6 setFrame:CGRectMake(button6.bounds.origin.x + 300,button6.bounds.origin.y + 111,button6.frame.size.width,button6.frame.size.height)];
//        [UIView commitAnimations];
//    }
}

-(IBAction)ThirdButton:(id)sender
{
#ifdef GOOGLE_ANALYTICS
    if (![[GANTracker sharedTracker] trackEvent:@"Nissan CarUitl Application iOS"
                                         action:@"Find My Car Pressed"
                                          label:@""
                                          value:-1
                                      withError:NULL]) {
        DebugLog(@"error in trackEvent");
    }
#endif 
    UIViewController *mapviewController = [[MapKitDisplayViewController alloc] initWithNibName:@"MapKitDisplayViewController" bundle:nil] ;
    mapviewController.title = @"Map View";
    //to push the UIView.
    mapviewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:mapviewController animated:YES];
    [mapviewController release];
}
-(IBAction)ForthButton:(id)sender
{
#ifdef GOOGLE_ANALYTICS
    if (![[GANTracker sharedTracker] trackEvent:@"Nissan CarUitl Application iOS"
                                         action:@"Expense Calc Pressed"
                                          label:@""
                                          value:-1
                                      withError:NULL]) {
        DebugLog(@"error in trackEvent");
    }
#endif 
    UIViewController *expviewController = [[ExpenseViewController alloc] initWithNibName:@"ExpenseViewController" bundle:nil] ;
    expviewController.title = @"Expense Calculator";
    //to push the UIView.
    expviewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:expviewController animated:YES];
    [expviewController release];

}

-(IBAction)FifthButton:(id)sender
{
#ifdef GOOGLE_ANALYTICS
    if (![[GANTracker sharedTracker] trackEvent:@"Nissan CarUitl Application iOS"
                                         action:@"Reminders Pressed"
                                          label:@""
                                          value:-1
                                      withError:NULL]) {
        DebugLog(@"error in trackEvent");
    }
#endif 
    UIViewController *reminderviewController = [[ReminderDetailsViewController alloc] initWithNibName:@"ReminderDetailsViewController" bundle:nil] ;
    reminderviewController.title = @"Reminders";
    //to push the UIView.
    reminderviewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:reminderviewController animated:YES];
    [reminderviewController release];
}

-(IBAction)sixthButton:(id)sender
{
#ifdef GOOGLE_ANALYTICS
    if (![[GANTracker sharedTracker] trackEvent:@"Nissan CarUitl Application iOS"
                                         action:@"Traffic Icons Pressed"
                                          label:@""
                                          value:-1
                                      withError:NULL]) {
        DebugLog(@"error in trackEvent");
    }
#endif    
//    UIViewController *trafficviewController = [[MenuViewController alloc] init];
//    trafficviewController.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:trafficviewController animated:YES];
//    [trafficviewController release];
    UIViewController *trafficviewController = [[CarUtilTrafficSignalsViewController alloc] init];
    trafficviewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:trafficviewController animated:YES];
    [trafficviewController release];
}

- (IBAction)seventhButton:(id)sender {
#ifdef GOOGLE_ANALYTICS
    if (![[GANTracker sharedTracker] trackEvent:@"Nissan CarUitl Application iOS"
                                         action:@"Mileage Calc Pressed"
                                          label:@""
                                          value:-1
                                      withError:NULL]) {
        DebugLog(@"error in trackEvent");
    }
#endif 
    MileageViewController *mileageviewController = [[MileageViewController alloc] initWithNibName:@"MileageViewController" bundle:nil] ;
    mileageviewController.title = @"Mileage Calculator";
    //to push the UIView.
    mileageviewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:mileageviewController animated:YES];
    [mileageviewController release];
}
							
- (void)viewDidLoad
{
    
#ifdef ENABLE_BACK_BUTTON
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [backButton release]; 

#endif
    [Flurry logEvent:@"Tools_Tab_Event"];
//    NSURL *slideUrl = [NSURL fileURLWithPath:[[NSBundle mainBundle]
//                                              pathForResource:@"tools_slide"
//                                              ofType:@"mp3"]];
//    NSError *error;
//    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:slideUrl error:&error];
//    
//    if (error)
//    {
//        DebugLog(@"Error in audioPlayer: %@", [error localizedDescription]);
//    } else {
//        audioPlayer.delegate = self;
//        [audioPlayer prepareToPlay];
//    }
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    if (![glowTimer isValid])  {
        glowTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self
                                                   selector: @selector(glowToolsButtons) userInfo:nil repeats:YES];
    }
}

-(void)glowToolsButtons
{
    int tag = (arc4random() % 5) + 1;
    DebugLog(@"%d",tag);
    switch (tag) {
        case 1:
        {
            [UIView  transitionWithView:button3 duration:2.0
                                options:UIViewAnimationOptionTransitionCrossDissolve |UIViewAnimationOptionAllowUserInteraction	
                             animations:^(void) {
                                 button3.highlighted = YES;
                             }
                             completion:^(BOOL finished) {
                                 [UIView  transitionWithView:button3 duration:2.0
                                                     options:UIViewAnimationOptionTransitionCrossDissolve |UIViewAnimationOptionAllowUserInteraction	
                                                  animations:^(void) {
                                                      button3.highlighted = NO;
                                                  }
                                                  completion:^(BOOL finished) {
                                                      
                                                  }];
                             }];
        }
            break;
            
        case 2:
        {
            [UIView  transitionWithView:button4 duration:2.0
                                options:UIViewAnimationOptionTransitionCrossDissolve |UIViewAnimationOptionAllowUserInteraction	
                             animations:^(void) {
                                 button4.highlighted = YES;
                             }
                             completion:^(BOOL finished) {
                                 [UIView  transitionWithView:button4 duration:2.0
                                                     options:UIViewAnimationOptionTransitionCrossDissolve |UIViewAnimationOptionAllowUserInteraction	
                                                  animations:^(void) {
                                                      button4.highlighted = NO;
                                                  }
                                                  completion:^(BOOL finished) {
                                                      
                                                  }];
                             }];
        }
            break;
            
        case 3:
        {
            [UIView  transitionWithView:button5 duration:2.0
                                options:UIViewAnimationOptionTransitionCrossDissolve |UIViewAnimationOptionAllowUserInteraction	
                             animations:^(void) {
                                 button5.highlighted = YES;
                             }
                             completion:^(BOOL finished) {
                                 [UIView  transitionWithView:button5 duration:2.0
                                                     options:UIViewAnimationOptionTransitionCrossDissolve |UIViewAnimationOptionAllowUserInteraction	
                                                  animations:^(void) {
                                                      button5.highlighted = NO;
                                                  }
                                                  completion:^(BOOL finished) {
                                                      
                                                  }];
                             }];
        }
            break;
            
        case 4:
        {
            [UIView  transitionWithView:button6 duration:2.0
                                options:UIViewAnimationOptionTransitionCrossDissolve |UIViewAnimationOptionAllowUserInteraction	
                             animations:^(void) {
                                 button6.highlighted = YES;
                             }
                             completion:^(BOOL finished) {
                                 [UIView  transitionWithView:button6 duration:2.0
                                                     options:UIViewAnimationOptionTransitionCrossDissolve |UIViewAnimationOptionAllowUserInteraction	
                                                  animations:^(void) {
                                                      button6.highlighted = NO;
                                                  }
                                                  completion:^(BOOL finished) {
                                                      
                                                  }];
                             }];
        }
            break;
            
        case 5:
        {
            [UIView  transitionWithView:button7 duration:2.0
                                options:UIViewAnimationOptionTransitionCrossDissolve |UIViewAnimationOptionAllowUserInteraction	
                             animations:^(void) {
                                 button7.highlighted = YES;
                             }
                             completion:^(BOOL finished) {
                                 [UIView  transitionWithView:button7 duration:2.0
                                                     options:UIViewAnimationOptionTransitionCrossDissolve |UIViewAnimationOptionAllowUserInteraction	
                                                  animations:^(void) {
                                                      button7.highlighted = NO;
                                                  }
                                                  completion:^(BOOL finished) {
                                                      
                                                  }];
                             }];
        }
            break;
            
        default:
            break;
    }
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    #ifdef TESTING
    [TestFlight passCheckpoint:@"Tools Tab Pressed"];
    #endif
//    UIDeviceOrientation deviceOrientation  = [[UIApplication sharedApplication] statusBarOrientation];
//    
//    if (UIDeviceOrientationIsLandscape(deviceOrientation)) {
//        [button3 setFrame:CGRectMake(button3.bounds.origin.x + 50,button3.bounds.origin.y + 5,button3.frame.size.width,button3.frame.size.height)];
//        [button4 setFrame:CGRectMake(button4.bounds.origin.x + 300,button4.bounds.origin.y + 5,button4.frame.size.width,button4.frame.size.height)];
//        [button5 setFrame:CGRectMake(button5.bounds.origin.x + 50,button5.bounds.origin.y + 111,button5.frame.size.width,button5.frame.size.height)];
//        [button6 setFrame:CGRectMake(button6.bounds.origin.x + 300,button6.bounds.origin.y + 111,button6.frame.size.width,button6.frame.size.height)];
//    } else {
//        [button3 setFrame:CGRectMake(button3.bounds.origin.x + 20 ,button3.bounds.origin.y + 50,button3.frame.size.width,button3.frame.size.height)];
//        [button4 setFrame:CGRectMake(button4.bounds.origin.x + 170,button4.bounds.origin.y + 50,button4.frame.size.width,button4.frame.size.height)];
//        [button5 setFrame:CGRectMake(button5.bounds.origin.x + 20,button5.bounds.origin.y + 220 ,button5.frame.size.width,button5.frame.size.height)];
//        [button6 setFrame:CGRectMake(button6.bounds.origin.x + 170,button6.bounds.origin.y + 220,button6.frame.size.width,button6.frame.size.height)];
//    }
}
        
- (void)viewDidUnload
{
    [self setButton3:nil];
    [self setButton4:nil];
    [self setButton5:nil];
    [self setButton6:nil];
    [self setButton7:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)dealloc {
    //[audioPlayer release];
    [button3 release];
    [button4 release];
    [button5 release];
    [button6 release];
    [button7 release];
    if ([glowTimer isValid])  {
        [glowTimer invalidate];
        glowTimer=nil;
    }
    [super dealloc];
}
@end
