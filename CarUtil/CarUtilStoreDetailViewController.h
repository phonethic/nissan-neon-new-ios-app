//
//  CarUtilStoreDetailViewController.h
//  CarUtil
//
//  Created by Kirti Nikam on 15/02/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "CarUtilDealerStore.h"

#define GETDIRECTION_LINK(CURLAT,CURLNG,DESTLAT,DESTLNG) [NSString stringWithFormat:@"http://maps.google.com/maps?saddr=%lf,%lf&daddr=%lf,%lf",CURLAT,CURLNG,DESTLAT,DESTLNG]

@interface CarUtilStoreDetailViewController : UIViewController <MFMailComposeViewControllerDelegate,CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
    int imageIndex;
}
@property (copy,nonatomic) NSString *phoneNumber;
@property (readwrite,nonatomic) double currentLatitude;
@property (readwrite,nonatomic) double currentLongitude;
@property (retain, nonatomic) CarUtilDealerStore *storeDetailObject;
@property (retain, nonatomic) IBOutlet UIView *detailView;
@property (retain, nonatomic) IBOutlet UIImageView *storeImageView;

@property (retain, nonatomic) IBOutlet MKMapView *storeDetailMapView;
@property (retain, nonatomic) IBOutlet UIButton *TakeMeThereBtn;

@property (retain, nonatomic) IBOutlet UITableView *detailTableView;
@property (retain, nonatomic) IBOutlet UIButton *loadMoreDetailsBtn;
@property (retain, nonatomic) IBOutlet UIImageView *pullImageView;

@property (retain, nonatomic) IBOutlet UIView *galleryView;
@property (retain, nonatomic) IBOutlet UIButton *previousBtn;
@property (retain, nonatomic) IBOutlet UIButton *nextBtn;
@property (retain, nonatomic) IBOutlet UIButton *closeGalleryBtn;
@property (retain, nonatomic) IBOutlet UIImageView *galleryImageView;
@property (retain, nonatomic) IBOutlet UIPageControl *galleryPageControl;

- (IBAction)previousBtnPressed:(id)sender;
- (IBAction)nextBtnPressed:(id)sender;
- (IBAction)closeGalleryBtn:(id)sender;

- (IBAction)loadMoreDetailsBtnPressed:(id)sender;
- (IBAction)TakeMeThereBtnPressed:(id)sender;
@end
