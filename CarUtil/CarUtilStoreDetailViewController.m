//
//  CarUtilStoreDetailViewController.m
//  CarUtil
//
//  Created by Kirti Nikam on 15/02/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
//#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>
#import "CarUtilStoreDetailViewController.h"
#import "DisplayMap.h"
#import "CarUtilAppDelegate.h"

@interface CarUtilStoreDetailViewController ()

@end

@implementation CarUtilStoreDetailViewController
@synthesize TakeMeThereBtn;
@synthesize storeDetailObject;
@synthesize storeDetailMapView;
@synthesize currentLatitude,currentLongitude;
@synthesize storeImageView,detailTableView,loadMoreDetailsBtn,pullImageView;
@synthesize phoneNumber;
@synthesize galleryView,galleryImageView,previousBtn,nextBtn,closeGalleryBtn,galleryPageControl;
@synthesize detailView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (id)init
{
    self = [super initWithNibName:@"CarUtilStoreDetailViewController" bundle:nil];
    if (self) {
        // Custom initialization
        storeDetailObject = [[CarUtilDealerStore alloc] init];
    }
    return self;
}
- (void)viewDidLoad
{
#ifdef ENABLE_BACK_BUTTON
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [backButton release];
#endif
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSDictionary *storeParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     storeDetailObject.storeName, @"DealerStoreName",
     nil];
    [Flurry logEvent:@"DealerStoreDetail_View_Event" withParameters:storeParams];
    
    if (storeDetailObject.storeLatitude == 0 && storeDetailObject.storeLongitute == 0)
    {
        [storeDetailMapView setHidden:YES];
        [TakeMeThereBtn setHidden:YES];
    }
    else
    {
        [storeDetailMapView setMapType:MKMapTypeStandard];
        [storeDetailMapView setZoomEnabled:YES];
        [storeDetailMapView setScrollEnabled:YES];
        storeDetailMapView.layer.cornerRadius = 10;
        
        MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
        
        region.center.latitude = storeDetailObject.storeLatitude;
        region.center.longitude = storeDetailObject.storeLongitute;
        
        region.span.longitudeDelta = 0.01f;
        region.span.latitudeDelta = 0.01;
        [storeDetailMapView setRegion:region animated:YES];
        
        DisplayMap *ann = [[DisplayMap alloc] init];
        ann.title = storeDetailObject.storeName;
        ann.subtitle = @"";
        ann.coordinate = region.center;
        [storeDetailMapView addAnnotation:ann];
        [ann release];
    }
    [self addControlsInScrollView];
    TakeMeThereBtn.enabled = NO;

    if ([CLLocationManager locationServicesEnabled])
    {
        DebugLog(@"Location services are enabled.");
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        [locationManager startUpdatingLocation];
        [self performSelector:@selector(stopUpdatingCoreLocation:) withObject:@"Timed Out" afterDelay:30];
    }
    else
    {
        DebugLog(@"Location services not enabled.");
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"You currently have all location services for this device disabled. If you proceed, you will be asked to confirm whether location services should be reenabled." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [servicesDisabledAlert show];
        [servicesDisabledAlert release];
    }
    
    UITapGestureRecognizer *imageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(imageViewTapDetected:)];
    imageViewTap.numberOfTapsRequired = 1;
    imageViewTap.cancelsTouchesInView = NO;
    [self.storeImageView  addGestureRecognizer:imageViewTap];
    [imageViewTap release];
    
    galleryImageView.layer.cornerRadius = 10;
    galleryImageView.layer.borderColor = DEFAULT_COLOR.CGColor;
    galleryImageView.layer.borderWidth = 2;
    galleryView.hidden = YES;
    
    UISwipeGestureRecognizer *rightswipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                               action:@selector(swipePressed:)];
    rightswipeRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [galleryView addGestureRecognizer:rightswipeRecognizer];
    [rightswipeRecognizer release];
    
    UISwipeGestureRecognizer *leftswipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                              action:@selector(swipePressed:)];
    leftswipeRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [galleryView addGestureRecognizer:leftswipeRecognizer];
    [leftswipeRecognizer release];

    detailTableView.backgroundColor    = BACKGROUD_COLOR;
    self.view.backgroundColor          = BACKGROUD_COLOR;
    detailView.backgroundColor         = BACKGROUD_COLOR;
    loadMoreDetailsBtn.backgroundColor = BACKGROUD_COLOR;
    loadMoreDetailsBtn.titleLabel.font      = HELVETICA_FONT(15);
    
    loadMoreDetailsBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    loadMoreDetailsBtn.layer.shadowOpacity = 1;
    loadMoreDetailsBtn.layer.shadowOffset = CGSizeMake(0,4);
    
    [loadMoreDetailsBtn setTitleColor:DEFAULT_COLOR forState:UIControlStateNormal];
    [loadMoreDetailsBtn setTitleColor:DEFAULT_COLOR forState:UIControlStateHighlighted];

}
- (void) locationManager:(CLLocationManager *) manager didFailWithError:(NSError *) error
{
    [locationManager stopUpdatingLocation];
}

- (void)stopUpdatingCoreLocation:(NSString *)state
{
    [locationManager stopUpdatingLocation];
    [self saveCurrentLocation:[locationManager location]];
}

-(void)saveCurrentLocation:(CLLocation *)lLocation
{
    // If it's not possible to get a location, then return.
	if (!lLocation) {
        UIAlertView *errorAlert = [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                              message:@"Failed to get your current location. Please check your internet connection and try again."
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil] autorelease];
        [errorAlert show];
		return;
	}
    // Configure the new event with information from the location.
    currentLatitude  = lLocation.coordinate.latitude;
    currentLongitude = lLocation.coordinate.longitude;
    DebugLog(@"%f --- %f",currentLatitude,currentLongitude);
    TakeMeThereBtn.enabled = YES;
}

-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    MKPinAnnotationView *pinView = nil;
    if(annotation != storeDetailMapView.userLocation)
    {
        static NSString *defaultPinID = @"StoreDetailCenterPin";
        pinView = (MKPinAnnotationView *)[storeDetailMapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinView == nil )
            pinView = [[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID] autorelease];
        
        pinView.pinColor = MKPinAnnotationColorRed;
        pinView.canShowCallout = YES;
        pinView.animatesDrop = YES;
    //    UIImage *pinImage = [UIImage imageNamed:@"micra_32x32.png"];
    //    [pinView setImage:pinImage];
    }
    else {
        [storeDetailMapView.userLocation setTitle:@"I am here"];
    }
    return pinView;
}

-(void)addControlsInScrollView
{
    if (storeDetailObject.storePhotos.count == 0)
    {
            self.storeImageView.image = [UIImage imageNamed:@"logonissan.png"];
    }
    else
    {
        for (NSString *link in storeDetailObject.storePhotos)
        {
           // [storeImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",link]]
            [storeImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",link]]
                               placeholderImage:[UIImage imageNamed:@"logonissan.png"]
                                        success:^(UIImage *image) {
                                            //[imgView setContentMode:UIViewContentModeTop];
                                        }
                                        failure:^(NSError *error) {
                                            //DebugLog(@"write error %@", error);
                                        }];
            break;
        }
       
    }
    storeImageView.contentMode          = UIViewContentModeScaleToFill;
    storeImageView.layer.cornerRadius   = 10;
    storeImageView.layer.masksToBounds  = YES;
    storeImageView.layer.borderColor    = [UIColor lightGrayColor].CGColor;
    storeImageView.layer.borderWidth    = 1.0;
    
    UILabel *lblName = [[UILabel alloc] initWithFrame:CGRectMake(self.storeImageView.frame.origin.x+self.storeImageView.frame.size.width+10, self.storeImageView.frame.origin.y, 215, 21)];
    lblName.textColor = DEFAULT_COLOR;
    lblName.font = HELVETICA_FONT(16.0);
//    lblName.shadowColor = [UIColor blackColor];
//    lblName.shadowOffset = CGSizeMake(0, 1);
    lblName.backgroundColor = [UIColor clearColor];
    lblName.textAlignment = UITextAlignmentCenter;
    lblName.text = storeDetailObject.storeName;
    lblName.frame = [self getNewFrame:lblName];
    [self.detailView addSubview:lblName];
    [lblName release];
    
    UILabel *lblAddress = [[UILabel alloc] initWithFrame:CGRectMake(self.storeImageView.frame.origin.x+self.storeImageView.frame.size.width+10,lblName.frame.origin.y+lblName.frame.size.height+5, 215, 21)];
    lblAddress.textColor = [UIColor darkGrayColor];
    lblAddress.font = HELVETICA_FONT(12.0);
//    lblAddress.shadowColor = [UIColor blackColor];
//    lblAddress.shadowOffset = CGSizeMake(0, 1);
    lblAddress.backgroundColor = [UIColor clearColor];
    lblAddress.textAlignment = UITextAlignmentLeft;
    lblAddress.text = storeDetailObject.storeAddress;
    lblAddress.frame = [self getNewFrame:lblAddress];
    [self.detailView addSubview:lblAddress];
    [lblAddress release];

    if ((lblAddress.frame.origin.y+lblAddress.frame.size.height) >= (self.storeImageView.frame.origin.y+self.storeImageView.frame.size.height))
    {
        self.detailView.frame = CGRectMake(self.detailView.frame.origin.x,self.detailView.frame.origin.y,self.detailView.frame.size.width,lblAddress.frame.origin.y+lblAddress.frame.size.height + 20);
    }
    else
    {
        self.detailView.frame = CGRectMake(self.detailView.frame.origin.x,self.detailView.frame.origin.y,self.detailView.frame.size.width,self.storeImageView.frame.origin.y + self.storeImageView.frame.size.height + 20 );
    }
    [self.view insertSubview:detailTableView belowSubview:detailView];
    
    double tableheight = (self.TakeMeThereBtn.frame.origin.y - 10 - (self.detailView.frame.origin.y+self.detailView.frame.size.height + self.loadMoreDetailsBtn.frame.size.height));
    DebugLog(@"tableheight %f self.TakeMeThereBtn.frame.origin.y %f",tableheight,self.TakeMeThereBtn.frame.origin.y);
    self.detailTableView.frame = CGRectMake(detailTableView.frame.origin.x, -(tableheight-detailView.frame.size.height), detailTableView.frame.size.width,tableheight);
    [detailTableView sizeToFit];
    [self addPullView];
}
-(void)addPullView
{
    DebugLog(@"%f",[[UIScreen mainScreen ] bounds].size.height);
    if([[UIScreen mainScreen ] bounds].size.height <  500) {
    self.detailTableView.frame = CGRectMake(detailTableView.frame.origin.x, -(self.detailTableView.frame.size.height-detailView.frame.size.height), detailTableView.frame.size.width,self.detailTableView.frame.size.height);
    DebugLog(@"self.detailTableView.frame.size.height %f",self.detailTableView.frame.size.height);
    self.loadMoreDetailsBtn.frame = CGRectMake(self.loadMoreDetailsBtn.frame.origin.x, self.detailView.frame.size.height, self.loadMoreDetailsBtn.frame.size.width, self.loadMoreDetailsBtn.frame.size.height);
    self.pullImageView.frame = CGRectMake(self.pullImageView.frame.origin.x, self.detailView.frame.size.height+15, self.pullImageView.frame.size.width, self.pullImageView.frame.size.height);
    } else {
        self.detailTableView.frame = CGRectMake(detailTableView.frame.origin.x, -(self.detailTableView.frame.size.height-detailView.frame.size.height) - 50, detailTableView.frame.size.width,self.detailTableView.frame.size.height);
        DebugLog(@"self.detailTableView.frame.size.height %f",self.detailTableView.frame.size.height);
        self.loadMoreDetailsBtn.frame = CGRectMake(self.loadMoreDetailsBtn.frame.origin.x, self.detailView.frame.size.height - 22, self.loadMoreDetailsBtn.frame.size.width, self.loadMoreDetailsBtn.frame.size.height);
        self.pullImageView.frame = CGRectMake(self.pullImageView.frame.origin.x, self.detailView.frame.size.height + 5, self.pullImageView.frame.size.width, self.pullImageView.frame.size.height);
    }
    [loadMoreDetailsBtn setTitle:@"Click here for more details" forState:UIControlStateNormal];
    loadMoreDetailsBtn.titleLabel.textColor = DEFAULT_COLOR;

}
-(CGRect)getNewFrame:(UILabel *)lLabel
{
    lLabel.lineBreakMode = UILineBreakModeWordWrap;
    lLabel.numberOfLines = 0;
    lLabel.adjustsFontSizeToFitWidth = YES;
    lLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [lLabel sizeToFit];

    // Set the height
    CGSize maximumLabelSize = CGSizeMake(215,9999);
    CGSize titleSize = [lLabel.text sizeWithFont:lLabel.font constrainedToSize:maximumLabelSize lineBreakMode:lLabel.lineBreakMode];
    //Adjust the label the the new height
    CGRect newFrame = lLabel.frame;
    newFrame.size.width = titleSize.width;
    newFrame.size.height = titleSize.height;
    return newFrame;
}
- (void)viewDidUnload
{
    self->locationManager = nil;
    [self setStoreDetailMapView:nil];
    [self setTakeMeThereBtn:nil];
    [self setDetailTableView:nil];
    [self setLoadMoreDetailsBtn:nil];
    [self setStoreImageView:nil];
    [self setPullImageView:nil];
    [self setGalleryView:nil];
    [self setPreviousBtn:nil];
    [self setNextBtn:nil];
    [self setGalleryImageView:nil];
    [self setCloseGalleryBtn:nil];
    [self setDetailView:nil];
    [self setGalleryPageControl:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
- (void)dealloc
{
    DebugLog(@"store detail dealloc");
    [locationManager release];
    [storeDetailMapView release];
    [TakeMeThereBtn release];
    [detailTableView release];
    [detailView release];
    [loadMoreDetailsBtn release];
    [storeImageView release];
    phoneNumber = nil;
    [pullImageView release];
    [galleryView release];
    [previousBtn release];
    [nextBtn release];
    [galleryImageView release];
    [closeGalleryBtn release];
    [galleryPageControl release];
    if (storeDetailObject != nil) {
        [storeDetailObject release];
    }
    storeDetailObject = nil;
    [super dealloc];
}

#pragma mark internal methods
- (void)callToDealerStore
{
    NSString *deviceType = [UIDevice currentDevice].model;
   if([deviceType isEqualToString:@"iPhone"])
    {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: [NSString stringWithFormat: @"Do you want to call %@ store number ?",storeDetailObject.storeName]
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"NO", @"YES", nil
                              ];
        [alert show];
        [alert release];
    }
    else
    {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Calling functionaltiy is not available in this device. "
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"OK", nil
                              ];
        [ alert show ];
        [alert release];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        UIApplication *myApp = [UIApplication sharedApplication];
        DebugLog(@"Num=%@",phoneNumber);
        //[str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSRange temprange = [phoneNumber rangeOfString:@"/"];
        DebugLog(@"location %d length %d",temprange.location,temprange.length);
        if ((temprange.location != NSNotFound))
        {
            phoneNumber = [phoneNumber substringToIndex:temprange.location];
        }
        DebugLog(@"phoneNumber = -%@-",[phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""]);
        [myApp openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",[phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""]]]];
        
        NSDictionary *storeParams =
        [NSDictionary dictionaryWithObjectsAndKeys:
         storeDetailObject.storeName, @"DealerStoreName",
         phoneNumber, @"DealerStoreTel",
         nil];
        [Flurry logEvent:@"DealerStoreDetail_Call_Event" withParameters:storeParams];
    }
}

- (void)emailToDealerStore
{
    @try{
        MFMailComposeViewController *picker=[[[MFMailComposeViewController alloc]init] autorelease];
        picker.mailComposeDelegate=self;
        picker.navigationBar.tintColor = DEFAULT_COLOR;
        [picker setToRecipients:[storeDetailObject.storeEmail componentsSeparatedByString:@","]];
        [picker setSubject:@""];
        [self presentModalViewController:picker animated:YES];
    }
    @catch (NSException *exception)
    {
        DebugLog(@"Exception %@",exception.reason);
    }
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    [controller dismissModalViewControllerAnimated:YES];
    if (result == MFMailComposeResultSent){
        NSDictionary *storeParams =
        [NSDictionary dictionaryWithObjectsAndKeys:
         storeDetailObject.storeName, @"DealerStoreName",
         storeDetailObject.storeEmail, @"DealerStoreEmailId",
         nil];
        [Flurry logEvent:@"DealerStoreDetail_Email_Event" withParameters:storeParams];
    }
}
- (void)openWebSiteToDealerStore
{
    DebugLog(@"openWebSiteToDealerStore");
}

- (void)imageViewTapDetected:(UIGestureRecognizer *)sender
{
    DebugLog(@"imageViewTapDetected");
    if (![NEONAppDelegate networkavailable])
    {
        DebugLog(@"network not available");
        [self.galleryView setHidden:YES];
    }
    else
    {
        galleryPageControl.numberOfPages = storeDetailObject.storePhotos.count;
        if (storeDetailObject.storePhotos.count == 0)
        {
            DebugLog(@"images not found");
            [self.galleryView setHidden:YES];
        }
        else if (storeDetailObject.storePhotos.count == 1)
        {
            DebugLog(@"images not found");
            [self.previousBtn setHidden:YES];
            [self.nextBtn setHidden:YES];
            [galleryImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[storeDetailObject.storePhotos objectAtIndex:0]]]
                             placeholderImage:[UIImage imageNamed:@"logonissan.png"]
                                      success:^(UIImage *image) {
                                          //[imgView setContentMode:UIViewContentModeTop];
                                      }
                                      failure:^(NSError *error) {
                                          //DebugLog(@"write error %@", error);
                                      }];
            [UIView transitionWithView:self.galleryView
                              duration:1.0f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                galleryView.hidden = NO;
                            } completion:NULL];
        }
        else
        {
            DebugLog(@"got photo and count %d",storeDetailObject.storePhotos.count);
            [self.previousBtn setHidden:YES];
            imageIndex = 0;
            for (int index = 0; index < storeDetailObject.storePhotos.count; index ++)
            {
                NSString *link = [storeDetailObject.storePhotos objectAtIndex:index];
                [galleryImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",link]]
                                     placeholderImage:[UIImage imageNamed:@"logonissan.png"]
                                              success:^(UIImage *image) {
                                                  //[imgView setContentMode:UIViewContentModeTop];
                                              }
                                              failure:^(NSError *error) {
                                                  //DebugLog(@"write error %@", error);
                                              }];
                imageIndex = index;
                galleryPageControl.currentPage = 0;
                [self.nextBtn setHidden:NO];
                link = nil;
                break;
            }
            [UIView transitionWithView:self.galleryView
                              duration:1.0f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                galleryView.hidden = NO;
                            } completion:NULL];
        }
    }
}

- (IBAction)previousBtnPressed:(id)sender
{

    for (int index = imageIndex-1; index >= 0; index --)
    {
        NSString *link = [storeDetailObject.storePhotos objectAtIndex:index];
        [self swipeAnimation:1];
        [galleryImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",link]]
                             placeholderImage:[UIImage imageNamed:@"logonissan.png"]
                                      success:^(UIImage *image) {
                                          //[imgView setContentMode:UIViewContentModeTop];
                                      }
                                      failure:^(NSError *error) {
                                          //DebugLog(@"write error %@", error);
                                      }];
        imageIndex = index;
        galleryPageControl.currentPage -= 1;
        [self.nextBtn setHidden:NO];
        if (imageIndex == 0)
        {
            [self.previousBtn setHidden:YES];
        }
        break;
    }
}

- (IBAction)nextBtnPressed:(id)sender
{
    for (int index = imageIndex+1; index < storeDetailObject.storePhotos.count; index ++)
    {
        NSString *link = [storeDetailObject.storePhotos objectAtIndex:index];
        [self swipeAnimation:0];
        [galleryImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",link]]
                             placeholderImage:[UIImage imageNamed:@"logonissan.png"]
                                      success:^(UIImage *image) {
                                          //[imgView setContentMode:UIViewContentModeTop];
                                      }
                                      failure:^(NSError *error) {
                                          //DebugLog(@"write error %@", error);
                                      }];
        imageIndex = index;
        galleryPageControl.currentPage += 1;
        [self.previousBtn setHidden:NO];
        link = nil;
        if (imageIndex == storeDetailObject.storePhotos.count-1)
        {
            [self.nextBtn setHidden:YES];
        }
            break;
        }
}

- (IBAction)closeGalleryBtn:(id)sender
{
    [UIView transitionWithView:self.galleryView
                      duration:1.0f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        galleryView.hidden = YES;
                    } completion:NULL];
}

-(void)swipeAnimation:(int)recognizer {
    
    // set up an animation for the transition between the views
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.6];
    [animation setType:kCATransitionPush];
    if (recognizer == 0)
    {
        [animation setSubtype:kCATransitionFromRight];
    }
    else if (recognizer == 1)
    {
        [animation setSubtype:kCATransitionFromLeft];
    }
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [[self.galleryView layer] addAnimation:animation forKey:@"SwapeImageView"];
}

-(void)swipePressed:(UISwipeGestureRecognizer *)recognizer
{
    if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft)
    {
        if(imageIndex != storeDetailObject.storePhotos.count-1)
            [self nextBtnPressed:nil];
    }
    else if (recognizer.direction == UISwipeGestureRecognizerDirectionRight)
    {
        if(imageIndex != 0)
            [self previousBtnPressed:nil];
    }
}

- (IBAction)loadMoreDetailsBtnPressed:(id)sender {
    UIButton *btn = (UIButton *)sender;
    DebugLog(@"loadMoreDetails");
    if ([btn.titleLabel.text isEqualToString:@"Click here for more details"])
    {
        [UIView beginAnimations:@"pulldown" context:nil];
        [UIView setAnimationDuration:1.5];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
        self.detailTableView.frame = CGRectMake(detailTableView.frame.origin.x, detailView.frame.size.height, detailTableView.frame.size.width,detailTableView.frame.size.height);
        self.loadMoreDetailsBtn.frame = CGRectMake(self.loadMoreDetailsBtn.frame.origin.x, self.detailTableView.frame.size.height+self.detailView.frame.size.height, self.loadMoreDetailsBtn.frame.size.width, self.loadMoreDetailsBtn.frame.size.height);
        self.pullImageView.frame = CGRectMake(self.pullImageView.frame.origin.x, self.detailTableView.frame.size.height+self.detailView.frame.size.height+15, self.pullImageView.frame.size.width, self.pullImageView.frame.size.height);
        [UIView commitAnimations];
    }
    else
    {
        [UIView beginAnimations:@"pullup" context:nil];
        [UIView setAnimationDuration:1.5];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
        self.detailTableView.frame = CGRectMake(detailTableView.frame.origin.x, -(detailTableView.frame.size.height-detailView.frame.size.height), detailTableView.frame.size.width,detailTableView.frame.size.height);
        self.loadMoreDetailsBtn.frame = CGRectMake(self.loadMoreDetailsBtn.frame.origin.x, self.detailView.frame.size.height, self.loadMoreDetailsBtn.frame.size.width, self.loadMoreDetailsBtn.frame.size.height);
        self.pullImageView.frame = CGRectMake(self.pullImageView.frame.origin.x, self.detailView.frame.size.height+15, self.pullImageView.frame.size.width, self.pullImageView.frame.size.height);
        [UIView commitAnimations];
    }
}
- (void)transitionDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    if ([animationID isEqualToString:@"pullup"])
    {
        [loadMoreDetailsBtn setTitle:@"Click here for more details" forState:UIControlStateNormal];
        [pullImageView setImage:[UIImage imageNamed:@"dropdownarrow.png" ]];
    }
    else
    {
        [loadMoreDetailsBtn setTitle:@"Hide details" forState:UIControlStateNormal];
        [pullImageView setImage:[UIImage imageNamed:@"dropdownuparrow.png"]];
    }
}

- (IBAction)TakeMeThereBtnPressed:(id)sender {
    
    DebugLog(@"TakeMeThereBtnPressed");
    UIApplication *app = [UIApplication sharedApplication];
    //NSString *urlString = [NSString stringWithFormat:@"http://maps.google.com/maps?saddr=Current+Location&daddr=%lf,%lf",compareTolatValue,compareTolongValue];
    [app openURL:[NSURL URLWithString:GETDIRECTION_LINK(currentLatitude,currentLongitude,storeDetailObject.storeLatitude,storeDetailObject.storeLongitute)]];
    
    NSDictionary *storeParams =
    [NSDictionary dictionaryWithObjectsAndKeys:
     storeDetailObject.storeName, @"DealerStoreName",
     nil];
    [Flurry logEvent:@"DealerStoreDetail_GetDirection_Event" withParameters:storeParams];
}

#pragma mark Table view methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section)
    {
            case 5:
                return 100;
                break;
            default:
                break;
    }
    return 61.5;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    switch (section) {
//        case 0: //phone
//            if (![storeDetailObject.storePhone1 isEqualToString:@""] || ![storeDetailObject.storePhone2 isEqualToString:@""])
//            {
//                return @"Phone";
//            }
//            break;
//        case 1: //mobile
//            if (![storeDetailObject.storeMobile1 isEqualToString:@""] || ![storeDetailObject.storeMobile2 isEqualToString:@""])
//            {
//                return @"Mobile";
//            }
//            break;
//        case 2: //fax
//            if (![storeDetailObject.storeFax isEqualToString:@""])
//            {
//                return @"Fax";
//            }
//            break;
//        case 3: //email
//            if (![storeDetailObject.storeEmail isEqualToString:@""])
//            {
//                return @"Email";
//            }
//            break;
//        case 4: //website
//            if (![storeDetailObject.storeWebsite isEqualToString:@""])
//            {
//                return @"Website";
//            }
//            break;
//        case 5: //description
//            if (![storeDetailObject.storeDescription isEqualToString:@""])
//            {
//                return @"Description";
//            }
//            break;
//        default:
//            break;
//    }
//    return nil;
//}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return [[storeListDic valueForKey:[[storeListDic allKeys] objectAtIndex:section]] count];
    switch (section) {
        case 0: //phone
            if ([storeDetailObject.storePhone1 length] > 3 && [storeDetailObject.storePhone2 length] > 3)
            {
                return 2;
            }
            else if ([storeDetailObject.storePhone1 length] < 3 && [storeDetailObject.storePhone2 length] < 3)
            {
                return 0;
            }
            else
            {
                return 1;
            }
            break;
        case 1: //mobile
            if ([storeDetailObject.storeMobile1 length] > 3 && [storeDetailObject.storeMobile2 length] > 3)
            {
                return 2;
            }
            else if ([storeDetailObject.storeMobile1 length] < 3 && [storeDetailObject.storeMobile2 length] < 3)
            {
                return 0;
            }
            else
            {
                return 1;
            }
            break;
        case 2: //fax
            if ([storeDetailObject.storeFax length] > 3)
            {
                return 1;
            }
            break;
        case 3: //email
            if (![storeDetailObject.storeEmail isEqualToString:@""])
            {
                return 1;
            }
            break;
        case 4: //website
            if (![storeDetailObject.storeWebsite isEqualToString:@""])
            {
                return 1;
            }
            break;
        case 5: //description
            if (![storeDetailObject.storeDescription isEqualToString:@""])
            {
                return 1;
            }
            break;
        default:
            break;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DebugLog(@"index section %d",indexPath.section);
    static NSString *CellIdentifier = @"StoreDetailCell";
    UIImageView *cellImageView;
    UILabel *lblText;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        
        cellImageView = [[UIImageView alloc] initWithFrame:CGRectMake(cell.frame.origin.x+10 , cell.frame.origin.y +10, 40, 40)];
        cellImageView.contentMode = UIViewContentModeScaleToFill;
        cellImageView.backgroundColor =  [UIColor clearColor];
        cellImageView.tag = 1;
        [cell.contentView addSubview:cellImageView];
        [cellImageView release];
        
        lblText = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellImageView.frame)+5, cell.frame.origin.y+15, 250, 30)];
        lblText.tag = 2;
        lblText.font = HELVETICA_FONT(15.0);
        lblText.textColor = [UIColor darkGrayColor];
        lblText .lineBreakMode =  UILineBreakModeTailTruncation;
        lblText.textAlignment = UITextAlignmentLeft;
        lblText.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblText];
        [lblText release];
        
        cell.backgroundColor = [UIColor whiteColor];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = TABLE_SELECTION_COLOR;
        cell.selectedBackgroundView = bgColorView;
        [bgColorView release];
    }
    cellImageView = (UIImageView *)[cell viewWithTag:1];
    switch (indexPath.section)
    {
        case 0: //phone
        case 1: //mobile
        {
            cellImageView.image = [UIImage imageNamed:@"contact_tel.png"];
        }
            break;
        case 2: //fax
        {
            cellImageView.image = [UIImage imageNamed:@"contact_fax.png"];
        }
            break;
        case 3: //email
        {
            
            cellImageView.image = [UIImage imageNamed:@"contact_mail.png"];
        }
            break;
        default:
            break;
    }

    lblText = (UILabel *)[cell viewWithTag:2];
    switch (indexPath.section)
    {
        case 0: //phone
            if ([storeDetailObject.storePhone1 length] < 3)
            {
                lblText.text = storeDetailObject.storePhone2;
            }
            else if ([storeDetailObject.storePhone2 length] < 3)
            {
                lblText.text = storeDetailObject.storePhone1;
            }
            else
            {
                if (indexPath.row == 0)
                {
                    lblText.text = storeDetailObject.storePhone1;
                }
                else
                {
                    lblText.text = storeDetailObject.storePhone2;
                }
            }
            break;
        case 1: //mobile
            if ([storeDetailObject.storeMobile1 length] < 3)
            {
                lblText.text = storeDetailObject.storeMobile2;
            }
            else if ([storeDetailObject.storeMobile2 length] < 3)
            {
                lblText.text = storeDetailObject.storeMobile1;
            }
            else
            {
                if (indexPath.row == 0)
                {
                    lblText.text = storeDetailObject.storeMobile1;
                }
                else
                {
                    lblText.text = storeDetailObject.storeMobile2;
                }
            }
            break;
        case 2: //fax
            lblText.text = storeDetailObject.storeFax;
            break;
        case 3: //email
            lblText.text = storeDetailObject.storeEmail;
            break;
        case 4: //website
            lblText.text = storeDetailObject.storeWebsite;
            break;
        case 5: //description
            lblText.text = storeDetailObject.storeDescription;
           break;
        default:
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0: //phone
            if ([storeDetailObject.storePhone1 isEqualToString:@""])
            {
                phoneNumber = storeDetailObject.storePhone2;
            }
            else if ([storeDetailObject.storePhone2 isEqualToString:@""])
            {
                phoneNumber = storeDetailObject.storePhone1;
            }
            else
            {
                if (indexPath.row == 0)
                {
                    phoneNumber = storeDetailObject.storePhone1;
                }
                else
                {
                    phoneNumber = storeDetailObject.storePhone2;
                }
            }
            [self callToDealerStore];
            break;
        case 1: //mobile
            if ([storeDetailObject.storeMobile1 isEqualToString:@""])
            {
                phoneNumber = storeDetailObject.storeMobile2;
            }
            else if ([storeDetailObject.storeMobile2 isEqualToString:@""])
            {
                phoneNumber = storeDetailObject.storeMobile1;
            }
            else
            {
                if (indexPath.row == 0)
                {
                    phoneNumber = storeDetailObject.storeMobile1;
                }
                else
                {
                    phoneNumber = storeDetailObject.storeMobile2;
                }
            }
            [self callToDealerStore];
            break;
        case 2: //fax
            break;
        case 3: //email
            [self emailToDealerStore];
            break;
        case 4: //website
            [self openWebSiteToDealerStore];
            break;
        case 5: //description
            break;
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end
