//
//  CarUtilStoreListViewController.h
//  CarUtil
//
//  Created by Kirti Nikam on 15/02/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarUtilDealerStore.h"
#define STORE_LINK @"http://stage.phonethics.in/proj/neon/neon_stores.php"

@interface CarUtilStoreListViewController : UIViewController <NSXMLParserDelegate>
{
    NSMutableData *responseAsyncData;
    NSXMLParser *xmlParser;
    CarUtilDealerStore *storeObj;
}
@property (retain, nonatomic) IBOutlet UITableView *storeTableView;

@property (nonatomic, retain) NSMutableDictionary *storeListDic;
@property (nonatomic, strong) NSMutableArray *storeListArray;
@property (nonatomic, strong) NSMutableArray *tempstoreListArray;
@property (nonatomic, copy) NSMutableString *elementKey;
@property (nonatomic, copy) NSMutableString *elementParent;
@property (nonatomic, copy) NSMutableString *elementStatus;
@property (nonatomic, copy) NSString *citysectionName;
@end
