//
//  CarUtilStoreListViewController.m
//  CarUtil
//
//  Created by Kirti Nikam on 15/02/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "CarUtilStoreListViewController.h"
#import "CarUtilStoreMapViewController.h"
#import "CarUtilDealerStore.h"
#import "CarUtilStoreDetailViewController.h"
#import "CarUtilAppDelegate.h"
#import "MBProgressHUD.h"

@interface CarUtilStoreListViewController ()
@property (nonatomic, retain) MBProgressHUD *progressHUD;
@end

@implementation CarUtilStoreListViewController
@synthesize storeTableView;
@synthesize storeListArray,tempstoreListArray;
@synthesize elementKey,elementParent,elementStatus;
@synthesize storeListDic,citysectionName;
@synthesize progressHUD;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    DebugLog(@"CarUtilStoreListViewController : viewWillAppear");
    if ([NEONAppDelegate networkavailable])
    {
        self.navigationItem.rightBarButtonItem.enabled = TRUE;
    }
}
- (void)viewDidLoad
{
#ifdef ENABLE_BACK_BUTTON
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(backbtn_Clicked:)];
    self.navigationItem.backBarButtonItem = backButton;
    [backButton release];
#endif
    [super viewDidLoad];
    [Flurry logEvent:@"Dealer_StoreLocator_List_Event"];

//    UIBarButtonItem *nearStoreButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"map.png"] style:UIBarButtonItemStyleBordered target:self  action:@selector(shownearstoreinmap_Clicked:)];
    UIBarButtonItem *nearStoreButton = [[UIBarButtonItem alloc] initWithTitle:@"Near Me" style:UIBarButtonItemStyleBordered target:self action:@selector(shownearstoreinmap_Clicked:)];
    self.navigationItem.rightBarButtonItem = nearStoreButton;
    self.navigationItem.rightBarButtonItem.enabled = FALSE;
    [nearStoreButton release];
    
    
    DebugLog(@"<<============================== sendGetStoreListHTTPRequest ==============================>>");
    if ([NEONAppDelegate networkavailable])
    {
        [self sendHttpRequest:STORE_LINK];
    }
    else
    {
        [self parseFromFile];
    }
    [storeTableView setHidden:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wifiNotifyCallBack) name:NETWORK_NOTIFICATION object:nil];
}
-(void)NetworkNotifyCallBack
{
    DebugLog(@"DealerStoreList: Got Network Notification");
    self.navigationItem.rightBarButtonItem.enabled = TRUE;
    [self performSelector:@selector(sendHttpRequest:) withObject:STORE_LINK afterDelay:2.0];
}
- (void)showProgressHUDWithMessage:(NSString *)message {
    self.progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.progressHUD.labelText = message;
    self.progressHUD.labelFont = HELVETICA_FONT(15);
    self.progressHUD.mode = MBProgressHUDModeIndeterminate;
    [self.view bringSubviewToFront:self.progressHUD];
    [self.progressHUD show:YES];
}
-(void)hideProgressHUD:(BOOL)animated
{
    [self.progressHUD hide:animated];
}
-(void)sendHttpRequest:(NSString *)urlString
{
    [self showProgressHUDWithMessage:@"Loading"];
    DebugLog(@"URL : %@",urlString);
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:YES timeoutInterval:10.0];
	NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    [theConnection autorelease];
}

- (void) shownearstoreinmap_Clicked: (id) sender
{
    CarUtilStoreMapViewController *storeLocMapViewController = [[CarUtilStoreMapViewController alloc] initWithNibName:@"CarUtilStoreMapViewController" bundle:nil] ;
    storeLocMapViewController.title = @"Dealers Near Me";
    storeLocMapViewController.hidesBottomBarWhenPushed = YES;
    storeLocMapViewController.storeListArray = self.storeListArray;
    [self.navigationController pushViewController:storeLocMapViewController animated:YES];
    [storeLocMapViewController release];
   
//    [UIView  transitionWithView:self.navigationController.view duration:1.0  options:UIViewAnimationOptionTransitionFlipFromRight
//                     animations:^(void) {
//                         BOOL oldState = [UIView areAnimationsEnabled];
//                         [UIView setAnimationsEnabled:NO];
//                         [self.navigationController popViewControllerAnimated:YES];
//                         [UIView setAnimationsEnabled:oldState];
//                     }
//                     completion:nil];
}

- (void) backbtn_Clicked: (id) sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)viewDidUnload
{
    [self setStoreTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)dealloc {
    DebugLog(@"Dealer List dealloc");
    [storeListArray removeAllObjects];
    [storeListArray release];
    [storeListDic removeAllObjects];
    [storeListDic release];
    [storeTableView release];
    [super dealloc];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark Table view methods
#pragma mark Table view methods

//- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
//{
//    return [[NSArray arrayWithObject:UITableViewIndexSearch] arrayByAddingObjectsFromArray:
//            [[UILocalizedIndexedCollation currentCollation] sectionIndexTitles]];
//}
//- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
//{
//    NSMutableSet *mySet = [[[NSMutableSet alloc] init] autorelease];
//    for ( NSString *s in [self.storeListDic allKeys] )
//    {
//        if ( s.length > 0 )
//            [mySet addObject:[s substringToIndex:1]];
//    }
//    NSArray *indexArray = [[mySet allObjects] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
//    return indexArray;
//}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 54;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *customHeaderView = [[[UIView alloc] initWithFrame:CGRectMake(0,0,320,54)] autorelease];
    customHeaderView.clipsToBounds = YES;
    customHeaderView.backgroundColor = BACKGROUD_COLOR;
    
    UIView *toplineView = [[UIView alloc] initWithFrame:CGRectMake(0,2,320,2)];
    toplineView.backgroundColor = DEFAULT_COLOR;
    [customHeaderView addSubview:toplineView];
    [toplineView release];
    
    
    UILabel *lblHeader = [[UILabel alloc] initWithFrame:CGRectMake(0 ,CGRectGetMaxY(toplineView.frame), 320, 46)];
    lblHeader.textColor = DEFAULT_COLOR;
    lblHeader.font = HELVETICA_FONT(18.0);
    lblHeader.backgroundColor = [UIColor whiteColor];
    lblHeader.textAlignment = UITextAlignmentCenter;
    lblHeader.text = [[[self.storeListDic allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
    [customHeaderView addSubview:lblHeader];
    [lblHeader release];

    UIView *bottomlineView = [[UIView alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(lblHeader.frame),320,2)];
    bottomlineView.backgroundColor = DEFAULT_COLOR;
    [customHeaderView addSubview:bottomlineView];
    [bottomlineView release];
    
    return customHeaderView;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[storeListDic allKeys] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    //    return [[self.storeListDic allKeys] objectAtIndex:section];
    return [[[self.storeListDic allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    CGRect screenBounds = [[UIScreen mainScreen] bounds];
//    if (screenBounds.size.height == 568) {
//        // code for 4-inch screen
//        return 65;
//    } else {
//        // code for 3.5-inch screen
//        return 61.5;
//    }
    return 95;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return [[storeListDic valueForKey:[[storeListDic allKeys] objectAtIndex:section]] count];
    return [[self.storeListDic valueForKey:[[[self.storeListDic allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:section]] count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UILabel *lblName;
    UILabel *lblAddress;
    static NSString *CellIdentifier = @"StoreCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        //Initialize Label with tag 2.(start date Label)
        lblName = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x + 35, cell.frame.origin.y+10, 250, 15)];
        lblName.tag = 1;
        lblName.font = HELVETICA_FONT(16.0);
        lblName.textColor = [UIColor darkGrayColor];
        lblName.textAlignment = UITextAlignmentLeft;
        lblName.lineBreakMode =  UILineBreakModeTailTruncation;
        lblName.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblName];
        [lblName release];
        
        //Initialize Label with tag 3.(start km Label)
        lblAddress = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x + 35, CGRectGetMaxY(lblName.frame), 250, 60)];
        lblAddress.tag = 2;
        lblAddress.font = HELVETICA_FONT(13.0);
        lblAddress.textColor = [UIColor darkGrayColor];
        lblAddress.textAlignment = UITextAlignmentLeft;
        lblAddress.lineBreakMode =  UILineBreakModeTailTruncation;
        lblAddress.numberOfLines = 4;
        lblAddress.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblAddress];
        [lblAddress release];
        
        UIView *selectedView = [[UIView alloc] init];
        selectedView.backgroundColor = TABLE_SELECTION_COLOR;
        cell.selectedBackgroundView = selectedView;
        [selectedView release];
    }
    //CarUtilDealerStore *tempObj = (CarUtilDealerStore *)[[storeListDic valueForKey:[[storeListDic allKeys] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
  //  CarUtilDealerStore *tempObj = (CarUtilDealerStore *)[[self.storeListDic valueForKey:[[[self.storeListDic allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    int cellindex =  [[[self.storeListDic valueForKey:[[[self.storeListDic allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] intValue];
    CarUtilDealerStore *tempObj = (CarUtilDealerStore *)[self.storeListArray objectAtIndex:cellindex];
    
    lblName         =   (UILabel *)[cell viewWithTag:1];
    lblName.text    =   tempObj.storeName;
    
    lblAddress      =   (UILabel *)[cell viewWithTag:2];
    lblAddress.text =   tempObj.storeAddress;
    
    UIImageView *accessoryImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow.png"]];
    cell.accessoryView  = accessoryImageView;
    [accessoryImageView release];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    //   cell.accessoryView  = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bigarrow.png"]] autorelease];
    //   cell.selectionStyle = UITableViewCellSelectionStyleGray;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    CarUtilStoreDetailViewController *storedetailviewController = [[CarUtilStoreDetailViewController alloc] initWithNibName:@"CarUtilStoreDetailViewController" bundle:nil];
       CarUtilStoreDetailViewController *storedetailviewController = [[CarUtilStoreDetailViewController alloc] init];
  //  storedetailviewController.storeDetailObject = (CarUtilDealerStore *)[[storeListDic valueForKey:[[storeListDic allKeys] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
  //   storedetailviewController.storeDetailObject = (CarUtilDealerStore *)[[self.storeListDic valueForKey:[[[self.storeListDic allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    int cellindex =  [[[self.storeListDic valueForKey:[[[self.storeListDic allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] intValue];
    storedetailviewController.storeDetailObject = (CarUtilDealerStore *)[self.storeListArray objectAtIndex:cellindex];
    storedetailviewController.title = [[[self.storeListDic allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.section];
    [self.navigationController pushViewController:storedetailviewController animated:YES];
    [storedetailviewController release];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    DebugLog(@"didFailWithError: %@", [error localizedDescription]);
    [self connectionDidFinishLoading:nil];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    DebugLog(@"connectionDidFinishLoading");
	NSString *xmlDataFromChannelSchemes;
	if(responseAsyncData)
	{
		NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
        [NEONAppDelegate writeToTextFile:result name:@"dealerstore"];
		DebugLog(@"\n result:%@\n\n", result);
        
		xmlDataFromChannelSchemes = [[NSString alloc] initWithString:result];
		NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
		xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
		[xmlParser setDelegate:self];
		[xmlParser parse];
        [xmlParser release];
		[xmlDataFromChannelSchemes release];
		[result release];
		[responseAsyncData release];
		responseAsyncData = nil;
	}
    else {
        [self parseFromFile];
    }
}

-(void) parseFromFile
{
    NSString *xmlDataFromChannelSchemes;
    NSString *data = [NEONAppDelegate getTextFromFile:@"dealerstore"];
    DebugLog(@"\nparseFromFile data:%@\n\n", data);
    if(data != nil && ![data isEqualToString:@""])
    {
        xmlDataFromChannelSchemes = [[NSString alloc] initWithString:data];
    }
    else
    {
        DebugLog(@"\n reading from local\n");
        NSString* path = [[NSBundle mainBundle] pathForResource:@"dealerstore" ofType:@"txt"];
        NSString* content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
        xmlDataFromChannelSchemes = [[NSString alloc] initWithString:content];
    }
    NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
    [xmlParser setDelegate:self];
    [xmlParser parse];
    [xmlParser release];
    [xmlDataFromChannelSchemes release];
}

#pragma mark xmlParser methods
/* Called when the parser runs into an open tag (<tag>) */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName 	attributes:(NSDictionary *)attributeDict
{
    if([elementName isEqualToString:@"stores"])
	{
        storeListDic = [[NSMutableDictionary alloc] init];
        storeListArray = [[NSMutableArray alloc] init];
	}
    else if([elementName isEqualToString:@"city"])
    {
        citysectionName = [[NSString alloc] initWithString:[attributeDict objectForKey:@"name"]];
        tempstoreListArray  = [[NSMutableArray alloc] init];
        DebugLog(@"citysectionName %@",citysectionName);
	}
    else if([elementName isEqualToString:@"store"])
    {
        storeObj = [[CarUtilDealerStore alloc] init];
        storeObj.storeId            = [[attributeDict objectForKey:@"id"] intValue];
        storeObj.storeName          = [attributeDict objectForKey:@"name"];
        storeObj.storeAddress       = [attributeDict objectForKey:@"address"];
        storeObj.storePhone1        = [attributeDict objectForKey:@"phone1"];
        storeObj.storePhone2        = [attributeDict objectForKey:@"phone2"];
        storeObj.storeMobile1       = [attributeDict objectForKey:@"mobile1"];
        storeObj.storeMobile2       = [attributeDict objectForKey:@"mobile2"];
        storeObj.storeFax           = [attributeDict objectForKey:@"fax"];
        storeObj.storeEmail         = [attributeDict objectForKey:@"email"];
        storeObj.storeWebsite       = [attributeDict objectForKey:@"website"];
        //storeObj.storePhotos        = [attributeDict objectForKey:@"photo1"];
        //storeObj.storePhoto2        = [attributeDict objectForKey:@"photo2"];
        [storeObj.storePhotos addObject:[attributeDict objectForKey:@"photo1"]];
        [storeObj.storePhotos addObject:[attributeDict objectForKey:@"photo2"]];
        [storeObj.storePhotos removeObject:@""];
        storeObj.storeDescription   = [attributeDict objectForKey:@"description"];
        storeObj.storeLatitude      = [[attributeDict objectForKey:@"latitude"] doubleValue];
        storeObj.storeLongitute     = [[attributeDict objectForKey:@"longitude"] doubleValue];
        DebugLog(@"store : %d %@",storeObj.storeId,storeObj.storeName);
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
}

/* Called when the parser runs into a close tag (</tag>). */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"stores"])
    {
        DebugLog(@"storeListArray %@",storeListArray);
        DebugLog(@"storeListDic count %d",[storeListDic count]);
        DebugLog(@"%@", storeListDic);
        [self hideProgressHUD:YES];
        [storeTableView reloadData];
        [storeTableView setHidden:NO];
        if ([NEONAppDelegate networkavailable])
        {
            self.navigationItem.rightBarButtonItem.enabled = TRUE;
        }
        [parser abortParsing];
    }
    else if([elementName isEqualToString:@"city"])
    {
        DebugLog(@"citysectionName %@",citysectionName);
        [storeListDic setObject:tempstoreListArray forKey:citysectionName];
        [tempstoreListArray release];
        tempstoreListArray = nil;
        [citysectionName release];
        citysectionName = nil;
    }
    else if([elementName isEqualToString:@"store"])
    {
        [storeListArray addObject:storeObj];
        [tempstoreListArray addObject:[NSNumber numberWithInt:[storeListArray indexOfObject:storeObj]]];
        [storeObj release];
        storeObj = nil;
    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    DebugLog(@"Error: %@", [parseError localizedDescription]);
}

- (void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError
{
    DebugLog(@"Error: %@", [validationError localizedDescription]);
}

@end
