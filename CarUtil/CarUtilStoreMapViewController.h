//
//  CarUtilStoreMapViewController.h
//  CarUtil
//
//  Created by Kirti Nikam on 15/02/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "CarUtilDealerStore.h"
#import "NearByMeObject.h"

#define NEAR100KM 100000 //100 km = 100000 /1000
#define NEARME_LINK @"http://localhost:90/nearme.xml"
#define NEAR_LOCATION(CURLAT,CURLNG,DESTSTR) [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/xml?origins=%f,%f&destinations=%@&mode=driving&sensor=false",CURLAT,CURLNG,DESTSTR]

@class DisplayMap;
@interface CarUtilStoreMapViewController : UIViewController <MKMapViewDelegate,CLLocationManagerDelegate,NSXMLParserDelegate>
{
    CLLocationManager *locationManager;
    NSMutableData *responseAsyncData;
    NSXMLParser *xmlParser;
    NearByMeObject *nearByMeObj;
    //DisplayMap *nearann;
    
    BOOL elementFound;
    int nearbymerowid;
}
@property (nonatomic, strong) NSMutableArray *storeListArray;
@property (nonatomic, strong) NSMutableArray *nearCenterListArray;
@property (readwrite,nonatomic) double currentLatitude;
@property (readwrite,nonatomic) double currentLongitude;
@property (nonatomic, copy) NSMutableString *elementKey;
@property (nonatomic, copy) NSMutableString *elementParent;
@property (nonatomic, copy) NSMutableString *elementStatus;

@property (retain, nonatomic) IBOutlet MKMapView *storeMapView;
@end
