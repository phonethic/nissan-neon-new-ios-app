//
//  CarUtilStoreMapViewController.m
//  CarUtil
//
//  Created by Kirti Nikam on 15/02/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "CarUtilStoreMapViewController.h"
#import "CarUtilStoreListViewController.h"
#import "CarUtilAppDelegate.h"
#import "DisplayMap.h"
#import "CarUtilStoreDetailViewController.h"
#import "MBProgressHUD.h"

@interface CarUtilStoreMapViewController ()
@property (nonatomic, retain) MBProgressHUD *progressHUD;
@end

@implementation CarUtilStoreMapViewController
@synthesize storeMapView;
@synthesize currentLatitude,currentLongitude;
@synthesize nearCenterListArray;
@synthesize elementKey,elementParent,elementStatus;
@synthesize storeListArray;
@synthesize progressHUD;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
   }
    return self;
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [locationManager stopUpdatingLocation];
}
- (void)viewDidLoad
{
#ifdef ENABLE_BACK_BUTTON
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [backButton release];
#endif
    [super viewDidLoad];
    
    [Flurry logEvent:@"Dealer_StoreLocator_NearByMe_Map_Event"];
    [self showProgressHUDWithMessage:@"Fetching your location"];

    nearbymerowid = 0;

    storeMapView.mapType = MKMapTypeStandard;
    storeMapView.zoomEnabled = YES;
    storeMapView.scrollEnabled = YES;
    storeMapView.showsUserLocation = YES;
    
    if ([CLLocationManager locationServicesEnabled])
    {
        DebugLog(@"Location services are enabled.");
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        [locationManager startUpdatingLocation];
        [self performSelector:@selector(stopUpdatingCoreLocation:) withObject:@"Timed Out" afterDelay:10];
    }
    else
    {
        DebugLog(@"Location services not enabled.");
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"You currently have all location services for this device disabled. If you proceed, you will be asked to confirm whether location services should be reenabled." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [servicesDisabledAlert show];
        [servicesDisabledAlert release];
    }
}
- (void)showProgressHUDWithMessage:(NSString *)message {
    self.progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.progressHUD.labelText = message;
    self.progressHUD.labelFont = HELVETICA_FONT(15);
    self.progressHUD.mode = MBProgressHUDModeIndeterminate;
    [self.view bringSubviewToFront:self.progressHUD];
    [self.progressHUD show:YES];
}
-(void)hideProgressHUD:(BOOL)animated
{
    [self.progressHUD hide:animated];
}
- (void) locationManager:(CLLocationManager *) manager didFailWithError:(NSError *) error
{
    [locationManager stopUpdatingLocation];
}

- (void)stopUpdatingCoreLocation:(NSString *)state
{
    [locationManager stopUpdatingLocation];
    [self saveCurrentLocation:[locationManager location]];
}

-(void)saveCurrentLocation:(CLLocation *)lLocation
{
    // If it's not possible to get a location, then return.
	if (!lLocation) {
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                              message:@"Failed to get your current location. Please check your internet connection and try again."
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
        [errorAlert show];
        [errorAlert release];
         [self hideProgressHUD:YES];
		return;
	}
    // Configure the new event with information from the location.
    [self.storeMapView setRegion:MKCoordinateRegionMake([lLocation coordinate], MKCoordinateSpanMake(0.5, 0.5))];
    currentLatitude  = lLocation.coordinate.latitude;
    currentLongitude = lLocation.coordinate.longitude;
    DebugLog(@"%f --- %f",currentLatitude,currentLongitude);
    self.navigationItem.rightBarButtonItem.enabled = YES;
    DebugLog(@"<<============================== sendGetNearStoreListHTTPRequest ==============================>>");
    [self sendNearLocationHTTPRequest];
}
-(void)sendNearLocationHTTPRequest
{
    [progressHUD setLabelText:@"Locating dealer near you"];
    NSMutableString *destinationString= [[NSMutableString alloc] initWithCapacity:0];
    DebugLog(@"storeListArray %d",storeListArray.count);
    //for (CarUtilDealerStore *tempObj in storeListArray)
    for (int index = 0; index < [storeListArray count]; index++)
    {
        CarUtilDealerStore *tempObj = (CarUtilDealerStore *)[storeListArray objectAtIndex:index];
        DebugLog(@"rowid %d ==> [%@,%f,%f]",index,tempObj.storeName,tempObj.storeLatitude,tempObj.storeLongitute);
        double latValue = tempObj.storeLatitude;
        double longValue = tempObj.storeLongitute;
        //if (latValue != 0)
        //{
            [destinationString appendString:[NSString stringWithFormat:@"%f,%f%%7C",latValue,longValue]];
        //}
    }
  //  DebugLog(@"destination string %@",destinationString);
    if (destinationString.length > 3)
    {
        NSString *newDestinationString = [destinationString substringToIndex:[destinationString length]-3];
        [self sendHttpRequest:NEAR_LOCATION(currentLatitude,currentLongitude,newDestinationString)];
    }
    [destinationString release];
}
-(void)sendHttpRequest:(NSString *)urlString
{
    DebugLog(@"URL : %@",urlString);
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:YES timeoutInterval:10.0];
	NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    [theConnection autorelease];
}

-(void)reloadMapData
{
    DebugLog(@"<<============================== reloadMapDataForNearLocation ==============================>>");

    
    NSSortDescriptor *sortDescriptor =  [[NSSortDescriptor alloc] initWithKey:@"distanceValue" ascending:YES];
    [nearCenterListArray sortUsingDescriptors:[NSMutableArray arrayWithObject:sortDescriptor]];
    [sortDescriptor release];
    DebugLog(@"nearCenterListArray.count %d",nearCenterListArray.count);
    //    CarUtilDealerStore *nearCarObj = (CarUtilDealerStore *)[storeListArray objectAtIndex:nearestObject.nearbymerowid];
    //nearann = nil;
    for (int index = 0; index < [nearCenterListArray count]; index++)
    {
        NearByMeObject *nearestObject = (NearByMeObject *)[nearCenterListArray objectAtIndex:index];
    
        DebugLog(@"row %d distance is %f %@",index,nearestObject.distanceValue,nearestObject.distanceText);
        if (nearestObject.distanceValue <= NEAR100KM)
        {
            CarUtilDealerStore *nearCarObj = (CarUtilDealerStore *)[storeListArray objectAtIndex:nearestObject.nearbymerowid];
            
            if (nearCarObj.storeLatitude != 0)
            {
                MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
                
                region.center.latitude      = nearCarObj.storeLatitude;
                region.center.longitude     = nearCarObj.storeLongitute;
                region.span.latitudeDelta   = 0.5f;
                region.span.longitudeDelta  = 0.5f;
                [storeMapView setRegion:region animated:YES];
                
                DisplayMap *ann = [[DisplayMap alloc] init];
                ann.title = nearCarObj.storeName;
                ann.subtitle = nearCarObj.storeAddress;
                ann.coordinate = region.center;
                [storeMapView addAnnotation:ann];
                
                DebugLog(@"row id %d Store : %@ Distance %f (%@)",nearestObject.nearbymerowid,nearCarObj.storeName,nearestObject.distanceValue,nearestObject.distanceText);
    //            if ([tempObj.storeName isEqualToString:nearCarObj.storeName])
    //            {
    //                DebugLog(@"Near LocNAme %@",ann.title);
    //                nearann = ann;
    //            }
                [ann release];
            }
        }
        nearestObject = nil;
    }
     [self hideProgressHUD:YES];
}
-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    MKPinAnnotationView *pinView = nil;
    if(annotation != storeMapView.userLocation)
    {
        static NSString *defaultPinID = @"StoreCenterPin";
        pinView = (MKPinAnnotationView *)[storeMapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if (pinView == nil)
        {
            pinView = [[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID] autorelease];
            pinView.pinColor = MKPinAnnotationColorRed;
            pinView.canShowCallout = YES;
            pinView.animatesDrop = YES;
            
            UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            pinView.rightCalloutAccessoryView = rightButton;
        }
        else
        {
            pinView.annotation = annotation;
        }
    }
    else
    {
        [storeMapView.userLocation setTitle:@"I am here"];
    }
    return pinView;
}
- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
    //DebugLog(@"didAddAnnotationViews %@",views);
//    if (nearann != nil)
//    {
//        MKPinAnnotationView *av = (MKPinAnnotationView *)[storeMapView viewForAnnotation:nearann];
//        av.pinColor = MKPinAnnotationColorGreen;
//        //ann.subtitle = [NSString stringWithFormat:@"Distance : %@ \nDuration : %@",nearestObject.distanceText,nearestObject.durationText];
//    }
}
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    for (CarUtilDealerStore *tempObj in storeListArray)
    {
        DisplayMap *ann = (DisplayMap *)view.annotation;
        DebugLog(@"title %@ %@",ann.title,tempObj.storeName);
        if ([ann.title isEqualToString:tempObj.storeName])
        {
            //CarUtilStoreDetailViewController *storedetailviewController = [[CarUtilStoreDetailViewController alloc] initWithNibName:@"CarUtilStoreDetailViewController" bundle:nil] ;
            CarUtilStoreDetailViewController *storedetailviewController = [[CarUtilStoreDetailViewController alloc] init];
            storedetailviewController.storeDetailObject = tempObj;
            [self.navigationController pushViewController:storedetailviewController animated:YES];
            [storedetailviewController release];
            break;
        }
        tempObj = nil;
    }       
}
#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    DebugLog(@"didFailWithError: %@", [error localizedDescription]);
    [self connectionDidFinishLoading:nil];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
     [self hideProgressHUD:YES];
    DebugLog(@"connectionDidFinishLoading");
	NSString *xmlDataFromChannelSchemes;
	if(responseAsyncData)
	{
		NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
        //[NEONAppDelegate writeToTextFile:result name:@"nearme"];
		//DebugLog(@"\n result:%@\n\n", result);
        
		xmlDataFromChannelSchemes = [[NSString alloc] initWithString:result];
		NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
		xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
		[xmlParser setDelegate:self];
		[xmlParser parse];
        [xmlParser release];
		[xmlDataFromChannelSchemes release];
		[result release];
		[responseAsyncData release];
		responseAsyncData = nil;
	}
//    else {
//        [self parseFromFile];
//    }
}

//-(void) parseFromFile
//{
//    NSString *xmlDataFromChannelSchemes;
//    NSString *data = [NEONAppDelegate getTextFromFile:@"nearme"];
//    
//    DebugLog(@"\nparseFromFile data:%@\n\n", data);
//    if(data != nil && ![data isEqualToString:@""])
//    {
//        xmlDataFromChannelSchemes = [[NSString alloc] initWithString:data];
//        NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//        xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
//        [xmlParser setDelegate:self];
//        [xmlParser parse];
//        [xmlParser release];
//        [xmlDataFromChannelSchemes release];
//    }
//    else {
//        UIAlertView *errorView = [[[UIAlertView alloc]
//                                   initWithTitle:@"No Network Connection"
//                                   message:@"Please check your internet connection and try again."
//                                   delegate:self
//                                   cancelButtonTitle:@"OK"
//                                   otherButtonTitles:nil] autorelease];
//        [errorView show];
//    }
//    
//}

#pragma mark xmlParser methods
/* Called when the parser runs into an open tag (<tag>) */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName 	attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"row"])
    {
        nearbymerowid = 0;
        if(nearCenterListArray == nil)
            nearCenterListArray = [[NSMutableArray alloc] init];
    }
    else if ([elementName isEqualToString:@"element"])
    {
        nearByMeObj = [[NearByMeObject alloc] init];
    }
    else if ([elementName isEqualToString:@"status"])
    {
        elementFound = YES;
        elementKey = [NSMutableString stringWithString:elementName];
    }
    else if ([elementName isEqualToString:@"duration"] || [elementName isEqualToString:@"distance"])
    {
        elementKey = [NSMutableString stringWithString:elementName];
    }
    else if ([elementName isEqualToString:@"value"] || [elementName isEqualToString:@"text"])
    {
        elementFound = YES;
        elementParent = [NSMutableString stringWithString:elementName];
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
   // DebugLog(@"1)found string as --%@--",string);
    if (elementFound)
    {
       // DebugLog(@"elementFound == YES");
       // DebugLog(@"elementKey == %@",elementKey);

        if ([elementKey isEqualToString:@"status"])
        {
           // DebugLog(@"Got status --%@--",string);
            if (![string isEqualToString:@""])
            {
                elementStatus = [NSMutableString stringWithString:string];
            }
        }
        else if ([elementKey isEqualToString:@"duration"] && elementStatus != nil && ![elementStatus isEqualToString:@""] && [elementStatus isEqualToString:@"OK"])
        {
           // DebugLog(@"Got duration");

            if ([elementParent isEqualToString:@"value"])
            {
           //     DebugLog(@"3)string durationValue %@",string);
                nearByMeObj.durationValue = [string doubleValue];
            }
            else if ([elementParent isEqualToString:@"text"])
            {
           //     DebugLog(@"3)string durationText %@",string);
                nearByMeObj.durationText = string;
            }
        }
        else if ([elementKey isEqualToString:@"distance"] && elementStatus != nil && ![elementStatus isEqualToString:@""] && [elementStatus isEqualToString:@"OK"])
        {
           // DebugLog(@"Got distance");

            if ([elementParent isEqualToString:@"value"])
            {
            //    DebugLog(@"3)string distanceValue %@",string);
                nearByMeObj.distanceValue = [string doubleValue];
            }
            else if ([elementParent isEqualToString:@"text"])
            {
             //   DebugLog(@"3)string distanceText %@",string);
                nearByMeObj.distanceText = string;
            }
        }
        else
        {
            elementFound = FALSE;
        }
    }
    elementFound = FALSE;
}

/* Called when the parser runs into a close tag (</tag>). */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if([elementName isEqualToString:@"row"])
    {
        [parser abortParsing];
        DebugLog(@"<<=========================================done near by me parsing====================================>>");
        DebugLog(@"nearCenterListArray.count %d",nearCenterListArray.count);
       [self reloadMapData];
        
    }
    else if ([elementName isEqualToString:@"element"] && nearByMeObj != nil)
    {
        nearByMeObj.nearbymerowid = nearbymerowid;
        DebugLog(@"6)rowid %d ==> [%@,%f,%@,%f]",nearbymerowid,nearByMeObj.distanceText,nearByMeObj.distanceValue,nearByMeObj.durationText,nearByMeObj.durationValue);
        [self.nearCenterListArray addObject:nearByMeObj];
        [nearByMeObj release];
        nearByMeObj = nil;
        
        elementStatus = nil;
        nearbymerowid +=1;
    }
    else if ([elementName isEqualToString:@"value"] || [elementName isEqualToString:@"text"])
    {
        elementParent = nil;
    }
    else if ([elementName isEqualToString:@"duration"] || [elementName isEqualToString:@"distance"])
    {
        elementKey = nil;
    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    DebugLog(@"Error: %@", [parseError localizedDescription]);
}

- (void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError
{
    DebugLog(@"Error: %@", [validationError localizedDescription]);
}

- (void)viewDidUnload
{
    self->locationManager = nil;
    [self setStoreMapView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)dealloc {
    DebugLog(@"map dealloc");
    [storeMapView release];
    [locationManager release];
    storeListArray = nil;
    [nearCenterListArray removeAllObjects];
    [nearCenterListArray release];
    nearCenterListArray = nil;
    [super dealloc];
}
@end
