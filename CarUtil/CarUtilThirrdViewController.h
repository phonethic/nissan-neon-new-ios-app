//
//  CarUtilThirrdViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 29/03/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#define MAINTENANCE @"Maintenance"
#define SAFETY @"Safety"
#define DRIVING @"Driving Tips"
#define LOCATION @"Location Based Tips"
#define SEASON @"Season Based Tips"
#define VIDEOS @"'How to' Videos"

          
@class CarUtilTips;
@interface CarUtilThirrdViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,NSXMLParserDelegate>
{
    NSXMLParser *xmlParser;
    NSMutableData *responseAsyncData;
    int status;
    BOOL elementFound;
    CarUtilTips *tipsObj;
    BOOL oneTimeAnimation;
}

@property (retain, nonatomic) IBOutlet UIImageView *loadingImageView;
@property (strong, nonatomic) NSArray *listData;
@property (retain, nonatomic) IBOutlet UITableView *tipstbl;
@property (retain, nonatomic) NSMutableDictionary *tipsdata;
@property (nonatomic, copy) NSString *sectionName;
@property (nonatomic, copy) NSString *sectionImage;
@property (strong, nonatomic) NSMutableArray *tipsArray;

@end
