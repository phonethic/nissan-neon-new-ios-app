//
//  CarUtilThirrdViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 29/03/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "CarUtilThirrdViewController.h"
#import "CarUtilAppDelegate.h"
#import "MaintenanceViewController.h"
#import "SafteyViewController.h"
#import "DrivingTipsViewController.h"
#import "LocationTipsViewController.h"
#import "SeasonTipsViewController.h"
#import "HowtoVideoViewController.h"
#import "UIImageView+WebCache.h"
#import "CarUtilTips.h"
#import "CarUtilAppDelegate.h"
#import "MKNumberBadgeView.h"
#import "CarUtilTipsSectionViewController.h"
#import "MBProgressHUD.h"

#import "MFSideMenu.h"

#define NEON_TIPS_LINK @"http://stage.phonethics.in/proj/neon/neon_tips.php"
//#define NEON_TIPS_LINK @"http://localhost/carutil/neon_tips.xml"

@interface CarUtilThirrdViewController ()
@property (nonatomic, retain) MBProgressHUD *progressHUD;
@end

@implementation CarUtilThirrdViewController
@synthesize loadingImageView;
@synthesize listData;
@synthesize tipstbl;
@synthesize tipsdata;
@synthesize sectionName,sectionImage;
@synthesize tipsArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.navigationItem.title = NSLocalizedString(@"Tips", @"Tips");
        //self.tabBarItem.image = [UIImage imageNamed:@"tips"];
        //[self setMyTitle:@"Tips"];
    }
    return self;
}
- (void)setMyTitle:(NSString *)title
{
    [super setTitle:title];
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont boldSystemFontOfSize:20.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor orangeColor]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text = title;
    [titleView sizeToFit];
}

- (void)viewDidLoad
{
#ifdef ENABLE_BACK_BUTTON
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [backButton release];
    
#endif
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    DebugLog(@"viewDidLoad");
    [Flurry logEvent:@"Tips_Tab_Event"];
    
    self.tipstbl.backgroundColor = BACKGROUD_COLOR;

    [self setupMenuBarButtonItems];
    self.navigationController.navigationBar.tintColor = DEFAULT_COLOR;
    self.view.backgroundColor = BACKGROUD_COLOR;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NetworkNotifyCallBack) name:NETWORK_NOTIFICATION object:nil];
}
-(void)NetworkNotifyCallBack
{
    DebugLog(@"Tips : Got Network Notification");
    [self performSelector:@selector(tipsListAsynchronousCall) withObject:nil afterDelay:2.0];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
#ifdef TESTING
    [TestFlight passCheckpoint:@"Tips Tab Pressed"];
#endif
    if([NEONAppDelegate networkavailable] && !oneTimeAnimation) {
        [self tipsListAsynchronousCall];
    }  else  {
        [self parseFromFile];
    }
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    DebugLog(@"nissan viewDidAppear");
    DebugLog(@"viewDidAppear");
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    DebugLog(@"Tips Tab nissan viewWillDisappear");
 //   MKNumberBadgeView *tempbadge = (MKNumberBadgeView *)[self.tabBarController.tabBar viewWithTag:20];
 //   tempbadge.value = 0;
//    [NEONAppDelegate setBadgeValue1:0];
//    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}


#pragma mark -
#pragma mark Table View Data Source Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    CGRect screenBounds = [[UIScreen mainScreen] bounds];
//    if (screenBounds.size.height == 568) {
//        // code for 4-inch screen
//        return 60;//504 / contactList.count;
//    }
    return 50;// 416 / contactList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tipsdata count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *SimpleTableIdentifier =  [NSString stringWithFormat:@"Tips%d%d",indexPath.section,indexPath.row];
    UIImageView *cellImageView;
    UILabel *textLabel;
    
    NSString *textString = [[tipsdata allKeys] objectAtIndex:indexPath.row];

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             SimpleTableIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SimpleTableIdentifier] autorelease];
        cell.tag = indexPath.row;
        cell.hidden = TRUE;
        
        DebugLog(@"imageURL  %@",[[tipsdata objectForKey:textString] objectForKey:@"imageURL"]);
        
        cellImageView = [[UIImageView alloc] initWithFrame:CGRectMake(cell.frame.origin.x+15 , cell.frame.origin.y +10, 30, 30)];
        cellImageView.contentMode = UIViewContentModeScaleToFill;
        cellImageView.backgroundColor =  [UIColor clearColor];
        cellImageView.tag = 111;
        [cell.contentView addSubview:cellImageView];
        [cellImageView release];
        
        textLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellImageView.frame)+15, cell.frame.origin.y+10, 250, 30)];
        textLabel.tag = 121;
        textLabel.font = HELVETICA_FONT(18.0);
        textLabel.minimumFontSize = 14.0;
        textLabel.adjustsFontSizeToFitWidth = YES;
        textLabel.textColor = [UIColor darkGrayColor];
        textLabel .lineBreakMode =  UILineBreakModeTailTruncation;
        textLabel.textAlignment = UITextAlignmentLeft;
        textLabel.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:textLabel];
        [textLabel release];
        
        cell.backgroundColor = [UIColor whiteColor];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = TABLE_SELECTION_COLOR;
        cell.selectedBackgroundView = bgColorView;
        [bgColorView release];
    }
//[cell.imageView setImageWithURL:[NSURL URLWithString:[[tipsdata objectForKey:textString] objectForKey:@"imageURL"]]
//                   placeholderImage:[UIImage imageNamed:@"side_tips.png"]
//                            success:^(UIImage *image) {
//                                [cell.imageView setContentMode:UIViewContentModeScaleToFill];
//                            }
//                            failure:^(NSError *error) {
//                                //DebugLog(@"write error %@", error);
//                            }];
//    cell.textLabel.text = textString;
//    cell.textLabel.textColor = [UIColor darkGrayColor];
//    cell.textLabel.font = HELVETICA_FONT(22.0);
    
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    
    DebugLog(@"text%@",textString);
    cellImageView = (UIImageView *)[cell viewWithTag:111];
    [cellImageView setImageWithURL:[NSURL URLWithString:[[tipsdata objectForKey:textString] objectForKey:@"imageURL"]]
                   placeholderImage:[UIImage imageNamed:@"side_tips.png"]
                            success:^(UIImage *image) {
                                [cell.imageView setContentMode:UIViewContentModeScaleToFill];
                            }
                            failure:^(NSError *error) {
                                //DebugLog(@"write error %@", error);
                            }];
    
    textLabel = (UILabel *)[cell viewWithTag:121];
    textLabel.text = textString;

    return cell;
}

#pragma mark -
#pragma mark Table Delegate Methods
//
//- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSUInteger row = [indexPath row];
//    return row;
//}

-(NSIndexPath *)tableView:(UITableView *)tableView  willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger row = [indexPath row];
    
    if (row == -1)
        return nil;
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  
    NSString *key = [[tipsdata allKeys] objectAtIndex:indexPath.row];
    DebugLog(@"selected catg %@ ",key);
    
    //NSMutableArray *tempArray = [tipsdata objectForKey:[[tipsdata allKeys] objectAtIndex:indexPath.row]];
    
    NSMutableArray *tempArray = [[tipsdata objectForKey:key] objectForKey:@"array"];
    DebugLog(@"selected array %@ ",tempArray);

    CarUtilTipsSectionViewController *maintenanceController = [[CarUtilTipsSectionViewController alloc] initWithNibName:@"CarUtilTipsSectionViewController" bundle:nil] ;
    maintenanceController.title = key;
    NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"tipsid" ascending:YES selector:@selector(compare:)];
    [tempArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];
    //to push the UIView.
    maintenanceController.tipsObjsArray = tempArray;
    maintenanceController.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:maintenanceController animated:YES];
    [maintenanceController release];
    [alphaDesc release]; alphaDesc = nil;
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

-(void)tipsListAsynchronousCall
{
    [self showProgressHUDWithMessage:@"Loading"];
	/****************Asynchronous Request**********************/
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:NEON_TIPS_LINK] cachePolicy:YES timeoutInterval:10.0];
    
	// Note: An NSOperation creates an autorelease pool, but doesn't schedule a run loop
	// Create the connection and schedule it on a run loop under our namespaced run mode
	NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    [theConnection autorelease];
	
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	[self connectionDidFinishLoading:nil];
    //[self parseFromFile];
    DebugLog(@"Error: %@", [error localizedDescription]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSString *xmlDataFromChannelSchemes;
    
	if(responseAsyncData)
	{
		NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
		//DebugLog(@"\n result:%@\n\n", result);
        [NEONAppDelegate writeToTextFile:result name:@"tips"];
		xmlDataFromChannelSchemes = [[NSString alloc] initWithString:result];
		NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
		xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
		[xmlParser setDelegate:self];
		[xmlParser parse];
        [xmlParser release];
		[xmlDataFromChannelSchemes release];
		[result release];
		[responseAsyncData release];
		responseAsyncData = nil;
	}
    else {
        [self parseFromFile];
    }
}

-(void) parseFromFile
{
    NSString *xmlDataFromChannelSchemes;
    NSString *data;
    data = [NEONAppDelegate getTextFromFile:@"tips"];
    DebugLog(@"\n data:%@\n\n", data);
    if(data != nil && ![data isEqualToString:@""])
    {
        xmlDataFromChannelSchemes = [[NSString alloc] initWithString:data];
        NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
        [xmlParser setDelegate:self];
        [xmlParser parse];
        [xmlParser release];
        [xmlDataFromChannelSchemes release];
    }
    else {
        [self hideProgressHUD:YES];
        UIAlertView *errorView = [[[UIAlertView alloc]
                                  initWithTitle: ALERT_TITLE
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil] autorelease];
        [errorView show];
    }

}

#pragma mark xmlParser methods
/* Called when the parser runs into an open tag (<tag>) */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName 	attributes:(NSDictionary *)attributeDict
{
	if([elementName isEqualToString:@"tipsgallery"])
	{
        tipsdata = [[NSMutableDictionary alloc] init];
	} else if([elementName isEqualToString:@"section"]) {

        sectionName = [[NSString alloc] initWithString:[attributeDict objectForKey:@"name"]];
        sectionImage = [[NSString alloc] initWithString:[attributeDict objectForKey:@"ilink"]];
        tipsArray = [[NSMutableArray alloc] init];
    } else if([elementName isEqualToString:@"photo"]) {
        tipsObj = [[CarUtilTips alloc] init];
        tipsObj.tipsid = [[attributeDict objectForKey:@"id"] intValue];
        tipsObj.ilink = [attributeDict objectForKey:@"ilink"];
        tipsObj.vlink = [attributeDict objectForKey:@"vlink"];
        //DebugLog(@"%@ %@ %@",[attributeDict objectForKey:@"id"],[attributeDict objectForKey:@"ilink"],[attributeDict objectForKey:@"vlink"]);
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    DebugLog(@"Processing Value: %@", string);
    //[ElementValue appendString:string];
}

/* Called when the parser runs into a close tag (</tag>). */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"tipsgallery"]) {
        DebugLog(@"tipsdata count %d",[tipsdata count]);
        //DebugLog(@"%@", tipsdata);
//        self.tipstbl.separatorColor = DEFAULT_COLOR;
//        self.tipstbl.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        [self hideProgressHUD:YES];
        if (!oneTimeAnimation) {
            [self.tipstbl reloadData];
            [self reloadData:TRUE];
            oneTimeAnimation = TRUE;
        }
    } else if([elementName isEqualToString:@"section"]) {
        
        NSMutableDictionary *tempData = [[NSMutableDictionary alloc] init];
        [tempData setObject:tipsArray forKey:@"array"];
        [tempData setObject:sectionImage forKey:@"imageURL"];
        [tipsdata setObject:tempData forKey:sectionName];
        
        [tipsArray release];
        tipsArray = nil;
        [sectionName release];
        sectionName = nil;
        [sectionImage release];
        sectionImage = nil;
        [tempData release];
        tempData = nil;
    } else if([elementName isEqualToString:@"photo"]) {
        [tipsArray addObject:tipsObj];
        [tipsObj release];
        tipsObj = nil;
    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    DebugLog(@"Error: %@", [parseError localizedDescription]);
}

- (void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError
{
    DebugLog(@"Error: %@", [validationError localizedDescription]);
}

- (void)showProgressHUDWithMessage:(NSString *)message {
    self.progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.progressHUD.labelText = message;
    self.progressHUD.labelFont = HELVETICA_FONT(15);
    self.progressHUD.mode = MBProgressHUDModeIndeterminate;
    [self.progressHUD show:YES];
}
-(void)hideProgressHUD:(BOOL)animated
{
    [self.progressHUD hide:animated];
}
- (void)reloadData:(BOOL)animated
{
    DebugLog(@"reloadData With Animation");
    NSArray *indexPaths = [self.tipstbl indexPathsForVisibleRows];
    DebugLog(@"indexArray %@",indexPaths);
    [self reloadCellWithAnimation:0 indexPathsArray:indexPaths];
}

-(void)reloadCellWithAnimation:(int)index indexPathsArray:(NSArray *)indexArray
{
    
    if (index == indexArray.count) {
        return;
    }
    UITableViewCell *cell = (UITableViewCell *)[self.tipstbl cellForRowAtIndexPath:[indexArray objectAtIndex:index]];
    DebugLog(@"reloadCellWithAnimation cell.tag %d: cell %@ ===> cell.hidden %d",cell.tag,[indexArray objectAtIndex:index],cell.hidden);
    
    if (cell.tag < tipsdata.count)
    {
        cell.hidden = TRUE;
        [cell setFrame:CGRectMake(320, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)];
        
        // [UIView transitionWithView:cell duration:0.2 options:UIViewAnimationOptionTransitionNone |UIViewAnimationOptionAllowUserInteraction
        [UIView animateWithDuration:0.2 animations:^(void) {
            cell.tag = cell.tag + tipsdata.count;
            cell.hidden = FALSE;
            [cell setFrame:CGRectMake(-20, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)];
        }
                         completion:^(BOOL finished){
                             [UIView animateWithDuration:0.15 animations:^{
                                 [cell setFrame:CGRectMake(0, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)];
                             }];
                             [self reloadCellWithAnimation:index+1 indexPathsArray:indexArray];
                         }];
    }
    else
    {
        [self reloadCellWithAnimation:index+1 indexPathsArray:indexArray];
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    DebugLog(@"scrollViewDidEndDecelerating");
    [self reloadData:YES];
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    DebugLog(@"scrollViewDidEndDragging");
    [self reloadData:YES];
}
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
        {
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
//            UIBarButtonItem *sideMenuBarButton = [[UIBarButtonItem alloc]
//                                                 initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
//                                                 target:self.navigationController.sideMenu
//                                                 action:@selector(toggleRightSideMenu)];
            
            UIBarButtonItem *sideMenuBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                            target:self.navigationController.sideMenu
                                            action:@selector(toggleRightSideMenu)];
            self.navigationItem.rightBarButtonItem = sideMenuBarButton;
            [sideMenuBarButton release];
        }
            break;
        case MFSideMenuStateLeftMenuOpen:
        {
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
        }
            break;
        case MFSideMenuStateRightMenuOpen:
        {
//            UIBarButtonItem *sideMenuBarButton = [[UIBarButtonItem alloc]
//                                                  initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
//                                                  target:self.navigationController.sideMenu
//                                                  action:@selector(toggleRightSideMenu)];
            UIBarButtonItem *sideMenuBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                                                                 target:self.navigationController.sideMenu
                                                                                 action:@selector(toggleRightSideMenu)];
            self.navigationItem.rightBarButtonItem = sideMenuBarButton;
            [sideMenuBarButton release];
        }
            break;
    }
}
- (UIBarButtonItem *)leftMenuBarButtonItem {
//    return [[UIBarButtonItem alloc]
//            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
//            target:self.navigationController.sideMenu
//            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
//    return [[UIBarButtonItem alloc]
//            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
//            target:self.navigationController.sideMenu
//            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewDidUnload
{
    self.listData = nil;
    [self setTipstbl:nil];
    [self setLoadingImageView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
- (void)dealloc {
    [listData release];
    [tipstbl release];
    [tipsdata release];
    [loadingImageView release];
    [super dealloc];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
