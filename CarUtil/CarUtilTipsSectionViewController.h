//
//  CarUtilTipsSectionViewController.h
//  CarUtil
//
//  Created by Rishi on 14/02/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface CarUtilTipsSectionViewController : UIViewController <UIScrollViewDelegate,AVAudioPlayerDelegate> {
    UIScrollView *scrollView;
    UIPageControl *pageControl;
    AVAudioPlayer *audioPlayer;
    NSMutableArray *viewControllers;
    BOOL pageControlUsed;
    int tabbartype;

}
@property (nonatomic, retain) NSMutableArray *tipsObjsArray;

@property (nonatomic, retain) NSMutableArray *viewControllers;
@property (nonatomic, retain) AVAudioPlayer *audioPlayer;
@property (retain, nonatomic) IBOutlet UIPageControl *pageControl;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;

- (void)changePage:(id)sender;
- (void)settabbartype:(int)ltype ;
- (void)loadScrollViewWithPage:(int)page ;
- (void)unloadScrollViewWithPage:(int)page ;

@end
