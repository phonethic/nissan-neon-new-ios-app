//
//  CarUtilTipsSectionViewController.m
//  CarUtil
//
//  Created by Rishi on 14/02/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "CarUtilTipsSectionViewController.h"


#import "ImageViewController.h"
#import "CarUtilTips.h"

NSUInteger kNumberOfPages;

@interface CarUtilTipsSectionViewController ()
@property (assign) NSUInteger page;
@property (assign) BOOL rotating;
@end

@implementation CarUtilTipsSectionViewController
@synthesize scrollView, pageControl, viewControllers;
@synthesize page = _page;
@synthesize rotating = _rotating;
@synthesize audioPlayer;
@synthesize tipsObjsArray = _tipsObjsArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)settabbartype:(int)ltype
{
    tabbartype = ltype;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    UIViewController *viewController = [viewControllers objectAtIndex:pageControl.currentPage];
	[viewController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	[viewController willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
	self.scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * kNumberOfPages, scrollView.frame.size.height);
	NSUInteger page = 0;
	for (viewController in viewControllers) {
		CGRect frame = self.scrollView.frame;
		frame.origin.x = frame.size.width * page;
		frame.origin.y = 0;
		viewController.view.frame = frame;
		page++;
	}
	
	CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * _page;
    frame.origin.y = 0;
	[self.scrollView scrollRectToVisible:frame animated:NO];
    
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    
	for (NSUInteger i =0; i < [viewControllers count]; i++) {
		[self loadScrollViewWithPage:i];
	}
    
	self.pageControl.currentPage = 0;
	_page = 0;
	[self.pageControl setNumberOfPages:kNumberOfPages];
    
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewWillAppear:animated];
	}
    
	self.scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * kNumberOfPages, scrollView.frame.size.height);
}


- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewDidAppear:animated];
	}
}

- (void)changePage:(id)sender
{
    DebugLog(@"Page changed");
    int page = pageControl.currentPage;
	
	
	// update the scroll view to the appropriate page
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    
	UIViewController *oldViewController = [viewControllers objectAtIndex:_page];
	UIViewController *newViewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	[oldViewController viewWillDisappear:YES];
	[newViewController viewWillAppear:YES];
    
    [UIView animateWithDuration:1
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{ [scrollView scrollRectToVisible:frame animated:NO]; }
                     completion:NULL];
    
    //[scrollView scrollRectToVisible:frame animated:YES];
    
	// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;
}

- (BOOL)automaticallyForwardAppearanceAndRotationMethodsToChildViewControllers {
	return NO;
}

- (void)viewDidLoad
{
#ifdef GOOGLE_ANALYTICS
    if (![[GANTracker sharedTracker] trackEvent:@"Nissan CarUitl Application iOS"
                                         action:@"Maintenanace Tips Pressed"
                                          label:@""
                                          value:-1
                                      withError:NULL]) {
        DebugLog(@"error in trackEvent");
    }
#endif
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    NSDictionary *tipsParams = [NSDictionary dictionaryWithObjectsAndKeys:self.title, @"Section", nil];
    [Flurry logEvent:@"Tips_Section_Event" withParameters:tipsParams];
    
    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Iphone_Unlock_Screen" ofType:@"mp3"]] error:&error];
    
    if (error)
    {
        DebugLog(@"Error in audioPlayer: %@",[error localizedDescription]);
    } else {
        audioPlayer.delegate = self;
        [audioPlayer prepareToPlay];
    }
    
    kNumberOfPages = [_tipsObjsArray count];
    DebugLog(@"kNumberOfPages %d",kNumberOfPages);
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < kNumberOfPages; i++)
    {
		[controllers addObject:[NSNull null]];
    }
    self.viewControllers = controllers;
    [controllers release];
    
    // a page is the width of the scroll view
    scrollView.clipsToBounds = YES;
	scrollView.scrollEnabled = YES;
    scrollView.pagingEnabled = YES;
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width * kNumberOfPages, scrollView.frame.size.height + 280);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.scrollsToTop = YES;
    scrollView.delegate = self;
    scrollView.bounces = NO;
    scrollView.directionalLockEnabled = YES;
    //scrollView.maximumZoomScale = 4.0;
	//scrollView.minimumZoomScale = 0.75;
    //scrollView.bouncesZoom = NO;
    //pageControl.frame = CGRectMake(0, 375, 320, 36);
    //pageControl.backgroundColor = [UIColor blackColor];
    //pageControl.numberOfPages = kNumberOfPages;
    //pageControl.currentPage = 0;
    pageControl.autoresizingMask = UIViewAutoresizingFlexibleWidth ;
    //[self.view bringSubviewToFront:pageControl];
    
    // pages are created on demand
    // load the visible page
    // load the page on either side to avoid flashes when the user starts scrolling
    //
    [self loadScrollViewWithPage:0];
    [self loadScrollViewWithPage:1];
    
    UISwipeGestureRecognizer *swipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                                                                          action:@selector(swipeDetected:)];
    swipeRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRecognizer];
    [swipeRecognizer release];

}
- (void)dealloc
{
    //[Flurry endTimedEvent:@"Tips_Section_Event" withParameters:nil];
    [audioPlayer release];
    [pageControl release];
    [scrollView release];
    [viewControllers release];
    [_tipsObjsArray release];
    [super dealloc];
}

-(void)audioPlayerDidFinishPlaying:
(AVAudioPlayer *)player successfully:(BOOL)flag
{
}

-(void)audioPlayerDecodeErrorDidOccur:
(AVAudioPlayer *)player error:(NSError *)error
{
}

-(void)audioPlayerBeginInterruption:(AVAudioPlayer *)player
{
}

-(void)audioPlayerEndInterruption:(AVAudioPlayer *)player
{
}

- (void)viewWillDisappear:(BOOL)animated {
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewWillDisappear:animated];
	}
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
	UIViewController *viewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewDidDisappear:animated];
	}
	[super viewDidDisappear:animated];
}

- (void)unloadScrollViewWithPage:(int)page {
    if (page < 0) return;
    if (page >= kNumberOfPages) return;
    
    UIViewController *controller = [viewControllers objectAtIndex:page];
    
    if ((NSNull *)controller != [NSNull null]) {
        if (nil != controller.view.superview) {
            [controller.view removeFromSuperview];
            controller.view=nil;
        }
        
        [viewControllers replaceObjectAtIndex:page withObject:[NSNull null]];
    }
}

- (IBAction)swipeDetected:(UISwipeGestureRecognizer *)recognizer {
    
	//CGPoint location = [recognizer locationInView:self.view];
	//[self showImageWithText:@"swipe" atPoint:location];
	
    if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft) {
        DebugLog(@"UISwipeGestureRecognizerDirectionLeft");
    } else if (recognizer.direction == UISwipeGestureRecognizerDirectionRight) {
        DebugLog(@"UISwipeGestureRecognizerDirectionRight");
    } else if (recognizer.direction == UISwipeGestureRecognizerDirectionUp) {
        DebugLog(@"UISwipeGestureRecognizerDirectionUp");
    } else if (recognizer.direction == UISwipeGestureRecognizerDirectionDown) {
        DebugLog(@"UISwipeGestureRecognizerDirectionDown");
    }
    
}

- (void)viewDidUnload
{
    scrollView = nil;
    pageControl = nil;
    viewControllers = nil;
    [self setPageControl:nil];
    [self setScrollView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)loadScrollViewWithPage:(int)page {
    if (page < 0) return;
    if (page >= kNumberOfPages) return;
	
    // replace the placeholder if necessary
    UIViewController *controller = [viewControllers objectAtIndex:page];
    if ((NSNull *)controller == [NSNull null])
    {
        CarUtilTips *tipsObj = (CarUtilTips *)[_tipsObjsArray objectAtIndex:page];
        controller = (UIViewController *)[[ImageViewController alloc] initWithImageUrl:tipsObj.ilink videourl:tipsObj.vlink];
        [(ImageViewController*)controller setScrollType:2];
        
        [viewControllers replaceObjectAtIndex:page withObject:controller];
        [controller release];
    }
    
	// add the controller's view to the scroll view
    if (controller.view.superview == nil) {
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        controller.view.frame = frame;
        [self.scrollView addSubview:controller.view];
    }
    
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
	UIViewController *oldViewController = [viewControllers objectAtIndex:_page];
	UIViewController *newViewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
	[oldViewController viewDidDisappear:YES];
	[newViewController viewDidAppear:YES];
    
	_page = self.pageControl.currentPage;
}


- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // We don't want a "feedback loop" between the UIPageControl and the scroll delegate in
    // which a scroll event generated from the user hitting the page control triggers updates from
    // the delegate method. We use a boolean to disable the delegate logic when the page control is used.
    if (pageControlUsed)
    {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
	
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if(pageControl.currentPage != page) {
        [audioPlayer prepareToPlay];
        [audioPlayer play];
    }
    if (self.pageControl.currentPage != page) {
		UIViewController *oldViewController = [viewControllers objectAtIndex:self.pageControl.currentPage];
		UIViewController *newViewController = [viewControllers objectAtIndex:page];
		[oldViewController viewWillDisappear:YES];
		[newViewController viewWillAppear:YES];
		self.pageControl.currentPage = page;
		[oldViewController viewDidDisappear:YES];
		[newViewController viewDidAppear:YES];
		_page = page;
	}
    
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
