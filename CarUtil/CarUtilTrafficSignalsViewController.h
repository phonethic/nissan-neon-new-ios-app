//
//  CarUtilTrafficSignalsViewController.h
//  CarUtil
//
//  Created by Kirti Nikam on 26/02/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#define TOTALSIGNS 53
#define STARTOFSIGN 1

@interface CarUtilTrafficSignalsViewController : UIViewController
@property (retain, nonatomic) IBOutlet UIImageView *trafficSignsTextImageView;
@property (retain, nonatomic) IBOutlet UIScrollView *trafficSignsScrollView;
@property (retain, nonatomic) IBOutlet UILabel *textlbl;
@end
