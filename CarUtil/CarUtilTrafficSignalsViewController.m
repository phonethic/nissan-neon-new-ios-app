//
//  CarUtilTrafficSignalsViewController.m
//  CarUtil
//
//  Created by Kirti Nikam on 26/02/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "CarUtilTrafficSignalsViewController.h"
#import "MFSideMenu.h"

@interface CarUtilTrafficSignalsViewController ()

@end

@implementation CarUtilTrafficSignalsViewController
@synthesize trafficSignsScrollView,trafficSignsTextImageView,textlbl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.navigationItem.title = NSLocalizedString(@"TRAFFIC SIGNS", @"TRAFFIC SIGNS");
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupMenuBarButtonItems];
    self.navigationController.navigationBar.tintColor = DEFAULT_COLOR;
    textlbl.textColor = [UIColor darkGrayColor];
    
    [Flurry logEvent:@"TrafficSigns_Event"];
    int X=25,Y=50;
    int Hspace  =   30;
    int Vspace  =   30;
    int width   =   70;
    int height  =   70;
    UIButton *button;
    for (int buttonIndex=STARTOFSIGN; buttonIndex<TOTALSIGNS; buttonIndex++)
    {
        DebugLog(@"%d",buttonIndex);
        CGRect frame = CGRectMake(X, Y, width, height);
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = frame;
        button.backgroundColor=[UIColor clearColor];
     //  button.titleLabel.font=[UIFont boldSystemFontOfSize:10];
      //[button setTitle:[NSString stringWithFormat:@"%d",buttonIndex] forState:UIControlStateNormal];
        button.tag = buttonIndex;
        [button addTarget:self action:@selector(trafficSignBtnPressed:)forControlEvents:UIControlEventTouchUpInside];
        [button setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%dB.png",buttonIndex]] forState:UIControlStateNormal];
        [button setTintColor:DEFAULT_COLOR];
        [trafficSignsScrollView addSubview:button];
        //[button release];
        button=nil;
        int NextX = X+width+Hspace;
        if (NextX < trafficSignsScrollView.frame.size.width-60)
        {
            X=NextX;
        }
        else
        {
            X=25;
            Y=Y+height+Vspace;
        }
    }
    trafficSignsScrollView.contentSize = CGSizeMake(self.trafficSignsScrollView.frame.size.width, Y+height+2*Vspace);
    
    textlbl.font = HELVETICA_FONT(20.0);
    textlbl.textColor = [UIColor darkGrayColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)trafficSignBtnPressed:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    switch (btn.tag) {
        case 1:
            textlbl.text = @"Straight no entry";
            break;
        case 2:
            textlbl.text = @"No parking";
            break;
        case 3:
            textlbl.text = @"No stopping or Standing";
            break;
        case 4:
            textlbl.text = @"Horns prohibited";
            break;
        case 5:
            textlbl.text = @"School Ahead";
            break;
        case 6:
            textlbl.text = @"Narrow Road ahead";
            break;
        case 7:
            textlbl.text = @"Narrow Bridge";
            break;
        case 8:
            textlbl.text = @"Right Hand Curve";
            break;
        case 9:
            textlbl.text = @"Left Hand Curve";
            break;
        case 10:
            textlbl.text = @"Compulsory Sound Horn";
            break;
        case 11:
            textlbl.text = @"One Way Sign";
            break;
        case 12:
            textlbl.text = @"One Way Sign";
            break;
        case 13:
            textlbl.text = @"Vehicles prohibited in both directions";
            break;
        case 14:
            textlbl.text = @"All Motor vehicles prohibited";
            break;
        case 15:
            textlbl.text = @"Tonga prohibited";
            break;
        case 16:
            textlbl.text = @"Truck prohibited";
            break;
        case 17:
            textlbl.text = @"Bullock Cart and Hand Cart prohibited";
            break;
        case 18:
            textlbl.text = @"Hand Cart prohibited";
            break;
        case 19:
            textlbl.text = @"Bullock Cart prohibited";
            break;
        case 20:
            textlbl.text = @"Cycle prohibited";
            break;
        case 21:
            textlbl.text = @"Pedestrian prohibited";
            break;
        case 22:
            textlbl.text = @"Right turn prohibited";
            break;
        case 23:
            textlbl.text = @"Left turn prohibited";
            break;
        case 24:
            textlbl.text = @"U-Turn prohibited";
            break;
        case 25:
            textlbl.text = @"Speed Limit";
            break;
        case 26:
            textlbl.text = @"Width Limit";
            break;
        case 27:
            textlbl.text = @"Height Limit";
            break;
        case 28:
            textlbl.text = @"Length Limit";
            break;
        case 29:
            textlbl.text = @"Load Limit";
            break;
        case 30:
            textlbl.text = @"Axe Load Limit";
            break;
        case 31:
            textlbl.text = @"Bus Stop";
            break;
        case 32:
            textlbl.text = @"Restriciton Ends sign";
            break;
        case 33:
            textlbl.text = @"Compulsory keep Left";
            break;
        case 34:
            textlbl.text = @"Compulsory turn Left";
            break;
        case 35:
            textlbl.text = @"Compulsory turn Right ahead";
            break;
        case 36:
            textlbl.text = @"Compulsory ahead or turn Left";
            break;
        case 37:
            textlbl.text = @"Compulsory ahead or turn Right";
            break;
        case 38:
            textlbl.text = @"Compulsory ahead only";
            break;
        case 39:
            textlbl.text = @"Compulsory cycle track";
            break;
        case 40:
            textlbl.text = @"Move On";
            break;
        case 41:
            textlbl.text = @"Stop";
            break;
        case 42:
            textlbl.text = @"Give Way";
            break;
        case 43:
            textlbl.text = @"Bump";
            break;
        case 44:
            textlbl.text = @"Ferry";
            break;
        case 45:
            textlbl.text = @"Right Hair Pin bend";
            break;
        case 46:
            textlbl.text = @"Left Hair Pin bend";
            break;
        case 47:
            textlbl.text = @"Left reverse bend";
            break;
        case 48:
            textlbl.text = @"Right reverse bend";
            break;
        case 49:
            textlbl.text = @"Steep Ascent";
            break;
        case 50:
            textlbl.text = @"Steep Descent";
            break;
        case 51:
            textlbl.text = @"Road Widens ahead";
            break;
        case 52:
            textlbl.text = @"Slippery Road";
            break;
        case 53:
            textlbl.text = @"Cycle crossing";
            break;
        default:
            break;
    }
}

- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
        {
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            UIBarButtonItem *sideMenuBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                                                                 target:self.navigationController.sideMenu
                                                                                 action:@selector(toggleRightSideMenu)];
            self.navigationItem.rightBarButtonItem = sideMenuBarButton;
            [sideMenuBarButton release];
    }
            break;
        case MFSideMenuStateLeftMenuOpen:
        {
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
        }
            break;
        case MFSideMenuStateRightMenuOpen:
        {
            UIBarButtonItem *sideMenuBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                                                                 target:self.navigationController.sideMenu
                                                                                 action:@selector(toggleRightSideMenu)];
            self.navigationItem.rightBarButtonItem = sideMenuBarButton;
            [sideMenuBarButton release];
        }
            break;
    }
}
- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc {
    //[Flurry endTimedEvent:@"TrafficSigns_Event" withParameters:nil];
    [trafficSignsTextImageView release];
    [trafficSignsScrollView release];
    [textlbl release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setTrafficSignsTextImageView:nil];
    [self setTrafficSignsScrollView:nil];
    [self setTextlbl:nil];
    [super viewDidUnload];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
