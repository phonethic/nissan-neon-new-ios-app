//
//  CarUtilWebViewController.h
//  CarUtil
//
//  Created by Kirti Nikam on 18/02/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#define WEB_LINK @"https://www.facebook.com/nissanindia"

@interface CarUtilWebViewController : UIViewController
@property (retain, nonatomic) IBOutlet UIWebView *webView;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *barbackBtn;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *barfwdBtn;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *barrefreshBtn;
@property (retain, nonatomic) IBOutlet UIToolbar *webviewtoolBar;

- (IBAction)webviewbackBtnPressed:(id)sender;
- (IBAction)webviewfwdBtnPressed:(id)sender;
- (IBAction)webviewrefreshBtnPressed:(id)sender;
@end
