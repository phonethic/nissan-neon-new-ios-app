//
//  CarUtilWebViewController.m
//  CarUtil
//
//  Created by Kirti Nikam on 18/02/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "CarUtilWebViewController.h"
#import "MBProgressHUD.h"

@interface CarUtilWebViewController ()
@property (nonatomic, retain) MBProgressHUD *progressHUD;
@end

@implementation CarUtilWebViewController
@synthesize webView;
@synthesize barbackBtn;
@synthesize barfwdBtn;
@synthesize barrefreshBtn;
@synthesize webviewtoolBar;
@synthesize progressHUD;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Flurry logEvent:@"Nissan_Facebook_Page_Event"];
    barbackBtn.enabled      = NO;
    barfwdBtn.enabled       = NO;
    barrefreshBtn.enabled   = NO;
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:WEB_LINK]];
    [webView loadRequest:requestObj];
}
- (void)showProgressHUDWithMessage:(NSString *)message {
    self.progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.progressHUD.labelText = message;
    self.progressHUD.labelFont = HELVETICA_FONT(15);
    self.progressHUD.mode = MBProgressHUDModeIndeterminate;
    [self.view bringSubviewToFront:self.progressHUD];
    [self.progressHUD show:YES];
}
-(void)hideProgressHUD:(BOOL)animated
{
    [self.progressHUD hide:animated];
}
- (void)viewDidUnload
{
    [self setWebView:nil];
    [self setBarbackBtn:nil];
    [self setBarfwdBtn:nil];
    [self setBarrefreshBtn:nil];
    [self setWebviewtoolBar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (void)dealloc {
    [webView release];
    [barbackBtn release];
    [barfwdBtn release];
    [barrefreshBtn release];
    [webviewtoolBar release];
    [super dealloc];
}

#pragma mark webView Delegate methods

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self showProgressHUDWithMessage:@"Loading"];
    barrefreshBtn.enabled   = NO;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if (self.webView.canGoBack)
    {
        barbackBtn.enabled = YES;
    }
    else
    {
        barbackBtn.enabled = NO;
    }
    if (self.webView.canGoForward)
    {
        barfwdBtn.enabled = YES;
    }
    else
    {
        barfwdBtn.enabled = NO;
    }
    barrefreshBtn.enabled = YES;
    [self hideProgressHUD:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    barrefreshBtn.enabled = YES;
    [self hideProgressHUD:YES];

}
- (IBAction)webviewbackBtnPressed:(id)sender {
    [webView goBack];
}

- (IBAction)webviewfwdBtnPressed:(id)sender {
    [webView goForward];
}

- (IBAction)webviewrefreshBtnPressed:(id)sender {
    [webView reload];
}
@end
