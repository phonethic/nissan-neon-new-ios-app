//
//  ContactViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 09/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

#define TESTDRIVE_TEL_TEXT @"Call to book a test drive"
#define CUSTSUPPORT_TEL_TEXT @"Call Nissan customer support"
#define CUSTSUPPORT_EMAIL_TEXT @"Email customer support"
#define BREAKDOWN_TEL_TEXT @"Call breakdown helpline"
#define BREAKDOWN_EMAIL_TEXT @"Email breakdown location"
#define FACEBOOK_TEXT @"Nissan India on facebook"
#define DEALER_TEXT @"Dealer locator"


#define TESTDRIVE_TEL @"tel://18002094080"
#define CUSTSUPPORT_TEL @"tel://18002094080"
#define CUSTSUPPORT_EMAILID @"customercare.nissan@hai.net.in"
#define BREAKDOWN_TEL @"tel://18002094080"

@interface ContactViewController : UIViewController <MFMailComposeViewControllerDelegate>{
    BOOL oneTimeAnimation;
}
@property (retain, nonatomic) IBOutlet UIImageView *bgImageView;
@property (strong, nonatomic) NSArray *contactList;
@property (retain, nonatomic) IBOutlet UITableView *contactTableView;
@end
