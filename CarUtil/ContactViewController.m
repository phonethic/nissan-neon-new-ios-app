//
//  ContactViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 09/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "ContactViewController.h"
#import "BreakdownLocationViewController.h"
#import "CarUtilAppDelegate.h"
#import "CarUtilWebViewController.h"
#import "CarUtilStoreListViewController.h"

#import "MFSideMenu.h"
#import <QuartzCore/QuartzCore.h>

@interface ContactViewController ()

@end

@implementation ContactViewController
@synthesize contactTableView;
@synthesize bgImageView;
@synthesize contactList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.navigationItem.title = NSLocalizedString(@"CONTACT US", @"CONTACT US");
        //self.tabBarItem.image = [UIImage imageNamed:@"contact"];
    }
    return self;
}
- (void)viewDidLoad
{
#ifdef ENABLE_BACK_BUTTON
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [backButton release];
    
#endif
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupMenuBarButtonItems];
    self.navigationController.navigationBar.tintColor = DEFAULT_COLOR;
    self.contactTableView.backgroundColor = [UIColor whiteColor];
//    self.contactTableView.separatorColor = DEFAULT_COLOR;
//    self.contactTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    
    contactList = [[NSArray alloc] initWithObjects:TESTDRIVE_TEL_TEXT,CUSTSUPPORT_TEL_TEXT,BREAKDOWN_TEL_TEXT,CUSTSUPPORT_EMAIL_TEXT,FACEBOOK_TEXT,BREAKDOWN_EMAIL_TEXT,DEALER_TEXT, nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    #ifdef TESTING
    [TestFlight passCheckpoint:@"Contact Tab Pressed"];
    #endif
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    DebugLog(@"viewDidAppear");
    if (!oneTimeAnimation) {
        [self.contactTableView reloadData];
        [self reloadData:TRUE];
        oneTimeAnimation = TRUE;
    }
}
- (void)viewDidUnload
{
    [self setContactTableView:nil];
    [self setContactList:nil];
    [self setBgImageView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc {
    [contactTableView release];
    [contactList release];
    [bgImageView release];
    [super dealloc];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    CGRect screenBounds = [[UIScreen mainScreen] bounds];
//    if (screenBounds.size.height == 568) {
//        // code for 4-inch screen
//        return 60;//504 / contactList.count;
//    }
    return 50;// 416 / contactList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [contactList count];;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier =  @"contactCell";//[NSString stringWithFormat:@"contact%d%d",indexPath.section,indexPath.row];
    UIImageView *cellImageView;
    UILabel *textLabel;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        DebugLog(@"cell == nil %@",CellIdentifier);
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
//        cell.textLabel.backgroundColor = [UIColor clearColor];
//        cell.textLabel.textColor = [UIColor darkGrayColor];
//        cell.textLabel.font = HELVETICA_FONT(20.0);
//        cell.backgroundColor = [UIColor whiteColor];
//        
//        if ([textString isEqualToString:TESTDRIVE_TEL_TEXT] || [textString isEqualToString:CUSTSUPPORT_TEL_TEXT] || [textString isEqualToString:BREAKDOWN_TEL_TEXT])
//        {
//            cell.imageView.image = [UIImage imageNamed:@"contact_tel.png"];
//        }
//        else if ([textString isEqualToString:CUSTSUPPORT_EMAIL_TEXT])
//        {
//            cell.imageView.image = [UIImage imageNamed:@"contact_mail.png"];
//        }
//        else if ([textString isEqualToString:BREAKDOWN_EMAIL_TEXT]|| [textString isEqualToString:DEALER_TEXT])
//        {
//            cell.imageView.image = [UIImage imageNamed:@"contact_location.png"];
//        }
//        else if ([textString isEqualToString:FACEBOOK_TEXT])
//        {
//            cell.imageView.image = [UIImage imageNamed:@"contact_fb.png"];
//        }
        
        cell.tag = indexPath.row;
        cell.hidden = TRUE;
        
        cellImageView = [[UIImageView alloc] initWithFrame:CGRectMake(cell.frame.origin.x+10 , cell.frame.origin.y +10, 30, 30)];
        cellImageView.contentMode = UIViewContentModeScaleToFill;
        cellImageView.backgroundColor =  [UIColor clearColor];
        cellImageView.tag = 11;
        [cell.contentView addSubview:cellImageView];
        [cellImageView release];
        
        textLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellImageView.frame)+5, cell.frame.origin.y+10, 270, 30)];
        textLabel.tag = 21;
        textLabel.font = HELVETICA_FONT(16.0);
        textLabel.minimumFontSize = 13;
        textLabel.adjustsFontSizeToFitWidth = YES;
        textLabel.textColor = [UIColor darkGrayColor];
        textLabel .lineBreakMode =  UILineBreakModeTailTruncation;
        textLabel.textAlignment = UITextAlignmentLeft;
        textLabel.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:textLabel];
        [textLabel release];
        
        cell.backgroundColor = [UIColor whiteColor];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = TABLE_SELECTION_COLOR;
        cell.selectedBackgroundView = bgColorView;
        [bgColorView release];
    }
    
    //    cell.textLabel.text = [contactList objectAtIndex:indexPath.row];
    cell.selectionStyle=UITableViewCellSelectionStyleBlue;
    
    NSString *text = [contactList objectAtIndex:indexPath.row];
    
    DebugLog(@"text%@",text);
    cellImageView = (UIImageView *)[cell viewWithTag:11];
    if ([text isEqualToString:TESTDRIVE_TEL_TEXT] || [text isEqualToString:CUSTSUPPORT_TEL_TEXT] || [text isEqualToString:BREAKDOWN_TEL_TEXT])
    {
        cellImageView.image = [UIImage imageNamed:@"contact_tel.png"];
    }
    else if ([text isEqualToString:CUSTSUPPORT_EMAIL_TEXT])
    {
        cellImageView.image = [UIImage imageNamed:@"contact_mail.png"];
    }
    else if ([text isEqualToString:BREAKDOWN_EMAIL_TEXT]|| [text isEqualToString:DEALER_TEXT])
    {
        cellImageView.image = [UIImage imageNamed:@"contact_location.png"];
    }
    else if ([text isEqualToString:FACEBOOK_TEXT])
    {
        cellImageView.image = [UIImage imageNamed:@"contact_fb.png"];
    }
    
    textLabel = (UILabel *)[cell viewWithTag:21];
    textLabel.text = text;
    
    return cell;
}

//- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
//    UILabel *textLabel = (UILabel *)[cell viewWithTag:21];
//    textLabel.textColor = [UIColor darkGrayColor];
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DebugLog(@"row=%d",indexPath.row);
//    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
//    UILabel *textLabel = (UILabel *)[cell viewWithTag:21];
//    textLabel.textColor = [UIColor whiteColor];
    
    NSString *selectedText =  [contactList objectAtIndex:indexPath.row];
    if ([selectedText isEqualToString:TESTDRIVE_TEL_TEXT])
    {
        [self bookTestDriveBtnPressed];
    }
    else if ([selectedText isEqualToString:CUSTSUPPORT_TEL_TEXT])
    {
        [self callCustSupportBtnPressed];
    }
    else if ([selectedText isEqualToString:CUSTSUPPORT_EMAIL_TEXT])
    {
        [self emailCustSupportBtnPressed];
    }
    else if ([selectedText isEqualToString:BREAKDOWN_TEL_TEXT])
    {
        [self callBreakdownBtnPressed];
    }
    else if ([selectedText isEqualToString:BREAKDOWN_EMAIL_TEXT])
    {
        [self sendBreakdownLocationbtnPressed];
    }
    else if ([selectedText isEqualToString:FACEBOOK_TEXT])
    {
        if([NEONAppDelegate networkavailable])
        {
            [self openWebSitebtnPressed];
        }
        else
        {
            UIAlertView *errorView = [[[UIAlertView alloc]
                                       initWithTitle: ALERT_TITLE
                                       message:@"Please check your internet connection and try again."
                                       delegate:self
                                       cancelButtonTitle:@"OK"
                                       otherButtonTitles:nil] autorelease];
            [errorView show];
        }
    }
    else if ([selectedText isEqualToString:DEALER_TEXT])
    {
        [self storeLocatebtnPressed];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark internal methods
- (void)bookTestDriveBtnPressed
{
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPhone"])
    {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Do you want to call Test Drive center number ?"
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"NO", @"YES", nil
                              ];
        [alert setTag:1];
        [ alert show ];
        [alert release];
    } else {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Calling functionaltiy is not available in this device. "
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"OK", nil
                              ];
        [ alert show ];
        [alert release];
    }
}

- (void)callCustSupportBtnPressed
{
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPhone"])
    {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Do you want to call our Customer Care ?"
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"NO", @"YES", nil
                              ];
        [alert setTag:2];
        [ alert show ];
        [alert release];
    } else {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Calling functionaltiy is not available in this device. "
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"OK", nil
                              ];
        [ alert show ];
        [alert release];
    }
}

- (void)emailCustSupportBtnPressed
{
    @try{
        MFMailComposeViewController *picker=[[[MFMailComposeViewController alloc]init] autorelease];
        picker.mailComposeDelegate=self;
        picker.navigationBar.tintColor = DEFAULT_COLOR;
        [picker setToRecipients:[NSArray arrayWithObject:CUSTSUPPORT_EMAILID]];
        [picker setSubject:@"Enquiry for NISSAN"];
        [self presentModalViewController:picker animated:YES];
    }
    @catch (NSException *exception)
    {
        //DebugLog(@"Exception %@",exception.reason);
    }
}

- (void)callBreakdownBtnPressed
{
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPhone"])
    {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Do you want to call our Helpline number ?"
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"NO", @"YES", nil
                              ];
        [alert setTag:3];
        [alert show ];
        [alert release];
    } else {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"Calling functionaltiy is not available in this device. "
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"OK", nil
                              ];
        [ alert show ];
        [alert release];
    }
}

- (void)sendBreakdownLocationbtnPressed
{
    BreakdownLocationViewController *breakdownLocViewController = [[BreakdownLocationViewController alloc] initWithNibName:@"BreakdownLocationViewController" bundle:nil] ;
    breakdownLocViewController.title = BREAKDOWN_EMAIL_TEXT;
    breakdownLocViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:breakdownLocViewController animated:YES];
    [breakdownLocViewController release];
}


- (void)openWebSitebtnPressed
{
    CarUtilWebViewController *webSiteViewController = [[CarUtilWebViewController alloc] initWithNibName:@"CarUtilWebViewController" bundle:nil] ;
    webSiteViewController.title = @"Facebook";
    webSiteViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:webSiteViewController animated:YES];
    [webSiteViewController release];
}
- (void)storeLocatebtnPressed
{
    CarUtilStoreListViewController *storeLocListViewController = [[CarUtilStoreListViewController alloc] initWithNibName:@"CarUtilStoreListViewController" bundle:nil] ;
    storeLocListViewController.title = @"Dealers";
    storeLocListViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:storeLocListViewController animated:YES];
    [storeLocListViewController release];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
#ifdef GOOGLE_ANALYTICS
    if (![[GANTracker sharedTracker] trackEvent:@"Nissan CarUitl Application iOS"
                                         action:@"Contact Number Dialed"
                                          label:@""
                                          value:-1
                                      withError:NULL]) {
        DebugLog(@"error in trackEvent");
    }
#endif
    if (buttonIndex == 1)
    {
        NSString *phoneNumber = nil;
        switch (alertView.tag) {
            case 1:
            {
                phoneNumber = TESTDRIVE_TEL;
                [Flurry logEvent:@"TestDrive_Call_Event"];
            }
                break;
            case 2:
            {
                phoneNumber = CUSTSUPPORT_TEL;
                [Flurry logEvent:@"CustomerSupport_Call_Event"];
            }
                break;
            case 3:
            {
                phoneNumber = BREAKDOWN_TEL;
                [Flurry logEvent:@"Breakdown_Call_Event"];
            }
                break;
            default:
                break;
        }
        UIApplication *myApp = [UIApplication sharedApplication];
        [myApp openURL:[NSURL URLWithString:phoneNumber]];
        return;
    }
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
     if (result == MFMailComposeResultSent)
     {
         [Flurry logEvent:@"Customer_Email_Event"];
     }
    [controller dismissModalViewControllerAnimated:YES];
}


- (void)reloadData:(BOOL)animated
{
 //   [self.contactTableView reloadData];
//    [self reloadCellWithAnimation:0];

    DebugLog(@"reloadData With Animation");
    
    NSArray *indexPaths = [self.contactTableView indexPathsForVisibleRows];
    DebugLog(@"indexArray %@",indexPaths);
   [self reloadCellWithAnimation:0 indexPathsArray:indexPaths];
}

//-(void)reloadCellWithAnimation:(int)index
-(void)reloadCellWithAnimation:(int)index indexPathsArray:(NSArray *)indexArray
{

//    if (index == contactList.count) {
//        return;
//    }
//        
//    UITableViewCell *cell = (UITableViewCell *)[contactTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
//    if (cell.hidden) {
//
//        [cell setFrame:CGRectMake(320, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)];
//        
//       // [UIView transitionWithView:cell duration:0.2 options:UIViewAnimationOptionTransitionNone |UIViewAnimationOptionAllowUserInteraction
//         [UIView animateWithDuration:0.2 animations:^(void) {
//                            cell.hidden = FALSE;
//                            [cell setFrame:CGRectMake(-20, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)];
//                        }
//                        completion:^(BOOL finished){
//                            [UIView animateWithDuration:0.15 animations:^{
//                                [cell setFrame:CGRectMake(0, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)];
//                            }];
//                            [self reloadCellWithAnimation:index+1];
//        }];
//    }
    
    if (index == indexArray.count) {
        return;
    }
    UITableViewCell *cell = (UITableViewCell *)[contactTableView cellForRowAtIndexPath:[indexArray objectAtIndex:index]];
     DebugLog(@"reloadCellWithAnimation cell.tag %d: cell %@ ===> cell.hidden %d",cell.tag,[indexArray objectAtIndex:index],cell.hidden);

     if (cell.tag < contactList.count)
     {
         cell.hidden = TRUE;
        [cell setFrame:CGRectMake(320, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)];

        // [UIView transitionWithView:cell duration:0.2 options:UIViewAnimationOptionTransitionNone |UIViewAnimationOptionAllowUserInteraction
        [UIView animateWithDuration:0.2 animations:^(void) {
            cell.tag = cell.tag + contactList.count;
            cell.hidden = FALSE;
            [cell setFrame:CGRectMake(-20, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)];
        }
                         completion:^(BOOL finished){
                             [UIView animateWithDuration:0.15 animations:^{
                                 [cell setFrame:CGRectMake(0, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height)];
                             }];
                             // [self reloadCellWithAnimation:index+1];
                             [self reloadCellWithAnimation:index+1 indexPathsArray:indexArray];
                         }];
     }
     else
     {
         [self reloadCellWithAnimation:index+1 indexPathsArray:indexArray];
     }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
 DebugLog(@"scrollViewDidEndDecelerating");
 [self reloadData:YES];
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
  DebugLog(@"scrollViewDidEndDragging");
  [self reloadData:YES];
}
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
        {
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            UIBarButtonItem *sideMenuBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                                                                 target:self.navigationController.sideMenu
                                                                                 action:@selector(toggleRightSideMenu)];
            self.navigationItem.rightBarButtonItem = sideMenuBarButton;
            [sideMenuBarButton release];
        }
            break;
        case MFSideMenuStateLeftMenuOpen:
        {
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
        }
            break;
        case MFSideMenuStateRightMenuOpen:
        {
            UIBarButtonItem *sideMenuBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                                                                 target:self.navigationController.sideMenu
                                                                                 action:@selector(toggleRightSideMenu)];
            self.navigationItem.rightBarButtonItem = sideMenuBarButton;
            [sideMenuBarButton release];
        }
            break;
    }
}
- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
