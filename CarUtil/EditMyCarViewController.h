//
//  MyCarViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 08/06/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <sqlite3.h>
#define CARBRAND_TAG 1000
#define CARMODEL_TAG 1001
#define DATE_TAG 1002

#define REPLACECHAR @" "
#define WITHCHAR @"-"

@class CarBrand;
@interface EditMyCarViewController : UIViewController <UIActionSheetDelegate,UITextFieldDelegate,UIScrollViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,NSXMLParserDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    sqlite3 *expenseDB;
    //NSString *databasePath;
    NSInteger dbrowID;
    double mfgDate;
    double policyDate;
    double carDate;
    int pickerType;
    int imageUpdated;
    NSString *imgName;
    NSString *selectedRow;
    UIView *indicator;
    NSXMLParser *xmlParser;
    NSMutableData *responseAsyncData;
    int status;
    BOOL elementFound;
     CarBrand *carbrandObj;
    //UIActivityIndicatorView *imagesaveIndicator;
    
    UIActionSheet *actionSheet;
    NSInteger selectedcartyperow;
    NSInteger selectedcarModelrow;
    int CURRENTCASE;
}
@property (retain, nonatomic) IBOutlet UIScrollView *mycarscrollview;
@property (retain, nonatomic) IBOutlet UIImageView *imgview;
@property (retain, nonatomic) UIView *indicator;
@property (retain, nonatomic) IBOutlet UITextField *modelfield;
@property (retain, nonatomic) IBOutlet UITextField *carnofield;
@property (retain, nonatomic) IBOutlet UITextField *chasisnofield;
@property (retain, nonatomic) IBOutlet UITextField *rcnumberfield;
@property (retain, nonatomic) IBOutlet UITextField *policyfield;
@property (retain, nonatomic) IBOutlet UITextField *purchagedate;
@property (retain, nonatomic) IBOutlet UITextField *carbrandfield;
@property (readwrite, nonatomic) NSInteger dbrowID;
@property (nonatomic, readwrite) double mfgDate;
@property (nonatomic, readwrite) double policyDate;
@property (nonatomic, readwrite) double carDate;
@property (nonatomic, copy) NSString *imgName;
@property (nonatomic, copy) NSString *selectedRow;
@property (readwrite, nonatomic) int imageUpdated;
@property (readwrite, nonatomic) int pickerType;
@property (retain, nonatomic) IBOutlet UITextField *mfgdatefield;
@property (retain, nonatomic) IBOutlet UITextField *insurername;
@property (retain, nonatomic) IBOutlet UITextField *policydatefield;
@property (retain, nonatomic) IBOutlet UIImageView *backimage;
@property (retain, nonatomic) NSMutableDictionary *carbranddata;
@property (nonatomic, copy) NSString *sectionName;
@property (strong, nonatomic) NSMutableArray *carlistArray;
@property (nonatomic, copy) NSMutableString *previousCarBrand;

@property (retain, nonatomic) IBOutlet UIDatePicker *editcardatePicker;
@property (retain, nonatomic) IBOutlet UIPickerView *carTypepicker;
@property (retain, nonatomic) IBOutlet UIPickerView *carModelPicker;

@property (retain, nonatomic) IBOutlet UILabel *carbrandlbl;
@property (retain, nonatomic) IBOutlet UILabel *modellbl;
@property (retain, nonatomic) IBOutlet UILabel *regNolbl;
@property (retain, nonatomic) IBOutlet UILabel *mfglbl;
@property (retain, nonatomic) IBOutlet UILabel *rcnumberlbl;
@property (retain, nonatomic) IBOutlet UILabel *insurerlbl;
@property (retain, nonatomic) IBOutlet UILabel *policynolbl;
@property (retain, nonatomic) IBOutlet UILabel *policydatelbl;
@property (retain, nonatomic) IBOutlet UILabel *chassislbl;
@property (retain, nonatomic) IBOutlet UILabel *purchasedatelbl;

@property (retain, nonatomic) IBOutlet UIButton *deletMyCar;




- (IBAction)deleteMyCarPressed:(id)sender;

- (void)useCamera;
- (void)useCameraRoll;
- (IBAction)datechangedNotify:(id)sender;

@end
