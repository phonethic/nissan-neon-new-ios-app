//
//  MyCarViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 08/06/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//
#import <Parse/Parse.h>
#import "EditMyCarViewController.h"
#import "MyCarViewController.h"
#import "CarUtilAppDelegate.h"
#import "CarBrand.h"
#import "UIImage+fixOrientation.h"

#define NEON_CAR_LINK @"http://stage.phonethics.in/proj/neon/neon_car.php"

@interface EditMyCarViewController ()

@end

@implementation EditMyCarViewController
@synthesize editcardatePicker;
@synthesize mycarscrollview;
@synthesize imgview;
@synthesize modelfield;
@synthesize carnofield;
@synthesize chasisnofield;
@synthesize rcnumberfield;
@synthesize policyfield;
@synthesize purchagedate;
@synthesize carbrandfield;
@synthesize dbrowID;
@synthesize imgName;
@synthesize mfgDate;
@synthesize policyDate;
@synthesize carDate;
@synthesize selectedRow;
@synthesize imageUpdated;
@synthesize mfgdatefield;
@synthesize insurername;
@synthesize policydatefield;
@synthesize backimage;
@synthesize indicator;
@synthesize pickerType;
@synthesize carbranddata;
@synthesize sectionName;
@synthesize carTypepicker;
@synthesize carModelPicker;
@synthesize carlistArray;
@synthesize previousCarBrand;
@synthesize carbrandlbl,modellbl,regNolbl,mfglbl,rcnumberlbl,insurerlbl,policynolbl,policydatelbl,chassislbl,purchasedatelbl;
@synthesize deletMyCar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)deleteMyCarPressed:(id)sender {
    DebugLog(@"deleteMyCarPressed");
    [self showChoice:@"Are you sure you would like to delete this vehicle?"];
}

- (void)showChoice:(NSString *) lmessage{
    UIAlertView *message = [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                       message:lmessage
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"NO", @"YES", nil] autorelease];
    
    [message show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 1:
        {
            [self deleteCarDetails];
            if(self.imgName != nil && ![self.imgName isEqualToString:@""] )
            {
                [self removeImage:self.imgName];
            }
            MyCarViewController *parent = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
            if(parent != nil){
                parent.updated = 3;
            }
            parent = nil;
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
            
        default:
            break;
    }
}

- (void) deleteCarDetails
{
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM MYCAR WHERE ID=\"%d\"", self.dbrowID];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            //[self showMessage:@"Contact deleted"];
            //[self clearData:nil];
        } else {
            //[self showMessage:@"Failed to delete contact"];
        }
        sqlite3_finalize(statement);
        NSString *findSQL = [NSString stringWithFormat:@"Select count(*) from MYCAR WHERE BRAND=\"%@\"", carbrandfield.text];
        const char *find_stmt = [findSQL UTF8String];
        if (sqlite3_prepare_v2(expenseDB, find_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                int rowcount =  sqlite3_column_int(statement, 0);
                DebugLog(@"rowcount %d carbrand %@",rowcount,carbrandfield.text);
                if (rowcount == 0)
                {
                    [PFPush unsubscribeFromChannelInBackground:carbrandfield.text];
                }
                
            } else
            {
                //Failed
            }
            sqlite3_finalize(statement);
        }
    }
    sqlite3_close(expenseDB);
}
- (void) useCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeImage,nil];
        imagePicker.allowsEditing = NO;
        [self presentModalViewController:imagePicker animated:YES];
        [imagePicker release];
    }
}

- (void) useCameraRoll
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        UIImagePickerController *imagePicker =  [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeImage, nil];
        imagePicker.allowsEditing = NO;
        [self presentModalViewController:imagePicker animated:YES];
        [imagePicker release];
    }
}



-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    [self dismissModalViewControllerAnimated:YES];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        if (image != nil) {
            image = [image fixOrientation];
            imgview.image = image;
            self.imageUpdated = 1;
            if(self.imgName == nil) {
                DebugLog(@"image name is empty");
            } else {
                DebugLog(@"image name is not empty");
            }
        }
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
		// Code here to support video if enabled
	}
}

-(void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: ALERT_TITLE
                              message: @"Failed to save image"\
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

//-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
//{
//    UITouch *touch = [touches anyObject];
//
//    if(touch.view.tag == 11) {
//        [self showActionsheet];
//    } 
//    
//}

-(void)showActionsheet
{
	UIActionSheet *uiActionSheetP= [[UIActionSheet alloc] 
					 initWithTitle: @"" 
					 delegate:self
					 cancelButtonTitle:@"CANCEL"
					 destructiveButtonTitle:nil
					 otherButtonTitles:@"Take Photo",@"Choose Exisiting Photo", nil];
	
	uiActionSheetP.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
	[uiActionSheetP showInView:self.view];
    [uiActionSheetP release];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    DebugLog(@"buttonIndex = %d",buttonIndex);
	
	if (buttonIndex == 0) {
        [self useCamera];
    } else if (buttonIndex == 1) {
        [self useCameraRoll];		
    } 
	
}


- (void)viewDidLoad
{
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] 
											   initWithBarButtonSystemItem:UIBarButtonSystemItemSave 
											   target:self action:@selector(save_Clicked:)] autorelease];
    [super viewDidLoad];
    
    [self createActivityIndicator];
    previousCarBrand = [[NSMutableString alloc] init];
    // Do any additional setup after loading the view from its nib.
    mycarscrollview.scrollEnabled = YES;
    mycarscrollview.clipsToBounds = YES;
    mycarscrollview.showsHorizontalScrollIndicator = NO;
    mycarscrollview.showsVerticalScrollIndicator = NO;
    mycarscrollview.scrollsToTop = YES;
    mycarscrollview.delegate = self;
    mycarscrollview.bounces = NO;
    [mycarscrollview setContentSize:CGSizeMake(self.view.frame.size.width, 420)];
    UITapGestureRecognizer *imageviewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    imageviewTap.numberOfTapsRequired = 1;
    imageviewTap.cancelsTouchesInView = NO;
    [self.imgview  addGestureRecognizer:imageviewTap];
    [imageviewTap release];
    
    UITapGestureRecognizer *mainviewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    mainviewTap.numberOfTapsRequired = 1;
    mainviewTap.cancelsTouchesInView = NO;
    [self.view  addGestureRecognizer:mainviewTap];
    [mainviewTap release];
    
    if([self.title isEqualToString:@"EDIT MY CAR"]) {
        DebugLog(@"edit");
        self.deletMyCar.hidden = FALSE;
        [self editCarData];
        [self loadImage:self.imgName];
    } else {
        DebugLog(@"add");
        self.deletMyCar.hidden = TRUE;
    }
    self.imageUpdated = 0;
//    imgview.layer.cornerRadius = 5;
//    imgview.layer.masksToBounds = YES;
//    imgview.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    imgview.layer.borderWidth = 1.0;

     [self addPickerWithDoneButton];
//    if(0 == (int)self.carDate) {
//        self.pickerType = 0;
//        [editcardatePicker setDate:[NSDate date] animated:YES];
//        [editcardatePicker sizeToFit];
//        [self datechangedNotify:nil];
//    } 
//    editcardatePicker.hidden = TRUE;

    if([NEONAppDelegate networkavailable]) {
        [self carListAsynchronousCall];
    }  else  {
        [self parseFromFile];
    }
//    [carTypepicker setHidden:TRUE];
//    [carModelPicker setHidden:TRUE];
    
//    CGRect screenBounds = [[UIScreen mainScreen] bounds];
//    if (screenBounds.size.height == 568) {
//        // code for 4-inch screen
//        editcardatePicker.frame = CGRectMake(editcardatePicker.frame.origin.x, editcardatePicker.frame.origin.y + 90 , editcardatePicker.frame.size.width,  editcardatePicker.frame.size.height);
//        carTypepicker.frame = CGRectMake(carTypepicker.frame.origin.x, carTypepicker.frame.origin.y + 90 , carTypepicker.frame.size.width,  carTypepicker.frame.size.height);
//        carModelPicker.frame = CGRectMake(carModelPicker.frame.origin.x, carModelPicker.frame.origin.y + 90 , carModelPicker.frame.size.width,  carModelPicker.frame.size.height);
//    }
    
    
    [self changeLabelFontAndTextColor:carbrandlbl];
    [self changeLabelFontAndTextColor:modellbl];
    [self changeLabelFontAndTextColor:regNolbl];
    [self changeLabelFontAndTextColor:mfglbl];
    [self changeLabelFontAndTextColor:rcnumberlbl];
    [self changeLabelFontAndTextColor:insurerlbl];
    [self changeLabelFontAndTextColor:policynolbl];
    [self changeLabelFontAndTextColor:policydatelbl];
    [self changeLabelFontAndTextColor:chassislbl];
    [self changeLabelFontAndTextColor:purchasedatelbl];

    [self changeTextFieldFontAndTextColor:carbrandfield];
    [self changeTextFieldFontAndTextColor:modelfield];
    [self changeTextFieldFontAndTextColor:carnofield];
    [self changeTextFieldFontAndTextColor:mfgdatefield];
    [self changeTextFieldFontAndTextColor:rcnumberfield];
    [self changeTextFieldFontAndTextColor:insurername];
    [self changeTextFieldFontAndTextColor:policyfield];
    [self changeTextFieldFontAndTextColor:policydatefield];
    [self changeTextFieldFontAndTextColor:chasisnofield];
    [self changeTextFieldFontAndTextColor:purchagedate];

}
-(void)changeLabelFontAndTextColor:(UILabel *)lbl
{
    lbl.textColor = TABLE_SELECTION_COLOR;
    lbl.font = HELVETICA_FONT(16);
}
-(void)changeTextFieldFontAndTextColor:(UITextField *)textField
{
    textField.textColor = DEFAULT_COLOR;
    textField.font = HELVETICA_FONT(12.8);
    textField.background =  [UIImage imageNamed:@"grey_field.png"];
}

-(void) createActivityIndicator {
    indicator = [[UIView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x + 100, self.view.bounds.origin.y + 85, 120, 70)];
    indicator.alpha = 0.6;
    indicator.backgroundColor = [UIColor blackColor];
    indicator.layer.cornerRadius = 5;
    indicator.layer.masksToBounds = YES;
    indicator.layer.borderColor = [UIColor whiteColor].CGColor;
    indicator.layer.borderWidth = 1.0;
    UIActivityIndicatorView *imagesaveIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    imagesaveIndicator.frame = CGRectMake(indicator.bounds.origin.x + 40, indicator.bounds.origin.y + 10, 40.0, 40.0);
    [indicator addSubview: imagesaveIndicator];
    [imagesaveIndicator startAnimating];
    [imagesaveIndicator release];
    UILabel *msgLabel = [[UILabel alloc] initWithFrame:CGRectMake(indicator.bounds.origin.x + 10, indicator.bounds.origin.y + 45, 110, 20)];
    msgLabel.text = @"Saving details..";
    msgLabel.backgroundColor = [UIColor clearColor]; 
    msgLabel.textColor = [UIColor whiteColor];
    msgLabel.shadowColor = [UIColor blackColor];
    msgLabel.shadowOffset = CGSizeMake(0, 1);
    msgLabel.font = [UIFont boldSystemFontOfSize:14.0];
    msgLabel.textAlignment = UITextAlignmentLeft;
    [indicator addSubview:msgLabel];
    [msgLabel release];
    [self.view addSubview: indicator];
    [indicator release];
    [self stopIndicatior];

}

- (void) startIndicatior {
    
    [indicator setHidden:FALSE];
}

- (void) stopIndicatior {
    
    [indicator setHidden:TRUE];
}

- (IBAction)datechangedNotify:(id)sender {
    NSDate *pickerDate = [editcardatePicker date]; 
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMMM yyyy"];
    NSString *dateString = [dateFormat stringFromDate:pickerDate];  
    [dateFormat release];
    if(pickerType == 0) {
        purchagedate.text = dateString;
        self.carDate = [[editcardatePicker date] timeIntervalSinceReferenceDate];
        mfgdatefield.text = dateString;
        self.mfgDate = [[editcardatePicker date] timeIntervalSinceReferenceDate];
        policydatefield.text = dateString;
        self.policyDate = [[editcardatePicker date] timeIntervalSinceReferenceDate];
        return;
    }
    else if (self.pickerType == 3) {
        purchagedate.text = dateString;
        self.carDate = [[editcardatePicker date] timeIntervalSinceReferenceDate];
    } else if(self.pickerType == 1) {
        mfgdatefield.text = dateString;
        self.mfgDate = [[editcardatePicker date] timeIntervalSinceReferenceDate];
    } else if(self.pickerType == 2) {
        policydatefield.text = dateString;
        self.policyDate = [[editcardatePicker date] timeIntervalSinceReferenceDate];
    }
}


- (void)editCarData {
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"SELECT * FROM MYCAR WHERE id = \"%d\"", self.dbrowID];
        
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(expenseDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                
                NSInteger primaryKey = sqlite3_column_int(statement, 0);                       
                NSString *imgPathCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                self.imgName = imgPathCol;
                DebugLog(@"img path = %@",self.imgName);
                //[self loadImage:self.imgName];
                previousCarBrand = [[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)] mutableCopy];
                NSString *modelCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
                NSString *carNoCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
                double mfgdateCol = sqlite3_column_double(statement, 5);
                self.mfgDate = mfgdateCol;
                NSString *rcNoCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 6)];
                NSString *insurerNameCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 7)];
                NSString *policyCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 8)];
				double policydateCol = sqlite3_column_double(statement, 9);
                self.policyDate = policydateCol;
                NSString *chassisNoCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 10)];
                double purchaseDateCol = sqlite3_column_double(statement, 11);
                self.carDate = purchaseDateCol;
                NSString *selectedCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 12)];
                self.selectedRow = selectedCol;
                
                DebugLog(@" %d - %@ - %@ - %@ - %@ - %f - %@ - %@ - %@ - %f - %@ - %f",primaryKey, imgPathCol, previousCarBrand, modelCol, carNoCol, mfgdateCol, rcNoCol, insurerNameCol, policyCol, policydateCol, chassisNoCol, purchaseDateCol);

                carbrandfield.text = previousCarBrand;
                modelfield.text = modelCol;
                carnofield.text = carNoCol;
                rcnumberfield.text = rcNoCol;
                insurername.text = insurerNameCol;
                policyfield.text = policyCol;
                chasisnofield.text = chassisNoCol;
                
                NSDate *Date1 = [NSDate dateWithTimeIntervalSinceReferenceDate:mfgdateCol]; 
                NSDateFormatter *dateFormat1= [[NSDateFormatter alloc] init];
                [dateFormat1 setDateFormat:@"dd MMMM yyyy"];
                NSString *dateString1 = [dateFormat1 stringFromDate:Date1];  
                mfgdatefield.text = dateString1;
                [dateFormat1 release];
                
                NSDate *Date2 = [NSDate dateWithTimeIntervalSinceReferenceDate:policydateCol]; 
                NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
                [dateFormat2 setDateFormat:@"dd MMMM yyyy"];
                NSString *dateString2 = [dateFormat2 stringFromDate:Date2];  
                policydatefield.text = dateString2;
                [dateFormat2 release];
                
                NSDate *Date3 = [NSDate dateWithTimeIntervalSinceReferenceDate:purchaseDateCol]; 
                NSDateFormatter *dateFormat3 = [[NSDateFormatter alloc] init];
                [dateFormat3 setDateFormat:@"dd MMMM yyyy"];
                NSString *dateString3 = [dateFormat3 stringFromDate:Date3];  
                purchagedate.text = dateString3;
                [dateFormat3 release];
                
                
            } else {
                //[self showMessage:@"Match not found"];
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(expenseDB);
    }
}


- (void) save_Clicked:(id)sender {
    [self startIndicatior];
    //[self performSelectorInBackground:@selector(saveFunction) withObject:nil];
    [NSTimer scheduledTimerWithTimeInterval:0.1
     
                                     target:self selector:@selector(saveFunction) userInfo:nil
     
                                    repeats:NO];
	//Pop back to the detail view.
	//[self.navigationController popViewControllerAnimated:YES];
}

-(void)saveFunction
{
    if([self.title isEqualToString:@"EDIT MY CAR"])
    {
        if(self.imgName == nil) {
            DebugLog(@"image name is empty");
            DebugLog(@"img name %@",self.imgName);
            NSString *savedName = [self saveImage:imgview.image];
            DebugLog(@"saved name = %@",savedName);
            self.imgName = savedName;
            
        } else {
            DebugLog(@"image name is not empty");
            DebugLog(@"%@",self.imgName);
            if(self.imgName != nil && ![self.imgName isEqualToString:@""] && self.imageUpdated == 1 )
                [self removeImage:self.imgName];
            NSString *savedName = [self saveImage:imgview.image];
            DebugLog(@"saved name = %@",savedName);
            self.imgName = savedName;
        }
        if(self.dbrowID > 0) {
            [self updateSavedata];
        }  else {
#ifdef TESTING
            [TestFlight passCheckpoint:@"New Car Added in MyCar Section"];
            [Flurry logEvent:@"NewCar_Added_Event"];
#endif
            [self Savedata:self.imgName];
        }
    }
    else
    {
        [Flurry logEvent:@"NewCar_Added_Event"];
#ifdef TESTING
        [TestFlight passCheckpoint:@"New Car Added in MyCar Section"];
#endif
        //Add the value.
        NSString *savedName = [self saveImage:imgview.image];
        self.imgName = savedName;
        [self Savedata:self.imgName];
    }
}

- (void) updateSavedata
{   
    sqlite3_stmt    *statement;
    
    DebugLog(@"db id = %d",self.dbrowID);
    DebugLog(@"image name = %@",self.imgName);
    DebugLog(@"selected = %@",selectedRow);
      DebugLog(@"replaced space with : -%@- -%@-",carbrandfield.text,[carbrandfield.text stringByReplacingOccurrencesOfString:@" " withString:@"-"]);
    if(self.imgName  == nil)
        self.imgName = @"";
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    //MYCAR(ID INTEGER PRIMARY KEY AUTOINCREMENT, IMAGE TEXT, MODEL TEXT, CARNUMBER TEXT, MFGDATE REAL, RCNO TEXT, INSURER TEXT, POLICY TEXT, POLICYDATE REAL, CHASSIS TEXT, BUYDATE REAL, SELECTED TEXT)";
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
         //DebugLog(@"%d  - %@  - %@ - %@ - %@ - %@ - %@  - %f",self.dbrowID, self.selectedRow, modelfield.text, modeltype.text, chasisnofield.text, carnumberfield.text, policyfield.text, self.carDate);
        NSString *updateSQL = [NSString stringWithFormat:@"update MYCAR set SELECTED=\"%@\" , IMAGE=\"%@\", BRAND=\"%@\", MODEL=\"%@\", CARNUMBER=\"%@\" , RCNO=\"%@\" , INSURER=\"%@\" ,  POLICY=\"%@\" ,  CHASSIS=\"%@\" , MFGDATE=\"%f\" ,  POLICYDATE=\"%f\", BUYDATE=\"%f\" where id=\"%d\"", selectedRow, self.imgName, carbrandfield.text , modelfield.text, carnofield.text , rcnumberfield.text, insurername.text , policyfield.text , self.chasisnofield.text ,self.mfgDate , self.policyDate , self.carDate , self.dbrowID ];

        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            //[self showMessage:@"Contact added"];
            DebugLog(@"previousCarBrand : -%@-",previousCarBrand);
            if (![previousCarBrand isEqualToString:carbrandfield.text])
            {
                [PFPush subscribeToChannelInBackground:[carbrandfield.text stringByReplacingOccurrencesOfString:REPLACECHAR withString:WITHCHAR]];
            }
        } else {
            //[self showMessage:@"Failed to add contact"];
        }
        sqlite3_finalize(statement);
        
        //unsubscribing previous carbrand if carbrand has been changed.
        if (![previousCarBrand isEqualToString:carbrandfield.text])
        {
            NSString *findSQL = [NSString stringWithFormat:@"Select count(*) from MYCAR WHERE BRAND=\"%@\"", previousCarBrand];
            const char *find_stmt = [findSQL UTF8String];
            if (sqlite3_prepare_v2(expenseDB, find_stmt, -1, &statement, NULL) == SQLITE_OK)
            {
                if (sqlite3_step(statement) == SQLITE_ROW)
                {
                    int rowcount =  sqlite3_column_int(statement, 0);
                    DebugLog(@"rowcount %d carbrand %@",rowcount,previousCarBrand);
                    if (rowcount == 0)
                    {
                        [PFPush unsubscribeFromChannelInBackground:[previousCarBrand stringByReplacingOccurrencesOfString:REPLACECHAR withString:WITHCHAR]];
                    }
                    
                } else
                {
                    //Failed
                }
                sqlite3_finalize(statement);
            }
        }
    } 
    sqlite3_close(expenseDB);

    MyCarViewController *parent = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    if(parent != nil) 
        parent.updated = 2;
    parent = nil;
    
    [self performSelectorOnMainThread:@selector(stopIndicatior) withObject:nil waitUntilDone:YES];
    [self.navigationController popViewControllerAnimated:YES];

//    [UIView  transitionWithView:self.navigationController.view duration:1.0  options:UIViewAnimationOptionTransitionFlipFromRight
//                     animations:^(void) {
//                         BOOL oldState = [UIView areAnimationsEnabled];
//                         [UIView setAnimationsEnabled:NO];
//                         [self.navigationController popViewControllerAnimated:NO];
//                         [UIView setAnimationsEnabled:oldState];
//                     } 
//                     completion:nil];
    
}

- (void) Savedata:(NSString *) lpath
{   
    DebugLog(@"path = %@",lpath);
    sqlite3_stmt    *statement;
    int personID;

    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];

    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        
        int carcount = 0;
        
        NSString *querySQL = [NSString stringWithFormat:@"SELECT count(ID) FROM MYCAR "];
        
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(expenseDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                carcount =  sqlite3_column_int(statement, 0);
                DebugLog(@"count = %d",carcount);
            } else {
                //Failed
            }
            sqlite3_finalize(statement);
        }
        
        NSString *selected;
        if(carcount == 0)
            selected = @"1";
        else 
            selected = @"0";

        NSString *insertSQL = [NSString stringWithFormat: 
                               @"INSERT INTO MYCAR (SELECTED, IMAGE , BRAND , MODEL, CARNUMBER, MFGDATE, RCNO, INSURER, POLICY, POLICYDATE, CHASSIS, BUYDATE) VALUES (\"%@\", \"%@\", \"%@\", \"%@\", \"%@\" , \"%f\", \"%@\", \"%@\", \"%@\", \"%f\", \"%@\", \"%f\")", 
                               selected, lpath, carbrandfield.text, modelfield.text, carnofield.text, self.mfgDate, rcnumberfield.text, insurername.text, policyfield.text, self.policyDate, chasisnofield.text, self.carDate];
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            personID = sqlite3_last_insert_rowid(expenseDB);
            DebugLog(@"%d",personID);
            [PFPush subscribeToChannelInBackground:carbrandfield.text];
           //[self showMessage:@"Contact added"];
        } else {
            //[self showMessage:@"Failed to add contact"];
        }
        sqlite3_finalize(statement);
    } 
    sqlite3_close(expenseDB);

    MyCarViewController *parent = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    if(parent != nil) 
        parent.updated = 1;
    parent = nil;
    
    [self performSelectorOnMainThread:@selector(stopIndicatior) withObject:nil waitUntilDone:YES];
    [self.navigationController popViewControllerAnimated:YES];

//    [UIView  transitionWithView:self.navigationController.view duration:1.0  options:UIViewAnimationOptionTransitionFlipFromRight
//                     animations:^(void) {
//                         BOOL oldState = [UIView areAnimationsEnabled];
//                         [UIView setAnimationsEnabled:NO];
//                         [self.navigationController popViewControllerAnimated:NO];
//                         [UIView setAnimationsEnabled:oldState];
//                     } 
//                     completion:nil];
    
}



- (NSString *)saveImage: (UIImage*)image
{
#if TARGET_IPHONE_SIMULATOR
    if (image == nil) {
        image = [UIImage imageNamed:@"About_5_pressed.png"];
    } 
#endif
    if (image != nil)
    {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,  NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filename = [NSString stringWithFormat: @"car_%llu.png",[[NSDate date] timeIntervalSinceReferenceDate]];
        DebugLog(@"filename = %@" , filename);
        NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:filename];
        DebugLog(@"fullpath = %@" , fullPath);
        NSData* data = UIImagePNGRepresentation(image);
        //[data writeToFile:fullPath atomically:YES];
        BOOL success = [fileManager createFileAtPath:fullPath contents:data attributes:nil];
        if(success == YES) 
            return filename;
        else 
            return @"";
    } else {
        return  @"";
    }
}

- (void)loadImage:(NSString *)limgName
{
    DebugLog(@"fullpath = %@" , limgName);
    if(limgName != nil) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:limgName];
        DebugLog(@"fullpath = %@" , fullPath);
        UIImage* image = [UIImage imageWithContentsOfFile:fullPath];
        if(image != nil)
            imgview.image = image;
    }
}

//removing an image

- (void)removeImage:(NSString*)lpath {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:lpath];
    BOOL success = [fileManager removeItemAtPath: fullPath error:NULL];
    if(success == YES) 
        DebugLog(@"image removed");
    else 
        DebugLog(@"image NOT removed");
}


- (IBAction)tapDetected:(UIGestureRecognizer *)sender {
	// Code to respond to gesture her
    if(sender.view.tag == 11)
        [self showActionsheet];
    else {
        [self hideActionSheet];
    }
}

-(void) hideActionSheet
{
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.280];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromBottom];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
    [[self.editcardatePicker layer] addAnimation:animation forKey:@"SwitchToView"];
    editcardatePicker.hidden = TRUE;
    carTypepicker.hidden = TRUE;
    carModelPicker.hidden = TRUE;
    [[self view] endEditing:YES];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:backimage cache:YES];
    backimage.frame = CGRectMake(0, 0 , 320, 418);
    imgview.frame = CGRectMake(16, 42 , 288, 170);
    mycarscrollview.frame = CGRectMake(0, 220 , 320, 270);
    [mycarscrollview setContentSize:CGSizeMake(self.view.frame.size.width, 420)];
    [UIView commitAnimations];
    [self scrollTobottom];
}

// Display the picker instead of a keyboard
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == purchagedate || textField == mfgdatefield || textField == policydatefield || textField == carbrandfield || textField == modelfield) {
        [[self view] endEditing:YES];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:backimage cache:YES];
        backimage.frame = CGRectMake(0, -110 , 320, 418);
        imgview.frame = CGRectMake(16, -90 , 288, 170);
        mycarscrollview.frame = CGRectMake(0, 100 , 320, 214);
        [mycarscrollview setContentSize:CGSizeMake(self.view.frame.size.width, 470)];
        [UIView commitAnimations];

        if(textField == mfgdatefield) {
            self.pickerType = 1;
            [self setMyCarDatePicker:self.mfgDate];
            [self scrollTotop:130];
            CURRENTCASE = DATE_TAG;
            [self showPicker];
//            if(editcardatePicker.isHidden == TRUE) {
//                // set up an animation for the transition between the views
//                CATransition *animation = [CATransition animation];
//                [animation setDuration:0.2];
//                [animation setType:kCATransitionPush];
//                [animation setSubtype:kCATransitionFromTop];
//                [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
//                [[self.editcardatePicker layer] addAnimation:animation forKey:@"SwitchToView"];
//                editcardatePicker.hidden = FALSE;
//                carTypepicker.hidden = TRUE;
//                carModelPicker.hidden = TRUE;
//            }
        } else if(textField == policydatefield) {
            self.pickerType = 2;
            [self setMyCarDatePicker:self.policyDate];
            [self scrollTotop:240];
            CURRENTCASE = DATE_TAG;
            [self showPicker];
//            if(editcardatePicker.isHidden == TRUE) {
//                // set up an animation for the transition between the views
//                CATransition *animation = [CATransition animation];
//                [animation setDuration:0.2];
//                [animation setType:kCATransitionPush];
//                [animation setSubtype:kCATransitionFromTop];
//                [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
//                [[self.editcardatePicker layer] addAnimation:animation forKey:@"SwitchToView"];
//                editcardatePicker.hidden = FALSE;
//                carTypepicker.hidden = TRUE;
//                carModelPicker.hidden = TRUE;
//            }
        } else if (textField == purchagedate) {
            self.pickerType = 3;
            [self setMyCarDatePicker:self.carDate];
            [self scrollTotop:100];
            CURRENTCASE = DATE_TAG;
            [self showPicker];
//            if(editcardatePicker.isHidden == TRUE) {
//                // set up an animation for the transition between the views
//                CATransition *animation = [CATransition animation];
//                [animation setDuration:0.2];
//                [animation setType:kCATransitionPush];
//                [animation setSubtype:kCATransitionFromTop];
//                [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
//                [[self.editcardatePicker layer] addAnimation:animation forKey:@"SwitchToView"];
//                editcardatePicker.hidden = FALSE;
//                carTypepicker.hidden = TRUE;
//                carModelPicker.hidden = TRUE;
//            }
        } else if(textField == carbrandfield) {
                CURRENTCASE = CARBRAND_TAG;
                [self showPicker];
//            if(carTypepicker.isHidden == TRUE) {
//                // set up an animation for the transition between the views
//                CATransition *animation = [CATransition animation];
//                [animation setDuration:0.2];
//                [animation setType:kCATransitionPush];
//                [animation setSubtype:kCATransitionFromTop];
//                [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
//                [[self.carTypepicker layer] addAnimation:animation forKey:@"SwitchToView"];
//                editcardatePicker.hidden = TRUE;
//                carTypepicker.hidden = FALSE;
//                carModelPicker.hidden = TRUE;
//            }
        } else if(textField == modelfield) {
            CURRENTCASE = CARMODEL_TAG;
            [self showPicker];
//            if(carModelPicker.isHidden == TRUE) {
//                // set up an animation for the transition between the views
//                CATransition *animation = [CATransition animation];
//                [animation setDuration:0.2];
//                [animation setType:kCATransitionPush];
//                [animation setSubtype:kCATransitionFromTop];
//                [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
//                [[self.carModelPicker layer] addAnimation:animation forKey:@"SwitchToView"];
//                editcardatePicker.hidden = TRUE;
//                carTypepicker.hidden = TRUE;
//                carModelPicker.hidden = FALSE;
//            }
        }
        return NO;
	}
    return YES;
    
}

-(void)setMyCarDatePicker:(double)ldate {
    if(0 == (int)self.carDate || 0 == (int)self.mfgDate || 0 == (int)self.policyDate)  {
        [editcardatePicker setDate:[NSDate date] animated:YES];
        [self datechangedNotify:nil];
    } else {
        NSDate *startDate = [NSDate dateWithTimeIntervalSinceReferenceDate:ldate]; 
        [self.editcardatePicker setDate:startDate animated:YES];
    }

    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:backimage cache:YES];
    backimage.frame = CGRectMake(0, -110 , 320, 418);
    imgview.frame = CGRectMake(16, -90 , 288, 170);
    mycarscrollview.frame = CGRectMake(0, 100 , 320, 214);
    [mycarscrollview setContentSize:CGSizeMake(self.view.frame.size.width, 470)];
    [UIView commitAnimations];
    
    if (textField == carbrandfield) {
	
    } else if (textField == modelfield) {
        [self scrollTotop:30];
	} else if (textField == carnofield) {
        [self scrollTotop:60];
	} else if (textField == mfgdatefield) {
        [self scrollTotop:100];
	} else if (textField == rcnumberfield) {
        [self scrollTotop:160];
	} else if (textField == insurername) {
        [self scrollTotop:190];
	} else if (textField == policyfield) {
        [self scrollTotop:220];
	} else if (textField == policydatefield) {
        [self scrollTotop:270];
	} else if (textField == chasisnofield) {
        [self scrollTotop:270];
	} 
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == carbrandfield) {
        [modelfield becomeFirstResponder];
	} else if (textField == modelfield) {
        [carnofield becomeFirstResponder];
	} else if (textField == carnofield) {
        [purchagedate becomeFirstResponder];
	} else if (textField == mfgdatefield) {
        [rcnumberfield becomeFirstResponder];
	} else if (textField == rcnumberfield) {
        [insurername becomeFirstResponder];
	}  else if (textField == insurername) {
        [policyfield becomeFirstResponder];
	} else if (textField == policyfield) {
        [policydatefield becomeFirstResponder];
	} else if (textField == policydatefield) {
        [chasisnofield becomeFirstResponder];
	} else if (textField == chasisnofield) {
        [chasisnofield resignFirstResponder];
        [self hideActionSheet];
	}
    
   	return YES;
}

-(void)scrollTobottom
{
    CGPoint bottomOffset = CGPointMake(0, 0);
    [mycarscrollview setContentOffset:bottomOffset animated:YES];
}

-(void)scrollTotop : (int) value
{
    CGPoint topOffset = CGPointMake(0, value);
    [mycarscrollview setContentOffset:topOffset animated:YES];
}

- (void)viewDidUnload
{
    [self setImgview:nil];
    [self setModelfield:nil];
    [self setCarnofield:nil];
    [self setChasisnofield:nil];
    [self setRcnumberfield:nil];
    [self setPolicyfield:nil];
    [self setPurchagedate:nil];
    [self setMycarscrollview:nil];
    [self setEditcardatePicker:nil];
    [self setMfgdatefield:nil];
    [self setInsurername:nil];
    [self setPolicydatefield:nil];
    [self setBackimage:nil];
    [self setCarbrandfield:nil];
    [self setCarTypepicker:nil];
    [self setCarModelPicker:nil];
    [self setCarbrandlbl:nil];
    [self setModellbl:nil];
    [self setRegNolbl:nil];
    [self setMfglbl:nil];
    [self setRcnumberlbl:nil];
    [self setInsurerlbl:nil];
    [self setPolicynolbl:nil];
    [self setPolicydatelbl:nil];
    [self setChassislbl:nil];
    [self setPurchasedatelbl:nil];
    [self setDeletMyCar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)dealloc {
    //[imagesaveIndicator release];
    self.selectedRow = nil;
    self.imgName = nil;
    [editcardatePicker release];
    [mycarscrollview release];
    [imgview release];    
    [modelfield release];
    [carnofield release];
    [chasisnofield release];
    [rcnumberfield release];
    [policyfield release];
    [purchagedate release];
    [mfgdatefield release];
    [insurername release];
    [policydatefield release];
    [backimage release];
    [carbrandfield release];
    [carTypepicker release];
    [carModelPicker release];
    [carbranddata release];
    previousCarBrand = nil;

    [carbrandlbl release];
    [modellbl release];
    [regNolbl release];
    [mfglbl release];
    [rcnumberlbl release];
    [insurerlbl release];
    [policynolbl release];
    [policydatelbl release];
    [chassislbl release];
    [purchasedatelbl release];
    [deletMyCar release];
    [super dealloc];
}


-(void)carListAsynchronousCall
{
	/****************Asynchronous Request**********************/
    //loadingImageView.hidden = FALSE;
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:NEON_CAR_LINK] cachePolicy:YES timeoutInterval:10.0];
    
	// Note: An NSOperation creates an autorelease pool, but doesn't schedule a run loop
	// Create the connection and schedule it on a run loop under our namespaced run mode
	NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    [theConnection autorelease];
	
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	[self connectionDidFinishLoading:nil];
    //[self parseFromFile];
    DebugLog(@"Error: %@", [error localizedDescription]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //loadingImageView.hidden = TRUE;
	NSString *xmlDataFromChannelSchemes;
    
	if(responseAsyncData)
	{
		NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
		//DebugLog(@"\n result:%@\n\n", result);
        [NEONAppDelegate writeToTextFile:result name:@"carbrand"];
		xmlDataFromChannelSchemes = [[NSString alloc] initWithString:result];
		NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
		xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
		[xmlParser setDelegate:self];
		[xmlParser parse];
        [carTypepicker reloadAllComponents];
        [carTypepicker selectRow:0 inComponent:0 animated:NO];
        selectedcartyperow = 0;
        selectedcarModelrow = 0;
        for (NSString *key in [carbranddata allKeys])
        {
            //DebugLog(@"%@ ",key);
            if([key isEqualToString:carbrandfield.text])
            {
                selectedcartyperow = [[carbranddata allKeys] indexOfObject:key];
                
                [carTypepicker selectRow:selectedcartyperow inComponent:0 animated:NO];
                [carModelPicker reloadAllComponents];
                
                for (CarBrand *tempObj in [carbranddata valueForKey:key])
                {
                    if ([tempObj.carmodel isEqualToString:modelfield.text])
                    {
                        selectedcarModelrow = [[carbranddata valueForKey:key] indexOfObject:tempObj];
                        [carModelPicker selectRow:selectedcarModelrow inComponent:0 animated:NO];
                        break;
                    }
                    else
                    {
                      [carModelPicker selectRow:selectedcarModelrow inComponent:0 animated:NO];
                    }
                }
                 break;
            }
            else
            {
                [carTypepicker selectRow:selectedcartyperow inComponent:0 animated:NO];
            }
        }
        

        if(![self.title isEqualToString:@"EDIT MY CAR"]) {
            carbrandfield.text = [[carbranddata allKeys] objectAtIndex:selectedcartyperow];
            CarBrand *tempObj = [[carbranddata valueForKey:[[carbranddata allKeys] objectAtIndex:selectedcartyperow]] objectAtIndex:selectedcartyperow];
            modelfield.text =  tempObj.carmodel;
        }
        [xmlParser release];
		[xmlDataFromChannelSchemes release];
		[result release];
		[responseAsyncData release];
		responseAsyncData = nil;
	}
    else {
        [self parseFromFile];
    }
}

-(void) parseFromFile
{
    NSString *xmlDataFromChannelSchemes;
    NSString *data;
    data = [NEONAppDelegate getTextFromFile:@"carbrand"];
    DebugLog(@"\n data:%@\n\n", data);
    if(data != nil && ![data isEqualToString:@""])
    {
        xmlDataFromChannelSchemes = [[NSString alloc] initWithString:data];
    }
    else
    {
        DebugLog(@"\n reading from local\n");
        NSString* path = [[NSBundle mainBundle] pathForResource:@"carbrand" ofType:@"txt"];
        NSString* content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
        xmlDataFromChannelSchemes = [[NSString alloc] initWithString:content];
    }
    NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
    [xmlParser setDelegate:self];
    [xmlParser parse];
    [carTypepicker reloadAllComponents];
    [carTypepicker selectRow:0 inComponent:0 animated:NO];
    selectedcartyperow = 0;
    selectedcarModelrow = 0;
    for (NSString *key in [carbranddata allKeys])
    {
        //DebugLog(@"%@ ",key);
        if([key isEqualToString:carbrandfield.text])
        {
            selectedcartyperow = [[carbranddata allKeys] indexOfObject:key];
            
            [carTypepicker selectRow:selectedcartyperow inComponent:0 animated:NO];
            [carModelPicker reloadAllComponents];
            
            for (CarBrand *tempObj in [carbranddata valueForKey:key])
            {
                if ([tempObj.carmodel isEqualToString:modelfield.text])
                {
                    selectedcarModelrow = [[carbranddata valueForKey:key] indexOfObject:tempObj];
                    [carModelPicker selectRow:selectedcarModelrow inComponent:0 animated:NO];
                    break;
                }
                else
                {
                    [carModelPicker selectRow:selectedcarModelrow inComponent:0 animated:NO];
                }
            }
            break;
        }
        else
        {
            [carTypepicker selectRow:selectedcartyperow inComponent:0 animated:NO];
        }
    }
    //[carModelPicker selectRow:0 inComponent:0 animated:NO];
    if(![self.title isEqualToString:@"EDIT MY CAR"]) {
        carbrandfield.text = [[carbranddata allKeys] objectAtIndex:selectedcartyperow];
        CarBrand *tempObj = [[carbranddata valueForKey:[[carbranddata allKeys] objectAtIndex:selectedcartyperow]] objectAtIndex:selectedcartyperow];
        modelfield.text =  tempObj.carmodel;
    }

    [xmlParser release];
    [xmlDataFromChannelSchemes release];
}

#pragma mark xmlParser methods
/* Called when the parser runs into an open tag (<tag>) */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName 	attributes:(NSDictionary *)attributeDict
{
	if([elementName isEqualToString:@"makegallery"])
	{
        carbranddata = [[NSMutableDictionary alloc] init];
	} else if([elementName isEqualToString:@"section"]) {
        sectionName = [[NSString alloc] initWithString:[attributeDict objectForKey:@"name"]];
        carlistArray = [[NSMutableArray alloc] init];
    } else if([elementName isEqualToString:@"car"]) {
        carbrandObj = [[CarBrand alloc] init];
        carbrandObj.carmodelid = [[attributeDict objectForKey:@"id"] intValue];
        carbrandObj.carmodel = [attributeDict objectForKey:@"model"];
        //DebugLog(@"%@ %@ %@",[attributeDict objectForKey:@"id"],[attributeDict objectForKey:@"ilink"],[attributeDict objectForKey:@"vlink"]);
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    DebugLog(@"Processing Value: %@", string);
    //[ElementValue appendString:string];
}

/* Called when the parser runs into a close tag (</tag>). */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"makegallery"]) {
        DebugLog(@"carlist count %d",[carbranddata count]);
        
    } else if([elementName isEqualToString:@"section"]) {
        [carbranddata setObject:carlistArray forKey:sectionName];
        [carlistArray release];
        carlistArray = nil;
        [sectionName release];
        sectionName = nil;

    } else if([elementName isEqualToString:@"car"]) {
        [carlistArray addObject:carbrandObj];
        [carbrandObj release];
        carbrandObj = nil;
    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    DebugLog(@"Error: %@", [parseError localizedDescription]);
}

- (void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError
{
    DebugLog(@"Error: %@", [validationError localizedDescription]);
}

#pragma mark Pickerview methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
	
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
	
	DebugLog(@"\ncarbranddata: %d\n",[carbranddata count]);
    if([thePickerView isEqual: carTypepicker]) {
        return [carbranddata count];
    } else {
        if([carbranddata count] == 0)
        {
            return 0;
        } else {
            DebugLog(@"==count==%d",[[carbranddata valueForKey:[[carbranddata allKeys] objectAtIndex:selectedcartyperow]] count]);
            return [[carbranddata valueForKey:[[carbranddata allKeys] objectAtIndex:selectedcartyperow]] count];
        }
    }
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if([thePickerView isEqual: carTypepicker]) {
         return [[carbranddata allKeys] objectAtIndex:row];
    } else {
        CarBrand *tempObj = [[carbranddata valueForKey:[[carbranddata allKeys] objectAtIndex:selectedcartyperow]] objectAtIndex:row];
        DebugLog(@"==title==%@",tempObj.carmodel);
        return tempObj.carmodel;
    }
}
 
- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	
	if(row > -1 && [thePickerView isEqual: carTypepicker])
	{
		DebugLog(@"Selected Car Brand: %@ Index of selected car: %i", [[carbranddata allKeys] objectAtIndex:row], row);
		selectedcartyperow = row;
//        carbrandfield.text = [[carbranddata allKeys] objectAtIndex:row];
//        [carModelPicker reloadAllComponents];
//        [carModelPicker selectRow:0 inComponent:0 animated:NO];
//        CarBrand *tempObj = [[carbranddata valueForKey:[[carbranddata allKeys] objectAtIndex:selectedcartyperow]] objectAtIndex:0];
//        DebugLog(@"Selected Car : %@ Index of selected car: %i", tempObj.carmodel, row);
//        modelfield.text =  tempObj.carmodel;
	} else {
//        CarBrand *tempObj = [[carbranddata valueForKey:[[carbranddata allKeys] objectAtIndex:selectedcartyperow]] objectAtIndex:row];
//        DebugLog(@"Selected Car : %@ Index of selected car: %i", tempObj.carmodel, row);
//        modelfield.text =  tempObj.carmodel;
         selectedcarModelrow = row;
    }
}
-(void) addPickerWithDoneButton
{
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:nil
                                     cancelButtonTitle:nil
                                destructiveButtonTitle:nil
                                     otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    
    carTypepicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, 0, 0)];
    carTypepicker.showsSelectionIndicator = YES;
    carTypepicker.dataSource = self;
    carTypepicker.delegate = self;
    [actionSheet addSubview:carTypepicker];
    
    carModelPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, 0, 0)];
    carModelPicker.showsSelectionIndicator = YES;
    carModelPicker.dataSource = self;
    carModelPicker.delegate = self;
    [actionSheet addSubview:carModelPicker];
    
    editcardatePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 40, 0, 0)];
    editcardatePicker.datePickerMode = UIDatePickerModeDate;
    // fueldatepicker.timeZone = [NSTimeZone localTimeZone];
    // [fueldatepicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    
    [actionSheet addSubview:editcardatePicker];
    
    UISegmentedControl *doneButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
    doneButton.momentary = YES;
    doneButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    doneButton.segmentedControlStyle = UISegmentedControlStyleBar;
    doneButton.tintColor = DEFAULT_COLOR;
    [doneButton addTarget:self action:@selector(donePickerBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:doneButton];
    [doneButton release];
    
    UISegmentedControl *cancelButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Cancel"]];
    cancelButton.momentary = YES;
    cancelButton.frame = CGRectMake(10, 7.0f, 60.0f, 30.0f);
    cancelButton.segmentedControlStyle = UISegmentedControlStyleBar;
    cancelButton.tintColor = [UIColor blackColor];
    [cancelButton addTarget:self action:@selector(cancelPickerBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:cancelButton];
    [cancelButton release];
    
    if(0 == (int)self.carDate)
    {
        self.pickerType = 0;
        [editcardatePicker setDate:[NSDate date] animated:YES];
        [editcardatePicker sizeToFit];
        [self datechangedNotify:nil];
    }
    [editcardatePicker setHidden:TRUE];
    [carTypepicker setHidden:TRUE];
    [carModelPicker setHidden:TRUE];
}
-(void)showPicker
{
    switch (CURRENTCASE) {
        case CARBRAND_TAG:
        {
            [carTypepicker setHidden:NO];
            [carModelPicker setHidden:YES];
            [editcardatePicker setHidden:YES];
        }
            break;
        case CARMODEL_TAG:
        {
            [carTypepicker setHidden:YES];
            [carModelPicker setHidden:NO];
            [editcardatePicker setHidden:YES];
        }
            break;
        case DATE_TAG:
        {
            [carTypepicker setHidden:YES];
            [carModelPicker setHidden:YES];
            [editcardatePicker setHidden:NO];
        }
            break;
        default:
            return;
    }
    [actionSheet showInView:self.view];
    [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
}
- (void) donePickerBtnClicked:(id)sender
{
    DebugLog(@"donePickerBtnClicked");
    switch (CURRENTCASE) {
        case CARBRAND_TAG:
        {
            carbrandfield.text = [[carbranddata allKeys] objectAtIndex:selectedcartyperow];
            [carModelPicker reloadAllComponents];
            [carModelPicker selectRow:0 inComponent:0 animated:NO];
            CarBrand *tempObj = [[carbranddata valueForKey:[[carbranddata allKeys] objectAtIndex:selectedcartyperow]] objectAtIndex:0];
            DebugLog(@"Selected Car : %@ Index of selected car: %i", tempObj.carmodel, selectedcartyperow);
            modelfield.text =  tempObj.carmodel;
        }
            break;
        case CARMODEL_TAG:
        {
            CarBrand *tempObj = [[carbranddata valueForKey:[[carbranddata allKeys] objectAtIndex:selectedcartyperow]] objectAtIndex:selectedcarModelrow];
            DebugLog(@"Selected Car : %@ Index of selected car: %i", tempObj.carmodel, selectedcartyperow);
            modelfield.text =  tempObj.carmodel;
        }
            break;
        case DATE_TAG:
        {
            [self datechangedNotify:nil];
        }
            break;
        default:
            break;
    }
    [self cancelPickerBtnClicked:nil];
}
- (void) cancelPickerBtnClicked:(id)sender
{
    DebugLog(@"cancelPickerBtnClicked");
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    [self hideActionSheet];
    switch (CURRENTCASE) {
        case CARBRAND_TAG:
            {
                selectedcartyperow = [[carbranddata allKeys] indexOfObject:carbrandfield.text];
                [carTypepicker selectRow:selectedcartyperow inComponent:0 animated:NO];
            }
            break;
        case CARMODEL_TAG:
        {
            for (CarBrand *tempObj in [carbranddata valueForKey:carbrandfield.text])
            {
                if ([tempObj.carmodel isEqualToString:modelfield.text])
                {
                    selectedcarModelrow = [[carbranddata valueForKey:carbrandfield.text] indexOfObject:tempObj];
                    [carModelPicker selectRow:selectedcarModelrow inComponent:0 animated:NO];
                    break;
                }
                else
                {
                    [carModelPicker selectRow:0 inComponent:0 animated:NO];
                }
            }
        }
        break;
    
        default:
            break;
    }
    CURRENTCASE = 0;

}
@end
