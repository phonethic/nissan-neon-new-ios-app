//
//  ExpenseType.h
//  CarUtil
//
//  Created by Rishi Saxena on 04/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExpenseType : NSObject {
    NSInteger expenseTypeid;
	NSString *type;
}

@property (nonatomic, readwrite) NSInteger expenseTypeid;
@property (nonatomic, copy) NSString *type;
-(id)initWithID:(NSInteger)typid  type:(NSString *)ltype ;

@end
