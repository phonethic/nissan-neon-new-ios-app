//
//  ExpenseViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 04/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface ExpenseViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITableView *expenseTable;
    sqlite3 *expenseDB;
    //NSString *databasePath;
    // Array to store the expense objects
	NSMutableArray *expenseArray;
    UIView *headerView;
    UIView *footerView;
    NSInteger cardId;
}

@property(nonatomic,retain)IBOutlet UITableView *expenseTable;
@property(nonatomic,retain)  UIView *headerView;
@property(nonatomic,retain)  UIView *footerView;
@property (readwrite, nonatomic) NSInteger cardId;
@property (nonatomic, copy) NSMutableArray *expenseArray;
@property (retain, nonatomic) IBOutlet UIButton *infoBtn;

-(void)adddataInExpenseArray:(NSInteger)lid  date:(NSString *)ldate cost:(NSString *)lcost type:(NSString *)ltype notes:(NSString *)lnotes reading:(NSString *)lreading;
-(void)updatedataInExpenseArray:(NSInteger)lid  date:(NSString *)ldate cost:(NSString *)lcost type:(NSString *)ltype notes:(NSString *)lnotes reading:(NSString *)lreading rowID:(NSInteger)lrowID;
- (void) editData:(NSInteger)ldbrowID rowID:(NSInteger)lrowID;

-(void)reloadExpenseArray;
- (IBAction)infoBtnPressed:(id)sender;


@end
