//
//  ExpenseViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 04/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "ExpenseViewController.h"
#import "AddExpenseViewController.h"
#import "DatePickerViewController.h"
#import "Expense.h"
#import "EditExpenseViewController.h"
#import "CarUtilAppDelegate.h"
#import "MyCarViewController.h"

#define TABLE_HEADER_HEIGHT 51
#define TABLE_FOOTER_HEIGHT 52
#define TABLE_ROW_HEIGHT 45

@interface ExpenseViewController ()

@end

@implementation ExpenseViewController
@synthesize expenseTable;
@synthesize expenseArray;
@synthesize infoBtn;
@synthesize footerView;
@synthesize headerView;
@synthesize cardId;

#pragma mark Table view methods
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return TABLE_HEADER_HEIGHT;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section   // custom view for header. will be adjusted to default or specified header height
{
    
    if(headerView == nil) {
        //allocate the view if it doesn't exist yet
        headerView  = [[UIView alloc] initWithFrame:CGRectMake(0.0f,0.0f,300.0f,TABLE_HEADER_HEIGHT)];
        //headerView.backgroundColor = [UIColor lightGrayColor];
        
        UIImage *image = [UIImage imageNamed:@"expense_add_butt.png"];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        CGRect frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        button.frame = frame;	// match the button's size with the image size
		button.tag = 4;
        [button setBackgroundImage:image forState:UIControlStateNormal];
        // set the button's target to this table view controller so we can interpret touch events and map that to a NSIndexSet
        [button addTarget:self action:@selector(checkAllButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        
        UIImage *image1 = [UIImage imageNamed:@"expense_headings.png"];
        UIImageView *tempImg = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 300, 51)] autorelease];
        [tempImg setImage:image1];
        
        //add the button to the view
        [headerView insertSubview:tempImg atIndex:1];
        [headerView insertSubview:button atIndex:3];   //select-all button

    }
    
    //return the view for the footer
    return headerView;
}

// specify the height of your footer section
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    //differ between your sections or if you
    //have only on section return a static value
    return TABLE_FOOTER_HEIGHT;
}

// custom view for footer. will be adjusted to default or specified footer height
// Notice: this will work only for one section within the table view
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    if(footerView == nil) {
        //allocate the view if it doesn't exist yet
        footerView  = [[UIView alloc] init];
        footerView.backgroundColor = [UIColor clearColor];
        CGRect rect = CGRectMake(180.0f,15.0f,100.0f,30.0f);
        
        UIImage *image = [UIImage imageNamed:@"expense_del_butt.png"];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        CGRect frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        button.frame = frame;	// match the button's size with the image size
		button.tag = 4;
        [button setBackgroundImage:image forState:UIControlStateNormal];
        //button.imageView.tag = 11;
        // set the button's target to this table view controller so we can interpret touch events and map that to a NSIndexSet
        [button addTarget:self action:@selector(cancelAllButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel* label  = [[[UILabel alloc] initWithFrame:rect] autorelease];
        label.backgroundColor   = [UIColor clearColor];
        label.text      =  @"0";
        label.baselineAdjustment= UIBaselineAdjustmentAlignCenters;
        label.lineBreakMode =  UILineBreakModeTailTruncation;
        label.textAlignment = UITextAlignmentCenter;
        label.font      = [UIFont systemFontOfSize:20];
        label.textColor     = [UIColor whiteColor];
        label.numberOfLines = 0;
        
         
        UIImage *deselectimage = [UIImage imageNamed:@"expense_total_cost.png"];
        CGRect deselcectframe = CGRectMake(0, 0.0, 300, deselectimage.size.height);
        UIImageView *tempImg = [[[UIImageView alloc] initWithFrame:deselcectframe] autorelease]; 
        [tempImg setImage:deselectimage];
        
        //add the button to the view
        [footerView insertSubview:tempImg atIndex:0];
        [footerView insertSubview:label atIndex:1];
        [footerView insertSubview:button atIndex:2];

    }
    
    //return the view for the footer
    return footerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TABLE_ROW_HEIGHT;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [expenseArray count];
}
//https://discussions.apple.com/thread/1966942?start=0&tstart=0
//http://rahul7star.blogspot.in/2011/01/add-button-to-uitableview-cell.html
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UILabel *lblDate;
    UILabel *lblType;
    UILabel *lblCost;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        
        UIImage *image = [UIImage imageNamed:@"un-check.png"];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        CGRect frame = CGRectMake(0.0, cell.frame.origin.y + 6 , image.size.width, image.size.height);
        button.frame = frame;	// match the button's size with the image size
		button.tag = 4;
        [button setBackgroundImage:image forState:UIControlStateNormal];
        button.imageView.tag = 10;
        // set the button's target to this table view controller so we can interpret touch events and map that to a NSIndexSet
        [button addTarget:self action:@selector(checkButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        cell.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:button];
        
        
        
        //Initialize Label with tag 1.(Date Label)
        lblDate = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x + 33, cell.frame.origin.y, 85, cell.frame.size.height)];
        lblDate.tag = 1;
        lblDate.font = [UIFont boldSystemFontOfSize:14];
        lblDate.textColor = [UIColor whiteColor];
        lblDate.textAlignment = UITextAlignmentCenter;
        lblDate .lineBreakMode =  UILineBreakModeTailTruncation;
        lblDate.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblDate];
        [lblDate release];
        
        //Initialize Label with tag 2.(Type Label)
        lblType = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x + 120, cell.frame.origin.y, 88, cell.frame.size.height)];
        lblType.tag = 2;
        lblType.font = [UIFont boldSystemFontOfSize:14];
        lblType.textColor = [UIColor whiteColor];
        lblType.textAlignment = UITextAlignmentCenter;
        lblType .lineBreakMode =  UILineBreakModeTailTruncation;
        lblType.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblType];
        [lblType release];
        
        //Initialize Label with tag 3.(Cost Label)
        lblCost = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x + 212, cell.frame.origin.y, 88, cell.frame.size.height)];
        lblCost.tag = 3;
        lblCost.font = [UIFont boldSystemFontOfSize:14];
        lblCost.textColor = [UIColor whiteColor];
        lblCost.textAlignment = UITextAlignmentCenter;
        lblCost .lineBreakMode =  UILineBreakModeTailTruncation;
        lblCost.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblCost];
        [lblCost release];
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor clearColor];
        UIImageView *tempImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 300, 45)];
        [tempImg setImage:[UIImage imageNamed:@"expense_row.png"]];
        cell.backgroundView = tempImg;
        [tempImg release];
    }
    
    // Set up the cell...
	//cell.textLabel.font=[UIFont fontWithName:@"Arial" size:16];
    
    // Set up the cell
	Expense *expenseObj = (Expense *)[expenseArray objectAtIndex:indexPath.row];
    //cell.textLabel.text = expenseObj.type;
    NSDate *pickerDate = [NSDate dateWithTimeIntervalSinceReferenceDate:[expenseObj.date doubleValue] ]; 
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMM yyyy"];
    NSString *dateString = [dateFormat stringFromDate:pickerDate];  
    lblDate = (UILabel *)[cell viewWithTag:1];
    lblDate.text = dateString;
    lblType = (UILabel *)[cell viewWithTag:2];
    lblType.text = expenseObj.type;
    lblCost = (UILabel *)[cell viewWithTag:3];
    lblCost.text = expenseObj.cost;
	//cell.detailTextLabel.text = dateString;
    UIButton *button = (UIButton *)[cell viewWithTag:4];
    if([expenseObj.selected isEqualToString:@"0"]){
        [button setBackgroundImage:[UIImage imageNamed:@"un-check.png"] forState:UIControlStateNormal];
    } else {
        [button setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];        
    }
    [dateFormat release];

    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    DebugLog(@"%d",indexPath.row);
    Expense *expenseObj = (Expense *)[expenseArray objectAtIndex:indexPath.row];
    [self editData:expenseObj.expenseid rowID:indexPath.row];
    //[expenseArray removeObject:expenseObj];
    [expenseTable deselectRowAtIndexPath:[expenseTable indexPathForSelectedRow] animated:NO];
    
}

- (void)tableView:(UITableView *)tv commitEditingStyle:(UITableViewCellEditingStyle)editingStyle 
forRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if(editingStyle == UITableViewCellEditingStyleDelete) {
		
		//Get the object to delete from the array.
        Expense *expenseObj = (Expense *)[expenseArray objectAtIndex:indexPath.row];
        [self deletedata:expenseObj.expenseid];
		[expenseArray removeObject:expenseObj];
		
		//Delete the object from the table.
		[self.expenseTable deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationMiddle];
	}
}


- (void)checkAllButtonTapped:(id)sender event:(id)event
{
    [expenseArray removeAllObjects];
    [self updateAllRowButtonSelection:@"1"];
    [self readExpenseFromDatabase];
    [self sortExpenseArray];
    [expenseTable reloadData];
    [self addTotalCost];

}

- (void)cancelAllButtonTapped:(id)sender event:(id)event 
{
    [expenseArray removeAllObjects];
    [self updateAllRowButtonSelection:@"0"];
    [self readExpenseFromDatabase];
    [self sortExpenseArray];
    [expenseTable reloadData];
    [self addTotalCost];

}

-(void)sortExpenseArray
{
    DebugLog(@"datebutton is selected");

    NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO selector:@selector(compare:)];
    [expenseArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];	
    [alphaDesc release], alphaDesc = nil;
}


- (void)checkButtonTapped:(id)sender event:(id)event
{
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.expenseTable];
	NSIndexPath *indexPath = [self.expenseTable indexPathForRowAtPoint: currentTouchPosition];
	if (indexPath != nil)
	{
        UITableViewCell *cell  = (UITableViewCell *)[self.expenseTable cellForRowAtIndexPath:indexPath];
        UIButton *button = (UIButton *)[cell viewWithTag:4];
        Expense *expenseObj = (Expense *)[expenseArray objectAtIndex:indexPath.row];
        if([expenseObj.selected isEqualToString:@"0"]){
            
            // Create a new expense object with the data from the database
            Expense *tempObj = [[Expense alloc] initWithID:expenseObj.expenseid date:expenseObj.date cost:expenseObj.cost type:expenseObj.type notes:expenseObj.notes reading:expenseObj.reading selected:@"1"];
            [expenseArray replaceObjectAtIndex:indexPath.row withObject:tempObj];
            [button setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
            [self updateRowButtonSelection:tempObj.expenseid selected:@"1"];
            [tempObj release];
        } else {
            // Create a new expense object with the data from the database
            Expense *tempObj = [[Expense alloc] initWithID:expenseObj.expenseid date:expenseObj.date cost:expenseObj.cost type:expenseObj.type notes:expenseObj.notes reading:expenseObj.reading selected:@"0"];
            [expenseArray replaceObjectAtIndex:indexPath.row withObject:tempObj];
            [button setBackgroundImage:[UIImage imageNamed:@"un-check.png"] forState:UIControlStateNormal];
            [self updateRowButtonSelection:tempObj.expenseid selected:@"0"];
            [tempObj release];
        }
	}
}

-(void)updateAllRowButtonSelection:(NSString *)selectedvalue
{
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"update expenses set selected=\"%@\" where CARID=\"%d\"",  selectedvalue , self.cardId];
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            //[self showMessage:@"Contact updated"];
            //[self clearData:nil];
        } else {
            //[self showMessage:@"Contact updatation failed"];
        }
        sqlite3_finalize(statement);
    }         
    sqlite3_close(expenseDB);
}


-(void)updateRowButtonSelection:(NSInteger)expId selected:(NSString *)value
{
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"update expenses set selected=\"%@\"  where id=\"%d\"",  value , expId];
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            //[self showMessage:@"Contact updated"];
            //[self clearData:nil];
        } else {
            //[self showMessage:@"Contact updatation failed"];
        }
        sqlite3_finalize(statement);
    }         
    sqlite3_close(expenseDB);
    [self addTotalCost];
    
}

-(void)addTotalCost
{
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"SELECT sum(cost) as TOTAL FROM expenses WHERE selected=\"%@\" AND CARID=\"%d\"", @"1", self.cardId];
        
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(expenseDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                double field1 =  sqlite3_column_double(statement, 0);
                UILabel *label = [footerView.subviews objectAtIndex:1];
                label.text = [NSString stringWithFormat:@"%.1f", field1];
                DebugLog(@"%d",field1);
                
            } else {
                //Failed
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(expenseDB);
    }
}

-(void)deletedata:(NSInteger)expenseid
{
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM EXPENSES WHERE id=\"%d\"", expenseid];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            //[self showMessage:@"Contact deleted"];
            //[self clearData:nil];
        } else {
            //[self showMessage:@"Failed to delete contact"];
        }
        sqlite3_finalize(statement);
    }         
    sqlite3_close(expenseDB);
    [self addTotalCost];
}

-(void)adddataInExpenseArray:(NSInteger)lid  date:(NSString *)ldate cost:(NSString *)lcost type:(NSString *)ltype notes:(NSString *)lnotes reading:(NSString *)lreading

{
    // Create a new expense object with the data from the database
    Expense *expenseObj = [[Expense alloc] initWithID:lid date:ldate cost:lcost type:ltype notes:lnotes reading:lreading selected:@"1"];
    
    // Add the animal object to the animals Array
    [expenseArray addObject:expenseObj];
    
    [expenseObj release];
    
    NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO selector:@selector(compare:)];
    [expenseArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];	
    [alphaDesc release], alphaDesc = nil;
    
    [self.expenseTable reloadData];
    
}

-(void)updatedataInExpenseArray:(NSInteger)lid  date:(NSString *)ldate cost:(NSString *)lcost type:(NSString *)ltype notes:(NSString *)lnotes reading:(NSString *)lreading rowID:(NSInteger)lrowID
{
    // Create a new expense object with the data from the database
    Expense *expenseObj = [[Expense alloc] initWithID:lid date:ldate cost:lcost type:ltype notes:lnotes reading:lreading selected:@"0"];
    
    [expenseArray replaceObjectAtIndex:lrowID withObject:expenseObj];
    
    [expenseObj release];
    [self.expenseTable reloadData];
    
}

-(void)reloadExpenseArray
{
    [expenseArray removeAllObjects];
    [self readExpenseFromDatabase];
    [expenseTable reloadData];
}

- (IBAction)infoBtnPressed:(id)sender {
    UIAlertView *message = [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                       message:@"1. To add new expenses, click on the + sign on top right.\n2. Select the date on which the expense was incurred. By default, it is the current date.\n3. Add the expense incurred.\n4. Select the Type of expense from the drop-down menu (such as Repair, Oil change etc)\n5. Write additional notes, if required.\n6. To edit your odometer, click on the > sign next to the 'ODOMETER(km)'\n7. Slide the numbers to indicate the distance travelled by your vehicle in Km. Save.\n8. Click Save again.\n9. Tap on the Check symbol at the left most column of the expense table. This will add up all your expenses to give you the Total Cost."
                                                      delegate:nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil] autorelease];
    
    [message show];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated 
{
    [self addTotalCost];
    [super viewDidAppear:animated];

}


- (void)viewDidLoad
{
#ifdef ENABLE_BACK_BUTTON
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [backButton release]; 
    
#endif
    // Do any additional setup after loading the view from its nib.
    DebugLog(@"ADD EXPENSE VIEWDIDLOAD");
    //[Flurry logEvent:@"Expense_Event" withParameters:nil timed:YES];
    
    UIBarButtonItem *AddButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(Adddata:)];
    self.navigationItem.rightBarButtonItem = AddButton;
    [AddButton release];
    
  //  infoBtn.frame = CGRectMake(infoBtn.frame.origin.x, self.expenseTable.frame.size.height+15, infoBtn.frame.size.width, infoBtn.frame.size.height);
   infoBtn.frame = CGRectMake(infoBtn.frame.origin.x, self.view.frame.size.height-30, infoBtn.frame.size.width, infoBtn.frame.size.height);
    
#ifdef TESTING
    [TestFlight passCheckpoint:@"Expense Calculator Pressed"];
#endif
    
    int currentVCIndex = [self.navigationController.viewControllers indexOfObject:self.navigationController.topViewController];
    DebugLog(@"parent %@",[self.navigationController.viewControllers objectAtIndex:currentVCIndex-1]);
    if([[self.navigationController.viewControllers objectAtIndex:currentVCIndex-1] isKindOfClass:[MyCarViewController class]])
    {
        DebugLog(@"Mycar controller");
        [self readCarIDFromDatabase];
    } else {
        DebugLog(@"CarUtilSecondViewController");
        self.cardId = 0;
    }
    [self readExpenseFromDatabase];
    
    NSSortDescriptor *alphaDesc = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO selector:@selector(compare:)];
    [expenseArray sortUsingDescriptors:[NSMutableArray arrayWithObjects:alphaDesc, nil]];	
    [alphaDesc release], alphaDesc = nil;
     UIButton *datebutton = [headerView.subviews objectAtIndex:2];
    datebutton.selected = YES;
    //[self.view addSubview:backImage];
    [self.view bringSubviewToFront:expenseTable];
    expenseTable.backgroundColor = [UIColor clearColor];

    expenseTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [super viewDidLoad];
}

-(void) readCarIDFromDatabase {
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
	
	// Open the database from the users filessytem
	if(sqlite3_open(dbpath, &expenseDB) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select ID from MYCAR WHERE selected=\"%@\"", @"1"];
        const char *sqlStatement = [querySQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(expenseDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
                self.cardId = sqlite3_column_int(compiledStatement, 0);
                DebugLog(@"car id -> %d",self.cardId);
            }
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(expenseDB);
}

-(void) readExpenseFromDatabase {
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
	    
    // Init the expense Array
    if(expenseArray == nil) {
        DebugLog(@"new  expense array created");
        expenseArray = [[NSMutableArray alloc] init];
    } else {
        DebugLog(@"old  expense array used");
        [expenseArray removeAllObjects];
    }	
	// Open the database from the users filessytem
	if(sqlite3_open(dbpath, &expenseDB) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"select * from EXPENSES where CARID=\"%d\"", self.cardId];
        const char *sqlStatement = [querySQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(expenseDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
                NSInteger primaryKey = sqlite3_column_int(compiledStatement, 0);
				NSString *dateCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
				NSString *costCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
				NSString *typeCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                NSString *notesCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                NSString *readingCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
                NSString *selectedCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
                
                
                //DebugLog(@" %d - %@ - %@ - %@ - %@ - %@",primaryKey, dateCol,costCol,typeCol,notesCol,readingCol);
				// Create a new expense object with the data from the database
				Expense *expenseObj = [[Expense alloc] initWithID:primaryKey date:dateCol cost:costCol type:typeCol notes:notesCol reading:readingCol selected:selectedCol];
				
				// Add the animal object to the animals Array
				[expenseArray addObject:expenseObj];
				
				[expenseObj release];
			}
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
		
	}
	sqlite3_close(expenseDB);
}

- (void) Adddata: (id) sender
{   
#ifdef GOOGLE_ANALYTICS
    if (![[GANTracker sharedTracker] trackEvent:@"Nissan CarUitl Application iOS"
                                         action:@""
                                          label:@"Add New Expense Pressed"
                                          value:-1
                                      withError:NULL]) {
        DebugLog(@"error in trackEvent");
    }
#endif 
    AddExpenseViewController *addexpController = [[AddExpenseViewController alloc] initWithNibName:@"AddExpenseViewController" bundle:nil] ;
    addexpController.title = @"Add Expense";
    addexpController.carID = self.cardId;
    //to push the UIView.
    addexpController.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:addexpController animated:YES];
    [addexpController release];
}

- (void) editData:(NSInteger)ldbrowID rowID:(NSInteger)lrowID
{   
    EditExpenseViewController *editexpController = [[EditExpenseViewController alloc] initWithNibName:@"EditExpenseViewController" bundle:nil] ;
    editexpController.title = @"Edit Expense";
    //to push the UIView.
    editexpController.hidesBottomBarWhenPushed = NO;
    DebugLog(@"db row id %d",ldbrowID);
    editexpController.carID = self.cardId;
    editexpController.dbowID = ldbrowID;
    editexpController.rowID = lrowID;
    [self.navigationController pushViewController:editexpController animated:YES];
    [editexpController release];
}

- (void)viewDidUnload
{
    //[self setBackImage:nil];
    self.expenseTable = nil;
    self.headerView = nil;
    self.footerView = nil;
    [self setInfoBtn:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)dealloc {
    //[Flurry endTimedEvent:@"Expense_Event" withParameters:nil];
    //[backImage release];
    if(headerView != nil)
        [headerView release];
    if(footerView != nil)
        [footerView release];
    if(expenseArray != nil)
        [expenseArray release];
    [expenseTable release];
    [infoBtn release];
    [super dealloc];
}
@end
