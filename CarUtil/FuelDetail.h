//
//  FuelDetail.h
//  CarUtil
//
//  Created by Rishi Saxena on 18/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FuelDetail : NSObject {
    NSInteger fueldetailid;
    NSInteger mileageid;
    double date;
    double fuel;
    double cost;

}
@property (nonatomic, readwrite) NSInteger fueldetailid;
@property (nonatomic, readwrite) NSInteger mileageid;
@property (nonatomic, readwrite) double date;
@property (nonatomic, readwrite) double fuel;
@property (nonatomic, readwrite) double cost;
@property (nonatomic, readwrite) double odometerReading;
@property (nonatomic, readwrite) NSInteger isPartial;
@property (nonatomic, readwrite) NSInteger flagType;

-(id)initWithID:(NSInteger)lfueldetailid milegaeID:(NSInteger)lmileageid  date:(double)ldate odometerReading:(double)lodometerReading fuel:(double)lfuel isPartial:(NSInteger)lispartial cost:(double)lcost flagType:(NSInteger)lflagType;

@end
