//
//  FuelDetail.m
//  CarUtil
//
//  Created by Rishi Saxena on 18/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "FuelDetail.h"

@implementation FuelDetail
@synthesize  fueldetailid;
@synthesize mileageid;
@synthesize date;
@synthesize fuel;
@synthesize cost;
@synthesize odometerReading;
@synthesize isPartial,flagType;

-(id)initWithID:(NSInteger)lfueldetailid milegaeID:(NSInteger)lmileageid  date:(double)ldate fuel:(double)lfuel cost:(double)lcost
{
    self = [super init];
    
    if(self) {
        self.fueldetailid = lfueldetailid;
        self.mileageid = lmileageid;
        self.date = ldate;
        self.fuel = lfuel;
        self.cost = lcost;
    }
	return self;
}

-(id)initWithID:(NSInteger)lfueldetailid milegaeID:(NSInteger)lmileageid  date:(double)ldate odometerReading:(double)lodometerReading fuel:(double)lfuel isPartial:(NSInteger)lispartial cost:(double)lcost flagType:(NSInteger)lflagType
{
    self = [super init];
    
    if(self) {
        self.fueldetailid = lfueldetailid;
        self.mileageid = lmileageid;
        self.date = ldate;
        self.fuel = lfuel;
        self.cost = lcost;
        self.odometerReading = lodometerReading;
        self.isPartial = lispartial;
        self.flagType = lflagType;
    }
	return self;
}

-(void)dealloc
{
    [super dealloc];
}


@end
