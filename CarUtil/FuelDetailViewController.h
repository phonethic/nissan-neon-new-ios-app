//
//  AddMFuelDetailViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 18/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface FuelDetailViewController : UIViewController {
    NSMutableArray *fuelArray;
    sqlite3 *expenseDB;
    NSInteger dbowID;
    //NSString *databasePath;
    UIView *headerView;
    UIView *footerView;
    NSInteger cardId;
    
    BOOL isSessionEnded;
    BOOL gotFull;
}
@property (retain, nonatomic) IBOutlet UIView *totalfooterView;
@property (readwrite, nonatomic) NSInteger cardId;
@property (readwrite, nonatomic) NSInteger dbowID;
@property(nonatomic,retain)  UIView *headerView;
@property(nonatomic,retain)  UIView *footerView;
@property (nonatomic, copy) NSMutableArray *fuelArray;
@property (retain, nonatomic) IBOutlet UITableView *fuelTable;
@property (retain, nonatomic) IBOutlet UIButton *calcMileageBtn;
@property (retain, nonatomic) IBOutlet UIView *mileageBoxView;
@property (retain, nonatomic) IBOutlet UIImageView *mileageBoxImageView;
@property (retain, nonatomic) IBOutlet UILabel *mileageBoxlbl;
@property (retain, nonatomic) IBOutlet UILabel *dayslbl;
@property (retain, nonatomic) IBOutlet UILabel *odometerSumlbl;
@property (retain, nonatomic) IBOutlet UILabel *fuelSumlbl;
@property (retain, nonatomic) IBOutlet UILabel *costSumlbl;
- (IBAction)calcMileageBtnPressed:(id)sender;
-(void)reloadFuelDetails;
@end
