//
//  GalleryGridViewController.h
//  GalleryScrollGrid
//
//  Created by Kirti Nikam on 15/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhotoBrowser.h"

@class CarUtilGallery;
@interface GalleryGridViewController : UIViewController <NSXMLParserDelegate,MWPhotoBrowserDelegate,SDWebImageManagerDelegate>
{
    NSXMLParser *xmlParser;
    NSMutableData *responseAsyncData;
    int status;
    CarUtilGallery *galleryObj;
    BOOL elementFound;
    int numberOfRows;
    int index;
    
    int groupId;
    int orderId;

    BOOL stopAnimation;
    BOOL oneTimeAnimation;
    NSURLConnection *conn;
}
@property (readwrite, nonatomic) double cellHeight;

@property (retain, nonatomic) NSMutableArray *animatedIndexArray;
@property (retain, nonatomic) NSMutableArray *photoUrlArray;
@property (nonatomic, strong) NSTimer *glowTimer;
@property (nonatomic, retain) NSArray *photos;
@property (retain, nonatomic) NSMutableArray *galleryArray;
@property (retain, nonatomic) NSMutableDictionary *galleryDict;

@property (nonatomic, copy) NSString *socialLink;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@end
