//
//  GalleryGridViewController.m
//  GalleryScrollGrid
//
//  Created by Kirti Nikam on 15/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "GalleryGridViewController.h"
#import "CarUtilGallery.h"
#import "MWPhotoBrowser.h"
#import <QuartzCore/QuartzCore.h>
#import "MPFlipTransition.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageManager.h"
#import "SDWebImageDownloader.h"
#import "MBProgressHUD.h"
#import "UIDevice+Resolutions.h"
#import "CarUtilAppDelegate.h"

#import "MFSideMenu.h"

#define NEON_GALLERY_LINK @"http://stage.phonethics.in/proj/neon/neon_gallery.php"
//#define NEON_GALLERY_LINK @"http://192.168.254.203:81/neon_gallery1.xml"

#define ONE 1
#define TWO 2
#define THREE 3
#define ONE_HEIGHT 233
#define TWO_HEIGHT 120
#define THREE_HEIGHT 100

#define FLIP_DURATION 1.0
#define DELAY_DURATION 3.0

#define FLIPFOLD_ANIMATION 1
#define SLIDE_ANIMATION 2
#define DISSOLVE_ANIMATION 3
#define FLIPFROMCENTER_ANIMATION 4

@interface GalleryGridViewController ()
{
    int noOfgrills;
}
@property (nonatomic, retain) MBProgressHUD *progressHUD;
@end

@implementation GalleryGridViewController
@synthesize photos = _photos;
@synthesize glowTimer;
@synthesize galleryArray;
@synthesize galleryDict;
@synthesize socialLink;
@synthesize tableView;
@synthesize photoUrlArray;
@synthesize animatedIndexArray;
@synthesize cellHeight;

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    if (galleryDict.allKeys.count > 0 && stopAnimation == NO) {
        stopAnimation = YES;
        if (conn != nil) {
            [conn cancel];
            [conn release];
            conn = nil;
        }
    }
    DebugLog(@"viewDidDisappear animatedIndexArray %@",animatedIndexArray);
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    DebugLog(@"viewWillAppear ");
    
    if (galleryDict.allKeys.count > 0 && stopAnimation == YES) {
        [self reloadTableData];
        stopAnimation = NO;
        oneTimeAnimation = TRUE;
        [self performSelector:@selector(doAnimation) withObject:nil afterDelay:2];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.title = NSLocalizedString(@"NISSAN", @"NISSAN");
    DebugLog(@"Grid");
    DebugLog(@"viewDidLoad ");
    //self.tableView.separatorColor = DEFAULT_COLOR;

    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self setupMenuBarButtonItems];
    
    self.navigationController.navigationBar.tintColor = DEFAULT_COLOR;
    
 //   [self formCellSizeByResolutionType];
    stopAnimation = NO;
    galleryDict = [[NSMutableDictionary alloc] init];
    
    if([NEONAppDelegate networkavailable])
    {
        DebugLog(@"networkavailable");
        [self firstReadFromFile];
    }
    else  {
        DebugLog(@"network not available");
        [self parseFromFile];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NetworkNotifyCallBack) name:NETWORK_NOTIFICATION object:nil];
}

-(void)NetworkNotifyCallBack
{
    DebugLog(@"Home : Got Network Notification");
    [self performSelector:@selector(carModelsListAsynchronousCall) withObject:nil afterDelay:2.0];
}
-(void)firstReadFromFile
{
        NSString *xmlDataFromChannelSchemes;
        NSString *data;
        data = [NEONAppDelegate getTextFromFile:@"gallery"];
        //DebugLog(@"\n data:%@\n\n", data);
        if(data != nil && ![data isEqualToString:@""])
        {
            xmlDataFromChannelSchemes = [[NSString alloc] initWithString:data];
            NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
            [xmlParser setDelegate:self];
            [xmlParser parse];
            [xmlParser release];
            [xmlDataFromChannelSchemes release];
            [self performSelector:@selector(carModelsListAsynchronousCall) withObject:nil afterDelay:5.0];
        }
        else
        {
            DebugLog(@"\n-------no data--------");
            [self showProgressHUDWithMessage:@"Loading"];
            [self carModelsListAsynchronousCall];
        }
}

-(void)initializeNumberOfRows
{
    numberOfRows = galleryDict.allKeys.count;
    DebugLog(@"nuberofRows %d",numberOfRows);
    if (animatedIndexArray == nil) {
        animatedIndexArray = [[NSMutableArray alloc] init];
        for (int i = 0 ; i < numberOfRows ; i++) {
            [animatedIndexArray addObject:[NSNumber numberWithBool:NO]];
        }
    }
    else
    {
        DebugLog(@"nuberofRows %d animatedIndexArray.count %d",numberOfRows,animatedIndexArray.count);
        int diff = numberOfRows - animatedIndexArray.count;
        for (int i = 0; i < diff; i++) {
            [animatedIndexArray addObject:[NSNumber numberWithBool:NO]];
        }
    }
    DebugLog(@"animatedarray.count = %d \n %@",animatedIndexArray.count,animatedIndexArray);
}
-(void)formCellSizeByResolutionType
{
    int valueDevice = [UIDevice currentResolution] ;
    DebugLog(@"valueDevice: %d ...", valueDevice);
    if (valueDevice == 0)
    {
        //unknow device - you got me!
        DebugLog(@"unknow device");
        double totalscreenSize = 480 - (20 + 44);
        cellHeight = totalscreenSize / 2;
    }
    else if (valueDevice == 1)
    {
        //standard iphone 3GS and lower
      DebugLog(@"standard iphone 3GS and lower : iPhone 1,3,3GS Standard Resolution   (320x480px) ");
        double totalscreenSize = 480 - (20 + 44);
        cellHeight = totalscreenSize / 2;
    }
    else if (valueDevice == 2)
    {
        //iphone 4 & 4S
       DebugLog(@"iPhone 4,4S High Resolution (640x960px)");
        double totalscreenSize = 960 - (40 + 88);
        cellHeight = totalscreenSize / 2;
    }
    else if (valueDevice == 3)
    {
        //iphone 5
        DebugLog(@"iPhone 5 High Resolution (320X568px)");
        double totalscreenSize = 568 - (20 + 44);
        cellHeight = totalscreenSize / 2;
    }
    else if (valueDevice == 4)
    {
        //iphone 5 retina
       DebugLog(@"iPhone 5 High Resolution (640x1136px)");
        double totalscreenSize = 1136 - (40 + 88);
        cellHeight = totalscreenSize / 2;
    }
    else if (valueDevice == 5)
    {
        //ipad 2
        DebugLog(@"iPad 1,2 Standard Resolution (1024x768px)");
        double totalscreenSize = 480 - (20 + 44);
        cellHeight = totalscreenSize / 2;
    }
    else if (valueDevice == 6)
    {
        //ipad 3 - retina display
        DebugLog(@"iPad 3 High Resolution (2048x1536px)");
        double totalscreenSize = 480 - (20 + 44);
        cellHeight = totalscreenSize / 2;
    }
}
- (void)showProgressHUDWithMessage:(NSString *)message {
    self.progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.progressHUD.labelText = message;
    self.progressHUD.labelFont = HELVETICA_FONT(15);
    self.progressHUD.mode = MBProgressHUDModeIndeterminate;
    [self.progressHUD show:YES];
}
-(void)hideProgressHUD:(BOOL)animated
{
    [self.progressHUD hide:animated];
}

-(void)reloadTableData
{
    [self.tableView reloadData];
    [self.tableView reloadInputViews];
    NSRange range = NSMakeRange(0, [self numberOfSectionsInTableView:self.tableView]);
    NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:range];
    [self.tableView reloadSections:sections withRowAnimation:UITableViewRowAnimationFade];
}
- (void)drawLine:(double)xValue y:(double)yValue  {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 5.0);
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();    
    CGFloat components[] = {0.0, 0.0, 1.0, 1.0};//{25/255.0, 170/255.0 , 223/255.0, 1.0};
    CGColorRef color = CGColorCreate(colorspace, components);
    CGContextSetStrokeColorWithColor(context, color);
    CGContextMoveToPoint(context, 0, 0);
    CGContextAddLineToPoint(context, xValue, yValue);
    CGContextStrokePath(context);
    CGColorSpaceRelease(colorspace);
    CGColorRelease(color);
}
-(void)demo
{
   // numberOfRows = (([_galleryDict.allKeys count] - 1) / 2) + 1;
    int totalIndex = [galleryDict.allKeys count] - 1;;
    numberOfRows = round(2.0f * (totalIndex/2)) / 2.0f;
    numberOfRows = 4;
    DebugLog(@"noOfRows = %d",numberOfRows);
    int noofParts;
    int startindex;
    for(int i = 0 ; i< numberOfRows; i++)
    {
     //   noofParts = [self getNoOfParts:i];
        noofParts = [self getNumberOfHalf:i];
        startindex = [self getCurrentIndexInDict:i];

        DebugLog(@"index = %d ==> parts %d ==> startindex dic Index %d",i,noofParts,startindex);

        DebugLog(@"no Of Parts %d",noofParts);
        if (noofParts == ONE)
        {
        }
        else if (noofParts == TWO) {
            DebugLog(@"dic Index %d",startindex+1);
            
        }
        else{
            DebugLog(@"dic Index %d",startindex+1);
            DebugLog(@"dic Index %d",startindex+2);
        }
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)carModelsListAsynchronousCall
{
	/****************Asynchronous Request**********************/
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:NEON_GALLERY_LINK] cachePolicy:YES timeoutInterval:20.0];
    
	// Note: An NSOperation creates an autorelease pool, but doesn't schedule a run loop
	// Create the connection and schedule it on a run loop under our namespaced run mode
   conn = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	[self connectionDidFinishLoading:nil];
    [self hideProgressHUD:YES];
    DebugLog(@"didFailWithError Error: %@", [error localizedDescription]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	
	NSString *xmlDataFromChannelSchemes;
    
	if(responseAsyncData)
	{
        NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
        DebugLog(@"\n result:%@\n\n", result);
        
        DebugLog(@"Got response -->");
        
        DebugLog(@"1) check file exists or not");
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *fileName = [NSString stringWithFormat:@"%@.txt",@"gallery"];
        DebugLog(@"filename = %@" , fileName);
        NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:fileName];
        DebugLog(@"fullpath = %@" , fullPath);
        
        NSError* error;
        if ([fileManager fileExistsAtPath:fullPath])
        {
            DebugLog(@"2)if yes then rename it as temp");
            NSString *tempfileName = [NSString stringWithFormat:@"%@.txt",@"tempgallery"];
            NSString *tempfullPath = [documentsDirectory stringByAppendingPathComponent:tempfileName];
            [result writeToFile:tempfullPath atomically:NO encoding:NSUTF8StringEncoding error:&error];
            
            DebugLog(@"2)calculate md5 of both old and newest file");
            DebugLog(@"oldfileName %@",fileName);
            NSString *oldFileResult = [NSString stringWithContentsOfFile:fullPath encoding:NSUTF8StringEncoding error:&error];
            ;
            NSString *oldFileMD5Hash = [NEONAppDelegate md5:oldFileResult];
            
            DebugLog(@"tempfileName %@",tempfileName);
            NSString *newFileResult = [NSString stringWithContentsOfFile:tempfullPath encoding:NSUTF8StringEncoding error:&error];
            ;
            
            NSString *newFileMD5Hash = [NEONAppDelegate md5:newFileResult];
            
            DebugLog(@"oldFileMD5Hash %@",oldFileMD5Hash);
            DebugLog(@"newFileMD5Hash %@",newFileMD5Hash);
            
            if ([oldFileMD5Hash isEqualToString:newFileMD5Hash]) {
                DebugLog(@"2)a)if both are same then remove newest temp file and return");
                [self hideProgressHUD:YES];
                [fileManager removeItemAtPath:tempfullPath error:NULL];
                return;
            }else{
                DebugLog(@"2)b)if both are not same then replaced oldest with new file and rename, save it");
                [result writeToFile:fullPath atomically:NO encoding:NSUTF8StringEncoding error:&error];
                [fileManager removeItemAtPath:tempfullPath error:NULL];
            }
        } else {
            DebugLog(@"2) if no then save it");
            [result writeToFile:fullPath atomically:NO encoding:NSUTF8StringEncoding error:&error];
        }
        
        DebugLog(@"3) refresh view with new images");
        
        //[NEONAppDelegate writeToTextFile:result name:@"gallery"];
        xmlDataFromChannelSchemes = [[NSString alloc] initWithString:result];
		NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
		xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
		[xmlParser setDelegate:self];
		[xmlParser parse];
        [xmlParser release];
		[xmlDataFromChannelSchemes release];
		[result release];
		[responseAsyncData release];
		responseAsyncData = nil;
        [conn release];
        conn = nil;
	}
    else {
        [self parseFromFile];
    }
}

-(void) parseFromFile
{
    NSString *xmlDataFromChannelSchemes;
    NSString *data;
    data = [NEONAppDelegate getTextFromFile:@"gallery"];
    //DebugLog(@"\n data:%@\n\n", data);
    if(data != nil && ![data isEqualToString:@""])
    {
        xmlDataFromChannelSchemes = [[NSString alloc] initWithString:data];
        NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
        [xmlParser setDelegate:self];
        [xmlParser parse];
        [xmlParser release];
		[xmlDataFromChannelSchemes release];
    }
    else
    {
        [self hideProgressHUD:YES];
        [self performSelector:@selector(showAlert) withObject:nil afterDelay:1.0];
        
    }
}
-(void)showAlert
{
    UIAlertView *errorView = [[UIAlertView alloc]
                              initWithTitle:ALERT_TITLE
                              message:@"Please check your internet connection and try again."
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
    [errorView show];
    [errorView release];
}

#pragma mark xmlParser methods
/* Called when the parser runs into an open tag (<tag>) */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName 	attributes:(NSDictionary *)attributeDict
{
	if([elementName isEqualToString:@"neongallery"])
	{
        if (galleryDict == nil) {
            galleryDict = [[NSMutableDictionary alloc] init];
        }
        else
        {
            [galleryDict removeAllObjects];
        }
        if (photoUrlArray == nil) {
            photoUrlArray = [[NSMutableArray alloc] init];
        }
        else
        {
            [photoUrlArray removeAllObjects];
        }
        socialLink = [attributeDict objectForKey:@"sociallink"];
	} else if([elementName isEqualToString:@"photo"])
    {
        galleryObj = [[CarUtilGallery alloc] init];
        galleryObj.ilink = [attributeDict objectForKey:@"ilink"];
        galleryObj.vlink = [attributeDict objectForKey:@"vlink"];
        galleryObj.caption = [attributeDict objectForKey:@"caption"];
        galleryObj.slink = [NSString stringWithFormat:@"%@%@/",socialLink,[attributeDict objectForKey:@"name"]];
        galleryObj.thumbnail = [attributeDict objectForKey:@"thumbnail"];
        galleryObj.description = [attributeDict objectForKey:@"description"];

        //DebugLog(@"thumbnail %@",galleryObj.thumbnail);
       // groupId = [[attributeDict objectForKey:@"group"] intValue] / 100;
        orderId = [[attributeDict objectForKey:@"order"] intValue];
        if (galleryObj.thumbnail != nil || ![galleryObj.thumbnail isEqualToString:@""])
        {
            
           [photoUrlArray addObject:galleryObj.thumbnail];
        }
        
    }
//    else if([elementName isEqualToString:@"description"]) {
//        elementFound = YES;
//    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    //DebugLog(@"Processing Value: %@", string);
    //[ElementValue appendString:string];
//    if (elementFound)
//    {
//        galleryObj.description = string;
//    }
}

/* Called when the parser runs into a close tag (</tag>). */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"neongallery"]) {
        //DebugLog(@"galleryDict %@",galleryDict);
//       [self demo];
        [self initializeNumberOfRows];
        [self reloadTableData];
        [self hideProgressHUD:YES];
        [self download];
    }
    else if([elementName isEqualToString:@"photo"]) {
        elementFound = NO;
         //DebugLog(@" -%@- -%@- -%@- -%@- -%@-", galleryObj.ilink,galleryObj.vlink,galleryObj.caption,galleryObj.description,galleryObj.thumbnail);
        if(galleryObj.description == nil)
        {
            galleryObj.description = @"";
        }
        
        //DebugLog(@"orderId : %d",orderId);
       
        BOOL keyfound = NO;
        NSString *keyString = [NSString stringWithFormat:@"%d",orderId];
        for (NSString *key in [galleryDict allKeys])
        {
            if ([key isEqualToString:keyString])
            {
                keyfound = YES;
                break;
            }
        }
        if (!keyfound)
        {
            [galleryDict setValue:[[[NSMutableArray alloc] init] autorelease] forKey:keyString];
        }
        [[galleryDict objectForKey:keyString] addObject:galleryObj];
        [galleryObj release];
        galleryObj = nil;
        keyString = nil;
        orderId = 0;
    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    DebugLog(@"Error: %@", [parseError localizedDescription]);
}

- (void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError
{
    DebugLog(@"Error: %@", [validationError localizedDescription]);
}


-(void)download
{
    //DebugLog(@"image : %@ %d",photoUrlArray, photoUrlArray.count);
//    if (photoUrlArray.count > 0) {
//        [self downloadImages:[photoUrlArray objectAtIndex:0] imgeindex:1];
//    }
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    for (int limageindex = 0; limageindex< photoUrlArray.count; limageindex++) {
       //DebugLog(@"index %d image : %@",i,[photoUrlArray objectAtIndex:i]);
        //[self performSelector:@selector(downloadImages:) withObject:[photoUrlArray objectAtIndex:i]  afterDelay:2.0];
        
        [manager downloadWithURL:[NSURL URLWithString:[photoUrlArray objectAtIndex:limageindex]] delegate:self options:0
                         success:^(UIImage *image) {
                             //DebugLog(@"downloadImages success %d",limageindex);
                         }
                         failure:^(NSError *error){
                             //DebugLog(@"downloadImages fail %d",limageindex);
                         }];
    }
    
    [photoUrlArray removeAllObjects];
    [photoUrlArray release];
    photoUrlArray = nil;
     DebugLog(@"perform animation oneTimeAnimation %d",oneTimeAnimation);
    if (oneTimeAnimation) {
        DebugLog(@"animation already started :)");
    }
    else
    {
        DebugLog(@"animation started ....");
        oneTimeAnimation = TRUE;
        [self performSelector:@selector(doAnimation) withObject:nil afterDelay:5];
    }
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    int noOfPartInCell = [self getNoOfParts:indexPath.row];
    int noOfPartsInCell = [self getNumberOfHalf:indexPath.row];
    if (noOfPartsInCell == ONE) {
        return ONE_HEIGHT;
    }
    else if (noOfPartsInCell == TWO)
    {
        return TWO_HEIGHT;
    }
    else if (noOfPartsInCell == THREE)
    {
        return THREE_HEIGHT;
    }
    else
    {
        return ONE_HEIGHT;
    }
}
// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   // DebugLog(@"numberOfRowsInSection %d",[_galleryDict.allKeys count]);
   // DebugLog(@"([_galleryDict.allKeys count] - 2) / 2) + 2 %d",(([_galleryDict.allKeys count] - 2) / 2) + 2);
   // return (([_galleryDict.allKeys count] - 2) / 2) + 2;
//    int totalIndex = [_galleryDict.allKeys count] - 1;;
//    numberOfRows = round(2.0f * (totalIndex/2)) / 2.0f;
    
    if (galleryDict.allKeys.count > 0) {
        return numberOfRows;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)ltableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    DebugLog(@"1)cellForRowAtIndexPath index %d ---",index);
//    DebugLog(@"1)cellForRowAtIndexPath index %d ---",index);
//    index = indexPath.row + (indexPath.row - 1) * 1;
//    DebugLog(@"2)cellForRowAtIndexPath index %d ---",index);

//    int noOfPartsInCell = [self getNoOfParts:indexPath.row];
    int noOfPartsInCell = [self getNumberOfHalf:indexPath.row];
    int startIndexInDict = [self getCurrentIndexInDict:indexPath.row];
    
    DebugLog(@"cellForRowAtIndexPath index = %d ==> parts %d ==> startindex dic Index %d",indexPath.row,noOfPartsInCell,startIndexInDict);

    if (noOfPartsInCell == ONE)
    {
        NSString *CellIdentifier    =   [NSString stringWithFormat:@"FullCell%d",indexPath.row];

        UIImageView *imageView1;
        UIImageView *backimageView1;

        UILabel *lblTitle;
        UIView *cellview;
        UITableViewCell *cell = [ltableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            DebugLog(@"got cell == nil %@",CellIdentifier);
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            cell.accessoryType = UITableViewCellAccessoryNone;
    
            cellview = [[UIView alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, ONE_HEIGHT)];
            cellview.backgroundColor = [UIColor clearColor];
            cellview.tag = 0;
//            cellview.layer.borderWidth = 2.0;
//            cellview.layer.borderColor = [UIColor blackColor].CGColor;
            cellview.layer.masksToBounds= YES;
            
            //Initialize Image View with tag 1.(Thumbnail Image)
            imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, cell.frame.size.width, ONE_HEIGHT-30)];
            imageView1.tag = 1;
            imageView1.contentMode = UIViewContentModeScaleToFill;
            imageView1.clipsToBounds = YES;
            
            backimageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, cell.frame.size.width, ONE_HEIGHT-30)];
            backimageView1.tag = 2;
            backimageView1.contentMode = UIViewContentModeScaleToFill;
            [cellview addSubview:backimageView1];
            [cellview addSubview:imageView1];
            [backimageView1 release];
            [imageView1 release];

            //Initialize Label with tag 2.(Title Label)
            lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x,ONE_HEIGHT-30,cell.frame.size.width,30)];
            lblTitle.tag = 3;
            lblTitle.font = HELVETICA_FONT(20.0);//[UIFont boldSystemFontOfSize:15];
            lblTitle.minimumFontSize = 15.0;
            lblTitle.adjustsFontSizeToFitWidth = YES;
            lblTitle.textAlignment = UITextAlignmentCenter;
            lblTitle.textColor = [UIColor whiteColor];
            lblTitle.backgroundColor = [UIColor darkGrayColor];//[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
            lblTitle.numberOfLines = 2;
//            lblTitle.layer.borderWidth = 2.0;
//            lblTitle.layer.borderColor = [UIColor blackColor].CGColor;
//            lblTitle.shadowColor = [UIColor blackColor];
//            lblTitle.shadowOffset = CGSizeMake(0, 4);
            [cellview addSubview:lblTitle];
            [lblTitle release];
            
            [cell.contentView addSubview:cellview];
            [cellview release];

        }
        
        DebugLog(@"====index %d ",startIndexInDict);

//        NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:nil ascending:YES selector:@selector(localizedCompare:)];
//        NSArray *sortedNumbers = [galleryDict.allKeys sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
//        DebugLog(@"sortedNumbers %@",sortedNumbers);
//        NSString *key = [[galleryDict.allKeys sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]] objectAtIndex:startIndexInDict];
        
//        NSArray *sortedNumbers = [galleryDict.allKeys sortedArrayUsingComparator:^(id firstObject, id secondObject) {
//            return [((NSString *)firstObject) compare:((NSString *)secondObject) options:NSNumericSearch];
//        }];
        NSString *key = [[galleryDict.allKeys sortedArrayUsingComparator:^(id firstObject, id secondObject) {
            return [((NSString *)firstObject) compare:((NSString *)secondObject) options:NSNumericSearch];
        }] objectAtIndex:startIndexInDict];
        
        NSArray *array = [galleryDict objectForKey:key];
        CarUtilGallery *galleryObj1 = (CarUtilGallery *)[array objectAtIndex:0];
        
        cellview = (UIView *)[cell viewWithTag:0];
        imageView1 = (UIImageView *)[cellview viewWithTag:1];
        [imageView1 setImageWithURL:[NSURL URLWithString:galleryObj1.thumbnail]
                         placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                  success:^(UIImage *image) {
                                      //DebugLog(@"success");
                                  }
                                  failure:^(NSError *error) {
                                      //DebugLog(@"write error %@", error);
                                  }];
        if (array.count > 1) {
            CarUtilGallery *galleryObj2 = (CarUtilGallery *)[array objectAtIndex:1];
            backimageView1 = (UIImageView *)[cellview viewWithTag:2];
            [backimageView1 setImageWithURL:[NSURL URLWithString:galleryObj2.thumbnail]
                       placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                success:^(UIImage *image) {
                                    //DebugLog(@"success");
                                }
                                failure:^(NSError *error) {
                                    //DebugLog(@"write error %@", error);
                                }];
            
            lblTitle = (UILabel *)[cellview viewWithTag:3];
            lblTitle.text = galleryObj1.caption;
        
            galleryObj2 = nil;
        }
        galleryObj1 = nil;

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    else if(noOfPartsInCell == TWO)
    {
        NSString *CellIdentifier1   =   [NSString stringWithFormat:@"TwoPartCell%d",indexPath.row];
        UIView *cellview1;
        UIView *cellview2;
        UIImageView *imageView1;
        UIImageView *imageView2;
        UIImageView *backimageView1;
        UIImageView *backimageView2;
        UILabel *lblTitle1;
        UILabel *lblTitle2;

        UITableViewCell *cell = [ltableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        if (cell == nil) {
            DebugLog(@"got cell == nil %@",CellIdentifier1);

            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1] autorelease];
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            cellview1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width/2, TWO_HEIGHT)];
            cellview1.backgroundColor = [UIColor clearColor];
            cellview1.tag = 0;
//            cellview1.layer.borderWidth = 2.0;
//            cellview1.layer.borderColor = [UIColor blackColor].CGColor;
            [cell.contentView addSubview:cellview1];

            //Initialize Image View with tag 1.(Thumbnail Image)
            imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cellview1.frame.size.width, TWO_HEIGHT-30)];
            imageView1.tag = 1;
            imageView1.contentMode = UIViewContentModeScaleToFill;

            
            //Initialize Image View with tag 1.(Thumbnail Image)
            backimageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cellview1.frame.size.width, TWO_HEIGHT-30)];
            backimageView1.tag = 2;
            backimageView1.contentMode = UIViewContentModeScaleToFill;
            [cellview1 addSubview:backimageView1];
            [cellview1 addSubview:imageView1];
            [imageView1 release];
            [backimageView1 release];
            
            //Initialize Label with tag 2.(Title Label)
            lblTitle1 = [[UILabel alloc] initWithFrame:CGRectMake(0,TWO_HEIGHT-30,cellview1.frame.size.width, 30)];
            lblTitle1.tag = 3;
            lblTitle1.font = [UIFont boldSystemFontOfSize:12];
            lblTitle1.textAlignment = UITextAlignmentCenter;
            lblTitle1.textColor = [UIColor whiteColor];
            lblTitle1.backgroundColor = [UIColor blackColor];//colorWithRed:0 green:0 blue:0 alpha:0.7];
            lblTitle1.numberOfLines = 2;
//            lblTitle1.layer.borderWidth = 2.0;
//            lblTitle1.layer.borderColor = [UIColor blackColor].CGColor;
            [cellview1 addSubview:lblTitle1];
            [lblTitle1 release];

            
            cellview2 = [[UIView alloc] initWithFrame:CGRectMake(cell.frame.size.width/2, 0, cell.frame.size.width/2, TWO_HEIGHT)];
            cellview2.backgroundColor = [UIColor clearColor];
            cellview2.tag = 4;
//            cellview2.layer.borderWidth = 2.0;
//            cellview2.layer.borderColor = [UIColor blackColor].CGColor;
            [cell.contentView addSubview:cellview2];
            
            imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cellview2.frame.size.width, TWO_HEIGHT-30)];
            imageView2.tag = 5;
            imageView2.contentMode = UIViewContentModeScaleToFill;


            
            backimageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cellview2.frame.size.width, TWO_HEIGHT-30)];
            backimageView2.tag = 6;
            backimageView2.contentMode = UIViewContentModeScaleToFill;
            [cellview2 addSubview:backimageView2];
            [cellview2 addSubview:imageView2];
            [imageView2 release];
            [backimageView2 release];

            //Initialize Label with tag 2.(Title Label)
            lblTitle2 = [[UILabel alloc] initWithFrame:CGRectMake(0,TWO_HEIGHT-30,cellview2.frame.size.width, 30)];
            lblTitle2.tag = 7;
            lblTitle2.font = [UIFont boldSystemFontOfSize:12];
            lblTitle2.textAlignment = UITextAlignmentCenter;
            lblTitle2.textColor = [UIColor whiteColor];
            lblTitle2.backgroundColor = [UIColor blackColor];//colorWithRed:0 green:0 blue:0 alpha:0.7];
            lblTitle2.numberOfLines = 2;
//            lblTitle2.layer.borderWidth = 2.0;
//            lblTitle2.layer.borderColor = [UIColor blackColor].CGColor;
            [cellview2 addSubview:lblTitle2];
            [lblTitle2 release];

            [cellview1 release];
            [cellview2 release];

        }
        DebugLog(@"1)index %d",startIndexInDict);
        NSString *key = [[galleryDict.allKeys sortedArrayUsingComparator:^(id firstObject, id secondObject) {
            return [((NSString *)firstObject) compare:((NSString *)secondObject) options:NSNumericSearch];
        }] objectAtIndex:startIndexInDict];
//        DebugLog(@"key %@",key);
        NSArray *array = [galleryDict objectForKey:key];
        
//        DebugLog(@"key %@ array %@",key,array);
        CarUtilGallery *galleryObj1temp1 = (CarUtilGallery *)[array objectAtIndex:0];
        cellview1 = (UIView *)[cell viewWithTag:0];
        imageView1 = (UIImageView *)[cellview1 viewWithTag:1];
        [imageView1 setImageWithURL:[NSURL URLWithString:galleryObj1temp1.thumbnail]
                     placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                              success:^(UIImage *image) {
                                  //DebugLog(@"success");
                              }
                              failure:^(NSError *error) {
                                  //DebugLog(@"write error %@", error);
                              }];
        
        CarUtilGallery *galleryObj1temp2 = (CarUtilGallery *)[array objectAtIndex:1];
        backimageView1 = (UIImageView *)[cellview1 viewWithTag:2];
        [backimageView1 setImageWithURL:[NSURL URLWithString:galleryObj1temp2.thumbnail]
                       placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                success:^(UIImage *image) {
                                    //DebugLog(@"success");
                                }
                                failure:^(NSError *error) {
                                    //DebugLog(@"write error %@", error);
                                }];
        
        lblTitle1 = (UILabel *)[cellview1 viewWithTag:3];
        lblTitle1.text = galleryObj1temp1.caption;
        
        galleryObj1temp1 = nil;
        galleryObj1temp2 = nil;
        
        DebugLog(@"2)index %d",startIndexInDict+1);
        
        NSString *key1 = [[galleryDict.allKeys sortedArrayUsingComparator:^(id firstObject, id secondObject) {
            return [((NSString *)firstObject) compare:((NSString *)secondObject) options:NSNumericSearch];
        }] objectAtIndex:startIndexInDict];
        
        NSArray *array1 = [galleryDict objectForKey:key1];
        
        cellview2 = (UIView *)[cell viewWithTag:4];
        
        CarUtilGallery *galleryObj2temp1 = (CarUtilGallery *)[array1 objectAtIndex:0];
        imageView2 = (UIImageView *)[cellview2 viewWithTag:5];
        [imageView2 setImageWithURL:[NSURL URLWithString:galleryObj2temp1.thumbnail]
                     placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                              success:^(UIImage *image) {
                                  //DebugLog(@"success");
                              }
                              failure:^(NSError *error) {
                                  //DebugLog(@"write error %@", error);
                              }];
        
        CarUtilGallery *galleryObj2temp2 = (CarUtilGallery *)[array1 objectAtIndex:1];
        backimageView2 = (UIImageView *)[cellview1 viewWithTag:6];
        [backimageView2 setImageWithURL:[NSURL URLWithString:galleryObj2temp2.thumbnail]
                       placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                success:^(UIImage *image) {
                                    //DebugLog(@"success");
                                }
                                failure:^(NSError *error) {
                                    //DebugLog(@"write error %@", error);
                                }];
        
      
        lblTitle2 = (UILabel *)[cellview2 viewWithTag:7];
        lblTitle2.text = galleryObj2temp1.caption;
        
        galleryObj2temp1 = nil;
        galleryObj2temp2 = nil;
        
        UITapGestureRecognizer *tap1=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapFirstAction:)];
        [tap1 setNumberOfTapsRequired:1];
        [cellview1 addGestureRecognizer:tap1];
        [tap1 release];

        UITapGestureRecognizer *tap2=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapSecondAction:)];
        [tap2 setNumberOfTapsRequired:1];
        [cellview2 addGestureRecognizer:tap2];
        [tap2 release];

        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        return cell;
    }
    else if(noOfPartsInCell == THREE)
    {
        NSString *CellIdentifier2   =   [NSString stringWithFormat:@"ThreePartCell%d",indexPath.row];

        UIView *cellview1;
        UIView *cellview2;
        UIView *cellview3;
        UIImageView *imageView1;
        UIImageView *imageView2;
        UIImageView *imageView3;
        UIImageView *backimageView1;
        UIImageView *backimageView2;
        UIImageView *backimageView3;
        UILabel *lblTitle1;
        UILabel *lblTitle2;
        UILabel *lblTitle3;

        UITableViewCell *cell = [ltableView dequeueReusableCellWithIdentifier:CellIdentifier2];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier2]autorelease];
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            cellview1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width/3, THREE_HEIGHT)];
            cellview1.backgroundColor = [UIColor clearColor];
            cellview1.tag = 0;
            cellview1.layer.borderWidth = 2.0;
            cellview1.layer.borderColor = [UIColor blackColor].CGColor;
            [cell.contentView addSubview:cellview1];
            
            //Initialize Image View with tag 1.(Thumbnail Image)
            imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cellview1.frame.size.width, THREE_HEIGHT-30)];
            imageView1.tag = 1;
            imageView1.contentMode = UIViewContentModeScaleToFill;
            [cellview1 addSubview:imageView1];
            
            //Initialize Image View with tag 1.(Thumbnail Image)
            backimageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cellview1.frame.size.width, THREE_HEIGHT-30)];
            backimageView1.tag = 2;
            backimageView1.contentMode = UIViewContentModeScaleToFill;
            [cellview1 addSubview:backimageView1];
            
            //Initialize Label with tag 2.(Title Label)
            lblTitle1 = [[UILabel alloc] initWithFrame:CGRectMake(0,THREE_HEIGHT-30,cellview1.frame.size.width, 30)];
            lblTitle1.tag = 3;
            lblTitle1.font = [UIFont boldSystemFontOfSize:10];
            lblTitle1.textAlignment = UITextAlignmentCenter;
            lblTitle1.textColor = [UIColor whiteColor];
            lblTitle1.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
            lblTitle1.numberOfLines = 2;
            lblTitle1.layer.borderWidth = 2.0;
            lblTitle1.layer.borderColor = [UIColor blackColor].CGColor;
            [cellview1 addSubview:lblTitle1];
            
            cellview2 = [[UIView alloc] initWithFrame:CGRectMake(cell.frame.size.width/3, 0, cell.frame.size.width/3, THREE_HEIGHT)];
            cellview2.backgroundColor = [UIColor clearColor];
            cellview2.tag = 4;
            cellview2.layer.borderWidth = 2.0;
            cellview2.layer.borderColor = [UIColor blackColor].CGColor;
            [cell.contentView addSubview:cellview2];
            
            imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cellview2.frame.size.width, THREE_HEIGHT-30)];
            imageView2.tag = 5;
            imageView2.contentMode = UIViewContentModeScaleToFill;
            [cellview2 addSubview:imageView2];
            
            backimageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cellview2.frame.size.width, THREE_HEIGHT-30)];
            backimageView2.tag = 6;
            backimageView2.contentMode = UIViewContentModeScaleToFill;
            [cellview2 addSubview:backimageView2];
            
            //Initialize Label with tag 2.(Title Label)
            lblTitle2 = [[UILabel alloc] initWithFrame:CGRectMake(0,THREE_HEIGHT-30,cellview2.frame.size.width, 30)];
            lblTitle2.tag = 7;
            lblTitle2.font = [UIFont boldSystemFontOfSize:10];
            lblTitle2.textAlignment = UITextAlignmentCenter;
            lblTitle2.textColor = [UIColor whiteColor];
            lblTitle2.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
            lblTitle2.numberOfLines = 2;
            lblTitle2.layer.borderWidth = 2.0;
            lblTitle2.layer.borderColor = [UIColor blackColor].CGColor;
            [cellview2 addSubview:lblTitle2];
            
            cellview3 = [[UIView alloc] initWithFrame:CGRectMake((cell.frame.size.width*2)/3, 0, cell.frame.size.width/3, THREE_HEIGHT)];
            cellview3.backgroundColor = [UIColor clearColor];
            cellview3.tag = 8;
            cellview3.layer.borderWidth = 2.0;
            cellview3.layer.borderColor = [UIColor blackColor].CGColor;
            [cell.contentView addSubview:cellview3];
            
            imageView3 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cellview3.frame.size.width, THREE_HEIGHT-30)];
            imageView3.tag = 9;
            imageView3.contentMode = UIViewContentModeScaleToFill;
            [cellview3 addSubview:imageView3];
            
            
            backimageView3 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cellview3.frame.size.width, THREE_HEIGHT-30)];
            backimageView3.tag = 10;
            backimageView3.contentMode = UIViewContentModeScaleToFill;
            [cellview3 addSubview:backimageView3];
            
            //Initialize Label with tag 2.(Title Label)
            lblTitle3 = [[UILabel alloc] initWithFrame:CGRectMake(0,THREE_HEIGHT-30,cellview3.frame.size.width,30)];
            lblTitle3.tag = 11;
            lblTitle3.font = [UIFont boldSystemFontOfSize:10];
            lblTitle3.textAlignment = UITextAlignmentCenter;
            lblTitle3.textColor = [UIColor whiteColor];
            lblTitle3.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
            lblTitle3.numberOfLines = 2;
            lblTitle3.layer.borderWidth = 2.0;
            lblTitle3.layer.borderColor = [UIColor blackColor].CGColor;
            [cellview3 addSubview:lblTitle3];
            
        }
        DebugLog(@"1)index %d",startIndexInDict);
        NSString *key = [galleryDict.allKeys objectAtIndex:startIndexInDict];
        //        DebugLog(@"key %@",key);
        NSArray *array = [galleryDict objectForKey:key];
        //        DebugLog(@"key %@ array %@",key,array);
        CarUtilGallery *galleryObj1 = (CarUtilGallery *)[array objectAtIndex:0];
        
        cellview1 = (UIView *)[cell viewWithTag:0];
        imageView1 = (UIImageView *)[cellview1 viewWithTag:1];
        [imageView1 setImageWithURL:[NSURL URLWithString:galleryObj1.ilink]
                   placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                            success:^(UIImage *image) {
                                //DebugLog(@"success");
                            }
                            failure:^(NSError *error) {
                                //DebugLog(@"write error %@", error);
                            }];
        lblTitle1 = (UILabel *)[cellview1 viewWithTag:3];
        lblTitle1.text = galleryObj1.caption;
        
        DebugLog(@"2)index %d",startIndexInDict+1);
        NSString *key1 = [galleryDict.allKeys objectAtIndex:startIndexInDict+1];
        NSArray *array1 = [galleryDict objectForKey:key1];
        CarUtilGallery *galleryObj2 = (CarUtilGallery *)[array1 objectAtIndex:0];
        
        cellview2 = (UIView *)[cell viewWithTag:4];
        imageView2 = (UIImageView *)[cellview2 viewWithTag:5];
        [imageView2 setImageWithURL:[NSURL URLWithString:galleryObj2.ilink]
                   placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                            success:^(UIImage *image) {
                                //DebugLog(@"success");
                            }
                            failure:^(NSError *error) {
                                //DebugLog(@"write error %@", error);
                            }];
        lblTitle2 = (UILabel *)[cellview2 viewWithTag:7];
        lblTitle2.text = galleryObj2.caption;
        
        DebugLog(@"3)index %d",startIndexInDict+2);
        NSString *key2 = [galleryDict.allKeys objectAtIndex:startIndexInDict+2];
        NSArray *array2 = [galleryDict objectForKey:key2];
        CarUtilGallery *galleryObj3 = (CarUtilGallery *)[array2 objectAtIndex:0];
        
        cellview3 = (UIView *)[cell viewWithTag:8];
        imageView3 = (UIImageView *)[cellview3 viewWithTag:9];
        [imageView3 setImageWithURL:[NSURL URLWithString:galleryObj3.ilink]
                   placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                            success:^(UIImage *image) {
                                //DebugLog(@"success");
                            }
                            failure:^(NSError *error) {
                                //DebugLog(@"write error %@", error);
                            }];
        
        lblTitle3 = (UILabel *)[cellview3 viewWithTag:11];
        lblTitle3.text = galleryObj3.caption;
        
        return cell;
    }
    return nil;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

   DebugLog(@"%d",indexPath.row);
    int startIndexInDict = [self getCurrentIndexInDict:indexPath.row];
//    NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:nil ascending:YES selector:@selector(localizedCompare:)];
//    NSString *key = [[galleryDict.allKeys sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]] objectAtIndex:startIndexInDict];
    
    NSString *key = [[galleryDict.allKeys sortedArrayUsingComparator:^(id firstObject, id secondObject) {
        return [((NSString *)firstObject) compare:((NSString *)secondObject) options:NSNumericSearch];
    }] objectAtIndex:startIndexInDict];
    
    NSArray *array1 = [galleryDict objectForKey:key];
    [self pushGalleryController:array1];
}
- (void)tapFirstAction:(id)sender {
    int startIndexInDict = 1;
    DebugLog(@"tapFirstAction %d",startIndexInDict);
    NSString *key = [[galleryDict.allKeys sortedArrayUsingComparator:^(id firstObject, id secondObject) {
        return [((NSString *)firstObject) compare:((NSString *)secondObject) options:NSNumericSearch];
    }] objectAtIndex:startIndexInDict];
    NSArray *array1 = [galleryDict objectForKey:key];
    [self pushGalleryController:array1];
}
- (void)tapSecondAction:(id)sender {
    int startIndexInDict = 2;
    DebugLog(@"tapSecondAction %d",startIndexInDict);
    NSString *key = [[galleryDict.allKeys sortedArrayUsingComparator:^(id firstObject, id secondObject) {
        return [((NSString *)firstObject) compare:((NSString *)secondObject) options:NSNumericSearch];
    }] objectAtIndex:startIndexInDict];
    NSArray *array1 = [galleryDict objectForKey:key];
    [self pushGalleryController:array1];
}

-(void)pushGalleryController:(NSArray *)lgalleryArray {
    
    NSMutableArray *photos = [[NSMutableArray alloc] init];
    MWPhoto *photo;
    
    for(int i = 0; i < [lgalleryArray count]; i++)
    {
        CarUtilGallery *tempgalleryObj = (CarUtilGallery *)[lgalleryArray objectAtIndex:i];
        photo = [MWPhoto photoWithURL:[NSURL URLWithString:tempgalleryObj.ilink]];
        photo.slink = tempgalleryObj.slink;
        photo.vlink = tempgalleryObj.vlink;
        photo.caption = [NSString stringWithFormat:@"%@\n%@",tempgalleryObj.caption,tempgalleryObj.description];
        [photos addObject:photo];
    }
    CarUtilGallery *tempgalleryObj = (CarUtilGallery *)[lgalleryArray objectAtIndex:0];
    NSString *sectionName = tempgalleryObj.caption;
    DebugLog(@"sectionName -%@-",sectionName);
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:sectionName,@"SectionName",nil];
    [Flurry logEvent:@"Home_Detail_Event" withParameters:params];
    
    
    self.photos = photos;
    
    // Create browser
	MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = YES;
    browser.wantsFullScreenLayout = YES;
    [browser setInitialPageIndex:0];
    [self.navigationController pushViewController:browser animated:YES];
    // Release
	[browser release];
	[photos release];
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _photos.count;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)lindex {
    if (lindex < _photos.count)
        return [_photos objectAtIndex:lindex];
    return nil;
}

- (NSString *)photoBrowser:(MWPhotoBrowser *)photoBrowser slinkAtIndex:(int)lindex {
    if (lindex < _photos.count)
    {
        MWPhoto *tempPhotoObj = (MWPhoto *)[_photos objectAtIndex:lindex];
        return tempPhotoObj.slink;
    }
    return nil;
}

- (NSString *)photoBrowser:(MWPhotoBrowser *)photoBrowser vlinkAtIndex:(int)lindex {
    if (lindex < _photos.count)
    {
        MWPhoto *tempPhotoObj = (MWPhoto *)[_photos objectAtIndex:lindex];
        return tempPhotoObj.vlink;
    }
    return @"";
}


-(int)getNumberOfHalf:(int)indexPathRow
{
//    switch (indexPathRow) {
//        case 0:
//        case 2:
//        case 3:
//            return ONE;
//            break;
//        case 1:
//            return TWO;
//            break;            
//        default:
//            break;
//    }
    return ONE;
}

-(int)getNoOfParts:(NSInteger)currentIndexPathRow
{
//    DebugLog(@"getNoOfParts %d",currentindex);
//    if (currentindex & 1) {
//        DebugLog(@"odd");
//    } else {
//        DebugLog(@"even");
//    }
    
    int lastindex = galleryDict.allKeys.count-1;
    if (currentIndexPathRow == numberOfRows-1) {
        int lStartIndex = [self getCurrentIndexInDict:currentIndexPathRow];
        if(lStartIndex == lastindex - 1)
            {
                return TWO;
            }
            else if(lStartIndex == lastindex - 2)
            {
                return THREE;
            }
            else
            {
                return ONE;
            }
    }
    else
    {
        if (currentIndexPathRow == 0 || currentIndexPathRow == lastindex) {
            return ONE;
        }
        else if(currentIndexPathRow & 1) // odd
        {
            return THREE;
        }
        else
        {
            return TWO;
        }
    }
}

-(int)getCurrentIndexInDict:(int)currentIndexPathRow
{
    int lastindex = galleryDict.allKeys.count-1;
    if (currentIndexPathRow == 0 || currentIndexPathRow == lastindex) {
        return currentIndexPathRow;
    }
    else {
        int total = 0;
        for (int i = currentIndexPathRow - 1; i >= 0; i --) {
           // total += [self getNoOfParts:i];
            total += [self getNumberOfHalf:i];
        }
        DebugLog(@"total %d",total);
        return total;
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self doAnimation];
}

#pragma animations callBacks
-(void)doAnimation
{
    
    DebugLog(@"doAnimation");
    NSArray *visibleCells = [tableView indexPathsForVisibleRows];//[tableView visibleCells];
    DebugLog(@"indexPaths %@",visibleCells);
    NSIndexPath *indexPath;
    for (int i = 0; i < visibleCells.count; i++) {
        DebugLog(@"i ==> %d",i);
        //[self flipNextCell:i];
        indexPath = (NSIndexPath *)[visibleCells objectAtIndex:i];
        DebugLog(@"indexPath ==> %d",indexPath.row);
        [self performAnimations:indexPath.row];
    }
    indexPath = nil;
    DebugLog(@"animatedIndexArray %@",animatedIndexArray);
}

-(void)performAnimations:(int)cellIndex
{
    DebugLog(@"cellIndex %d numberOfRows %d",cellIndex,numberOfRows);
    if (cellIndex == numberOfRows) {
        return;
    }
    DebugLog(@"Animation : %d ==> %@ ",cellIndex,[animatedIndexArray objectAtIndex:cellIndex]);
    
    if ([[animatedIndexArray objectAtIndex:cellIndex] boolValue] == YES) {
        DebugLog(@"cellIndex %d already animated",cellIndex);
        return;
    }
    // int noOfPartsInCell = [self getNumberOfHalf:cellIndex];
    int startIndexInDict = [self getCurrentIndexInDict:cellIndex];
    
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:cellIndex inSection:0]];
    NSString *key = [[galleryDict.allKeys sortedArrayUsingComparator:^(id firstObject, id secondObject) {
        return [((NSString *)firstObject) compare:((NSString *)secondObject) options:NSNumericSearch];
    }] objectAtIndex:startIndexInDict];
    
    NSArray *arrayValue = [galleryDict objectForKey:key];
    UIView *cellview = (UIView *)[cell viewWithTag:0];
    [animatedIndexArray replaceObjectAtIndex:cellIndex withObject:[NSNumber numberWithBool:YES]];
    //[animatedIndexArray setObject:[NSNumber numberWithBool:YES] atIndexedSubscript:cellIndex];
    DebugLog(@"animation started %d",cellIndex);
    
    if (arrayValue.count == 1) {
        [self singleAnimation:cellIndex view:cellview object:[arrayValue objectAtIndex:0]];
    }
    else
    {
        int animationtype = [self getAnimationType:cellIndex+1];
        if (animationtype == FLIPFOLD_ANIMATION) {
            [self flipAnimation:cellIndex toggle:0 nextIndex:1 array:arrayValue view:cellview type:0];
        }
        else if (animationtype == SLIDE_ANIMATION) {
            UIImageView *backImageView = (UIImageView *)[cellview viewWithTag:cellview.tag+2];
            [backImageView setHidden:YES];
            [self slideAnimation:cellIndex toggle:0 nextindex:1 array:arrayValue view:cellview type:0];
        }
        else if (animationtype == DISSOLVE_ANIMATION) {
            [self dissolveAnimation:cellIndex toggle:0 nextindex:1 array:arrayValue view:cellview];
        }
        else if (animationtype == FLIPFROMCENTER_ANIMATION) {
            UIImageView *backImageView = (UIImageView *)[cellview viewWithTag:cellview.tag+2];
            [backImageView setBackgroundColor:[UIColor blackColor]];
            [self flipFromCenterAnimation:cellIndex toggle:0 nextindex:1 array:arrayValue view:cellview type:0 ];
        }else{
            UIImageView *backImageView = (UIImageView *)[cellview viewWithTag:cellview.tag+2];
            [backImageView setHidden:YES];
            [self slideAnimation:cellIndex toggle:0 nextindex:1 array:arrayValue view:cellview type:0];
        }
        //        if (cellIndex == 0 || cellIndex == 4 || cellIndex == 8 || cellIndex == 12) {
        //            [self flipAnimation:cellIndex toggle:0 nextIndex:1 array:arrayValue view:cellview type:0];
        //        }
        //        else if (cellIndex == 1 || cellIndex == 5 || cellIndex == 9) {
        //            UIImageView *backImageView = (UIImageView *)[cellview viewWithTag:cellview.tag+2];
        //            [backImageView setHidden:YES];
        //            [self slideAnimation:cellIndex toggle:0 nextindex:1 array:arrayValue view:cellview type:0];
        //        }
        //        else if (cellIndex == 2 || cellIndex == 6 || cellIndex == 10) {
        //            [self dissolveAnimation:cellIndex toggle:0 nextindex:1 array:arrayValue view:cellview];
        //        }
        //        else if (cellIndex == 3 || cellIndex == 7 || cellIndex == 11) {
        //            UIImageView *backImageView = (UIImageView *)[cellview viewWithTag:cellview.tag+2];
        //            [backImageView setBackgroundColor:[UIColor blackColor]];
        //            [self flipFromCenterAnimation:cellIndex toggle:0 nextindex:1 array:arrayValue view:cellview type:0 ];
        //        }else{
        //            UIImageView *backImageView = (UIImageView *)[cellview viewWithTag:cellview.tag+2];
        //            [backImageView setHidden:YES];
        //            [self slideAnimation:cellIndex toggle:0 nextindex:1 array:arrayValue view:cellview type:0];
        //        }
    }
}
-(int)getIndexValue:(int)value
{
    int result = value - 4;
    if (result > 4) {
        result = [self getIndexValue:result];
    }
    return result;
}
-(int)getAnimationType:(int)cellIndex
{
    if (cellIndex <= 4) {
        return cellIndex;
    }
    
    int result = [self getIndexValue:cellIndex];
    
    switch (result) {
        case 1:
            return FLIPFOLD_ANIMATION;
            break;
        case 2:
            return SLIDE_ANIMATION;
            break;
        case 3:
            return DISSOLVE_ANIMATION;
            break;
        case 4:
            return FLIPFROMCENTER_ANIMATION;
            break;
        default:
            break;
    }
    return SLIDE_ANIMATION;
}
-(void)singleAnimation:(int)cellIndex view:(UIView *)lview object:(CarUtilGallery *)tempgalleryObj
{
    DebugLog(@"singleAnimation started for index %d",cellIndex);
    if (stopAnimation == YES) {
        [animatedIndexArray replaceObjectAtIndex:cellIndex withObject:[NSNumber numberWithBool:NO]];
        return;
    }
    UILabel *lblTitle = (UILabel *)[lview viewWithTag:lview.tag+3];
    UIImageView *previousImageView  = (UIImageView *)[lview viewWithTag:lview.tag+1];
    
    [UIView animateWithDuration:2.0 delay:1.5 options:UIViewAnimationOptionTransitionCrossDissolve |UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionRepeat animations:^(void) {
        previousImageView.alpha = !previousImageView.alpha;
        if (![tempgalleryObj.caption isEqualToString:@""]) {
            lblTitle.text = tempgalleryObj.caption;
        }
    }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:2.0 delay:1.5 options:UIViewAnimationOptionTransitionCrossDissolve |UIViewAnimationOptionAllowUserInteraction  animations:^(void) {
                             previousImageView.alpha = !previousImageView.alpha;
                         }
                                          completion:^(BOOL finished){
                                              
                                          }];
                     }];
    
}

-(void)flipAnimation:(int)cellIndex toggle:(int)toggleStatus nextIndex:(int)lnextIndex array:(NSArray *)larray view:(UIView *)lview type:(int)animationtype
{
    if (stopAnimation == YES) {
        [animatedIndexArray replaceObjectAtIndex:cellIndex withObject:[NSNumber numberWithBool:NO]];
        return;
    }
    // DebugLog(@"array %@",larray);
    MPFlipStyle flipStyle = MPFlipStyleOrientationVertical;
    switch (animationtype) {
        case 0:
            flipStyle = MPFlipStyleOrientationVertical;
            break;
        case 1:
            flipStyle = MPFlipStyleDirectionBackward;
            break;
        case 2:
            flipStyle = MPFlipStylePerspectiveReverse;
            break;
        default:
            flipStyle = MPFlipStyleFlipDirectionBit(flipStyle);
            break;
    }
    
    DebugLog(@"fliping cellIndex %d",cellIndex);
    
    CarUtilGallery *tempgalleryObj = (CarUtilGallery *)[larray objectAtIndex:lnextIndex];
    UIImageView *previousImageView;
    UIImageView *nextImageView;
    UILabel *lbl = (UILabel *)[lview viewWithTag:lview.tag+3];
    
    if (toggleStatus) {
        previousImageView  = (UIImageView *)[lview viewWithTag:lview.tag+2];
        nextImageView  = (UIImageView *)[lview viewWithTag:lview.tag+1];
        toggleStatus = 0;
    }
    else
    {
        previousImageView  = (UIImageView *)[lview viewWithTag:lview.tag+1];
        nextImageView  = (UIImageView *)[lview viewWithTag:lview.tag+2];
        toggleStatus = 1;
    }
    previousImageView.alpha = 1.0;
    nextImageView.alpha = 1.0;
    [nextImageView setImageWithURL:[NSURL URLWithString:tempgalleryObj.thumbnail]
                  placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                           success:^(UIImage *image) {
                               if (![tempgalleryObj.caption isEqualToString:@""]) {
                                   lbl.text = tempgalleryObj.caption;
                               }
                               
                               // DebugLog(@"flip success lnextIndex %d",lnextIndex);
                               [MPFlipTransition transitionFromView:previousImageView
                                                             toView:nextImageView
                                                           duration:FLIP_DURATION
                                                              style:MPFlipStyleFlipDirectionBit(flipStyle)
                                                   transitionAction:MPTransitionActionShowHide
                                                         completion:^(BOOL finished) {
                                                             if (stopAnimation) {
                                                                 DebugLog(@"animation stopped %d",cellIndex);
                                                                 //                                                                 [animatedIndexArray setObject:[NSNumber numberWithBool:NO] atIndexedSubscript:cellIndex];
                                                                 [animatedIndexArray replaceObjectAtIndex:cellIndex withObject:[NSNumber numberWithBool:NO]];
                                                                 
                                                             }
                                                             else
                                                             {
                                                                 //DebugLog(@"array : %d %d %d",cellIndex,toggleStatus,lnextIndex);
                                                                 
                                                                 NSMutableArray *passarray = [[NSMutableArray alloc]init];
                                                                 [passarray addObject:[NSNumber numberWithInt:cellIndex]];
                                                                 [passarray addObject:[NSNumber numberWithInt:toggleStatus]];
                                                                 //if (lnextIndex == 3 || lnextIndex == larray.count-1) {
                                                                 if (lnextIndex == larray.count-1) {
                                                                     // [self flipAnimation:cellIndex toggle:toggleStatus nextIndex:0 array:larray view:lview];
                                                                     [passarray addObject:[NSNumber numberWithInt:0]];
                                                                     
                                                                 }
                                                                 else
                                                                 {
                                                                     //   [self flipAnimation:cellIndex toggle:toggleStatus nextIndex:lnextIndex+1 array:larray view:lview];
                                                                     [passarray addObject:[NSNumber numberWithInt:lnextIndex+1]];
                                                                     
                                                                 }
                                                                 [passarray addObject:larray];
                                                                 [passarray addObject:lview];
                                                                 [self performSelector:@selector(delayAndDoRandomAnimationFunction:)
                                                                            withObject:passarray
                                                                            afterDelay:DELAY_DURATION];
                                                             }
                                                         }];
                               
                           }
                           failure:^(NSError *error) {
                               DebugLog(@"flip error lnextIndex %d",lnextIndex);
                           }];
    
    tempgalleryObj = nil;
}


-(void)delayAndDoRandomAnimationFunction:(NSMutableArray *)array
{
    //   DebugLog(@"delay : %@",array);
    int cellIndex = [[array objectAtIndex:0] intValue];
    int toggle   = [[array objectAtIndex:1] intValue];
    int nextIndex = [[array objectAtIndex:2]  intValue];
    NSArray *passarray = [NSArray arrayWithArray:[array objectAtIndex:3]];
    UIView *cellview = [array objectAtIndex:4];
    
    // int random = arc4random() % 1 + 1;
    
    DebugLog(@"random %d",random);
    int preanimationtype = [self getAnimationType:cellIndex+1];
    if (preanimationtype == FLIPFOLD_ANIMATION) {
        int animationtype = rand() % 4;
        DebugLog(@"flipAnimation animationtype %d",animationtype);
        [self flipAnimation:cellIndex toggle:toggle nextIndex:nextIndex array:passarray view:cellview type:animationtype];
    }
    else if (preanimationtype == SLIDE_ANIMATION) {
        int animationtype = rand() % 4;
        DebugLog(@"slideAnimation animationtype %d",animationtype);
        [self slideAnimation:cellIndex toggle:toggle nextindex:nextIndex array:passarray view:cellview type:animationtype];
    }
    else if (preanimationtype == DISSOLVE_ANIMATION) {
        [self dissolveAnimation:cellIndex toggle:toggle nextindex:nextIndex array:passarray view:cellview];
    }
    else if (preanimationtype == FLIPFROMCENTER_ANIMATION) {
        int animationtype = rand() % 4;
        DebugLog(@"flipFromCenterAnimation animationtype %d",animationtype);
        [self flipFromCenterAnimation:cellIndex toggle:toggle nextindex:nextIndex array:passarray view:cellview type:animationtype];
    }else{
        int animationtype = rand() % 4;
        DebugLog(@"slideAnimation animationtype %d",animationtype);
        [self slideAnimation:cellIndex toggle:toggle nextindex:nextIndex array:passarray view:cellview type:animationtype];
    }
    
    
    //    if (cellIndex == 0 || cellIndex == 4 || cellIndex == 8 || cellIndex == 12) {
    //        int animationtype = rand() % 4;
    //        DebugLog(@"flipAnimation animationtype %d",animationtype);
    //        [self flipAnimation:cellIndex toggle:toggle nextIndex:nextIndex array:passarray view:cellview type:animationtype];
    //    }
    //    else if (cellIndex == 1 || cellIndex == 5 || cellIndex == 9) {
    //        int animationtype = rand() % 4;
    //        DebugLog(@"slideAnimation animationtype %d",animationtype);
    //        [self slideAnimation:cellIndex toggle:toggle nextindex:nextIndex array:passarray view:cellview type:animationtype];
    //    }
    //    else if (cellIndex == 2 || cellIndex == 6 || cellIndex == 10) {
    //        [self dissolveAnimation:cellIndex toggle:toggle nextindex:nextIndex array:passarray view:cellview];
    //    }
    //    else if (cellIndex == 3 || cellIndex == 7 || cellIndex == 11) {
    //        int animationtype = rand() % 4;
    //        DebugLog(@"flipFromCenterAnimation animationtype %d",animationtype);
    //        [self flipFromCenterAnimation:cellIndex toggle:toggle nextindex:nextIndex array:passarray view:cellview type:animationtype];
    //    }else{
    //        int animationtype = rand() % 4;
    //        DebugLog(@"slideAnimation animationtype %d",animationtype);
    //        [self slideAnimation:cellIndex toggle:toggle nextindex:nextIndex array:passarray view:cellview type:animationtype];
    //    }
}


//-(void)delayAnimationFunction:(NSMutableArray *)array
//{
//    //   DebugLog(@"delay : %@",array);
//    int cellIndex = [[array objectAtIndex:0] intValue];
//    int toggle   = [[array objectAtIndex:1] intValue];
//    int nextIndex = [[array objectAtIndex:2]  intValue];
//    NSArray *passarray = [NSArray arrayWithArray:[array objectAtIndex:3]];
//    UIView *cellview = [array objectAtIndex:4];
//
//    if (cellIndex == 0) {
//        [self flipAnimation:cellIndex toggle:toggle nextIndex:nextIndex array:passarray view:cellview];
//    }
//    else if (cellIndex == 1 || cellIndex == 2 || cellIndex == 4) {
//        [self slideAnimation:cellIndex nextindex:nextIndex array:passarray view:cellview];
//    }
//    else if (cellIndex == 3) {
//        [self dissolveAnimation:cellIndex toggle:toggle nextindex:nextIndex array:passarray view:cellview];
//    }
//}

-(void)dissolveAnimation:(int)cellIndex toggle:(int)toggleStatus nextindex:(int)lnextIndex array:(NSArray *)larray view:(UIView *)lview
{
    if (stopAnimation == YES) {
        [animatedIndexArray replaceObjectAtIndex:cellIndex withObject:[NSNumber numberWithBool:NO]];
        return;
    }
    CarUtilGallery *tempgalleryObj = (CarUtilGallery *)[larray objectAtIndex:lnextIndex];
    UIImageView *previousImageView;
    UIImageView *nextImageView;
    UILabel *lblTitle = (UILabel *)[lview viewWithTag:lview.tag+3];
    
    if (toggleStatus) {
        previousImageView  = (UIImageView *)[lview viewWithTag:lview.tag+2];
        nextImageView  = (UIImageView *)[lview viewWithTag:lview.tag+1];
        toggleStatus = 0;
    }
    else
    {
        previousImageView  = (UIImageView *)[lview viewWithTag:lview.tag+1];
        nextImageView  = (UIImageView *)[lview viewWithTag:lview.tag+2];
        toggleStatus = 1;
    }
    
    [UIView  transitionWithView:previousImageView duration:FLIP_DURATION  options:UIViewAnimationOptionTransitionNone |UIViewAnimationOptionAllowUserInteraction
                     animations:^(void) {
                         previousImageView.alpha = 0.0;
                         [UIView  transitionWithView:nextImageView duration:3.0  options:UIViewAnimationOptionTransitionNone |UIViewAnimationOptionAllowUserInteraction
                                          animations:^(void) {
                                              nextImageView.alpha = 1.0;
                                              [nextImageView setImageWithURL:[NSURL URLWithString:tempgalleryObj.thumbnail]
                                                            placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                                                     success:^(UIImage *image) {
                                                                         //DebugLog(@"success");
                                                                         if (![tempgalleryObj.caption isEqualToString:@""]) {
                                                                             lblTitle.text = tempgalleryObj.caption;
                                                                         }
                                                                     }
                                                                     failure:^(NSError *error) {
                                                                         //DebugLog(@"write error %@", error);
                                                                     }];
                                          }
                                          completion:^(BOOL finished) {
                                              if (stopAnimation) {
                                                  DebugLog(@"fadeInFadeoutanimation stopped %d",cellIndex);
                                                  //[animatedIndexArray setObject:[NSNumber numberWithBool:NO] atIndexedSubscript:cellIndex];
                                                  [animatedIndexArray replaceObjectAtIndex:cellIndex withObject:[NSNumber numberWithBool:NO]];
                                              }
                                              else
                                              {
                                                  DebugLog(@"array : %d %d %d",cellIndex,toggleStatus,lnextIndex);
                                                  
                                                  NSMutableArray *passarray = [[NSMutableArray alloc]init];
                                                  [passarray addObject:[NSNumber numberWithInt:cellIndex]];
                                                  [passarray addObject:[NSNumber numberWithInt:toggleStatus]];
                                                  //if (lnextIndex == 3 || lnextIndex == larray.count-1) {
                                                  if (lnextIndex == larray.count-1) {
                                                      [passarray addObject:[NSNumber numberWithInt:0]];
                                                      
                                                  }
                                                  else
                                                  {
                                                      [passarray addObject:[NSNumber numberWithInt:lnextIndex+1]];
                                                      
                                                  }
                                                  [passarray addObject:larray];
                                                  [passarray addObject:lview];
                                                  [self performSelector:@selector(delayAndDoRandomAnimationFunction:)
                                                             withObject:passarray
                                                             afterDelay:DELAY_DURATION];
                                              }
                                          }];
                         
                     }
                     completion:^(BOOL finished){
                     }];
}
-(void)slideAnimation:(int)cellIndex toggle:(int)toggleStatus nextindex:(int)lnextIndex array:(NSArray *)larray view:(UIView *)lview type:(int)animationType
{
    if (stopAnimation == YES) {
        [animatedIndexArray replaceObjectAtIndex:cellIndex withObject:[NSNumber numberWithBool:NO]];
        return;
    }
    CarUtilGallery *tempgalleryObj = (CarUtilGallery *)[larray objectAtIndex:lnextIndex];
    UIImageView *previousImageView;
    previousImageView  = (UIImageView *)[lview viewWithTag:lview.tag+1];
    
    //    if (toggleStatus) {
    //        previousImageView  = (UIImageView *)[lview viewWithTag:lview.tag+2];
    //        nextImageView  = (UIImageView *)[lview viewWithTag:lview.tag+1];
    //        toggleStatus = 0;
    //    }
    //    else
    //    {
    //        previousImageView  = (UIImageView *)[lview viewWithTag:lview.tag+1];
    //        nextImageView  = (UIImageView *)[lview viewWithTag:lview.tag+2];
    //        toggleStatus = 1;
    //    }
    NSMutableArray *passarray = [[NSMutableArray alloc]init];
    [passarray addObject:[NSNumber numberWithInt:cellIndex]];
    [passarray addObject:[NSNumber numberWithInt:toggleStatus]];
    if (lnextIndex == larray.count-1) {
        [passarray addObject:[NSNumber numberWithInt:0]];
        
    }
    else
    {
        [passarray addObject:[NSNumber numberWithInt:lnextIndex+1]];
        
    }
    [passarray addObject:larray];
    [passarray addObject:lview];
    
    UILabel *lblTitle = (UILabel *)[lview viewWithTag:lview.tag+3];
    
    CATransition *slideAnimation = [CATransition animation];
    [slideAnimation setDuration:FLIP_DURATION];
    [slideAnimation setType:kCATransitionPush];
    [slideAnimation setDelegate:self];
    slideAnimation.removedOnCompletion = NO;
    [slideAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    
    if (animationType == 0) {
        [slideAnimation setSubtype:kCATransitionFromLeft];
    }
    else if (animationType == 1) {
        [slideAnimation setSubtype:kCATransitionFromRight];
    }
    else if (animationType == 2) {
        [slideAnimation setSubtype:kCATransitionFromTop];
    }
    else{
        [slideAnimation setSubtype:kCATransitionFromBottom];
    }
    [slideAnimation setValue:@"slideCommonAnimation" forKey:@"slideAnimation"];
    [slideAnimation setValue:passarray forKey:@"slideCommonAnimation"];
    
    [previousImageView setImageWithURL:[NSURL URLWithString:tempgalleryObj.thumbnail]
                      placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                               success:^(UIImage *image) {
                                   //DebugLog(@"success");
                                   if (![tempgalleryObj.caption isEqualToString:@""]) {
                                       lblTitle.text = tempgalleryObj.caption;
                                   }
                               }
                               failure:^(NSError *error) {
                                   //DebugLog(@"write error %@", error);
                               }];
    [[previousImageView layer] addAnimation:slideAnimation forKey:@"slideAnimation"];
}

- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag
{
    DebugLog(@"theAnimation %@",theAnimation);
    
    NSString* value = [theAnimation valueForKey:@"slideAnimation"];
    
    if ([value isEqualToString:@"slideCommonAnimation"]){
        
        NSMutableArray *passarray = [theAnimation valueForKey:@"slideCommonAnimation"];
        int cellIndex = [[passarray objectAtIndex:0] intValue];
        if (stopAnimation) {
            DebugLog(@"slideCommonAnimation stopped %d",cellIndex);
            //[animatedIndexArray setObject:[NSNumber numberWithBool:NO] atIndexedSubscript:cellIndex];
            [animatedIndexArray replaceObjectAtIndex:cellIndex withObject:[NSNumber numberWithBool:NO]];
        }
        else
        {
            [self performSelector:@selector(delayAndDoRandomAnimationFunction:)
                       withObject:passarray
                       afterDelay:DELAY_DURATION];
        }
        passarray = nil;
    }
}

-(void)flipFromCenterAnimation:(int)cellIndex toggle:(int)toggleStatus nextindex:(int)lnextIndex array:(NSArray *)larray view:(UIView *)lview type:(int)animationType
{
    if (stopAnimation == YES) {
        [animatedIndexArray replaceObjectAtIndex:cellIndex withObject:[NSNumber numberWithBool:NO]];
        return;
    }
    CarUtilGallery *tempgalleryObj = (CarUtilGallery *)[larray objectAtIndex:lnextIndex];
    UIImageView *previousImageView;
    UIImageView *nextImageView;
    UILabel *lblTitle = (UILabel *)[lview viewWithTag:lview.tag+3];
    
    if (toggleStatus) {
        previousImageView  = (UIImageView *)[lview viewWithTag:lview.tag+2];
        nextImageView  = (UIImageView *)[lview viewWithTag:lview.tag+1];
        toggleStatus = 0;
    }
    else
    {
        previousImageView  = (UIImageView *)[lview viewWithTag:lview.tag+1];
        nextImageView  = (UIImageView *)[lview viewWithTag:lview.tag+2];
        toggleStatus = 1;
    }
    UIViewAnimationOptions animationOptions = UIViewAnimationOptionTransitionFlipFromLeft |UIViewAnimationOptionAllowUserInteraction;
    
    if (animationType == 0) {
        animationOptions = UIViewAnimationOptionTransitionFlipFromRight |UIViewAnimationOptionAllowUserInteraction;
    }
    else if (animationType == 1) {
        animationOptions = UIViewAnimationOptionTransitionFlipFromTop |UIViewAnimationOptionAllowUserInteraction;
    }
    else if (animationType == 2) {
        animationOptions = UIViewAnimationOptionTransitionFlipFromBottom |UIViewAnimationOptionAllowUserInteraction;
    }
    else{
        animationOptions = UIViewAnimationOptionTransitionFlipFromLeft |UIViewAnimationOptionAllowUserInteraction;
    }
    
    [UIView  transitionWithView:previousImageView duration:FLIP_DURATION  options:animationOptions
                     animations:^(void) {
                         previousImageView.alpha = 0.0;
                         [UIView  transitionWithView:nextImageView duration:3.0  options:animationOptions
                                          animations:^(void) {
                                              nextImageView.alpha = 1.0;
                                              [nextImageView setImageWithURL:[NSURL URLWithString:tempgalleryObj.thumbnail]
                                                            placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                                                                     success:^(UIImage *image) {
                                                                         //DebugLog(@"success");
                                                                         if (![tempgalleryObj.caption isEqualToString:@""]) {
                                                                             lblTitle.text = tempgalleryObj.caption;
                                                                         }
                                                                     }
                                                                     failure:^(NSError *error) {
                                                                         //DebugLog(@"write error %@", error);
                                                                     }];
                                          }
                                          completion:^(BOOL finished) {
                                              if (stopAnimation) {
                                                  DebugLog(@"fadeInFadeoutanimation stopped %d",cellIndex);
                                                  //[animatedIndexArray setObject:[NSNumber numberWithBool:NO] atIndexedSubscript:cellIndex];
                                                  [animatedIndexArray replaceObjectAtIndex:cellIndex withObject:[NSNumber numberWithBool:NO]];
                                              }
                                              else
                                              {
                                                  DebugLog(@"array : %d %d %d",cellIndex,toggleStatus,lnextIndex);
                                                  
                                                  NSMutableArray *passarray = [[NSMutableArray alloc]init];
                                                  [passarray addObject:[NSNumber numberWithInt:cellIndex]];
                                                  [passarray addObject:[NSNumber numberWithInt:toggleStatus]];
                                                  //if (lnextIndex == 3 || lnextIndex == larray.count-1) {
                                                  if (lnextIndex == larray.count-1) {
                                                      [passarray addObject:[NSNumber numberWithInt:0]];
                                                      
                                                  }
                                                  else
                                                  {
                                                      [passarray addObject:[NSNumber numberWithInt:lnextIndex+1]];
                                                      
                                                  }
                                                  [passarray addObject:larray];
                                                  [passarray addObject:lview];
                                                  [self performSelector:@selector(delayAndDoRandomAnimationFunction:)
                                                             withObject:passarray
                                                             afterDelay:DELAY_DURATION];
                                              }
                                          }];
                         
                     }
                     completion:^(BOOL finished){
                     }];
}

#pragma setNavigationBar barbuttons callbacks
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
        {
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            UIBarButtonItem *sideMenuBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                                                                 target:self.navigationController.sideMenu
                                                                                 action:@selector(toggleRightSideMenu)];
            self.navigationItem.rightBarButtonItem = sideMenuBarButton;
            [sideMenuBarButton release];
        }
            break;
        case MFSideMenuStateLeftMenuOpen:
        {
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
        }
            break;
        case MFSideMenuStateRightMenuOpen:
        {
            UIBarButtonItem *sideMenuBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                                                                 target:self.navigationController.sideMenu
                                                                                 action:@selector(toggleRightSideMenu)];
            self.navigationItem.rightBarButtonItem = sideMenuBarButton;
            [sideMenuBarButton release];
        }
            break;
    }
}
- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
- (void)dealloc {
    DebugLog(@"gallerygrid dealloc");
    [_photos release];
    [galleryDict release];
    socialLink = nil;
    [tableView release];
    [super dealloc];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewDidUnload {
    [self setTableView:nil];
    [super viewDidUnload];
}
@end
