//
//  ImageViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 03/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "ImageViewController.h"
#import "CarUtilAppDelegate.h"
//#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+WebCache.h"

#define HOWTOVIDEO_1 @"http://www.youtube.com/watch?v=OJwVwvEHh3Q"
#define HOWTOVIDEO_2 @"http://www.youtube.com/watch?v=tMzviqD5cCs"

@interface ImageViewController ()

@end

@implementation ImageViewController
@synthesize imgView;
@synthesize imageUrl;
@synthesize videoUrl;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)setScrollType:(int)ltype
{
    scrollType = ltype;
}

- (id)initWithPageNumber:(int)page
{
    if (self = [super initWithNibName:@"ImageViewController" bundle:nil])
    {
        pageNumber = page;
        DebugLog(@"page init = %d",page + 1);
    }
    return self;
}
- (id)initWithImageUrl:(NSString *)limageUrl videourl:(NSString *)lvideoUrl
{
    if (self = [super initWithNibName:@"ImageViewController" bundle:nil])
    {
        imageUrl = limageUrl;
        videoUrl = lvideoUrl;
        DebugLog(@"imageUrl init = -%@- video -%@-",imageUrl,videoUrl);
    }
    return self;
}
-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    
    if(videoUrl != nil && ![videoUrl isEqualToString:@""]) {
        UITouch *touch = [touches anyObject];
        CGPoint pos = [touch locationInView: [UIApplication sharedApplication].keyWindow];
        
        if((pos.x > 42 &&  pos.x < 280) && (pos.y > 142 &&  pos.y < 286))
        {
            //DebugLog(@"Position of touch: %.3f, %.3f", pos.x, pos.y);
            [self showAlert];
        }
    }
}

-(void)showAlert
{
    
    CarUtilAppDelegate *appDelegate = (CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.networkavailable == YES) {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"This will open YouTube appication. Do you want to play this video ?"
                                                          delegate: self
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"NO", @"YES", nil ];
        [alert setTag:1];
        [ alert show ];
        [alert release];
    } else {
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: @"No network available. Please check your internet connection and try again."
                                                          delegate: self
                                                 cancelButtonTitle: @"OK"
                                                 otherButtonTitles: nil, nil ];
        [ alert show ];
        [alert release];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag == 1)
    {
        if (buttonIndex == 1) {
            [self openVideoinYoutube];
            return;
        }
    }
    
}

-(void) openVideoinYoutube
{
    NSURL *url = [NSURL URLWithString:videoUrl];
    [[UIApplication sharedApplication] openURL:url];
    
    //    DebugLog(@"%d",pageNumber);
    //    switch (pageNumber) {
    //        case 0:
    //        {
    //            NSURL *url = [NSURL URLWithString:HOWTOVIDEO_1];
    //            [[UIApplication sharedApplication] openURL:url];
    //        }
    //            break;
    //        case 1:
    //        {
    //            NSURL *url = [NSURL URLWithString:HOWTOVIDEO_2];
    //            [[UIApplication sharedApplication] openURL:url];
    //        }
    //            break;
    //
    //        default:
    //            break;
    //    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //self.view.backgroundColor = [UIColor yellowColor];
    
//    CGRect frame = [[UIScreen mainScreen] bounds];
//    DebugLog(@"page width = %f",frame.size.width);
//    DebugLog(@"page height = %f",frame.size.height);
    
//    NSData *mydata = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:imageUrl]];
//    [imgView setImage:[UIImage imageWithData:mydata]];
//    [imgView setContentMode:UIViewContentModeTop];
//    [self.view addSubview:imgView];
//    [imgView release];
    
    [imgView setImageWithURL:[NSURL URLWithString:imageUrl]
                 placeholderImage:[UIImage imageNamed:@"loading.png"]
                     success:^(UIImage *image) {
                         //[imgView setContentMode:UIViewContentModeTop];
                         [self.view addSubview:imgView];
                         [imgView release];
                          }
                          failure:^(NSError *error) {
                              //DebugLog(@"write error %@", error);
                              imgView.image = nil;
                          }];

}

- (UIView *)getView {
    return imgView; //
}
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //    if(toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown){
    //        if(pageNumber == 0) {
    //            [imgView setImage:[UIImage imageNamed:@"carservicestips.jpg"]];
    //        }
    //
    //    }
    //    else if(toInterfaceOrientation == UIInterfaceOrientationLandscapeRight || toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft){
    //        if(pageNumber == 0) {
    //            [imgView setImage:[UIImage imageNamed:@"NISSAN-SUNNY.jpg"]];
    //        }
    //    }
}

- (void)viewWillAppear:(BOOL)animated
{
    
    //    UIInterfaceOrientation orientation = [[UIDevice currentDevice] orientation];
    //
    //    if(orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown){
    //        if(pageNumber == 0) {
    //            [imgView setImage:[UIImage imageNamed:@"tips1.jpg"]];
    //        }
    //
    //    }
    //    else if(orientation == UIInterfaceOrientationLandscapeRight || orientation == UIInterfaceOrientationLandscapeLeft){
    //        if(pageNumber == 0) {
    //            [imgView setImage:[UIImage imageNamed:@"NISSAN-SUNNY.jpg"]];
    //        }
    //    }
    
}
- (void)dealloc {
    DebugLog(@"page dealloc = %d",pageNumber +1);
    imageUrl = nil;
    videoUrl = nil;
	[super dealloc];
}
- (void)viewDidUnload
{
    imgView = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
