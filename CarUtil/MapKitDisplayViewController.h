//
//  MapKitDisplayViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 03/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@class DisplayMap;
@interface MapKitDisplayViewController : UIViewController <CLLocationManagerDelegate,MKMapViewDelegate,UIAlertViewDelegate>{
    MKMapView *mapView;
    CLLocationManager *locationManager;  //Location Manager object
    UIToolbar *toolbar;
    UIBarButtonItem *barItem1;
    UIBarButtonItem *barItem2;
}

@property (nonatomic, retain) MKMapView *mapView;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (retain, nonatomic) IBOutlet UIView *mapdetailview;
@property (retain, nonatomic) IBOutlet UILabel *accuracylbl;
@property (retain, nonatomic) IBOutlet UILabel *distancelbl;
@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (retain, nonatomic) IBOutlet UIButton *parkHereBtn;
@property (retain, nonatomic) IBOutlet UIButton *getDirectionBtn;

- (IBAction)parkherePressed:(id)sender;
- (IBAction)getdirectionsPressed:(id)sender;




- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation;

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error;
@end
