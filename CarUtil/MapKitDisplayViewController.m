//
//  MapKitDisplayViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 03/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "MapKitDisplayViewController.h"
#import "DisplayMap.h"
#import "CarUtilAppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "MFSideMenu.h"

#define METERS_PER_MILE 1609.344
@interface MapKitDisplayViewController ()

@end

@implementation MapKitDisplayViewController
@synthesize mapdetailview;
@synthesize accuracylbl;
@synthesize distancelbl;
@synthesize indicator;
@synthesize locationManager;
@synthesize mapView;

@synthesize parkHereBtn,getDirectionBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.navigationItem.title = NSLocalizedString(@"FIND MY CAR", @"FIND MY CAR");

    }
    return self;
}

- (IBAction)parkherePressed:(id)sender
{
    [Flurry logEvent:@"CarPark_Event"];
#ifdef GOOGLE_ANALYTICS
    if (![[GANTracker sharedTracker] trackEvent:@"Nissan CarUitl Application iOS"
                                         action:@"Car Parked Pressed"
                                          label:@""
                                          value:-1
                                      withError:NULL]) {
        DebugLog(@"error in trackEvent");
    }
#endif
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([[NSUserDefaults standardUserDefaults] doubleForKey:@"savedlatitude"] )
    {
        DebugLog(@"NON EMPTY");
        UIAlertView *message = [[UIAlertView alloc] initWithTitle: ALERT_TITLE
                                                          message:@"This will erase your previous parking location. Do you want to proceed?"
                                                         delegate:self
                                                cancelButtonTitle:@"NO"
                                                otherButtonTitles:@"YES", nil];
        
        [message show];
        [message release];
        
    } else {
        DebugLog(@"EMPTY");
        MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } }; 
        region.center.latitude = [defaults doubleForKey:@"latitude"] ;
        region.center.longitude = [defaults doubleForKey:@"longitude"];
        region.span.longitudeDelta = 0.01f;
        region.span.latitudeDelta = 0.01f;
        [mapView setRegion:region animated:YES]; 
        DisplayMap *ann = [[DisplayMap alloc] init]; 
        ann.title = @"My Car";
        [self saveparkedLocation:region.center.latitude longitude:region.center.longitude];
        ann.coordinate = region.center; 
        [mapView addAnnotation:ann];
        [ann release];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"YES"])
    {
        DebugLog(@"Button YES was selected.");
        [mapView removeAnnotations:mapView.annotations];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } }; 
        region.center.latitude = [defaults doubleForKey:@"latitude"] ;
        region.center.longitude = [defaults doubleForKey:@"longitude"];
        region.span.longitudeDelta = 0.01f;
        region.span.latitudeDelta = 0.01f;
        [mapView setRegion:region animated:YES]; 
        DisplayMap *ann = [[DisplayMap alloc] init];
        ann.title = @"My Car";
        [self saveparkedLocation:region.center.latitude longitude:region.center.longitude];
        ann.coordinate = region.center; 
        [mapView addAnnotation:ann];
        [ann release];
        
    }
}

-(void)saveparkedLocation:(double)llatitude longitude:(double)llongitude 
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setDouble:llatitude forKey:@"savedlatitude"];
    [prefs setDouble:llongitude forKey:@"savedlongitude"];
    //[prefs setValue:ltitle forKey:@"savedtitle"];
    //[prefs setValue:lsubtitle forKey:@"savedsubtitle"];
    [prefs synchronize];
}

- (IBAction)getdirectionsPressed:(id)sender
{
    [Flurry logEvent:@"GetDirection_MyCar_Event"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    double latValue = [defaults doubleForKey:@"savedlatitude"];
    double longValue  = [defaults doubleForKey:@"savedlongitude"];
    UIApplication *app = [UIApplication sharedApplication];  
    [app openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?saddr=Current+Location&daddr=%lf,%lf",latValue,longValue]]]; 
    
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
#ifdef TESTING
    [TestFlight passCheckpoint:@"Find My Car Pressed"];
#endif 
    
    [self setupMenuBarButtonItems];
    self.navigationController.navigationBar.tintColor = DEFAULT_COLOR;
    
    
    //mapdetailview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bar2x.png"]];
    mapView = [[MKMapView alloc] initWithFrame:self.view.bounds];
    [mapView setMapType:MKMapTypeStandard];
	[mapView setZoomEnabled:YES];
	[mapView setScrollEnabled:YES];
    mapView.showsUserLocation = YES;
    [mapView setDelegate:self];
    
    [self.view addSubview:mapView];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([[NSUserDefaults standardUserDefaults] doubleForKey:@"savedlatitude"] )
    {
        DebugLog(@"NON EMPTY");
        
        MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } }; 
        region.center.latitude = [defaults doubleForKey:@"savedlatitude"] ;
        region.center.longitude = [defaults doubleForKey:@"savedlongitude"];
        region.span.longitudeDelta = 0.01f;
        region.span.latitudeDelta = 0.01f;
        //[mapView setRegion:region animated:YES]; 
        
        DisplayMap *ann = [[DisplayMap alloc] init]; 
        //ann.title = [defaults stringForKey:@"savedtitle"];
        //ann.subtitle = [defaults stringForKey:@"savedsubtitle"]; 
        ann.coordinate = region.center; 
        [mapView addAnnotation:ann];
        [ann release];
        
        [self startLoctionManager];
        
    } else {
        DebugLog(@"update map with current user location");
        [self startLoctionManager];
    }
    accuracylbl.text = @"";
    distancelbl.text = @"";
    [self.view bringSubviewToFront:toolbar];
    [self.view bringSubviewToFront:mapdetailview];
    
    [self.view bringSubviewToFront:parkHereBtn];
    [self.view bringSubviewToFront:getDirectionBtn];
    
    parkHereBtn.layer.borderColor = TABLE_SELECTION_COLOR.CGColor;
    parkHereBtn.layer.borderWidth = 2.0;
    getDirectionBtn.layer.borderColor = TABLE_SELECTION_COLOR.CGColor;
    getDirectionBtn.layer.borderWidth = 2.0;
}

-(void) startLoctionManager
{
    locationManager = [[CLLocationManager alloc] init] ;
    self.locationManager.delegate = self; // send loc updates to myself
    self.locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager setPurpose:@"CarUtil will use your location for assisting you in finding you car parking location."];
    
    if ([CLLocationManager locationServicesEnabled] == NO)
	{
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle: ALERT_TITLE message:@"You currently have all location services for this device disabled. If you proceed, you will be asked to confirm whether location services should be reenabled." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [servicesDisabledAlert show];
        [servicesDisabledAlert release];
    } else {
        [self performSelector:@selector(stopUpdatingCoreLocation:) withObject:@"Timed Out" afterDelay:30];
        [locationManager startUpdatingLocation];
    }
}

- (void)stopUpdatingCoreLocation:(NSString *)state
{
	[locationManager stopUpdatingLocation];
    [indicator stopAnimating];
    [self saveCurrentocation:[locationManager location]];
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //DebugLog(@"Location: %@", [newLocation description]);
	//DebugLog(@"location x:%lf, y:%lf", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
	//DebugLog(@"\n%@\n", newLocation.altitude);
	//DebugLog(@"\n%@\n", newLocation.course);
	//DebugLog(@"\n%@\n", newLocation.speed);
	//DebugLog(@"\n%@\n", newLocation.timestamp);
    //DebugLog(@"\n H= %f\n", newLocation.horizontalAccuracy);
    NSString *currentHorizontalAccuracy = [[NSString alloc] initWithFormat:@"Accuracy : %g Meters / %g Yards", newLocation.horizontalAccuracy , newLocation.horizontalAccuracy * 1.0936133];
    accuracylbl.text = currentHorizontalAccuracy;
    [currentHorizontalAccuracy release];
    
    if ([[NSUserDefaults standardUserDefaults] doubleForKey:@"savedlatitude"])
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        CLLocation *savedlocation = [[CLLocation alloc] initWithLatitude:[defaults doubleForKey:@"latitude"] longitude:[defaults doubleForKey:@"longitude"]];
        
        CLLocationDistance distanceBetween = [newLocation distanceFromLocation:savedlocation];
        
        NSString *tripString = [[NSString alloc] initWithFormat:@"Distance : %f Meters / %f Yards", distanceBetween,distanceBetween * 1.0936133];
        distancelbl.text = tripString;
        [tripString release];
        
        [savedlocation release];
    }

    MKCoordinateSpan span;
    span.latitudeDelta=.01;
    span.longitudeDelta=.01;
    MKCoordinateRegion region;
    region.center = newLocation.coordinate;
    region.span=span;
    [mapView setRegion:region animated:TRUE]; 
    
    
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error 
{
	DebugLog(@"Error: %@", [error description]);
	
	switch ([error code])
	{
		case kCLErrorDenied:
			//[errorString appendFormat:@"%@\n", NSLocalizedString(@"LocationDenied", nil)];
			break;
			
			
		case kCLErrorLocationUnknown:
			//[errorString appendFormat:@"%@\n", NSLocalizedString(@"LocationUnknown", nil)];
			break;
			
		case kCLErrorNetwork:
			//[errorString appendFormat:@"%@\n", NSLocalizedString(@"LocationUnknown", nil)];
			break;
            
		case kCLErrorHeadingFailure:
			//[errorString appendFormat:@"%@\n", NSLocalizedString(@"LocationUnknown", nil)];
			break;
			
		default:
			//[errorString appendFormat:@"%@ %d\n", NSLocalizedString(@"GenericLocationError", nil), [error code]];
			break;
	}
    [self stopUpdatingCoreLocation:nil];
     accuracylbl.text = @"Failed to obtain positioning information.";
     distancelbl.text = @"          Please try again later.";
}


- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
	//DebugLog(@"did update heading %@", [newHeading description]);
}

-(void)saveCurrentocation:(CLLocation *)lLocation
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setDouble:lLocation.coordinate.latitude forKey:@"latitude"];
    [prefs setDouble:lLocation.coordinate.longitude forKey:@"longitude"];
    //[prefs setValue:myPlacemark.subAdministrativeArea forKey:@"title"];
    //[prefs setValue:myPlacemark.subLocality forKey:@"subtitle"];
    [prefs synchronize];
    
    
}

- (void)mapViewDidFailLoadingMap:(MKMapView *)mapView withError:(NSError *)error 
{ 
    DebugLog(@"map view failed to load map: %@", error.localizedDescription);
    
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                         message:@"Error while Loading Map. Please check your internet connection and try again."
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil] ;
    [errorAlert show];
    [errorAlert release];
    
}

-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
	MKPinAnnotationView *pinView = nil; 
	if(annotation != mapView.userLocation) 
	{
		static NSString *defaultPinID = @"carPin";
		pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
		if ( pinView == nil ) 
            pinView = [[[MKPinAnnotationView alloc]
                        initWithAnnotation:annotation reuseIdentifier:defaultPinID] autorelease];
        
		//pinView.pinColor = MKPinAnnotationColorRed; 
		pinView.canShowCallout = YES;
		//pinView.animatesDrop = YES; //To support custom image as a PIN
        UIImage *pinImage = [UIImage imageNamed:@"micra_32x32.png"];
        [pinView setImage:pinImage];
        pinView.tag = 100;
        //pinView.annotation = annotation;
        
    } 
	else {
		[mapView.userLocation setTitle:@"I am here"];
	}
	return pinView;
}


- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
        {
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            UIBarButtonItem *sideMenuBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                                                                 target:self.navigationController.sideMenu
                                                                                 action:@selector(toggleRightSideMenu)];
            self.navigationItem.rightBarButtonItem = sideMenuBarButton;
            [sideMenuBarButton release];
        }
            break;
        case MFSideMenuStateLeftMenuOpen:
        {
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
        }
            break;
        case MFSideMenuStateRightMenuOpen:
        {
            UIBarButtonItem *sideMenuBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                                                                 target:self.navigationController.sideMenu
                                                                                 action:@selector(toggleRightSideMenu)];
            self.navigationItem.rightBarButtonItem = sideMenuBarButton;
            [sideMenuBarButton release];
        }
            break;
    }
}
- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewDidUnload
{
    self.locationManager = nil; 
    [self setMapdetailview:nil];
    [self setAccuracylbl:nil];
    [self setIndicator:nil];
    [self setDistancelbl:nil];
    [self setParkHereBtn:nil];
    [self setGetDirectionBtn:nil];
    [super viewDidUnload];
    // Release any retained sub views of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc {
	[mapView release];
    [locationManager release];
    [mapdetailview release];
    [accuracylbl release];
    [indicator release];
    [distancelbl release];
    [parkHereBtn release];
    [getDirectionBtn release];
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end