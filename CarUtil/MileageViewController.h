//
//  MileageViewController.h
//  CarUtil
//
//  Created by Sagar Mody on 12/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface MileageViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
	NSMutableArray *mileageArray;
    UIView *headerView;
    UIView *footerView;
    sqlite3 *expenseDB;
    //NSString *databasePath;
    NSInteger cardId;
}
@property (readwrite, nonatomic) NSInteger activeSessionRowId;
@property (retain, nonatomic) IBOutlet UITableView *mileageTable;
@property (nonatomic, copy) NSMutableArray *mileageArray;
@property(nonatomic,retain)  UIView *headerView;
@property(nonatomic,retain)  UIView *footerView;
@property (readwrite, nonatomic) NSInteger cardId;
@property (retain, nonatomic) IBOutlet UIButton *infoBtn;

//-(void)adddataInMileageArray:(NSInteger)lid  startdate:(double)sdate enddate:(double)edate startreading:(double)sreading endreading:(double)ereading avgMileage:(double)lfuel average:(double)lavg cost:(double)lcost selected:(NSString *)lselected;
//- (void) editMileageData:(NSInteger)ldbrowID rowID:(NSInteger)lrowID;
-(void)reloadAll;
- (IBAction)infoBtnPressed:(id)sender;

@end
