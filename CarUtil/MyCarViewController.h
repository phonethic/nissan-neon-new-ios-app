//
//  MyCarViewController.h
//  CarUtil
//
//  Created by Sagar Mody on 10/06/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <sqlite3.h>
#import "MyCar.h"

@interface MyCarViewController : UIViewController <UITableViewDelegate>{
    sqlite3 *expenseDB;
    //NSString *databasePath;
    NSInteger dbrowID;
    NSInteger carcount;
    NSString *imgName;
    NSInteger dbmaxrowID;
    NSInteger dbminrowID;
    NSInteger updated;
    MyCar *mycarObj;
}
@property (retain, nonatomic) IBOutlet UILabel *headerlbl;
@property (retain, nonatomic) IBOutlet UILabel *helplbl;
@property (retain, nonatomic) IBOutlet UITableView *cardetailtbl;
@property (retain, nonatomic) IBOutlet UIImageView *mycarImage;
@property (retain, nonatomic) IBOutlet UIButton *deletMyCar;
@property (retain, nonatomic) IBOutlet UIButton *backBtn;
@property (retain, nonatomic) IBOutlet UIButton *fwdBtn;
@property (readwrite, nonatomic) NSInteger dbrowID;
@property (readwrite, nonatomic) NSInteger carcount;
@property (readwrite, nonatomic) NSInteger dbmaxrowID;
@property (readwrite, nonatomic) NSInteger dbminrowID;
@property (readwrite, nonatomic) NSInteger updated;
@property (nonatomic, copy) NSString *imgName;
@property (nonatomic, retain) MyCar *mycarObj;
@property (retain, nonatomic) IBOutlet UILabel *addcarmsglbl;

- (IBAction)deleteMyCarPressed:(id)sender;
- (IBAction)moveCarBack:(id)sender;
- (IBAction)moveCarFwd:(id)sender;

-(void)reloadDataViews;
@end
