//
//  MyCarViewController.m
//  CarUtil
//
//  Created by Sagar Mody on 10/06/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <Parse/Parse.h>

#import "MyCarViewController.h"
#import "EditMyCarViewController.h"
#import "MileageViewController.h"
#import "ExpenseViewController.h"
#import "ReminderDetailsViewController.h"
#import "MyCar.h"
#import "CarUtilAppDelegate.h"

#import "MFSideMenu.h"


@interface MyCarViewController ()

@end

@implementation MyCarViewController
@synthesize helplbl;
@synthesize cardetailtbl;
@synthesize mycarImage;
@synthesize deletMyCar;
@synthesize backBtn;
@synthesize fwdBtn;
@synthesize dbrowID;
@synthesize carcount;
@synthesize dbmaxrowID;
@synthesize dbminrowID;
@synthesize updated;
@synthesize imgName;
@synthesize mycarObj;
@synthesize addcarmsglbl;
@synthesize headerlbl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.navigationItem.title = NSLocalizedString(@"MY CAR", @"MY CAR");
    }
    return self;
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 46;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //return [fuelArray count];
    return 13;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UILabel *lblText;
    UILabel *lblValue;
    UIImageView *arrowImage;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        
        //Initialize Label with tag 1.(Date Label)
        lblText = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x + 20 , cell.frame.origin.y + 10, 120, cell.frame.size.height - 20)];
        lblText.tag = 1;
        //lblDate.text = @"12/07/1984";
        lblText.font = HELVETICA_FONT(14);
        lblText.textAlignment = UITextAlignmentLeft;
        lblText.textColor = [UIColor whiteColor];
        lblText.backgroundColor =  [UIColor clearColor];
//        lblText.shadowColor = [UIColor blackColor];
//        lblText.shadowOffset = CGSizeMake(0, 1);
        [cell.contentView addSubview:lblText];
        [lblText release];
        
        //Initialize Label with tag 2.(Fuel Label)
        lblValue = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblText.frame)+10, cell.frame.origin.y + 10, 150, cell.frame.size.height - 20)];
        lblValue.tag = 2;
        //lblfuel.text = @"5614";
        lblValue.font = HELVETICA_FONT(14);
        lblValue.textColor = [UIColor whiteColor];
        lblValue .lineBreakMode =  UILineBreakModeTailTruncation;
        lblValue.textAlignment = UITextAlignmentRight;
        lblValue.backgroundColor =  [UIColor clearColor];
//        lblValue.shadowColor = [UIColor blackColor];
//        lblValue.shadowOffset = CGSizeMake(0, 1);
        [cell.contentView addSubview:lblValue];
        [lblValue release];
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor clearColor];
        UIImageView *tempImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 46)];
        [tempImg setImage:[UIImage imageNamed:@"published_page_table_bg.png"]];
        cell.backgroundView = tempImg;
        [tempImg release];
        
        UIImageView *arrowImg = [[UIImageView alloc] initWithFrame:CGRectMake(280, 12, 10, 19)];
        [arrowImg setImage:[UIImage imageNamed:@"arrow_right.png"]];
        arrowImg.tag = 3;
        arrowImg.hidden = TRUE;
        [cell.contentView addSubview:arrowImg];
        [arrowImg release];

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (mycarObj != nil && mycarObj.brand != nil && mycarObj.brand != nil && ![mycarObj.brand isEqualToString:@""] && ![mycarObj.model isEqualToString:@""]) {
        headerlbl.text = [NSString stringWithFormat:@"%@ - %@",mycarObj.brand,mycarObj.model];
    }
    // Set up the cell
    switch (indexPath.row) {
        case 0:
            lblText = (UILabel *)[cell viewWithTag:1];
            lblText.text = @"Reminders";
            lblValue = (UILabel *)[cell viewWithTag:2];
            lblValue.text = @"";
            arrowImage = (UIImageView *)[cell viewWithTag:3];
            arrowImage.hidden = FALSE;
            break;
        case 1:
        {
            lblText = (UILabel *)[cell viewWithTag:1];
            lblText.text = @"Car Brand";
            lblValue = (UILabel *)[cell viewWithTag:2];
            lblValue.text = mycarObj.brand;
            arrowImage = (UIImageView *)[cell viewWithTag:3];
            arrowImage.hidden = TRUE;
        }
            break;
            
        case 2:
        {
            lblText = (UILabel *)[cell viewWithTag:1];
            lblText.text = @"Model";
            lblValue = (UILabel *)[cell viewWithTag:2];
            lblValue.text = mycarObj.model;
            arrowImage = (UIImageView *)[cell viewWithTag:3];
            arrowImage.hidden = TRUE;
        }
            break;
            
        case 3:
        {
            lblText = (UILabel *)[cell viewWithTag:1];
            lblText.text = @"Reg No.";
            lblValue = (UILabel *)[cell viewWithTag:2];
            lblValue.text = mycarObj.carnumber;
            arrowImage = (UIImageView *)[cell viewWithTag:3];
            arrowImage.hidden = TRUE;
        }
            break;
        case 4:
        {
            lblText = (UILabel *)[cell viewWithTag:1];
            lblText.text = @"Purchase Dt.";
            lblValue = (UILabel *)[cell viewWithTag:2];
            if(0 != (int)mycarObj.purchasedate) {
                NSDate *Date = [NSDate dateWithTimeIntervalSinceReferenceDate:mycarObj.purchasedate];
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"dd MMMM yyyy"];
                NSString *dateString = [dateFormat stringFromDate:Date];
                lblValue.text = dateString;
                [dateFormat release];
            } else {
                lblValue.text = @"";
            }
            arrowImage = (UIImageView *)[cell viewWithTag:3];
            arrowImage.hidden = TRUE;
            
        }
            break;
        case 5:
        {
            lblText = (UILabel *)[cell viewWithTag:1];
            lblText.text = @"Mfg Date";
            lblValue = (UILabel *)[cell viewWithTag:2];
            if(0 != (int)mycarObj.mfgdate) {
                NSDate *Date = [NSDate dateWithTimeIntervalSinceReferenceDate:mycarObj.mfgdate]; 
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"dd MMMM yyyy"];
                NSString *dateString = [dateFormat stringFromDate:Date];  
                lblValue.text = dateString;
                [dateFormat release];
            } else {
                lblValue.text = @"";
            }
            arrowImage = (UIImageView *)[cell viewWithTag:3];
            arrowImage.hidden = TRUE;
        }
            break;
            
        case 6:
        {
            lblText = (UILabel *)[cell viewWithTag:1];
            lblText.text = @"RC No.";
            lblValue = (UILabel *)[cell viewWithTag:2];
            lblValue.text = mycarObj.RCno;
            arrowImage = (UIImageView *)[cell viewWithTag:3];
            arrowImage.hidden = TRUE;
        }
            break;
            
        case 7:
        {
            lblText = (UILabel *)[cell viewWithTag:1];
            lblText.text = @"Insurer";
            lblValue = (UILabel *)[cell viewWithTag:2];
            lblValue.text = mycarObj.insurername;
            arrowImage = (UIImageView *)[cell viewWithTag:3];
            arrowImage.hidden = TRUE;
        }
            break;
            
        case 8:
        {
            lblText = (UILabel *)[cell viewWithTag:1];
            lblText.text = @"Policy No.";
            lblValue = (UILabel *)[cell viewWithTag:2];
            lblValue.text = mycarObj.policyno;
            arrowImage = (UIImageView *)[cell viewWithTag:3];
            arrowImage.hidden = TRUE;
        }
            break;
               
        case 9:
        {
            lblText = (UILabel *)[cell viewWithTag:1];
            lblText.text = @"Policy Dt.";
            lblValue = (UILabel *)[cell viewWithTag:2];
            if(0 != (int)mycarObj.policydate) {
                NSDate *Date = [NSDate dateWithTimeIntervalSinceReferenceDate:mycarObj.policydate]; 
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"dd MMMM yyyy"];
                NSString *dateString = [dateFormat stringFromDate:Date];  
                lblValue.text = dateString;
                [dateFormat release];
            } else {
                lblValue.text = @"";
            }
            arrowImage = (UIImageView *)[cell viewWithTag:3];
            arrowImage.hidden = TRUE;
        }
            break;
            
        case 10:
        {
            lblText = (UILabel *)[cell viewWithTag:1];
            lblText.text = @"Chassis No.";
            lblValue = (UILabel *)[cell viewWithTag:2];
            lblValue.text = mycarObj.chassisno;
            arrowImage = (UIImageView *)[cell viewWithTag:3];
            arrowImage.hidden = TRUE;
        }
            break;
            
        default:
            break;
    }

    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    DebugLog(@"%d",indexPath.row);
    if(indexPath.row == 0) {
        UIViewController *reminderviewController = [[ReminderDetailsViewController alloc] initWithNibName:@"ReminderDetailsViewController" bundle:nil] ;
        reminderviewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:reminderviewController animated:YES];
        [reminderviewController release];
    }
    else
    {
         [self editCarDetails];
    }
    [cardetailtbl deselectRowAtIndexPath:[cardetailtbl indexPathForSelectedRow] animated:NO];
}


//-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
//{
//    UITouch *touch = [touches anyObject];
//    
//    if(touch.view.tag == 11) {
//        [self editCarDetails];
//    } 
//    
//}
-(void)reloadDataViews
{
    DebugLog(@"%d - %d  - %d",self.dbrowID,dbmaxrowID,dbminrowID);
    if(self.dbrowID == self.dbminrowID) {
        [self moveCarFwd:nil];
        backBtn.hidden = TRUE;
    } else if(self.dbrowID == self.dbmaxrowID) {
        [self moveCarBack:nil];
        fwdBtn.hidden = TRUE;
    } else {
        [self moveCarFwd:nil];
    }
    [self getMaxCarRowID];
    [self getMinCarRowID];
    [self countTotalRows];
    DebugLog(@"%d - %d  - %d",self.dbrowID,dbmaxrowID,dbminrowID);
}

- (IBAction)moveCarBack:(id)sender {
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"SELECT * FROM MYCAR WHERE id < \"%d\" order by id desc limit 1 ", self.dbrowID];
        
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(expenseDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                
                NSInteger primaryKey = sqlite3_column_int(statement, 0);
                self.dbrowID = primaryKey;
                
                if(self.dbrowID == self.dbminrowID)
                {
                    backBtn.hidden = TRUE;
                }  
                
                if(self.dbrowID < self.self.dbmaxrowID)
                {
                    fwdBtn.hidden = FALSE;
                }
            
                self.imgName = @"";
                NSString *imgNameCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                NSString *brandCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
                NSString *modelCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
                NSString *carNoCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
                double mfgdateCol = sqlite3_column_double(statement, 5);
                NSString *rcNoCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 6)];
                NSString *insurerNameCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 7)];
                NSString *policyCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 8)];
				double policydateCol = sqlite3_column_double(statement, 9);
                NSString *chassisNoCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 10)];
                double purchaseDateCol = sqlite3_column_double(statement, 11);
                NSString *selectedCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 12)];
                
                
                DebugLog(@" %d - %@ - %@ - %@ - %@ - %f - %@ - %@ - %@ - %f - %@ - %f",primaryKey, imgNameCol, brandCol, modelCol, carNoCol, mfgdateCol, rcNoCol, insurerNameCol, policyCol, policydateCol, chassisNoCol, purchaseDateCol);
                
                if(mycarObj != nil)
                {
                    [mycarObj release];
                    mycarObj = nil;
                }
                
				// Create a new expense object with the data from the database
				mycarObj = [[MyCar alloc] initWithID:primaryKey imgName:imgNameCol  brand:brandCol model:modelCol carnumber:carNoCol mfgdate:mfgdateCol RCno:rcNoCol insurername:insurerNameCol policyno:policyCol policydate:policydateCol chassis:chassisNoCol purchasedate:purchaseDateCol selected:selectedCol];

                [cardetailtbl reloadData];
                self.imgName = imgNameCol;
                [self loadImage:self.imgName];
                [self swipeAnimation:1];
                if(![self.imgName isEqualToString:@""])
                {
 //                   self.helplbl.hidden = TRUE;
                }
                else {
 //                   self.helplbl.hidden = FALSE;
                }

            } else {
                //[self showMessage:@"Match not found"];
            }
            sqlite3_finalize(statement);
            [self selectedCarID:self.dbrowID];
        }
        sqlite3_close(expenseDB);
    }
}

- (IBAction)moveCarFwd:(id)sender {
    sqlite3_stmt    *statement;

    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"SELECT * FROM MYCAR WHERE id > \"%d\" order by id asc limit 1 ", self.dbrowID];
        
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(expenseDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                NSInteger primaryKey = sqlite3_column_int(statement, 0);
                self.dbrowID = primaryKey;
                
                if(self.dbrowID == self.dbmaxrowID)
                {
                    fwdBtn.hidden = TRUE;
                } 
                
                if(self.dbrowID > self.dbminrowID)
                {
                    backBtn.hidden = FALSE;
                }

                NSString *imgNameCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                NSString *brandCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
                NSString *modelCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
                NSString *carNoCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
                double mfgdateCol = sqlite3_column_double(statement, 5);
                NSString *rcNoCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 6)];
                NSString *insurerNameCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 7)];
                NSString *policyCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 8)];
				double policydateCol = sqlite3_column_double(statement, 9);
                NSString *chassisNoCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 10)];
                double purchaseDateCol = sqlite3_column_double(statement, 11);
                NSString *selectedCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 12)];
                
                
                 DebugLog(@" %d - %@ - %@ - %@ - %@ - %f - %@ - %@ - %@ - %f - %@ - %f",primaryKey, imgNameCol, brandCol, modelCol, carNoCol, mfgdateCol, rcNoCol, insurerNameCol, policyCol, policydateCol, chassisNoCol, purchaseDateCol);
                
                if(mycarObj != nil)
                    [mycarObj release];
                
				// Create a new expense object with the data from the database
				mycarObj = [[MyCar alloc] initWithID:primaryKey imgName:imgNameCol  brand:brandCol model:modelCol carnumber:carNoCol mfgdate:mfgdateCol RCno:rcNoCol insurername:insurerNameCol policyno:policyCol policydate:policydateCol chassis:chassisNoCol purchasedate:purchaseDateCol selected:selectedCol];

                [cardetailtbl reloadData];
                self.imgName = imgNameCol;
                [self loadImage:self.imgName];
                [self swipeAnimation:0];
                if(![self.imgName isEqualToString:@""])
                {
//                    self.helplbl.hidden = TRUE;
                }
                else {
//                    self.helplbl.hidden = FALSE;
                }

            } else {
                //[self showMessage:@"Match not found"];
            }
            sqlite3_finalize(statement);
            [self selectedCarID:self.dbrowID];
        }
        sqlite3_close(expenseDB);
    }
}


-(void)swipeAnimation:(int)recognizer {
    
    // set up an animation for the transition between the views
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.6];
    [animation setType:kCATransitionPush];
    if (recognizer == 0) {
        [animation setSubtype:kCATransitionFromRight];
    } else if (recognizer == 1) {
        [animation setSubtype:kCATransitionFromLeft];
    }
    
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [[self.view layer] addAnimation:animation forKey:@"SwitchToView"];

}

-(void)selectedCarID:(NSInteger)dbID
{
    sqlite3_stmt    *statement;
    sqlite3_stmt    *statement1;
    
    DebugLog(@"IDDDD = %d",dbID);
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"update MYCAR set SELECTED=\"%@\"",  @"0"];
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            //[self showMessage:@"Contact updated"];
            //[self clearData:nil];
            
            DebugLog(@"updated1");
        } else {
            //[self showMessage:@"Contact updatation failed"];
             DebugLog(@"failed1");
        }
        sqlite3_finalize(statement);
        
        NSString *updateSQL1 = [NSString stringWithFormat:@"update MYCAR set SELECTED=\"%@\" where ID=\"%d\"",  @"1" , dbID];
        const char *update_stmt1 = [updateSQL1 UTF8String];
        sqlite3_prepare_v2(expenseDB, update_stmt1, -1, &statement1, NULL);
        if (sqlite3_step(statement1) == SQLITE_DONE)
        {
            //[self showMessage:@"Contact updated"];
            //[self clearData:nil];
             DebugLog(@"updated2");
        } else {
            //[self showMessage:@"Contact updatation failed"];
             DebugLog(@"failed2");
        }
        sqlite3_finalize(statement1);
    }         
    sqlite3_close(expenseDB);
}


-(void)getMaxCarRowID
{
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"SELECT max(id) FROM MYCAR"]; 
        
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(expenseDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                
                NSInteger primaryKey = sqlite3_column_int(statement, 0);
                self.dbmaxrowID = primaryKey;
                
            } else {
                //[self showMessage:@"Match not found"];
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(expenseDB);
    }
}

-(void)getMinCarRowID
{
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"SELECT min(id) FROM MYCAR"];       
        
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(expenseDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                
                NSInteger primaryKey = sqlite3_column_int(statement, 0);
                self.dbminrowID = primaryKey;
                
            } else {
                //[self showMessage:@"Match not found"];
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(expenseDB);
    }
}


-(void)editCarDetails
{
    EditMyCarViewController *addmycarController = [[EditMyCarViewController alloc] initWithNibName:@"EditMyCarViewController" bundle:nil] ;
    if(self.dbrowID > 0) {
        addmycarController.title = @"EDIT MY CAR";
    } else {
        addmycarController.title = @"ADD MY CAR";
    }
    
    //to push the UIView.
    addmycarController.hidesBottomBarWhenPushed = YES;
    addmycarController.dbrowID = self.dbrowID;
    [self.navigationController pushViewController:addmycarController animated:YES];
    [addmycarController release];
    
//    [UIView  transitionWithView:self.navigationController.view duration:1.0  options:UIViewAnimationOptionTransitionFlipFromLeft
//                     animations:^(void) {
//                         BOOL oldState = [UIView areAnimationsEnabled];
//                         [UIView setAnimationsEnabled:NO];
//                         [self.navigationController pushViewController:addmycarController animated:NO];
//                         [addmycarController release];
//                         [UIView setAnimationsEnabled:oldState];
//                     } 
//                     completion:nil];


    
}

-(void)countTotalRows
{
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"SELECT count(ID)  FROM MYCAR "];
        
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(expenseDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                self.carcount =  sqlite3_column_int(statement, 0);
                DebugLog(@"count = %d",self.carcount);
                if(self.carcount == 0)
                {
                    headerlbl.text = @"";
                    mycarImage.image = [UIImage imageNamed:@"car_transparent.png"];
                    mycarImage.layer.borderWidth = 0.0;
                    addcarmsglbl.hidden = FALSE;
                    cardetailtbl.hidden = TRUE;
                    deletMyCar.hidden = TRUE;
//                    self.helplbl.hidden = FALSE;
                    self.dbrowID = 0;
                } else {
                    addcarmsglbl.hidden = TRUE;
                    cardetailtbl.hidden = FALSE;
                    deletMyCar.hidden = FALSE;
//                    self.helplbl.hidden = TRUE;
                }
            } else {
                //Failed
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(expenseDB);
    }
}

- (void)viewDidLoad
{
#ifdef ENABLE_BACK_BUTTON
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [backButton release]; 
    
#endif
    
    [self setupMenuBarButtonItems];
    self.navigationController.navigationBar.tintColor = DEFAULT_COLOR;
    
//    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc]
//											   initWithBarButtonSystemItem:UIBarButtonSystemItemAdd 
//											   target:self action:@selector(addCar_Clicked:)] autorelease];
    #ifdef TESTING
    [TestFlight passCheckpoint:@"MyCar tab Pressed"];
    #endif
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.dbrowID = 0;
    headerlbl.text = @"";
    [self getMaxCarRowID];
    [self getMinCarRowID];
    [self readCarDetailFromDatabase];
    DebugLog(@"%d - %d  - %d",self.dbrowID,dbmaxrowID,dbminrowID);
    if((self.dbrowID == self.dbminrowID) && (self.dbrowID == self.dbmaxrowID))
    {
        backBtn.hidden = TRUE;
        fwdBtn.hidden = TRUE;
    } else if(self.dbrowID == self.dbminrowID) {
        backBtn.hidden = TRUE;
    } else if(self.dbrowID == self.dbmaxrowID) {
        fwdBtn.hidden = TRUE;
    }
    self.updated = 0;
    [self countTotalRows];
    UISwipeGestureRecognizer *rightswipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self 
                                                                                               action:@selector(carswipeDetected:)];
    rightswipeRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:rightswipeRecognizer];
    [rightswipeRecognizer release];
    
    UISwipeGestureRecognizer *leftswipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self 
                                                                                              action:@selector(carswipeDetected:)];
    leftswipeRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:leftswipeRecognizer];
    [leftswipeRecognizer release];
    
    UITapGestureRecognizer *imgviewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    imgviewTap.numberOfTapsRequired = 1;
    imgviewTap.cancelsTouchesInView = NO;
    [self.mycarImage  addGestureRecognizer:imgviewTap];
    [imgviewTap release];
    
    
    addcarmsglbl.textColor = DEFAULT_COLOR;
    addcarmsglbl.font = HELVETICA_FONT(20);

    headerlbl.textColor = DEFAULT_COLOR;
    headerlbl.font = HELVETICA_FONT(20);
    headerlbl.minimumFontSize = 11;
    headerlbl.adjustsFontSizeToFitWidth = YES;
}

- (IBAction)tapDetected:(UIGestureRecognizer *)sender {
	// Code to respond to gesture her
    [self editCarDetails];
}

-(void)carswipeDetected:(UISwipeGestureRecognizer *)recognizer {
    if (backBtn.isHidden == TRUE && fwdBtn.isHidden == TRUE){
        return;
    } else if(backBtn.isHidden == TRUE) {
        if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft) {
            DebugLog(@"UISwipeGestureRecognizerDirectionLeft");
        } else if (recognizer.direction == UISwipeGestureRecognizerDirectionRight) {
           DebugLog(@"UISwipeGestureRecognizerDirectionRight");
            return;
        }
    } else if(fwdBtn.isHidden == TRUE){
        if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft) {
            DebugLog(@"UISwipeGestureRecognizerDirectionLeft");
            return;
        } else if (recognizer.direction == UISwipeGestureRecognizerDirectionRight) {
            DebugLog(@"UISwipeGestureRecognizerDirectionRight");
        }
    } 
    // set up an animation for the transition between the views
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.6];
    [animation setType:kCATransitionPush];
    if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft) {
        [animation setSubtype:kCATransitionFromRight];
        [self moveCarFwd:nil];
    } else if (recognizer.direction == UISwipeGestureRecognizerDirectionRight) {
        [animation setSubtype:kCATransitionFromLeft];
        [self moveCarBack:nil];
    }
    
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
    
    [[self.view layer] addAnimation:animation forKey:@"SwitchToView"];
    
    [cardetailtbl setContentOffset:CGPointZero animated:NO];

}


-(void) readCarDetailFromDatabase {
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
	
	// Open the database from the users filessytem
	if(sqlite3_open(dbpath, &expenseDB) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        NSString *querySQL = [NSString stringWithFormat:@"Select * from MYCAR WHERE selected=\"%@\"", @"1"];
        const char *sqlStatement = [querySQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(expenseDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
                NSInteger primaryKey = sqlite3_column_int(compiledStatement, 0);
                self.dbrowID = primaryKey;
                NSString *imgNameCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                NSString *brandCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                NSString *modelCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                NSString *carNoCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                double mfgdateCol = sqlite3_column_double(compiledStatement, 5);
                NSString *rcNoCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
                NSString *insurerNameCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 7)];
                NSString *policyCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 8)];
				double policydateCol = sqlite3_column_double(compiledStatement, 9);
                NSString *chassisNoCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 10)];
                double purchaseDateCol = sqlite3_column_double(compiledStatement, 11);
                NSString *selectedCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 12)];
                
                
                DebugLog(@" %d - %@ - %@ - %@ - %@ - %f - %@ - %@ - %@ - %f - %@ - %f",primaryKey, imgNameCol, brandCol, modelCol, carNoCol, mfgdateCol, rcNoCol, insurerNameCol, policyCol, policydateCol, chassisNoCol, purchaseDateCol);
                
                if(mycarObj != nil)
                {
                    [mycarObj release];
                    mycarObj = nil;
                }
                
				// Create a new expense object with the data from the database
				MyCar *tempmycarObj = [[MyCar alloc] initWithID:primaryKey imgName:imgNameCol brand:brandCol model:modelCol carnumber:carNoCol mfgdate:mfgdateCol RCno:rcNoCol insurername:insurerNameCol policyno:policyCol policydate:policydateCol chassis:chassisNoCol purchasedate:purchaseDateCol selected:selectedCol];
                self.mycarObj = tempmycarObj;
                [tempmycarObj release];
                tempmycarObj = nil;
                [cardetailtbl reloadData];
                self.imgName = imgNameCol;
                [self loadImage:self.imgName];
                if(![self.imgName isEqualToString:@""])
                {
//                    self.helplbl.hidden = TRUE;
                }
                else {
//                    self.helplbl.hidden = FALSE;
                }
			}
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(expenseDB);
}

- (void) deleteCarDetails
{   
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM MYCAR WHERE ID=\"%d\"", self.dbrowID];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            //[self showMessage:@"Contact deleted"];
            //[self clearData:nil];
        } else {
            //[self showMessage:@"Failed to delete contact"];
        }
        sqlite3_finalize(statement);
        NSString *findSQL = [NSString stringWithFormat:@"Select count(*) from MYCAR WHERE BRAND=\"%@\"", mycarObj.brand];
        const char *find_stmt = [findSQL UTF8String];
        if (sqlite3_prepare_v2(expenseDB, find_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
               int rowcount =  sqlite3_column_int(statement, 0);
                DebugLog(@"rowcount %d carbrand %@",rowcount,mycarObj.brand);
                if (rowcount == 0)
                {
                    [PFPush unsubscribeFromChannelInBackground:mycarObj.brand];
                }
                
            } else
            {
                //Failed
            }
            sqlite3_finalize(statement);
        }
    }
    sqlite3_close(expenseDB);  
}

- (void)removeImage:(NSString*)limageName {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:limageName];
    BOOL success = [fileManager removeItemAtPath: fullPath error:NULL];
    if(success == YES) 
        DebugLog(@"image removed");
    else 
        DebugLog(@"image NOT removed");
}

- (void)loadImage:(NSString *)limgName
{
    DebugLog(@"image name = %@" , limgName);
    if(limgName != nil) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* fullPath = [documentsDirectory stringByAppendingPathComponent:limgName];
        DebugLog(@"fullpath = %@" , fullPath);
        UIImage* image = [UIImage imageWithContentsOfFile:fullPath];
        if(image != nil)
        {
            mycarImage.image = image;
            mycarImage.layer.cornerRadius = 5;
            mycarImage.layer.masksToBounds = YES;
            mycarImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
            mycarImage.layer.borderWidth = 1.0;
        }
        else {
            mycarImage.image = [UIImage imageNamed:@"car_transparent.png"];
        }
    }
}

- (void) addCar_Clicked:(id)sender {
	
#ifdef GOOGLE_ANALYTICS
    if (![[GANTracker sharedTracker] trackEvent:@"Nissan CarUitl Application iOS"
                                         action:@"Add New Car Pressed"
                                          label:@""
                                          value:-1
                                      withError:NULL]) {
        DebugLog(@"error in trackEvent");
    }
#endif
    EditMyCarViewController *addmycarController = [[EditMyCarViewController alloc] initWithNibName:@"EditMyCarViewController" bundle:nil] ;
    addmycarController.title = @"ADD MY CAR";
    //to push the UIView.
    addmycarController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:addmycarController animated:YES];
    [addmycarController release];
    
//    [UIView  transitionWithView:self.navigationController.view duration:1.0  options:UIViewAnimationOptionTransitionFlipFromLeft
//                     animations:^(void) {
//                         BOOL oldState = [UIView areAnimationsEnabled];
//                         [UIView setAnimationsEnabled:NO];
//                         [self.navigationController pushViewController:addmycarController animated:YES];
//                         [addmycarController release];
//                         [UIView setAnimationsEnabled:oldState];
//                     } 
//                     completion:nil];
}

- (IBAction)deleteMyCarPressed:(id)sender {
    [self showChoice:@"Are you sure you would like to delete this vehicle?"];
}

- (void)showChoice:(NSString *) lmessage{
    UIAlertView *message = [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                       message:lmessage
                                                      delegate:self
                                             cancelButtonTitle:nil
                                             otherButtonTitles:@"NO", @"YES", nil] autorelease];
    
    [message show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self countTotalRows];
    switch (buttonIndex) {
        case 1:
        {
            [self deleteCarDetails];
            if(self.imgName != nil && ![self.imgName isEqualToString:@""] )
            {
                [self removeImage:self.imgName];
                mycarImage.image = [UIImage imageNamed:@"car_transparent.png"];
            }
            DebugLog(@"%d - %d  - %d",self.dbrowID,dbmaxrowID,dbminrowID);
            if(self.dbrowID == self.dbminrowID) {
                [self moveCarFwd:nil];
                backBtn.hidden = TRUE;
            } else if(self.dbrowID == self.dbmaxrowID) {
                [self moveCarBack:nil];
                fwdBtn.hidden = TRUE;
            } else {
                [self moveCarFwd:nil];
            }
            [self getMaxCarRowID];
            [self getMinCarRowID];
            [self countTotalRows];
            DebugLog(@"%d - %d  - %d",self.dbrowID,dbmaxrowID,dbminrowID);
        }
            break;
            
        default:
            break;
    }
}

- (void)showMessage:(NSString *) lmessage{
    UIAlertView *message = [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                       message:lmessage
                                                      delegate:nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil] autorelease];
    
    [message show];
}

- (void)viewDidAppear:(BOOL)animated
{
    if(self.updated == 1)  // while save new record
    {
        [self countTotalRows];
        [self getMaxCarRowID];
        [self getMinCarRowID];
        DebugLog(@"%d - %d  - %d",self.dbrowID,dbmaxrowID,dbminrowID);
        if(self.dbrowID == 0) {
             [self readCarDetailFromDatabase];
        } else if(self.dbrowID == self.dbminrowID && self.dbrowID < self.dbmaxrowID) {
            backBtn.hidden = TRUE;
            fwdBtn.hidden = FALSE;
        } else if(self.dbrowID == self.dbmaxrowID) {
            fwdBtn.hidden = TRUE;
        } else if(self.dbrowID < self.dbmaxrowID){
            fwdBtn.hidden = FALSE;
        }
        DebugLog(@"%d - %d  - %d",self.dbrowID,dbmaxrowID,dbminrowID);
    } else if(self.updated == 2)  // while update
    {
         [self readCarDetailFromDatabase];
    }
    else if (self.updated == 3) // while delete
    {
        [self countTotalRows];
        DebugLog(@"%d - %d  - %d",self.dbrowID,dbmaxrowID,dbminrowID);
        if(self.dbrowID == self.dbminrowID) {
            [self moveCarFwd:nil];
            backBtn.hidden = TRUE;
        } else if(self.dbrowID == self.dbmaxrowID) {
            [self moveCarBack:nil];
            fwdBtn.hidden = TRUE;
        } else {
            [self moveCarFwd:nil];
        }
        [self getMaxCarRowID];
        [self getMinCarRowID];
        [self countTotalRows];
        DebugLog(@"%d - %d  - %d",self.dbrowID,dbmaxrowID,dbminrowID);
    }
    self.updated = 0;
    [super viewDidAppear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
}


- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
        {
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            UIBarButtonItem *sideMenuBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                                                                 target:self.navigationController.sideMenu
                                                                                 action:@selector(toggleRightSideMenu)];
            //            self.navigationItem.rightBarButtonItem = sideMeuBarButton; whiteplus.png
            //            UIBarButtonItem *newReminderButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(AddNewReminder:)];
            UIBarButtonItem *newCarButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"whiteplus.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(addCar_Clicked:)];
            NSArray *array = [[NSArray alloc] initWithObjects:sideMenuBarButton,newCarButton, nil];
            self.navigationItem.rightBarButtonItems = array;
            [array release];
            [newCarButton release];
            [sideMenuBarButton release];
        }
            break;
        case MFSideMenuStateLeftMenuOpen:
        {
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
        }
            break;
        case MFSideMenuStateRightMenuOpen:
        {
            UIBarButtonItem *sideMenuBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                                                                 target:self.navigationController.sideMenu
                                                                                 action:@selector(toggleRightSideMenu)];
            //            self.navigationItem.rightBarButtonItem = sideMeuBarButton;
            UIBarButtonItem *newCarButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"whiteplus.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(addCar_Clicked:)];
            NSArray *array = [[NSArray alloc] initWithObjects:sideMenuBarButton,newCarButton, nil];
            self.navigationItem.rightBarButtonItems = array;
            [array release];
            [newCarButton release];
            [sideMenuBarButton release];
        }
            break;
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewDidUnload
{
    self.imgName = nil;
    self.mycarObj = nil;
    [self setMycarImage:nil];
    [self setDeletMyCar:nil];
    [self setBackBtn:nil];
    [self setFwdBtn:nil];
    [self setCardetailtbl:nil];
    [self setHelplbl:nil];
    [self setAddcarmsglbl:nil];
    [self setHeaderlbl:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)dealloc {
    if(mycarObj != nil)
        [mycarObj release];
    [imgName release];
    [mycarImage release];
    [deletMyCar release];
    [backBtn release];
    [fwdBtn release];
    [cardetailtbl release];
    [helplbl release];
    [addcarmsglbl release];
    [headerlbl release];
    [super dealloc];
}

@end
