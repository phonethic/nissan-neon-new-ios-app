//
//  ViewController.m
//
//  Created by Cameron Saul on 02/20/2012.
//  Copyright 2012 Cameron Saul. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "MenuViewController.h"
#import "NAMenuItem.h"
#import "SampleViewController.h"

@interface MenuViewController()
- (NSArray *)createMenuItems;
@end

@implementation MenuViewController

#pragma mark - Memory Management

- (id)init {
	self = [super init];
	
	if (self) {
		[self setMenuItems:[self createMenuItems]];
	}
	
	return self;
}


#pragma mark - Local Methods

- (NSArray *)createMenuItems {
	NSMutableArray *items = [[[NSMutableArray alloc] init] autorelease];
	
//	// First Item
//	NAMenuItem *item1 = [[[NAMenuItem alloc] initWithTitle:@"" 
//													 image:[UIImage imageNamed:@"1B.png"] 
//												   vcClass:[SampleViewController class]] autorelease];
//	[items addObject:item1];
//	
//	// Second Item
//	NAMenuItem *item2 = [[[NAMenuItem alloc] initWithTitle:@"" 
//													 image:[UIImage imageNamed:@"2B.png"] 
//												   vcClass:[SampleViewController class]] autorelease];
//	[items addObject:item2];
//	
//	// Third Item
//	NAMenuItem *item3 = [[[NAMenuItem alloc] initWithTitle:@"" 
//													 image:[UIImage imageNamed:@"3B.png"] 
//												   vcClass:[SampleViewController class]] autorelease];
//	[items addObject:item3];
//	
//	// Fourth Item
//	NAMenuItem *item4 = [[[NAMenuItem alloc] initWithTitle:@"" 
//													 image:[UIImage imageNamed:@"4B.png"] 
//												   vcClass:[SampleViewController class]] autorelease];
//	[items addObject:item4];
//	
//	// Fifth Item
//	NAMenuItem *item5 = [[[NAMenuItem alloc] initWithTitle:@"" 
//													 image:[UIImage imageNamed:@"5B.png"] 
//												   vcClass:[SampleViewController class]] autorelease];
//	[items addObject:item5];
//	
//	// Sixth Item
//	NAMenuItem *item6 = [[[NAMenuItem alloc] initWithTitle:@"" 
//													 image:[UIImage imageNamed:@"6B.png"] 
//												   vcClass:[SampleViewController class]] autorelease];
//	[items addObject:item6];
//	
//	// Seventh Item
//	NAMenuItem *item7 = [[[NAMenuItem alloc] initWithTitle:@"" 
//													 image:[UIImage imageNamed:@"7B.png"] 
//												   vcClass:[SampleViewController class]] autorelease];
//	[items addObject:item7];
//	
//	// Eighth Item
//	NAMenuItem *item8 = [[[NAMenuItem alloc] initWithTitle:@"" 
//													 image:[UIImage imageNamed:@"8B.png"] 
//												   vcClass:[SampleViewController class]] autorelease];
//	[items addObject:item8];
//
//	// Ninth Item
//	NAMenuItem *item9 = [[[NAMenuItem alloc] initWithTitle:@"" 
//													 image:[UIImage imageNamed:@"9B.png"] 
//												   vcClass:[SampleViewController class]] autorelease];
//	[items addObject:item9];
//    
//  	// Tenth Item
//    NAMenuItem *item10 = [[[NAMenuItem alloc] initWithTitle:@"" 
//													 image:[UIImage imageNamed:@"10B.png"] 
//												   vcClass:[SampleViewController class]] autorelease];
//	[items addObject:item10];

    int i;
    //MICRA IMAGES
    for(i=1;i<=53;i++) {
        
        
        // First Item
        NAMenuItem *item = [[[NAMenuItem alloc] initWithTitle:@"" 
                                                        image: [UIImage imageNamed:[NSString stringWithFormat:@"%dB.png",i]]
                                                      vcClass:[SampleViewController class]] autorelease];
        [items addObject:item];
        
        
    }

	
	return items;
}


#pragma mark - View Lifecycle

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad {
	[super viewDidLoad];
    #ifdef TESTING
    [TestFlight passCheckpoint:@"Traffic Signs Button Pressed"];
    #endif 
	self.navigationItem.title = @"Traffic Signs";
	self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"BG_all.jpg"]];
}

@end
