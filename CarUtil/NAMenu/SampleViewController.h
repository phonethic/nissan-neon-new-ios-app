//
//  SampleViewController.h
//  NAMenu
//
//  Created by Cameron Saul on 2/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RadioButton.h"

@interface SampleViewController : UIViewController  {
    int imgindex;
}
@property (strong, nonatomic) IBOutlet UIImageView *iconimgView;
@property (nonatomic,readwrite) int imgindex;
@property (retain, nonatomic) IBOutlet UILabel *textlbl;

@end
