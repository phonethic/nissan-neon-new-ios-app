//
//  SampleViewController.m
//  NAMenu
//
//  Created by Cameron Saul on 2/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SampleViewController.h"
#import <QuartzCore/QuartzCore.h>


@implementation SampleViewController
@synthesize iconimgView;
@synthesize imgindex;
@synthesize textlbl;

#pragma mark - View Lifecycle

- (void)viewDidLoad {
#ifdef GOOGLE_ANALYTICS
    if (![[GANTracker sharedTracker] trackEvent:@"Nissan CarUitl Application iOS"
                                         action:@"Traffic Icon Pressed"
                                          label:@""
                                          value:-1
                                      withError:NULL]) {
        DebugLog(@"error in trackEvent");
    }
#endif 
	[super viewDidLoad];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(goBack)];
    self.navigationItem.rightBarButtonItem = backButton;
    [backButton release]; 
    [self.navigationItem setHidesBackButton:YES animated:NO];
    [iconimgView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%dB.png",self.imgindex]]];
    //self.iconimgView.layer.borderWidth = 1.0;
    self.iconimgView.layer.borderColor = [UIColor colorWithWhite:0.4 alpha:0.4].CGColor;
    self.iconimgView.clipsToBounds = YES;
    self.iconimgView.layer.cornerRadius = 5.0;
	self.navigationItem.title = @"Sign Details";
    switch (self.imgindex) {
        case 1:
             textlbl.text = @"Straight no entry";
            break;
        case 2:
            textlbl.text = @"No parking";
            break;
        case 3:
            textlbl.text = @"No stopping or Standing";
            break;
        case 4:
            textlbl.text = @"Horns prohibited";
            break;
        case 5:
            textlbl.text = @"School Ahead";
            break;
        case 6:
            textlbl.text = @"Narrow Road ahead";
            break;
        case 7:
            textlbl.text = @"Narrow Bridge";
            break;
        case 8:
            textlbl.text = @"Right Hand Curve";
            break;
        case 9:
            textlbl.text = @"Left Hand Curve";
            break;
        case 10:
            textlbl.text = @"Compulsory Sound Horn";
            break;
        case 11:
            textlbl.text = @"One Way Sign";
            break;
        case 12:
            textlbl.text = @"One Way Sign";
            break;
        case 13:
            textlbl.text = @"Vehicles prohibited in both directions";
            break;
        case 14:
            textlbl.text = @"All Motor vehicles prohibited";
            break;
        case 15:
            textlbl.text = @"Tonga prohibited";
            break;
        case 16:
            textlbl.text = @"Truck prohibited";
            break;
        case 17:
            textlbl.text = @"Bullock Cart and Hand Cart prohibited";
            break;
        case 18:
            textlbl.text = @"Hand Cart prohibited";
            break;
        case 19:
            textlbl.text = @"Bullock Cart prohibited";
            break;
        case 20:
            textlbl.text = @"Cycle prohibited";
            break;
        case 21:
            textlbl.text = @"Pedestrian prohibited";
            break;
        case 22:
            textlbl.text = @"Right turn prohibited";
            break;
        case 23:
            textlbl.text = @"Left turn prohibited";
            break;
        case 24:
            textlbl.text = @"U-Turn prohibited";
            break;
        case 25:
            textlbl.text = @"Speed Limit";
            break;
        case 26:
            textlbl.text = @"Width Limit";
            break;
        case 27:
            textlbl.text = @"Height Limit";
            break;
        case 28:
            textlbl.text = @"Length Limit";
            break;
        case 29:
            textlbl.text = @"Load Limit";
            break;
        case 30:
            textlbl.text = @"Axe Load Limit";
            break;
        case 31:
            textlbl.text = @"Bus Stop";
            break;
        case 32:
            textlbl.text = @"Restriciton Ends sign";
            break;
        case 33:
            textlbl.text = @"Compulsory keep Left";
            break;
        case 34:
            textlbl.text = @"Compulsory turn Left";
            break;
        case 35:
            textlbl.text = @"Compulsory turn Right ahead";
            break;
        case 36:
            textlbl.text = @"Compulsory ahead or turn Left";
            break;
        case 37:
            textlbl.text = @"Compulsory ahead or turn Right";
            break;
        case 38:
            textlbl.text = @"Compulsory ahead only";
            break;
        case 39:
            textlbl.text = @"Compulsory cycle track";
            break;
        case 40:
            textlbl.text = @"Move On";
            break;
        case 41:
            textlbl.text = @"Stop";
            break;
        case 42:
            textlbl.text = @"Give Way";
            break;
        case 43:
            textlbl.text = @"Bump";
            break;
        case 44:
            textlbl.text = @"Ferry";
            break;
        case 45:
            textlbl.text = @"Right Hair Pin bend";
            break;
        case 46:
            textlbl.text = @"Left Hair Pin bend";
            break;
        case 47:
            textlbl.text = @"Left reverse bend";
            break;
        case 48:
            textlbl.text = @"Right reverse bend";
            break;
        case 49:
            textlbl.text = @"Steep Ascent";
            break;
        case 50:
            textlbl.text = @"Steep Descent";
            break;
        case 51:
            textlbl.text = @"Road Widens ahead";
            break;
        case 52:
            textlbl.text = @"Slippery Road";
            break;
        case 53:
            textlbl.text = @"Cycle crossing";
            break;
            
        default:
            break;
    }
}

-(void)goBack {
    [[self navigationController] popViewControllerAnimated:NO];
}

- (void)dealloc {
    [iconimgView release];
    [textlbl release];
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidUnload {
    self.iconimgView = nil;
    [self setTextlbl:nil];
    [super viewDidUnload];
}
@end
