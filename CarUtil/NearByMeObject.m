//
//  NearByMeObject.m
//  Medical
//
//  Created by Kirti Nikam on 26/11/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "NearByMeObject.h"

@implementation NearByMeObject
@synthesize nearbymerowid,durationValue,distanceValue,durationText,distanceText;

-(void)dealloc{
    [durationText release];
    [distanceText release];
	[super dealloc];
}
@end
