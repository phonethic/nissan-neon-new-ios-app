//
//  ReminderDetailsViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 24/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface ReminderDetailsViewController : UIViewController {
    NSMutableArray *reminderArray;
    sqlite3 *expenseDB;
    NSInteger dbowID;
    //NSString *databasePath;
    NSIndexPath *tempIndex;
}
@property (retain, nonatomic) IBOutlet UITableView *reminderDetailtbl;
@property (nonatomic, copy) NSMutableArray *reminderArray;
@property (readwrite, nonatomic) NSInteger dbowID;
@property (readonly, nonatomic) NSIndexPath *tempIndex;
@property (retain, nonatomic) IBOutlet UIImageView *emptymessage;
@property (retain, nonatomic) IBOutlet UIButton *addReminderBtn;
@property (retain, nonatomic) IBOutlet UILabel *infolbl;
- (IBAction)addReminderBtnClicked:(id)sender;

-(void)updateReminderONOFFButtonSelection:(NSInteger)expId selected:(NSInteger)value;
-(void)updateCompletedRowButtonSelection:(NSInteger)expId selected:(NSInteger)value;
-(void) reloadReminderTable:(int)type;
-(void)addReminderData:(double)ldate text:(NSString *)message fireDate:(NSDate *)lpickerdate prevalue:(NSInteger)lprevalue pretype:(NSString *)lpretype;
-(void)editReminderData:(NSInteger)lreminderdetailid date:(double)ldatedouble text:(NSString *)message fireDate:(NSDate *)lpickerdate prevalue:(NSInteger)lprevalue pretype:(NSString *)lpretype;
- (int) SaveReminderdata:(double)ldate text:(NSString *)message prevalue:(NSInteger)lprevalue pretype:(NSString *)lpretype;


@end
