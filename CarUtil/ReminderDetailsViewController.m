//
//  ReminderDetailsViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 24/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "ReminderDetailsViewController.h"
#import "ReminderViewController.h"
#import "ReminderDetails.h"
#import "CarUtilAppDelegate.h"

#import "MFSideMenu.h"
#import "HMSegmentedControl.h"

@interface ReminderDetailsViewController ()
@property(nonatomic,strong) IBOutlet UISegmentedControl *segmentedControl;
@property(nonatomic,strong) IBOutlet HMSegmentedControl *hmsegmentedControl;
@end

@implementation ReminderDetailsViewController
@synthesize reminderDetailtbl;
@synthesize reminderArray;
@synthesize dbowID;
@synthesize tempIndex;
@synthesize emptymessage;
@synthesize segmentedControl;
@synthesize addReminderBtn;
@synthesize infolbl;
@synthesize hmsegmentedControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.navigationItem.title = NSLocalizedString(@"Reminders", @"Reminders");
    }
    return self;
}

#pragma mark Table view methods
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    //return NO;
    return YES;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 33;
//}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section   // custom view for header. will be adjusted to default or specified header height
//{
//    if(headerView == nil) {
//        //allocate the view if it doesn't exist yet
//        headerView  = [[UIView alloc] initWithFrame:CGRectMake(0.0f,0.0f,320.0f,33)];
//        
//        UIImageView *tempImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 350, 33)];
//        [tempImg setImage:[UIImage imageNamed:@"heading_bg.png"]];
//    
//        UILabel* label  = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 250, 33)];
//        label.backgroundColor   = [UIColor clearColor];
//        label.text = @"Pending Reminders List";
//        label.tag = 1;
//        //[label setBackgroundColor:[UIColor colorWithPatternImage:image1]];
//        label.baselineAdjustment= UIBaselineAdjustmentAlignCenters;
//        label.lineBreakMode =  UILineBreakModeWordWrap;
//        label.textAlignment = UITextAlignmentLeft;
////        label.shadowColor   = [UIColor blackColor];
////        label.shadowOffset  = CGSizeMake(0.0, 1.0);
//        label.font      = HELVETICA_FONT(15);
//        label.textColor     = [UIColor whiteColor];
//        label.numberOfLines = 0;
//    
//        //add the button to the view
//        [headerView addSubview:tempImg];
//        [headerView addSubview:label];
//        [tempImg release];
//        [label release];
//
//    } 
//    if([reminderArray count] == 0) {
//        headerView.hidden = TRUE;
//        emptymessage.hidden = FALSE;
//    } else {
//        emptymessage.hidden = TRUE;
//        headerView.hidden = FALSE;
//    }
//    //return the view for the footer
//    return headerView;
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [reminderArray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UILabel *lblDate;
    UILabel *lblType;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        
        UIImage *image = [UIImage imageNamed:@"onbutton.png"];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(5.0, cell.frame.origin.y + 4, image.size.width + 10 , image.size.height + 22);
		button.tag = 1;
        [button setImage:image forState:UIControlStateNormal];
        button.imageView.tag = 10;
        // set the button's target to this table view controller so we can interpret touch events and map that to a NSIndexSet
        [button addTarget:self action:@selector(onButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        button.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:button];
        
        //Initialize Label with tag 2.(Type Label)
        lblType = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x + 50, cell.frame.origin.y+8, 225, cell.frame.size.height-20)];
        lblType.tag = 2;
//        lblType.shadowColor   = [UIColor blackColor];
//        lblType.shadowOffset  = CGSizeMake(0.0, 1.0);
        //lblType.text = @"My Text";
        lblType.font = HELVETICA_FONT(16);
        lblType.textColor = [UIColor whiteColor];
        lblType .lineBreakMode =  UILineBreakModeTailTruncation;
        lblType.textAlignment = UITextAlignmentLeft;
        lblType.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblType];
        [lblType release];
        
        //Initialize Label with tag 3.(Date Label)
        lblDate = [[UILabel alloc] initWithFrame:CGRectMake(cell.frame.origin.x + 50, cell.frame.origin.y + 30, 235, cell.frame.size.height-25)];
        lblDate.tag = 3;
        //lblDate.text = @"12/07/1983";
//        lblDate.shadowColor   = [UIColor blackColor];
//        lblDate.shadowOffset  = CGSizeMake(0.0, 1.0);
        lblDate.font = HELVETICA_FONT(12);
        lblDate.minimumFontSize = 10;
        lblDate.adjustsFontSizeToFitWidth = YES;
        lblDate.textAlignment = UITextAlignmentLeft;
        lblDate.textColor = [UIColor whiteColor];
        lblDate.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblDate];
        [lblDate release];
 
        UIImage *image1 = [UIImage imageNamed:@"unchecked.png"];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        button1.frame = CGRectMake(275.0, cell.frame.origin.y + 4 , image1.size.width + 20 , image1.size.height + 33);
		button1.tag = 4;
        [button1 setImage:image1 forState:UIControlStateNormal];
        button1.imageView.tag = 10;
        // set the button's target to this table view controller so we can interpret touch events and map that to a NSIndexSet
        [button1 addTarget:self action:@selector(checkreminderButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
        button1.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:button1];
     
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor =  [UIColor colorWithRed:163/255.0 green:0/255.0 blue:32/255.0 alpha:0.9];
        cell.selectedBackgroundView = bgColorView;
        [bgColorView release];
        
    }
    
    // Set up the cell...
	//cell.textLabel.font=[UIFont fontWithName:@"Arial" size:16];
    
    // Set up the cell
	ReminderDetails *reminderObj = (ReminderDetails *)[reminderArray objectAtIndex:indexPath.row];
    //cell.textLabel.text = expenseObj.type;
    NSDate *pickerDate = [NSDate dateWithTimeIntervalSinceReferenceDate:reminderObj.date]; 
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMM yyyy hh:mm a"];
    NSString *dateString = [dateFormat stringFromDate:pickerDate];  
    lblDate = (UILabel *)[cell viewWithTag:3];
    if([reminderObj.pretype isEqualToString:@"No Pre-Notify"])
        lblDate.text = [NSString stringWithFormat:@"%@ (%@)", dateString , reminderObj.pretype];
    else 
         lblDate.text = [NSString stringWithFormat:@"%@ (%d %@ Before)", dateString , reminderObj.prevalue , reminderObj.pretype];
    lblType = (UILabel *)[cell viewWithTag:2];
    if([reminderObj.message isEqualToString:@""])
        lblType.text = @"Reminder";
    else 
        lblType.text = [reminderObj.message capitalizedString];
	//cell.detailTextLabel.text = dateString;
    
    UIButton *button1 = (UIButton *)[cell viewWithTag:1];
    DebugLog(@"date : %@",pickerDate);
    NSDate* currentDate = [NSDate date];
    switch ([currentDate compare:pickerDate]){
        case NSOrderedAscending:
        {
            DebugLog(@"NSOrderedAscending");
            [button1 setImage:[UIImage imageNamed:@"onbutton.png"] forState:UIControlStateNormal];
            reminderObj.reminderdeOnOff = 1;
        }
            break;
        case NSOrderedSame:
            DebugLog(@"NSOrderedSame");
        case NSOrderedDescending:
            DebugLog(@"NSOrderedDescending");
            [button1 setImage:[UIImage imageNamed:@"offbutton.png"] forState:UIControlStateNormal];
            reminderObj.reminderdeOnOff = 0;
            break;
    }
    [reminderArray replaceObjectAtIndex:indexPath.row withObject:reminderObj];
        
//    if(reminderObj.reminderdeOnOff==1){
//        //lblType.textColor = DEFAULT_COLOR;
//        [button1 setImage:[UIImage imageNamed:@"onbutton.png"] forState:UIControlStateNormal];
//    } else {
//        //lblType.textColor = [UIColor lightGrayColor];
//        [button1 setImage:[UIImage imageNamed:@"offbutton.png"] forState:UIControlStateNormal];        
//    }
    UIButton *button = (UIButton *)[cell viewWithTag:4];
    if(reminderObj.completed==1){
        [button setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
        button.hidden = TRUE;
    } else {
        [button setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];   
        button.hidden = FALSE;
    }
    [dateFormat release];
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.backgroundColor = [UIColor clearColor];
    
    UIImageView *tempImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    [tempImg setImage:[UIImage imageNamed:@"bg_grey.jpg"]];
    if(reminderObj.completed==1) {
        button1.userInteractionEnabled = FALSE;
    } else {
        button1.userInteractionEnabled = TRUE;
    }
    cell.backgroundView = tempImg;
    [tempImg release];
    
//    UIImageView *tempImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
//    if(reminderObj.completed==1) {
//        [tempImg setImage:[UIImage imageNamed:@"reminder_bg2.png"]];
//        button1.userInteractionEnabled = FALSE;
//    } else { 
//        [tempImg setImage:[UIImage imageNamed:@"reminder_bg1.png"]];
//        button1.userInteractionEnabled = TRUE;
//    }
//    cell.backgroundView = tempImg;
//    [tempImg release];
    //cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    DebugLog(@"%d",indexPath.row);
    ReminderDetails *reminderObj = (ReminderDetails *)[reminderArray objectAtIndex:indexPath.row];
    ReminderViewController *reminderviewController = [[ReminderViewController alloc] initWithNibName:@"ReminderViewController" bundle:nil] ;
    if(reminderObj.reminderdeOnOff == 0)
        reminderviewController.title = @"Recycle";
    else 
        reminderviewController.title = @"Edit Reminder";
    //to push the UIView.
    reminderviewController.hidesBottomBarWhenPushed = YES;
    reminderviewController.dbowID = reminderObj.reminderdetailid;
    [self.navigationController pushViewController:reminderviewController animated:YES];
    [reminderviewController release];
    [reminderDetailtbl deselectRowAtIndexPath:[reminderDetailtbl indexPathForSelectedRow] animated:YES];
    
}

- (void)tableView:(UITableView *)tv commitEditingStyle:(UITableViewCellEditingStyle)editingStyle 
forRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if(editingStyle == UITableViewCellEditingStyleDelete) {
		
		//Get the object to delete from the array.
        ReminderDetails *reminderObj = (ReminderDetails *)[reminderArray objectAtIndex:indexPath.row];
        [self cancelNotification:reminderObj.reminderdetailid];
        [self deleteReminderdata:reminderObj.reminderdetailid];
		[reminderArray removeObject:reminderObj];
        if([reminderArray count] == 0) {
            emptymessage.hidden = FALSE;
            infolbl.hidden = FALSE;
        }
		//Delete the object from the table.
		[self.reminderDetailtbl deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationMiddle];
	}
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    DebugLog(@"reminderArray.count %d",reminderArray.count);
    if(hmsegmentedControl.selectedSegmentIndex == 0){
        [self reloadReminderTable:0];
	} else if(hmsegmentedControl.selectedSegmentIndex == 1){
        [self reloadReminderTable:1];
	}
}
-(void)viewDidAppear:(BOOL)animated
{
//    if (self.segmentedControl == nil)
//    {
//        NSArray *segmentTextContent = [NSArray arrayWithObjects:@"Pending",@"Completed", nil];
//        segmentedControl = [[UISegmentedControl alloc] initWithItems:segmentTextContent];
//        segmentedControl.selectedSegmentIndex = 0;
//        segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleWidth;
//        segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
//        segmentedControl.frame = CGRectMake(0, 0, 150, 30);
//        [segmentedControl setMomentary:YES];
//        [segmentedControl setTitleTextAttributes:@{
//                        UITextAttributeTextColor: [UIColor whiteColor],
//                  UITextAttributeTextShadowColor: [UIColor blackColor],
//                 UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)],
//                             UITextAttributeFont: HELVETICA_FONT(15.0)
//         } forState:UIControlStateNormal];
//        [segmentedControl setBackgroundColor:DEFAULT_COLOR];
//        [segmentedControl addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged];
//        //	self.navigationItem.titleView = segmentedControl;
//        [self.navigationController.navigationBar.topItem setTitleView:segmentedControl];
//    }
    [super viewDidAppear:animated];
}
- (void)viewDidLoad
{
#ifdef ENABLE_BACK_BUTTON
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [backButton release]; 
    
#endif
    
    [self setupMenuBarButtonItems];
    self.navigationController.navigationBar.tintColor = DEFAULT_COLOR;
    #ifdef TESTING
    [TestFlight passCheckpoint:@"Reminder Button Pressed"];
    //[Flurry logEvent:@"Reminder_Event" withParameters:nil timed:YES];
    [Flurry logEvent:@"Reminder_Event"];
    #endif
//    UIBarButtonItem *newReminderButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(AddNewReminder:)];
//    self.navigationItem.leftBarButtonItem = newReminderButton;
//    [newReminderButton release];
    
    [super viewDidLoad]; 
    [self readRemindersFromDatabase:0];
    
    //self.reminderDetailtbl.separatorColor = [UIColor blackColor];
//    [addReminderBtn setTitle:@"      Add Reminder" forState:UIControlStateNormal];
//    [addReminderBtn setTitle:@"      Add Reminder" forState:UIControlStateHighlighted];
//    [addReminderBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [addReminderBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
//    [addReminderBtn setBackgroundImage:[UIImage imageNamed:@"bg_grey.jpg"] forState:UIControlStateNormal];
//    addReminderBtn.titleLabel.font = HELVETICA_FONT(20);
    
    // Do any additional setup after loading the view from its nib.
    //CREATE TABLE IF NOT EXISTS REMINDERS(ID INTEGER PRIMARY KEY AUTOINCREMENT, REMINDERONOFF INTEGER, MESSAGE TEXT, DATE REAL, COMPLETED INTEGER)";
    NSArray *segmentTextContent = [NSArray arrayWithObjects:@"Pending",@"Completed", nil];
    hmsegmentedControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, 320, 36)];
	hmsegmentedControl.segmentAtTop = NO;
    [hmsegmentedControl setSectionTitles:segmentTextContent];
    [hmsegmentedControl setSelectedSegmentIndex:0];
    [hmsegmentedControl setSelectionIndicatorHeight:4.0f];
    [hmsegmentedControl setBackgroundColor:[UIColor whiteColor]];
    [hmsegmentedControl setTextColor:DEFAULT_COLOR];
    [hmsegmentedControl setFont:HELVETICA_FONT(18)];
    [hmsegmentedControl setSelectionIndicatorColor:DEFAULT_COLOR];
    [hmsegmentedControl setSelectionIndicatorMode:HMSelectionIndicatorFillsSegment];
    [hmsegmentedControl setSegmentEdgeInset:UIEdgeInsetsMake(0, 6, 0, 6)];
    [hmsegmentedControl addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged];
    
    hmsegmentedControl.layer.shadowColor = [UIColor blackColor].CGColor;
    hmsegmentedControl.layer.shadowOpacity = 1;
    hmsegmentedControl.layer.shadowOffset = CGSizeMake(0,2);

    [self.view addSubview:hmsegmentedControl];
    
    infolbl.textColor = DEFAULT_COLOR;
    infolbl.font = HELVETICA_FONT(20);
}
- (void)segmentAction:(id)sender
{
	UISegmentedControl* segCtl = sender;
    if(segCtl.selectedSegmentIndex == 0){
        [self reloadReminderTable:0];
	} else if(segCtl.selectedSegmentIndex == 1){
        [self reloadReminderTable:1];
	}
//    if(segCtl.selectedSegmentIndex == 0){
//        [self reloadReminderTable:0];
//        UILabel *lblMesg = (UILabel *)[headerView viewWithTag:1];
//        if(lblMesg!=nil) {
//            lblMesg.text = @"Pending Reminders List";
//        }
//        //[sender setImage:[UIImage imageNamed:@"button.png"] forSegmentAtIndex:0];
//        //[sender setImage:[UIImage imageNamed:@"button.png"] forSegmentAtIndex:1];  
//	} else if(segCtl.selectedSegmentIndex == 1){
//        [self reloadReminderTable:1];
//        UILabel *lblMesg = (UILabel *)[headerView viewWithTag:1];
//        if(lblMesg!=nil) {
//            lblMesg.text = @"Completed Reminders List";
//        }
//        //[sender setImage:[UIImage imageNamed:@"button.png"] forSegmentAtIndex:0]; 
//        //[sender setImage:[UIImage imageNamed:@"button.png"] forSegmentAtIndex:1]; 
//	}
}

-(void)addReminderData:(double)ldate text:(NSString *)message fireDate:(NSDate *)lpickerdate prevalue:(NSInteger)lprevalue pretype:(NSString *)lpretype
{
    
    int notificationID =  [self SaveReminderdata:ldate text:message prevalue:lprevalue pretype:lpretype];
    if([lpretype isEqualToString:@"Minutes"]) 
    {
        NSDateComponents* dateComponents = [[NSDateComponents alloc]init];
        [dateComponents setMinute:-lprevalue];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDate* newDate = [calendar dateByAddingComponents:dateComponents toDate:lpickerdate options:0];
        [dateComponents release];
        [self addNotification:notificationID text:message fireDate:newDate];
        [self reloadReminderTable:0];
    }  else if ([lpretype isEqualToString:@"Hours"]) {
        NSDateComponents* dateComponents = [[NSDateComponents alloc]init];
        [dateComponents setHour:-lprevalue];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDate* newDate = [calendar dateByAddingComponents:dateComponents toDate:lpickerdate options:0];
        [dateComponents release];
        [self addNotification:notificationID text:message fireDate:newDate];
        [self reloadReminderTable:0];
    } else if ([lpretype isEqualToString:@"Days"]) {
        NSDateComponents* dateComponents = [[NSDateComponents alloc]init];
        [dateComponents setDay:-lprevalue];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDate* newDate = [calendar dateByAddingComponents:dateComponents toDate:lpickerdate options:0];
        [dateComponents release];
        [self addNotification:notificationID text:message fireDate:newDate];
        [self reloadReminderTable:0];
    } else if ([lpretype isEqualToString:@"Weeks"]) {
        NSDateComponents* dateComponents = [[NSDateComponents alloc]init];
        [dateComponents setWeek:-lprevalue];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDate* newDate = [calendar dateByAddingComponents:dateComponents toDate:lpickerdate options:0];
        [dateComponents release];
        [self addNotification:notificationID text:message fireDate:newDate];
        [self reloadReminderTable:0];
    } else if ([lpretype isEqualToString:@"No Pre-Notify"]) {
        [self addNotification:notificationID text:message fireDate:lpickerdate];
        [self reloadReminderTable:0];
    } else {
        DebugLog(@"NO VALUE");
        [self addNotification:notificationID text:message fireDate:lpickerdate];
        [self reloadReminderTable:0];
    }
}

-(void)editReminderData:(NSInteger)lreminderdetailid date:(double)ldatedouble text:(NSString *)message fireDate:(NSDate *)lpickerdate prevalue:(NSInteger)lprevalue pretype:(NSString *)lpretype
{
    
    [self EditReminderdata:lreminderdetailid date:ldatedouble text:message prevalue:lprevalue pretype:lpretype];
    if([lpretype isEqualToString:@"Minutes"]) 
    {
        NSDateComponents* dateComponents = [[NSDateComponents alloc]init];
        [dateComponents setMinute:-lprevalue];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDate* newDate = [calendar dateByAddingComponents:dateComponents toDate:lpickerdate options:0];
        [dateComponents release];
        [self cancelNotification:lreminderdetailid];
        [self addNotification:lreminderdetailid text:message fireDate:newDate];
        [self reloadReminderTable:0];
    }  else if ([lpretype isEqualToString:@"Hours"]) {
        NSDateComponents* dateComponents = [[NSDateComponents alloc]init];
        [dateComponents setHour:-lprevalue];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDate* newDate = [calendar dateByAddingComponents:dateComponents toDate:lpickerdate options:0];
        [dateComponents release];
        [self cancelNotification:lreminderdetailid];
        [self addNotification:lreminderdetailid text:message fireDate:newDate];
        [self reloadReminderTable:0];
    } else if ([lpretype isEqualToString:@"Days"]) {
        NSDateComponents* dateComponents = [[NSDateComponents alloc]init];
        [dateComponents setDay:-lprevalue];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDate* newDate = [calendar dateByAddingComponents:dateComponents toDate:lpickerdate options:0];
        [dateComponents release];
        [self cancelNotification:lreminderdetailid];
        [self addNotification:lreminderdetailid text:message fireDate:newDate];
        [self reloadReminderTable:0];
    } else if ([lpretype isEqualToString:@"Weeks"]) {
        NSDateComponents* dateComponents = [[NSDateComponents alloc]init];
        [dateComponents setWeek:-lprevalue];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDate* newDate = [calendar dateByAddingComponents:dateComponents toDate:lpickerdate options:0];
        [dateComponents release];
        [self cancelNotification:lreminderdetailid];
        [self addNotification:lreminderdetailid text:message fireDate:newDate];
        [self reloadReminderTable:0];
    } else {
        DebugLog(@"NO VALUE");
        [self cancelNotification:lreminderdetailid];
        [self addNotification:lreminderdetailid text:message fireDate:lpickerdate];
        [self reloadReminderTable:0];
    }
}


-(void) reloadReminderTable:(int)type
{
    [reminderArray removeAllObjects];
    [self readRemindersFromDatabase:type];
    [reminderDetailtbl reloadData];
    if(reminderArray == nil || [reminderArray count] <= 0) {
        emptymessage.hidden = FALSE;
        infolbl.hidden = FALSE;
        reminderDetailtbl.hidden = TRUE;
    }
    else
    {
        infolbl.hidden = TRUE;
        emptymessage.hidden = TRUE;
        reminderDetailtbl.hidden = FALSE;
        [self.reminderDetailtbl reloadData];
    }
}

- (int) SaveReminderdata:(double)ldate text:(NSString *)message prevalue:(NSInteger)lprevalue pretype:(NSString *)lpretype
{   
    sqlite3_stmt    *statement;
    int personID = 0;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat: 
                               @"INSERT INTO REMINDERS (REMINDERONOFF, MESSAGE, DATE, COMPLETED, PREVALUE, PRETYPE) VALUES (\"%d\", \"%@\", \"%f\" , \"%d\" , \"%d\" , \"%@\")", 
                               1, message, ldate, 0 , lprevalue , lpretype];
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            personID = sqlite3_last_insert_rowid(expenseDB);
            //DebugLog(@"%d",personID);
            //[self showMessage:@"Contact added"];
        } else {
            //[self showMessage:@"Failed to add contact"];
            personID = -1;
        }
        sqlite3_finalize(statement);
    } 
    sqlite3_close(expenseDB);
    return personID;
}

- (void) EditReminderdata:(NSInteger)lreminderdetailid date:(double)ldatedouble text:(NSString *)message prevalue:(NSInteger)lprevalue pretype:(NSString *)lpretype
{   
    sqlite3_stmt    *statement;
    int personID;
    
     const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {

     NSString *updateSQL = [NSString stringWithFormat:@"update REMINDERS set DATE=\"%f\" , MESSAGE=\"%@\" ,  PREVALUE=\"%d\" , PRETYPE=\"%@\" where id=\"%d\"", ldatedouble, message, lprevalue, lpretype, lreminderdetailid];
        
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            personID = sqlite3_last_insert_rowid(expenseDB);
            DebugLog(@"%d",personID);
        } else {
            //[self showMessage:@"Failed to add contact"];
        }
        sqlite3_finalize(statement);
    } 
    sqlite3_close(expenseDB);
}

-(void)deleteReminderdata:(NSInteger)reminderid
{
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM REMINDERS WHERE id=\"%d\"", reminderid];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            //[self showMessage:@"Contact deleted"];
            //[self clearData:nil];
        } else {
            //[self showMessage:@"Failed to delete contact"];
        }
        sqlite3_finalize(statement);
    }         
    sqlite3_close(expenseDB);
}

-(void) readRemindersFromDatabase:(int) completed {
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
	
    // Init the expense Array
    if(reminderArray == nil) {
        DebugLog(@"new reminder array created");
        reminderArray = [[NSMutableArray alloc] init];
    } else {
        DebugLog(@"old reminder array used");
        [reminderArray removeAllObjects];
    }
	
	// Open the database from the users filessytem
	if(sqlite3_open(dbpath, &expenseDB) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
		//const char *sqlStatement = "select * from MILEAGE_BREAKS where ";
        NSString *findSQL = [NSString stringWithFormat:@"select * from REMINDERS WHERE COMPLETED=\"%d\"", completed];
        const char *sqlStatement = [findSQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(expenseDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
                NSInteger primaryKey = sqlite3_column_int(compiledStatement, 0);
				int remindonoffCol = sqlite3_column_int(compiledStatement, 1);
                NSString *messageCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
				double dateCol = sqlite3_column_double(compiledStatement, 3);
				int completedCol = sqlite3_column_int(compiledStatement, 4);
                //NSInteger carId = sqlite3_column_int(compiledStatement, 5);
                NSInteger preval = sqlite3_column_int(compiledStatement, 6);
                NSString *pretype = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 7)];
                //DebugLog(@" %d - %d - %@ - %f - %d ",primaryKey, remindonoffCol, messageCol, dateCol, completedCol);
				// Create a new expense object with the data from the database
				ReminderDetails *reminderObj = [[ReminderDetails alloc] initWithID:primaryKey remindOnOff:remindonoffCol message:messageCol date:dateCol completed:completedCol prevalue:preval pretype:pretype];
				// Add the animal object to the animals Array
				[reminderArray addObject:reminderObj];
				[reminderObj release];
			}
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(expenseDB);
}
//NSInteger reminderdetailid;
//NSInteger reminderdeOnOff;
//NSString *message;
//double date;
//NSInteger completed;
//http://stackoverflow.com/questions/3158264/cancel-uilocalnotification
//http://www.icodeblog.com/2010/07/29/iphone-programming-tutorial-local-notifications/
//http://iphonesdkdev.blogspot.in/2010/04/local-push-notification-sample-code-os.html
//http://useyourloaf.com/blog/2010/07/31/adding-local-notifications-with-ios-4.html
- (void)onButtonTapped:(id)sender event:(id)event
{
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.reminderDetailtbl];
	NSIndexPath *indexPath = [self.reminderDetailtbl indexPathForRowAtPoint: currentTouchPosition];
	if (indexPath != nil)
	{
        DebugLog(@"indexpath %@",indexPath);
        UITableViewCell *cell  = (UITableViewCell *)[self.reminderDetailtbl cellForRowAtIndexPath:indexPath];
        UIButton *button = (UIButton *)[cell viewWithTag:1];
        ReminderDetails *reminderObj = (ReminderDetails *)[reminderArray objectAtIndex:indexPath.row];
        if(reminderObj.reminderdeOnOff==0){
            
            // Create a new expense object with the data from the database
            ReminderDetails *tempObj = [[ReminderDetails alloc] initWithID:reminderObj.reminderdetailid remindOnOff:1 message:reminderObj.message date:reminderObj.date completed:reminderObj.completed prevalue:reminderObj.prevalue pretype:reminderObj.pretype];
            NSDate *startDate = [NSDate dateWithTimeIntervalSinceReferenceDate:tempObj.date]; 
            if ([startDate timeIntervalSinceNow] > 0) {
                [reminderArray replaceObjectAtIndex:indexPath.row withObject:tempObj];
                [self addNotification:tempObj.reminderdetailid text:tempObj.message fireDate:startDate];
                [button setImage:[UIImage imageNamed:@"onbutton.png"] forState:UIControlStateNormal];
                [self updateReminderONOFFButtonSelection:tempObj.reminderdetailid selected:1];
            } else {
                UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                                   message: @"Reminder date is already passed."
                                                                  delegate: nil
                                                         cancelButtonTitle: @"OK"
                                                         otherButtonTitles: nil, nil];
                [ alert show ];
                [alert release];
            }
            [tempObj release];
        } else {
            // Create a new expense object with the data from the database
            ReminderDetails *tempObj = [[ReminderDetails alloc] initWithID:reminderObj.reminderdetailid remindOnOff:0 message:reminderObj.message date:reminderObj.date completed:reminderObj.completed prevalue:reminderObj.prevalue pretype:reminderObj.pretype];
            [reminderArray replaceObjectAtIndex:indexPath.row withObject:tempObj];
            [self cancelNotification:tempObj.reminderdetailid];
            [button setImage:[UIImage imageNamed:@"offbutton.png"] forState:UIControlStateNormal];
            [self updateReminderONOFFButtonSelection:tempObj.reminderdetailid selected:0];
            [tempObj release];
            
        }
	}    
}

-(void)addNotification:(int)lnotificationID text:(NSString *)text fireDate:(NSDate *)lfiredate {
    NSString *notificationstringID = [NSString stringWithFormat:@"%d", lnotificationID];
    UILocalNotification *localNotification = [[UILocalNotification alloc] init]; //Create the localNotification object
    if (localNotification == nil)
        return;
    [localNotification setFireDate:lfiredate]; //Set the date when the alert will be launched using the date adding the time the user selected on the timer
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    [localNotification setAlertAction:@"Launch"]; //The button's text that launches the application and is shown in the alert
    [localNotification setAlertBody:text]; //Set the message in the notification from the textField's text
    [localNotification setHasAction: YES]; //Set that pushing the button will launch the application
    [localNotification setApplicationIconBadgeNumber:[[UIApplication sharedApplication] applicationIconBadgeNumber]+1]; 
    //Set the Application Icon Badge Number of the application's icon to the current Application Icon Badge Number plus 1
    // Specify custom data for the notification
    NSDictionary *infoDict = [NSDictionary dictionaryWithObject:notificationstringID forKey:@"nissan"];
    localNotification.userInfo = infoDict;

    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification]; //Schedule the notification with the system
    [localNotification release];
}

-(void)cancelNotification:(NSInteger)notifyID {
    NSString *notificationID = [NSString stringWithFormat:@"%d", notifyID];
    UILocalNotification *notificationToCancel=nil;
    for(UILocalNotification *aNotif in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        if([[aNotif.userInfo objectForKey:@"nissan"] isEqualToString:notificationID]) {
            notificationToCancel=aNotif;
            break;
        }
    }
    if(notificationToCancel != nil)
        [[UIApplication sharedApplication] cancelLocalNotification:notificationToCancel];
}

- (IBAction)addReminderBtnClicked:(id)sender {
    [self AddNewReminder:nil];
}


-(void)updateReminderONOFFButtonSelection:(NSInteger)expId selected:(NSInteger)value
{
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"update REMINDERS set REMINDERONOFF=\"%d\"  where id=\"%d\"",  value , expId];
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            //[self showMessage:@"Contact updated"];
            //[self clearData:nil];
        } else {
            //[self showMessage:@"Contact updatation failed"];
        }
        sqlite3_finalize(statement);
    }         
    sqlite3_close(expenseDB);
}

- (void)checkreminderButtonTapped:(id)sender event:(id)event
{
	NSSet *touches = [event allTouches];
	UITouch *touch = [touches anyObject];
	CGPoint currentTouchPosition = [touch locationInView:self.reminderDetailtbl];
	NSIndexPath *indexPath = [self.reminderDetailtbl indexPathForRowAtPoint: currentTouchPosition];
    tempIndex = indexPath;
    [tempIndex retain];
	if (indexPath != nil)
	{
        UITableViewCell *cell  = (UITableViewCell *)[self.reminderDetailtbl cellForRowAtIndexPath:indexPath];
        UIButton *button = (UIButton *)[cell viewWithTag:4];
        ReminderDetails *reminderObj = (ReminderDetails *)[reminderArray objectAtIndex:indexPath.row];
        if(reminderObj.completed==0){
            
            // Create a new expense object with the data from the database
            //ReminderDetails *tempObj = [[ReminderDetails alloc] initWithID:reminderObj.reminderdetailid remindOnOff:reminderObj.reminderdeOnOff message:reminderObj.message date:reminderObj.date completed:1];
            //[reminderArray replaceObjectAtIndex:indexPath.row withObject:tempObj];
            [button setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
            //[self updateCompletedRowButtonSelection:tempObj.reminderdetailid selected:1];
            //[tempObj release];
            //[self reloadReminderTable:0];
            [self showAlert:0];

        } else {
            // Create a new expense object with the data from the database
            //ReminderDetails *tempObj = [[ReminderDetails alloc] initWithID:reminderObj.reminderdetailid remindOnOff:reminderObj.reminderdeOnOff message:reminderObj.message date:reminderObj.date completed:0];
            //[reminderArray replaceObjectAtIndex:indexPath.row withObject:tempObj];
            [button setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
            //[self updateCompletedRowButtonSelection:tempObj.reminderdetailid selected:0];
            //[tempObj release];
            //[self reloadReminderTable:1];
            [self showAlert:1];
        }
        
	}

}

-(void)showAlert:(int)tag
{
    UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                       message: @"Mark this reminder as completed?"
                                                      delegate: self
                                             cancelButtonTitle: nil
                                             otherButtonTitles: @"NO", @"YES", nil
                          ];
    [alert setTag:tag];
    [ alert show ];
    [alert release];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DebugLog(@"tempIndex %@",tempIndex);
    if ([tempIndex isKindOfClass:[NSIndexPath class]] && reminderArray.count > 0) {
    UITableViewCell *cell  = (UITableViewCell *)[self.reminderDetailtbl cellForRowAtIndexPath:tempIndex];
    UIButton *button = (UIButton *)[cell viewWithTag:4];
    ReminderDetails *reminderObj = (ReminderDetails *)[reminderArray objectAtIndex:tempIndex.row];
    if(alertView.tag == 0)
    {
        // set your logic
        if (buttonIndex == 1) {
            // Create a new expense object with the data from the database
            ReminderDetails *tempObj = [[ReminderDetails alloc] initWithID:reminderObj.reminderdetailid remindOnOff:0 message:reminderObj.message date:reminderObj.date completed:1 prevalue:reminderObj.prevalue pretype:reminderObj.pretype];
            [reminderArray replaceObjectAtIndex:tempIndex.row withObject:tempObj];
            [button setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
            [self cancelNotification:tempObj.reminderdetailid];
            [self updateCompletedRowButtonSelection:tempObj.reminderdetailid selected:1];
             [self updateReminderONOFFButtonSelection:tempObj.reminderdetailid selected:0];
            [tempObj release];
            [self reloadReminderTable:0];
        } else {
            [button setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
        }
    } else if (alertView.tag == 1) {
        // set your logic
        if (buttonIndex == 1) {
            // Create a new expense object with the data from the database
            ReminderDetails *tempObj = [[ReminderDetails alloc] initWithID:reminderObj.reminderdetailid remindOnOff:reminderObj.reminderdeOnOff message:reminderObj.message date:reminderObj.date completed:0 prevalue:reminderObj.prevalue pretype:reminderObj.pretype];
            [reminderArray replaceObjectAtIndex:tempIndex.row withObject:tempObj];
            [button setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
            [self updateCompletedRowButtonSelection:tempObj.reminderdetailid selected:0];
            [tempObj release];
            [self reloadReminderTable:1];
        } else {
            [button setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
        }
    }
    [tempIndex release];
    [reminderDetailtbl reloadData];
    }
}

-(void)updateCompletedRowButtonSelection:(NSInteger)expId selected:(NSInteger)value
{
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"update REMINDERS set COMPLETED=\"%d\"  where id=\"%d\"",  value , expId];
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            //[self showMessage:@"Contact updated"];
            //[self clearData:nil];
        } else {
            //[self showMessage:@"Contact updatation failed"];
        }
        sqlite3_finalize(statement);
    }         
    sqlite3_close(expenseDB);
}

- (void)AddNewReminder:(id) sender {
#ifdef GOOGLE_ANALYTICS
    if (![[GANTracker sharedTracker] trackEvent:@"Nissan CarUitl Application iOS"
                                         action:@"Add New Reminder Pressed"
                                          label:@""
                                          value:-1
                                      withError:NULL]) {
        DebugLog(@"error in trackEvent");
    }
#endif 
    UIViewController *reminderviewController = [[ReminderViewController alloc] initWithNibName:@"ReminderViewController" bundle:nil] ;
    reminderviewController.title = @"Add Reminder";
    //to push the UIView.
    reminderviewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:reminderviewController animated:YES];
    [reminderviewController release];
}

- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
        {
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            UIBarButtonItem *sideMenuBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                                                                 target:self.navigationController.sideMenu
                                                                                 action:@selector(toggleRightSideMenu)];

//            self.navigationItem.rightBarButtonItem = sideMeuBarButton; whiteplus.png
//            UIBarButtonItem *newReminderButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(AddNewReminder:)];
            UIBarButtonItem *newReminderButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"whiteplus.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(AddNewReminder:)];
            NSArray *array = [[NSArray alloc] initWithObjects:sideMenuBarButton,newReminderButton, nil];
            self.navigationItem.rightBarButtonItems = array;
            [array release];
            [newReminderButton release];
            [sideMenuBarButton release];
        }
            break;
        case MFSideMenuStateLeftMenuOpen:
        {
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
        }
            break;
        case MFSideMenuStateRightMenuOpen:
        {
            UIBarButtonItem *sideMenuBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                                                                 target:self.navigationController.sideMenu
                                                                                 action:@selector(toggleRightSideMenu)];

//            self.navigationItem.rightBarButtonItem = sideMeuBarButton;
            UIBarButtonItem *newReminderButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"whiteplus.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(AddNewReminder:)];
            NSArray *array = [[NSArray alloc] initWithObjects:sideMenuBarButton,newReminderButton, nil];
            self.navigationItem.rightBarButtonItems = array;
            [array release];
            [newReminderButton release];
            [sideMenuBarButton release];
        }
            break;
    }
}
- (UIBarButtonItem *)leftMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleLeftSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    //    return [[UIBarButtonItem alloc]
    //            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
    //            target:self.navigationController.sideMenu
    //            action:@selector(toggleRightSideMenu)];
    return [[UIBarButtonItem alloc] initWithTitle:@"Menu" style:UIBarButtonItemStyleBordered
                                           target:self.navigationController.sideMenu
                                           action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewDidUnload
{
    [self setReminderDetailtbl:nil];
    [self setEmptymessage:nil];
    [self setSegmentedControl:nil];
    [self setAddReminderBtn:nil];
    [self setInfolbl:nil];
    [self setHmsegmentedControl:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)dealloc {
    //[Flurry endTimedEvent:@"Reminder_Event" withParameters:nil];
    [reminderDetailtbl release];
    [reminderArray release];
    [emptymessage release];
    [segmentedControl release];
    [addReminderBtn release];
    [infolbl release];
    [hmsegmentedControl release];
    [super dealloc];
}

@end
