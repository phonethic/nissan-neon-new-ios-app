//
//  ReminderSettingsViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 17/07/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReminderSettingsViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource> {
    NSInteger prevalueid;
    NSString *pretype;
}
@property (readwrite, nonatomic) NSInteger prevalueid;
@property (nonatomic, copy) NSString *pretype;
@property (retain, nonatomic) IBOutlet UIPickerView *prealarmPicker;
@property (retain, nonatomic) IBOutlet UILabel *prevalue;
@property (retain, nonatomic) IBOutlet UIImageView *pickerMainView;
@property (retain, nonatomic) IBOutlet UILabel *prenotifylbl;

@end
