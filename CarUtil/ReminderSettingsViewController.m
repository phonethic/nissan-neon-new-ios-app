//
//  ReminderSettingsViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 17/07/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "ReminderSettingsViewController.h"
#import "ReminderViewController.h"

@interface ReminderSettingsViewController ()

@end

@implementation ReminderSettingsViewController
@synthesize prealarmPicker;
@synthesize prevalue;
@synthesize prevalueid;
@synthesize pretype;
@synthesize pickerMainView;
@synthesize prenotifylbl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    UIBarButtonItem *savenotifyButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(SavePreNotify:)];
    self.navigationItem.rightBarButtonItem = savenotifyButton;
    [savenotifyButton release];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
       DebugLog(@"setting %@ ",self.navigationController.viewControllers);
    
    if([self.title isEqualToString:@"Edit PreNotify"]) {
        
        DebugLog(@"prevalue1 = %d, pretype1 = %@",self.prevalueid,self.pretype);
        DebugLog(@"pretype val = %d",[self setType:self.pretype]);
        [prealarmPicker selectRow:[self setType:self.pretype] inComponent:1 animated:NO];
        [prealarmPicker reloadComponent:1];
        [prealarmPicker reloadComponent:0];
        if(self.prevalueid > 0)
            [prealarmPicker selectRow:self.prevalueid - 1 inComponent:0 animated:NO];
        else {
            [prealarmPicker selectRow:self.prevalueid inComponent:0 animated:NO];
        }
       [prealarmPicker reloadComponent:0];
        DebugLog(@"row = %d, text = %@",[self.prealarmPicker selectedRowInComponent:0],[self getType:[self.prealarmPicker selectedRowInComponent:1]]);

        if([self.pretype isEqualToString:@"No Pre-Notify"]) {
            prevalue.text = [NSString stringWithFormat:@"%@", [self getType:[self.prealarmPicker selectedRowInComponent:1]]];
        } else {
            prevalue.text = [NSString stringWithFormat:@"%d %@",[self.prealarmPicker selectedRowInComponent:0] + 1 ,[self getType:[self.prealarmPicker selectedRowInComponent:1]]];
        }
    } else {
        prevalue.text = [NSString stringWithFormat:@"%@",[self getType:[self.prealarmPicker selectedRowInComponent:1]]];
        [prealarmPicker reloadComponent:0];
    }
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        // code for 4-inch screen
        prealarmPicker.frame = CGRectMake(prealarmPicker.frame.origin.x, prealarmPicker.frame.origin.y + 90 , prealarmPicker.frame.size.width,  prealarmPicker.frame.size.height);
        pickerMainView.frame = CGRectMake(pickerMainView.frame.origin.x, pickerMainView.frame.origin.y + 90 , pickerMainView.frame.size.width,  pickerMainView.frame.size.height);
    }
    
    prenotifylbl.textColor = TABLE_SELECTION_COLOR;
    prenotifylbl.font = HELVETICA_FONT(16);
    prevalue.textColor = DEFAULT_COLOR;
    prevalue.font = HELVETICA_FONT(16);
}

- (void)SavePreNotify:(id) sender {
    ReminderViewController *parent = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    //call the method on the parent view controller
    if(parent != nil) 
        [parent SavePreNotifyValues:[self.prealarmPicker selectedRowInComponent:0] + 1 type:[self getType:[self.prealarmPicker selectedRowInComponent:1]]];
     parent = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Picker Data Source Methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (component){
        case 0: {
            if(self.isViewLoaded && self.view.window) {
                if([[self getType:[self.prealarmPicker selectedRowInComponent:1]] isEqualToString:@"No Pre-Notify"])
                {
                    //DebugLog(@"-----0----------");
                    return 0;
                } else {
                    //DebugLog(@"--------30----------");
                    return 30;
                }
            } else {
                if(![self.pretype isEqualToString:@"No Pre-Notify"] && [self.title isEqualToString:@"Edit PreNotify"])
                {
                    //DebugLog(@"-----0----------");
                    return 30;
                } else {
                    //DebugLog(@"--------30----------");
                    return 0;
                }
            }
        }
        case 1: 
            return 5;
    }
    [pickerView reloadAllComponents];
    return 0;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    switch (component){
        case 0: 
            return 80.0f;
        case 1: 
            return 240.0f;
    }
    return 0;
}

#pragma mark Picker Delegate Methods

-(NSString *)pickerView:(UIPickerView *)pickerView   titleForRow:(NSInteger)row   forComponent:(NSInteger)component 
{
    switch (component){
        case 0: 
            return [NSString stringWithFormat:@"%d",row + 1];
        case 1: 
            return [self getType:row];
    }
    return @"";
}

-(NSString *) getType:(int)lrow
{
    switch (lrow){
        case 0: return @"No Pre-Notify";
        case 1: return @"Minutes";
        case 2: return @"Hours";
        case 3: return @"Days";
        case 4: return @"Weeks";
    }
     return @"";
}

-(int) setType:(NSString *)ltype
{
    if([ltype isEqualToString:@"No Pre-Notify"])
       return 0;
    else if([ltype isEqualToString:@"Minutes"])
       return 1;
    else if([ltype isEqualToString:@"Hours"])
       return 2;
    else if([ltype isEqualToString:@"Days"])
       return 3;
    else if([ltype isEqualToString:@"Weeks"])
       return 4;
    else {
        return -1;
    }
}

- (void)pickerView:(UIPickerView *)thePickerView  didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    DebugLog(@"--------didSelectRow----------");
    if(component==1)
        [thePickerView reloadComponent:component-1];
	//get the values
	NSInteger row_one = [thePickerView selectedRowInComponent:0];
	NSInteger row_two = [thePickerView selectedRowInComponent:1];

    NSString *value_one = [NSString stringWithFormat:@"%d",row_one + 1];
    NSString *value_two = [self getType:row_two];
    
    if([value_two isEqualToString:@"No Pre-Notify"])
       prevalue.text = [NSString stringWithFormat:@"%@",value_two];
    else 
       prevalue.text = [NSString stringWithFormat:@"%@ %@",value_one,value_two];

}


- (void)viewDidUnload
{
    [self setPrealarmPicker:nil];
    [self setPrevalue:nil];
    [self setPickerMainView:nil];
    [self setPrenotifylbl:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)dealloc {
    pretype = nil;
    [prealarmPicker release];
    [prevalue release];
    [pickerMainView release];
    [prenotifylbl release];
    [super dealloc];
}
@end
