//
//  ReminderViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 23/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@class ReminderDetails;
@interface ReminderViewController : UIViewController <UITextFieldDelegate>{
    sqlite3 *expenseDB;
    NSInteger dbowID;
    //NSString *databasePath;
    NSInteger prevalue;
    NSString *prevaltype;
}
@property (retain, nonatomic) IBOutlet UITextField *typelbl;
@property (retain, nonatomic) IBOutlet UILabel *datelbl;
@property (retain, nonatomic) IBOutlet UIDatePicker *reminderdatePicker;
@property (retain, nonatomic) IBOutlet UIButton *currentTime;
@property (retain, nonatomic) IBOutlet UIButton *plusoneweek;
@property (retain, nonatomic) IBOutlet UIButton *plusonemonth;
@property (retain, nonatomic) IBOutlet UIButton *plussixmonths;
@property (retain, nonatomic) IBOutlet UIButton *atsevenam;
@property (retain, nonatomic) IBOutlet UIButton *attwelvepm;
@property (retain, nonatomic) IBOutlet UIButton *atfivepm;
@property (retain, nonatomic) IBOutlet UIButton *atninepm;
@property (readwrite, nonatomic) NSInteger dbowID;
@property (retain, nonatomic) IBOutlet UIButton *settingsbtn;
@property (readwrite, nonatomic) NSInteger prevalue;
@property (nonatomic, copy) NSString *prevaltype;
@property (retain, nonatomic) IBOutlet UIView *pickerMainView;
@property (retain, nonatomic) IBOutlet UILabel *notelbl;
@property (retain, nonatomic) IBOutlet UILabel *datelbl1;




- (IBAction)currentTimeSet:(id)sender;
- (IBAction)plusoneweekPressed:(id)sender;
- (IBAction)plusonemonthPressed:(id)sender;
- (IBAction)plussxmonthsPressed:(id)sender;
- (IBAction)plussevenamPressed:(id)sender;
- (IBAction)plustwelvepmpressed:(id)sender;
- (IBAction)plusfivepmPressed:(id)sender;
- (IBAction)plusninepmPressed:(id)sender;
- (IBAction)settingbtnPressed:(id)sender;

-(void) readeditReminderFromDatabase:(int) reminderId ;
- (void)SavePreNotifyValues:(int)lval type:(NSString *)lrval;
@end
