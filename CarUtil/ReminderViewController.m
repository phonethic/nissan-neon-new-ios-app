//
//  ReminderViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 23/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "ReminderViewController.h"
#import "ReminderDetailsViewController.h"
#import "ReminderSettingsViewController.h"
#import "ReminderDetails.h"
#import "CarUtilAppDelegate.h"

#import "MFSideMenu.h"
#import <QuartzCore/QuartzCore.h>

@interface ReminderViewController ()

@end

@implementation ReminderViewController
@synthesize typelbl;
@synthesize datelbl;
@synthesize reminderdatePicker;
@synthesize currentTime;
@synthesize plusoneweek;
@synthesize plusonemonth;
@synthesize plussixmonths;
@synthesize atsevenam;
@synthesize attwelvepm;
@synthesize atfivepm;
@synthesize atninepm;
@synthesize dbowID;
@synthesize settingsbtn;
@synthesize prevalue;
@synthesize prevaltype;
@synthesize pickerMainView;
@synthesize notelbl,datelbl1;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    UITouch *touch = [touches anyObject];
    
    if(touch.view.tag != 1) {
        [typelbl resignFirstResponder];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
#ifdef ENABLE_BACK_BUTTON
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [backButton release];
    
#endif
    
    DebugLog(@"reminder %@ ",self.navigationController.viewControllers);

    
    if([self.title isEqualToString:@"Edit Reminder"] || [self.title isEqualToString:@"Recycle"]) {
        UIBarButtonItem *editsaveremindButton = [[UIBarButtonItem alloc]  initWithTitle:@"Save" style:UIBarButtonSystemItemSave target:self action:@selector(EditReminder:)];
        self.navigationItem.rightBarButtonItem = editsaveremindButton;
        [editsaveremindButton release];
        [self readeditReminderFromDatabase:dbowID];
    } else {
        UIBarButtonItem *saveremindButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(SaveNewReminder:)];
        self.navigationItem.rightBarButtonItem = saveremindButton;
        [saveremindButton release];
        [reminderdatePicker setDate:[NSDate date] animated:YES];
        self.prevalue = 0;
        self.prevaltype = @"";
    }
    reminderdatePicker.timeZone = [NSTimeZone localTimeZone];
    [reminderdatePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    typelbl.delegate = self;
    [self dateChanged:nil];
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        // code for 4-inch screen
        pickerMainView.frame = CGRectMake(pickerMainView.frame.origin.x, pickerMainView.frame.origin.y + 90 , pickerMainView.frame.size.width,  pickerMainView.frame.size.height);
        settingsbtn.frame = CGRectMake(settingsbtn.frame.origin.x, settingsbtn.frame.origin.y + 30 , settingsbtn.frame.size.width,  settingsbtn.frame.size.height);
    }
    [self changeLabelFontAndTextColor:notelbl];
    [self changeLabelFontAndTextColor:datelbl1];
    
    [self changeLabel1FontAndTextColor:datelbl];
    [self changeTextFieldFontAndTextColor:typelbl];

    
    [self changeButtonFontAndTextColor:currentTime];
    [self changeButtonFontAndTextColor:plusoneweek];
    [self changeButtonFontAndTextColor:plusonemonth];
    [self changeButtonFontAndTextColor:plussixmonths];
    [self changeButtonFontAndTextColor:atsevenam];
    [self changeButtonFontAndTextColor:attwelvepm];
    [self changeButtonFontAndTextColor:atfivepm];
    [self changeButtonFontAndTextColor:atninepm];
    
    [settingsbtn setTitle:@"Reminder Settings" forState:UIControlStateNormal];
    [settingsbtn setTitle:@"Reminder Settings" forState:UIControlStateHighlighted];
    [settingsbtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [settingsbtn setTitleColor:TABLE_SELECTION_COLOR forState:UIControlStateHighlighted];
    [settingsbtn setBackgroundColor:DEFAULT_COLOR];
    settingsbtn.titleLabel.font = HELVETICA_FONT(14);
    
    settingsbtn.layer.cornerRadius = 5;
    settingsbtn.layer.masksToBounds = YES;
    settingsbtn.layer.borderColor = TABLE_SELECTION_COLOR.CGColor;
    settingsbtn.layer.borderWidth = 1.0;
}
-(void)changeLabelFontAndTextColor:(UILabel *)lbl
{
    lbl.textColor = TABLE_SELECTION_COLOR;
    lbl.font = HELVETICA_FONT(16);
}
-(void)changeLabel1FontAndTextColor:(UILabel *)lbl
{
    lbl.textColor = DEFAULT_COLOR;
    lbl.font = HELVETICA_FONT(14);
}
-(void)changeTextFieldFontAndTextColor:(UITextField *)textField
{
    textField.textColor = DEFAULT_COLOR;
    textField.font = HELVETICA_FONT(14);
}
-(void)changeButtonFontAndTextColor:(UIButton *)btn
{
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:TABLE_SELECTION_COLOR forState:UIControlStateHighlighted];
    [btn setBackgroundColor:[UIColor grayColor]];
    btn.titleLabel.font = HELVETICA_FONT(13.5);
    btn.layer.borderColor = TABLE_SELECTION_COLOR.CGColor;
    btn.layer.borderWidth = 2.0;
}
-(void)setPredatePicker:(double)predate status:(int) lstatus{
    if(lstatus == 0) { //Alarm is marked completed
        NSDate *preDate = [NSDate dateWithTimeIntervalSinceReferenceDate:predate]; 
        NSCalendar *gregorian1 = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *dateComponents1 = [gregorian1 components:(NSHourCalendarUnit  | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:preDate];
        NSInteger hour = [dateComponents1 hour];
        NSInteger minute = [dateComponents1 minute];
        NSInteger second = [dateComponents1 second];
       
        NSCalendar *gregorian2 = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *dateComponents2 = [gregorian2 components:(NSYearCalendarUnit  | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[NSDate date]]; 
        [dateComponents2 setHour:hour];
        [dateComponents2 setMinute:minute];
        [dateComponents2 setSecond:second];
        NSDate *newDate = [gregorian2 dateFromComponents:dateComponents2];
        NSDateFormatter *sdateFormat = [[NSDateFormatter alloc] init];
        [sdateFormat setDateFormat:@"dd MMMM yyyy"];
        NSString *sdateString = [sdateFormat stringFromDate:newDate];  
        datelbl.text = sdateString;
        [sdateFormat release];
        [gregorian1 release];
        [gregorian2 release];
        [reminderdatePicker setDate:newDate animated:NO];
    } else {
        NSDate *preDate = [NSDate dateWithTimeIntervalSinceReferenceDate:predate]; 
        NSDateFormatter *sdateFormat = [[NSDateFormatter alloc] init];
        [sdateFormat setDateFormat:@"dd MMMM yyyy"];
        NSString *sdateString = [sdateFormat stringFromDate:preDate];  
        datelbl.text = sdateString;
        [sdateFormat release];
        [reminderdatePicker setDate:preDate animated:NO];
    }
}

- (void) dateChanged:(id)sender {
    // handle date changes 
    NSDate *pickerDate = [reminderdatePicker date]; 
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMMM yyyy"];
    NSString *dateString = [dateFormat stringFromDate:pickerDate];  
    [dateFormat release];
    datelbl.text = dateString;
    
}

- (void)SavePreNotifyValues:(int)lval type:(NSString *)lrval {
    DebugLog(@"%d %@" , lval , lrval);
    self.prevalue = lval;
    self.prevaltype = lrval;
    
}

- (void)SaveNewReminder:(id) sender {
    [Flurry logEvent:@"Reminder_Added_Event"];
#ifdef TESTING
    [TestFlight passCheckpoint:@"New Reminder Added"];
#endif 
    DebugLog(@"%@ ",self.navigationController.viewControllers);
    ReminderDetailsViewController *parent = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    //call the method on the parent view controller
    if([self.prevaltype length] == 0)
        self.prevaltype = @"No Pre-Notify";
    DebugLog(@"--%d--%@--" , self.prevalue , self.prevaltype);
    if(parent != nil) 
        [parent addReminderData:[[[reminderdatePicker date] dateByAddingTimeInterval:10]timeIntervalSinceReferenceDate] text:typelbl.text fireDate:[[reminderdatePicker date] dateByAddingTimeInterval:10] prevalue:self.prevalue pretype:self.prevaltype];
     parent = nil;
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)EditReminder:(id) sender {
#ifdef TESTING
    [TestFlight passCheckpoint:@"Edit Reminder Pressed"];
#endif 
    if([self.title isEqualToString:@"Edit Reminder"]){
        ReminderDetailsViewController *parent = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
        //call the method on the parent view controller
        if(parent != nil) 
            [parent editReminderData:self.dbowID date:[[[reminderdatePicker date] dateByAddingTimeInterval:10]timeIntervalSinceReferenceDate] text:typelbl.text fireDate:[[reminderdatePicker date] dateByAddingTimeInterval:10] prevalue:self.prevalue pretype:self.prevaltype];
         parent = nil;
    } else {
        [self SaveNewReminder:nil];
    }
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void) readeditReminderFromDatabase:(int) reminderId {
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];

	// Open the database from the users filessytem
	if(sqlite3_open(dbpath, &expenseDB) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
        NSString *findSQL = [NSString stringWithFormat:@"select * from REMINDERS WHERE id=\"%d\"", reminderId];
        const char *sqlStatement = [findSQL UTF8String];
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(expenseDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
                //NSInteger primaryKey = sqlite3_column_int(compiledStatement, 0);
				int remindonoffCol = sqlite3_column_int(compiledStatement, 1);
                NSString *messageCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
				double dateCol = sqlite3_column_double(compiledStatement, 3);
				//int completedCol = sqlite3_column_int(compiledStatement, 4);
                NSInteger preval = sqlite3_column_int(compiledStatement, 6);
                NSString *pretype = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 7)];
                
                //DebugLog(@" %d - %d - %@ - %f - %d ",primaryKey, remindonoffCol, messageCol, dateCol, completedCol);
                typelbl.text = messageCol;
                [self setPredatePicker:dateCol status:remindonoffCol];
                 DebugLog(@"prevalue = %d, pretype = %@, on/off = %d",preval,pretype,remindonoffCol);
                self.prevalue = preval;
                self.prevaltype = pretype;
			}
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(expenseDB);
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	if (textField == typelbl) {
		[typelbl resignFirstResponder];
	}
   	return YES;
}

- (void)viewDidUnload
{
    [self setTypelbl:nil];
    [self setDatelbl:nil];
    [self setReminderdatePicker:nil];
    [self setCurrentTime:nil];
    [self setPlusoneweek:nil];
    [self setPlusonemonth:nil];
    [self setPlussixmonths:nil];
    [self setAtsevenam:nil];
    [self setAttwelvepm:nil];
    [self setAtfivepm:nil];
    [self setAtninepm:nil];
    [self setSettingsbtn:nil];
    [self setPickerMainView:nil];
    [self setNotelbl:nil];
    [self setDatelbl:nil];
    [self setDatelbl1:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)dealloc {
    prevaltype = nil;
    [typelbl release];
    [datelbl release];
    [reminderdatePicker release];
    [currentTime release];
    [plusoneweek release];
    [plusonemonth release];
    [plussixmonths release];
    [atsevenam release];
    [attwelvepm release];
    [atfivepm release];
    [atninepm release];
    [settingsbtn release];
    [pickerMainView release];
    [notelbl release];
    [datelbl1 release];
    [super dealloc];
}
- (IBAction)currentTimeSet:(id)sender {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMMM yyyy"];
    NSString *dateString = [dateFormat stringFromDate:[NSDate date]];  
    [dateFormat release];
    [reminderdatePicker setDate:[NSDate date] animated:YES];
    datelbl.text = dateString;
}

- (IBAction)plusoneweekPressed:(id)sender {
    NSDateComponents* dateComponents = [[NSDateComponents alloc]init];
    [dateComponents setWeek:1];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDate* newDate = [calendar dateByAddingComponents:dateComponents toDate:[reminderdatePicker date] options:0];
    [dateComponents release];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMMM yyyy"];
    NSString *dateString = [dateFormat stringFromDate:newDate];  
    [dateFormat release];
    [reminderdatePicker setDate:newDate animated:YES];
    datelbl.text = dateString;
}

- (IBAction)plusonemonthPressed:(id)sender {
    NSDateComponents* dateComponents = [[NSDateComponents alloc]init];
    [dateComponents setMonth:1];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDate* newDate = [calendar dateByAddingComponents:dateComponents toDate:[reminderdatePicker date] options:0];
    [dateComponents release];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMMM yyyy"];
    NSString *dateString = [dateFormat stringFromDate:newDate];  
    [dateFormat release];
    [reminderdatePicker setDate:newDate animated:YES];
    datelbl.text = dateString;

}

- (IBAction)plussxmonthsPressed:(id)sender {
    NSDateComponents* dateComponents = [[NSDateComponents alloc]init];
    [dateComponents setMonth:6];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDate* newDate = [calendar dateByAddingComponents:dateComponents toDate:[reminderdatePicker date] options:0];
    [dateComponents release];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMMM yyyy"];
    NSString *dateString = [dateFormat stringFromDate:newDate];  
    [dateFormat release];
    [reminderdatePicker setDate:newDate animated:YES];
    datelbl.text = dateString;
}

- (IBAction)plussevenamPressed:(id)sender {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dateComponents = [gregorian components:(NSYearCalendarUnit  | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[NSDate date]]; 
    [dateComponents setHour:7];
    [dateComponents setMinute:0];
    [dateComponents setSecond:0];
    NSDate *newDate = [gregorian dateFromComponents:dateComponents];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMMM yyyy"];
    NSString *dateString = [dateFormat stringFromDate:newDate];
    datelbl.text = dateString;
    [reminderdatePicker setDate:newDate animated:YES];
    [dateFormat release];
    [gregorian release];
}

- (IBAction)plustwelvepmpressed:(id)sender {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dateComponents = [gregorian components:(NSYearCalendarUnit  | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[NSDate date]]; 
    [dateComponents setHour:12];
    [dateComponents setMinute:0];
    [dateComponents setSecond:0];
    NSDate *newDate = [gregorian dateFromComponents:dateComponents];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMMM yyyy"];
    NSString *dateString = [dateFormat stringFromDate:newDate];
    datelbl.text = dateString;
    [reminderdatePicker setDate:newDate animated:YES];
    [dateFormat release];
    [gregorian release];
}

- (IBAction)plusfivepmPressed:(id)sender {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dateComponents = [gregorian components:(NSYearCalendarUnit  | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[NSDate date]]; 
    [dateComponents setHour:17];
    [dateComponents setMinute:0];
    [dateComponents setSecond:0];
    NSDate *newDate = [gregorian dateFromComponents:dateComponents];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMMM yyyy"];
    NSString *dateString = [dateFormat stringFromDate:newDate];
    datelbl.text = dateString;
    [reminderdatePicker setDate:newDate animated:YES];
    [dateFormat release];
    [gregorian release];
    
}

- (IBAction)plusninepmPressed:(id)sender {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dateComponents = [gregorian components:(NSYearCalendarUnit  | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[NSDate date]]; 
    [dateComponents setHour:21];
    [dateComponents setMinute:0];
    [dateComponents setSecond:0];
    NSDate *newDate = [gregorian dateFromComponents:dateComponents];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMMM yyyy"];
    NSString *dateString = [dateFormat stringFromDate:newDate];
    datelbl.text = dateString;
    [reminderdatePicker setDate:newDate animated:YES];
    [dateFormat release];
    [gregorian release];
}

- (IBAction)settingbtnPressed:(id)sender {
     if([self.title isEqualToString:@"Edit Reminder"] || [self.title isEqualToString:@"Recycle"]) {
         ReminderSettingsViewController *reminderSettingsController = [[ReminderSettingsViewController alloc] initWithNibName:@"ReminderSettingsViewController" bundle:nil] ;
         reminderSettingsController.title = @"Edit PreNotify";
         DebugLog(@"prevalue0 = %d, pretype0 = %@",self.prevalue,self.prevaltype);
         reminderSettingsController.prevalueid = self.prevalue;
         reminderSettingsController.pretype = self.prevaltype;
         //to push the UIView.
         reminderSettingsController.hidesBottomBarWhenPushed = YES;
         [self.navigationController pushViewController:reminderSettingsController animated:YES];
         [reminderSettingsController release];
     } else {
         ReminderSettingsViewController *reminderSettingsController = [[ReminderSettingsViewController alloc] initWithNibName:@"ReminderSettingsViewController" bundle:nil] ;
         reminderSettingsController.title = @"Add PreNotify";
         //to push the UIView.
         reminderSettingsController.hidesBottomBarWhenPushed = YES;
         [self.navigationController pushViewController:reminderSettingsController animated:YES];
         [reminderSettingsController release];
     }
}
@end
