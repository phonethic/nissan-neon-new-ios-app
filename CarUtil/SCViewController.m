/*
 * Copyright 2012 Facebook
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#import "SCViewController.h"
#import "CarUtilAppDelegate.h"

NSString *const kPlaceholderPostMessage = @"Say something about this...";

@interface SCViewController() <UINavigationControllerDelegate>

@property (strong, nonatomic) FBUserSettingsViewController *settingsViewController;
@property (strong, nonatomic) IBOutlet FBProfilePictureView *userProfileImage;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UIButton *announceButton;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;

- (IBAction)announce:(id)sender;
- (void)populateUserDetails;
- (void)centerAndShowActivityIndicator;

@end

@implementation SCViewController
@synthesize userNameLabel = _userNameLabel;
@synthesize userProfileImage = _userProfileImage;
@synthesize announceButton = _announceButton;
@synthesize activityIndicator = _activityIndicator;
@synthesize cancelButton = _cancelButton;
@synthesize settingsViewController = _settingsViewController;
@synthesize postParams = _postParams;
@synthesize appIcon = _appIcon;
@synthesize message = _message;
@synthesize loginFBBtn = _loginFBBtn;
@synthesize FBtitle = _FBtitle;
@synthesize FBtLink = _FBtLink;
@synthesize showorhide;
@synthesize loginCancelBtn;
@synthesize loginFBView;

- (void)resetPostMessage
{
    self.message.text = @"";
    self.message.textColor = [UIColor blackColor];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    // Clear the message text when the user starts editing
//    if ([textView.text isEqualToString:kPlaceholderPostMessage]) {
//        textView.text = @"";
//        textView.textColor = [UIColor blackColor];
//    }
    
    textView.textColor = [UIColor blackColor];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    // Reset to placeholder text if the user is done
    // editing and no message has been entered.
    if ([textView.text isEqualToString:@""]) {
        [self resetPostMessage];
    }
}
#pragma mark open graph


// FBSample logic
// Handles the user clicking the Announce button, by either creating an Open Graph Action
// or first uploading a photo and then creating the action.
- (IBAction)announce:(id)sender {

    // Add user message parameter if user filled it in
    if (![self.message.text isEqualToString:kPlaceholderPostMessage] &&
        ![self.message.text isEqualToString:@""]) {
        [self.postParams setObject:self.message.text forKey:@"message"];
        [self.postParams setObject:self.FBtitle forKey:@"name"];
        [self.postParams setObject:self.FBtLink forKey:@"link"];
    }
    
    [FBRequestConnection
     startWithGraphPath:@"me/feed"
     parameters:self.postParams
     HTTPMethod:@"POST"
     completionHandler:^(FBRequestConnection *connection,
                         id result,
                         NSError *error) {
         NSString *alertText;
         if (error) {
             alertText = [NSString stringWithFormat:
                          @"error: domain = %@, code = %d",
                          error.domain, error.code];
         } else {
//             alertText = [NSString stringWithFormat:
//                          @"Posted action, id: %@",
//                          [result objectForKey:@"id"]];
              alertText = @"Your message has been successfully posted on your facebook wall.";
             NSArray* stringComponents = [self.FBtLink  componentsSeparatedByString:@"/"];
             if([stringComponents count] >= 2 ) {
                 DebugLog(@"Car Name --%@--",[stringComponents objectAtIndex:[stringComponents count] - 2]);
                 NSDictionary *fbshareParams = [NSDictionary dictionaryWithObjectsAndKeys:[stringComponents objectAtIndex:[stringComponents count] - 2], @"CarName", nil];
                 [Flurry logEvent:@"Gallery_FB_Share_Event" withParameters:fbshareParams];
             }
         }
         // Show the result in an alert
         [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                     message:alertText
                                    delegate:nil
                           cancelButtonTitle:@"OK!"
                           otherButtonTitles:nil]
          show];
     }];
    
    [self dismissModalViewControllerAnimated:YES];
    
}

//- (void) alertView:(UIAlertView *)alertView
//didDismissWithButtonIndex:(NSInteger)buttonIndex
//{
//    [self resetPostMessage];
//
//}

- (void)centerAndShowActivityIndicator {
    CGRect frame = self.view.frame;
    CGPoint center = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame));
    self.activityIndicator.center = center;
    [self.activityIndicator startAnimating];

}
// FBSample logic
// Displays the user's name and profile picture so they are aware of the Facebook
// identity they are logged in as.
- (void)populateUserDetails {
    if (FBSession.activeSession.isOpen) {
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
             if (!error) {
                 //DebugLog(@"%@",user.id);
                 if(self.showorhide==1)
                 {
                     [self cancelModalView:nil];
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setObject:user.id forKey:@"userId"];
                     [defaults setObject:[FBSession.activeSession accessToken] forKey:@"accessToken"];
                     [defaults synchronize];
                     return ;
                 }
                 self.userNameLabel.text = user.name;
                 self.userProfileImage.profileID = [user objectForKey:@"id"];
                 _announceButton.enabled =  TRUE;
                 _announceButton.hidden = FALSE;
                 self.mainFBView.hidden = FALSE;
                 self.loginFBView.hidden = TRUE;
                //[_loginFBBtn setTitle:@"Logout" forState:UIControlStateNormal];
                 _loginFBBtn.hidden = TRUE;
                 DebugLog(@"id = %@ token = %@",user.id,[FBSession.activeSession accessToken]);
                 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                 [defaults setObject:user.id forKey:@"userId"];
                 [defaults setObject:[FBSession.activeSession accessToken] forKey:@"accessToken"];
                 [defaults synchronize];
             } else {
                 _announceButton.enabled =  FALSE;
                 _announceButton.hidden = TRUE;
                 _loginFBBtn.hidden = FALSE;
                 self.mainFBView.hidden = TRUE;
                 self.loginFBView.hidden = FALSE;
                 //[_loginFBBtn setTitle:@"Log In" forState:UIControlStateNormal];
             }
         }];   
    }
}


- (IBAction)loginFB:(id)sender {
    UIButton * button = (UIButton*) sender;
    if([button.titleLabel.text isEqualToString:@"Login"]) {
        CarUtilAppDelegate *appDelegate = (CarUtilAppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate openSessionWithAllowLoginUI:YES];
    } else {
        [FBSession.activeSession closeAndClearTokenInformation];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:@"" forKey:@"userId"];
        [defaults setObject:@"" forKey:@"accessToken"];
        [defaults synchronize];
        [self resetPostMessage];
        _announceButton.enabled =  FALSE;
        //[_loginFBBtn setTitle:@"Login" forState:UIControlStateNormal];
    }
}

- (IBAction)cancelModalView:(id)sender {
       [self dismissModalViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.postParams = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
     @"", @"link",
     @"http://upload.wikimedia.org/wikipedia/ru/8/8c/Nissan_logo.png", @"picture",
     @"Title of Post", @"name",
     @"", @"caption",
     @"Check out this post.", @"description",
     nil];
    UIImage *cancelButtonImage = [[UIImage imageNamed:@"DEFacebookSendButtonPortrait"] stretchableImageWithLeftCapWidth:4 topCapHeight:0];
    [self.cancelButton setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
    [self.announceButton setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
    [self.loginCancelBtn setBackgroundImage:cancelButtonImage forState:UIControlStateNormal];
    //_cancelButton.frame = CGRectMake(8, 7, 63, 30);
//    self.postParams =
//    [[NSMutableDictionary alloc] initWithObjectsAndKeys:
//     @"https://developers.facebook.com/ios", @"link",
//     @"https://developers.facebook.com/attachment/iossdk_logo.png", @"picture",
//     @"Facebook SDK for iOS", @"name",
//     @"Build great social apps and get more installs.", @"caption",
//     @"The Facebook SDK for iOS makes it easier and faster to develop Facebook integrated iOS apps.", @"description",
//     nil];
    
    // Set up the post information, hard-coded for this sample
    //self.name.text = [self.postParams objectForKey:@"name"];
    //self.caption.text = [self.postParams objectForKey:@"caption"];
    //[self.caption sizeToFit];
    //self.description.text = [self.postParams objectForKey:@"description"];
    //[self.description sizeToFit];
    
    self.message.text = [NSString stringWithFormat:@"%@\n", self.FBtitle];

    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithTitle:@"Settings" 
                                                style:UIBarButtonItemStyleBordered 
                                              target:self 
                                              action:@selector(settingsButtonWasPressed:)];

    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityIndicator.hidesWhenStopped = YES;
    [self.view addSubview:self.activityIndicator];

    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(sessionStateChanged:) 
                                                 name:SCSessionStateChangedNotification
                                               object:nil];
    self.mainFBView.layer.cornerRadius = 10;
    self.mainFBView.layer.masksToBounds = YES;
    //self.mainFBView.layer.borderColor = [UIColor whiteColor].CGColor;
    //self.mainFBView.layer.borderWidth = 2.0;
    //self.appIcon.layer.cornerRadius = 10;
    //self.appIcon.layer.masksToBounds = YES;
    //self.appIcon.layer.borderColor = [UIColor grayColor].CGColor;
    //self.appIcon.layer.borderWidth = 1.0;
    
    _announceButton.hidden = TRUE;
    _loginFBBtn.hidden = FALSE;
    self.mainFBView.hidden = TRUE;
    self.loginFBView.hidden = FALSE;
    CarUtilAppDelegate *appDelegate = (CarUtilAppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate openSessionWithAllowLoginUI:NO];
    
}

/*
 * A simple way to dismiss the message text view:
 * whenever the user clicks outside the view.
 */
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *) event
{
    UITouch *touch = [[event allTouches] anyObject];
    if ([self.message isFirstResponder] &&
        (self.message != touch.view))
    {
        [self.message resignFirstResponder];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (FBSession.activeSession.isOpen) {
        [self populateUserDetails];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [_message becomeFirstResponder];
}

-(void)settingsButtonWasPressed:(id)sender {
    if (self.settingsViewController == nil) {
        self.settingsViewController = [[FBUserSettingsViewController alloc] init];
    }
    [self.navigationController pushViewController:self.settingsViewController animated:YES];
}

- (void)viewDidUnload {
    [self setAppIcon:nil];
    [self setMessage:nil];
    [self setLoginFBBtn:nil];
    [self setCancelButton:nil];
    [self setLoginFBView:nil];
    [self setLoginCancelBtn:nil];
    [super viewDidUnload];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    // Release any retained subviews of the main view.
}

- (void)sessionStateChanged:(NSNotification*)notification {
    // A more complex app might check the state to see what the appropriate course of
    // action is, but our needs are simple, so just make sure our idea of the session is
    // up to date and repopulate the user's name and picture (which will fail if the session
    // has become invalid).
    [self populateUserDetails];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	DebugLog(@"toInterface %d",toInterfaceOrientation);
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        self.mainFBView.frame = CGRectMake(self.mainFBView.frame.origin.x, 12, self.mainFBView.frame.size.width, self.mainFBView.frame.size.height);
        self.loginFBBtn.frame = CGRectMake(self.loginFBBtn.frame.origin.x, 129, self.loginFBBtn.frame.size.width, self.loginFBBtn.frame.size.height);
    }
    else
    {
        self.mainFBView.frame = CGRectMake(self.mainFBView.frame.origin.x,10, self.mainFBView.frame.size.width, self.mainFBView.frame.size.height);
        self.loginFBBtn.frame = CGRectMake(self.loginFBBtn.frame.origin.x, 90, self.loginFBBtn.frame.size.width, self.loginFBBtn.frame.size.height);
    }
    
}
- (void)dealloc {
    [loginFBView release];
    [loginCancelBtn release];
    [super dealloc];
}

@end
