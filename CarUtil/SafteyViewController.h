//
//  SafteyViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 17/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface SafteyViewController : UIViewController <UIScrollViewDelegate,AVAudioPlayerDelegate>{
    UIScrollView *scrollView;
    UIPageControl *pageControl;
    AVAudioPlayer *audioPlayer;
    NSMutableArray *viewControllers;
    BOOL pageControlUsed;
    int tabbartype;
}
@property (nonatomic, retain) NSMutableArray *tipsObjsArray;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIPageControl *pageControl;
@property (nonatomic, retain) NSMutableArray *viewControllers;
@property (nonatomic, retain) AVAudioPlayer *audioPlayer;

- (IBAction)changePage:(id)sender;
- (void)settabbartype:(int)ltype ;
- (void)loadScrollViewWithPage:(int)page ;
- (void)unloadScrollViewWithPage:(int)page ;
@end