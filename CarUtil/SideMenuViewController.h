//
//  SideMenuViewController.h
//  MFSideMenuDemo
//
//  Created by Michael Frederick on 3/19/12.

#import <UIKit/UIKit.h>
#import "MFSideMenu.h"

#define BADGE_NOTIFICATIONAME @"badge push notification"

@class MKNumberBadgeView;
@interface SideMenuViewController : UITableViewController
{
    int currentindex;
}
@property (nonatomic, retain) NSMutableArray *sideMenuArray;
@property (nonatomic, assign) MFSideMenu *sideMenu;
@end