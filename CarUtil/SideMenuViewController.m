//
//  SideMenuViewController.m
//  MFSideMenuDemo
//
//  Created by Michael Frederick on 3/19/12.

#import "SideMenuViewController.h"
#import "MFSideMenu.h"

#import "GalleryGridViewController.h"
#import "MapKitDisplayViewController.h"
#import "ReminderDetailsViewController.h"
#import "CarUtilTrafficSignalsViewController.h"
#import "CarUtilThirrdViewController.h"
#import "MyCarViewController.h"
#import "ContactViewController.h"
#import "MKNumberBadgeView.h"

#define HOME @"Home"
#define FIND_MYCAR @"Find My Car"
#define REMINDERS @"Reminders"
#define TRAFFIC_SIGNS @"Traffic Signs"
#define TIPS @"Tips"
#define MYCAR @"My Car"
#define CONTACT_US @"Contact Us"


#define TIPS_BADGE @"tipsbadge"
#define GALLERY_BADGE @"gallerybadge"
#define REMINDER_BADGE @"reminderbadge"

@interface SideMenuViewController()
@end

@implementation SideMenuViewController

@synthesize sideMenu;
@synthesize sideMenuArray;


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:currentindex inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    if (self.tableView != nil) {
        UITableViewCell *cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:currentindex inSection:0]];
        UILabel *textLabel = (UILabel *)[cell viewWithTag:2];
        textLabel.textColor = [UIColor whiteColor];
        MKNumberBadgeView *badgeView = (MKNumberBadgeView *)[cell viewWithTag:3];
        badgeView.value = 0;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
 //       int oldbadgevalue = [defaults integerForKey:GALLERY_BADGE];
//        [UIApplication sharedApplication].applicationIconBadgeNumber -= oldbadgevalue;
        [defaults setInteger:0 forKey:GALLERY_BADGE];
        [defaults synchronize];
    }
}

- (void) viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.backgroundColor  = BACKGROUD_COLOR;
    self.tableView.separatorColor   = TABLE_SEPERATOR_COLOR;
    self.tableView.scrollEnabled = NO;
    
    sideMenuArray = [[NSMutableArray alloc] init];
    [sideMenuArray addObject:HOME];
    [sideMenuArray addObject:FIND_MYCAR];
    [sideMenuArray addObject:REMINDERS];
    [sideMenuArray addObject:TRAFFIC_SIGNS];
    [sideMenuArray addObject:TIPS];
    [sideMenuArray addObject:MYCAR];
    [sideMenuArray addObject:CONTACT_US];
    
//    CGRect searchBarFrame = CGRectMake(0, 0, self.tableView.frame.size.width, 45.0);
//    self.searchBar = [[UISearchBar alloc] initWithFrame:searchBarFrame];
//    self.searchBar.delegate = self;
//    
//    self.tableView.tableHeaderView = self.searchBar;
    
    UINavigationBar *tempNavigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [tempNavigationBar setBarStyle:UIBarStyleDefault];
    tempNavigationBar.tintColor = DEFAULT_COLOR;
    UINavigationItem *navItem = [UINavigationItem alloc];
    navItem.title = @"Menu";
    [tempNavigationBar pushNavigationItem:navItem animated:false];
    self.tableView.tableHeaderView = tempNavigationBar;

//    NSArray *fonts = [UIFont familyNames];
//
//    for(NSString *string in fonts){
//        DebugLog(@"%@", string);
//    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleResposeOfNotificationWithString:) name:BADGE_NOTIFICATIONAME object:nil];
    
    currentindex = 0;
}

#pragma mark -
#pragma mark - UITableViewDataSource

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    return [NSString stringWithFormat:@"Section %d", section];
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    CGRect screenBounds = [[UIScreen mainScreen] bounds];
//    if (screenBounds.size.height == 568) {
//        // code for 4-inch screen
//        return 504 / sideMenuArray.count;
//    }
//    return 416 / sideMenuArray.count;
    return 60;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [sideMenuArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UIImageView *cellImageView;
    UILabel *textLabel;
    MKNumberBadgeView* badgeTwo;
    NSString *text = [sideMenuArray objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
//        cell.textLabel.backgroundColor = [UIColor clearColor];
////        cell.textLabel.shadowColor = [UIColor blackColor];
////        cell.textLabel.shadowOffset = CGSizeMake(0, -1);
//
//        cell.textLabel.textColor = DEFAULT_COLOR;
//        cell.textLabel.font = HELVETICA_FONT(18.0);//[UIFont boldSystemFontOfSize:20.0];
//
//        if (indexPath.row == 0) { //Home
//            cell.imageView.image = [UIImage imageNamed:@"side_home.png"];
//        } else if (indexPath.row == 1) { //Find My Car
//            cell.imageView.image = [UIImage imageNamed:@"side_location.png"];
//        } else if (indexPath.row == 2) { //Reminders
//            cell.imageView.image = [UIImage imageNamed:@"side_reminder.png"];
//        } else if (indexPath.row == 3) { //Traffic Signs
//            cell.imageView.image = [UIImage imageNamed:@"side_traffic.png"];
//        } else if (indexPath.row == 4) { //Tips
//            cell.imageView.image = [UIImage imageNamed:@"side_tips.png"];
//        } else if (indexPath.row == 5) { //My Car
//            cell.imageView.image = [UIImage imageNamed:@"side_mycar.png"];
//        }  else if (indexPath.row == 6) { //Contact Us
//            cell.imageView.image = [UIImage imageNamed:@"side_contact.png"];
//        }

        cellImageView = [[UIImageView alloc] initWithFrame:CGRectMake(cell.frame.origin.x+10 , cell.frame.origin.y + 15, 30, 30)];
        cellImageView.contentMode = UIViewContentModeScaleToFill;
        cellImageView.backgroundColor =  [UIColor clearColor];
        cellImageView.tag = 1;
        [cell.contentView addSubview:cellImageView];
        
        //Initialize Label with tag 2.(Fuel Label)
        textLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellImageView.frame)+10, cell.frame.origin.y+17, 120, 30)];
        textLabel.tag = 2;
        textLabel.font = HELVETICA_FONT(16.0);
        textLabel.textColor = [UIColor darkGrayColor];
        textLabel.lineBreakMode =  UILineBreakModeTailTruncation;
        textLabel.textAlignment = UITextAlignmentLeft;
        textLabel.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:textLabel];
        
        if ([text isEqualToString:HOME] || [text isEqualToString:REMINDERS]  || [text isEqualToString:TIPS] ) {
            badgeTwo = [[MKNumberBadgeView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(textLabel.frame)-25, 13, 60, 40)];
            badgeTwo.hideWhenZero = YES;
            badgeTwo.backgroundColor = [UIColor clearColor];
            badgeTwo.tag = 3;
            badgeTwo.value = 0;
            badgeTwo.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:13];//HELVETICA_FONT(13);
            badgeTwo.shine = NO;
            badgeTwo.alignment = UITextAlignmentCenter;
            badgeTwo.fillColor = DEFAULT_COLOR;
            badgeTwo.textColor = [UIColor whiteColor];
            badgeTwo.strokeWidth = 0.0;
            badgeTwo.strokeColor = [UIColor whiteColor];
            badgeTwo.shadow = YES;
            badgeTwo.shadowOffset = CGSizeMake(1, 1);
            badgeTwo.shadowColor = [[UIColor blackColor] colorWithAlphaComponent:1];
            [cell.contentView addSubview:badgeTwo];
        }
        cell.backgroundColor = [UIColor whiteColor];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = TABLE_SELECTION_COLOR;
        cell.selectedBackgroundView = bgColorView;
        
        cell.textLabel.highlightedTextColor = [UIColor whiteColor];
    }
    
//    cell.textLabel.text = [sideMenuArray objectAtIndex:indexPath.row];
    cell.selectionStyle=UITableViewCellSelectionStyleBlue;
    
    cellImageView = (UIImageView *)[cell viewWithTag:1];
    if ([text isEqualToString:HOME]) { //Home
        cellImageView.image = [UIImage imageNamed:@"side_home.png"];
    } else if ([text isEqualToString:FIND_MYCAR]) { //Find My Car
        cellImageView.image = [UIImage imageNamed:@"side_location.png"];
    } else if ([text isEqualToString:REMINDERS]) { //Reminders
        cellImageView.image = [UIImage imageNamed:@"side_reminder.png"];
    } else if ([text isEqualToString:TRAFFIC_SIGNS]) { //Traffic Signs
        cellImageView.image = [UIImage imageNamed:@"side_traffic.png"];
    } else if ([text isEqualToString:TIPS]) { //Tips
        cellImageView.image = [UIImage imageNamed:@"side_tips.png"];
    } else if ([text isEqualToString:MYCAR]) { //My Car
        cellImageView.image = [UIImage imageNamed:@"side_mycar.png"];
    }  else if ([text isEqualToString:CONTACT_US]) { //Contact Us
        cellImageView.image = [UIImage imageNamed:@"side_contact.png"];
    }
    
    textLabel = (UILabel *)[cell viewWithTag:2];
    textLabel.text = text;
    textLabel.textColor = [UIColor darkGrayColor];
    return cell;
}

#pragma mark -
#pragma mark - Seperator
- (UIImage *)separatorImage
{
    UIGraphicsBeginImageContext(CGSizeMake(1, 4));
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIGraphicsPushContext(context);
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:1].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 2));
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:79/255.0 green:79/255.0 blue:77/255.0 alpha:1].CGColor);
    CGContextFillRect(context, CGRectMake(0, 3, 1, 2));
    UIGraphicsPopContext();
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithCGImage:outputImage.CGImage scale:2.0 orientation:UIImageOrientationUp];
}

#pragma mark -
#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
//    cell.contentView.backgroundColor = [UIColor colorWithRed:209/255.0 green:196/255.0 blue:182/255.0 alpha:1];
//    cell.textLabel.textColor = [UIColor blackColor];
    
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    UILabel *textLabel = (UILabel *)[cell viewWithTag:2];
    textLabel.textColor = [UIColor darkGrayColor];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if (currentindex == indexPath.row) {
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        return;
    }
    
    currentindex = indexPath.row;
    
    DebugLog(@"row=%d",indexPath.row);
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    UILabel *textLabel = (UILabel *)[cell viewWithTag:2];
    textLabel.textColor = [UIColor whiteColor];

    
    NSString *text = [sideMenuArray objectAtIndex:indexPath.row];
    if ([text isEqualToString:HOME])   //HOME
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        DebugLog(@"%@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[GalleryGridViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        }
        GalleryGridViewController *homeController = [[GalleryGridViewController alloc] initWithNibName:@"GalleryGridViewController" bundle:nil];
        homeController.title = @"NISSAN";
        NSArray *controllers = [NSArray arrayWithObject:homeController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        
        MKNumberBadgeView *badgeView = (MKNumberBadgeView *)[cell viewWithTag:3];
        badgeView.value = 0;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        int oldbadgevalue = [defaults integerForKey:GALLERY_BADGE];
//        [UIApplication sharedApplication].applicationIconBadgeNumber -= oldbadgevalue;
        [defaults setInteger:0 forKey:GALLERY_BADGE];
        [defaults synchronize];
    }
   else if ([text isEqualToString:FIND_MYCAR])  //Find my car
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        DebugLog(@"%@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[MapKitDisplayViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        }
        MapKitDisplayViewController *findMycarController = [[MapKitDisplayViewController alloc] initWithNibName:@"MapKitDisplayViewController" bundle:nil];
        findMycarController.title = @"Find My Car";
        NSArray *controllers = [NSArray arrayWithObject:findMycarController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
    }
    else if ([text isEqualToString:REMINDERS])   //Reminder
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        DebugLog(@"%@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[ReminderDetailsViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        }
        
        ReminderDetailsViewController *reminderController = [[ReminderDetailsViewController alloc] initWithNibName:@"ReminderDetailsViewController" bundle:nil];
        reminderController.title = @"Reminders";
        
        //
        //            UINavigationController *myCarNavigationController = [ [ UINavigationController alloc ] initWithRootViewController: reminderController ];
        //            myCarNavigationController.navigationBar.tag = 1;
        
        NSArray *controllers = [NSArray arrayWithObject:reminderController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        
        MKNumberBadgeView *badgeView = (MKNumberBadgeView *)[cell viewWithTag:3];
        badgeView.value = 0;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        int oldbadgevalue = [defaults integerForKey:REMINDER_BADGE];
//        [UIApplication sharedApplication].applicationIconBadgeNumber -= oldbadgevalue;
        [defaults setInteger:0 forKey:REMINDER_BADGE];
        [defaults synchronize];
    }
    else if ([text isEqualToString:TRAFFIC_SIGNS])   //Traffic Rules
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        DebugLog(@"%@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[CarUtilTrafficSignalsViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        }
        CarUtilTrafficSignalsViewController *trafficRulesController = [[CarUtilTrafficSignalsViewController alloc] initWithNibName:@"CarUtilTrafficSignalsViewController" bundle:nil];
        trafficRulesController.title = @"Traffic Signs";
        NSArray *controllers = [NSArray arrayWithObject:trafficRulesController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
    }
    else if ([text isEqualToString:TIPS])   //Tips
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        DebugLog(@"%@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[CarUtilThirrdViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        }
        CarUtilThirrdViewController *tipsController = [[CarUtilThirrdViewController alloc] initWithNibName:@"CarUtilThirrdViewController" bundle:nil];
        tipsController.title = @"Tips";
        NSArray *controllers = [NSArray arrayWithObject:tipsController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        
        MKNumberBadgeView *badgeView = (MKNumberBadgeView *)[cell viewWithTag:3];
        badgeView.value = 0;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        int oldbadgevalue = [defaults integerForKey:TIPS_BADGE];
//        [UIApplication sharedApplication].applicationIconBadgeNumber -= oldbadgevalue;
        [defaults setInteger:0 forKey:TIPS_BADGE];
        [defaults synchronize];
    }
    else if ([text isEqualToString:MYCAR])   //MyCar
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        DebugLog(@"%@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[MyCarViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        }
        MyCarViewController *mycarController = [[MyCarViewController alloc] initWithNibName:@"MyCarViewController" bundle:nil];
        mycarController.title = @"My Car";
        NSArray *controllers = [NSArray arrayWithObject:mycarController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
    }
    else if ([text isEqualToString:CONTACT_US])  //Contact Us
    {
        NSArray *controllersArray = self.sideMenu.navigationController.viewControllers;
        DebugLog(@"%@",[controllersArray objectAtIndex:[controllersArray count] - 1]);
        id obj = [controllersArray objectAtIndex:[controllersArray count] - 1];
        if ([obj isKindOfClass:[ContactViewController class]])
        {
            [self.sideMenu setMenuState:MFSideMenuStateClosed];
            return;
        }
        ContactViewController *contactController = [[ContactViewController alloc] initWithNibName:@"ContactViewController" bundle:nil];
        contactController.title = @"Contact Us";
        NSArray *controllers = [NSArray arrayWithObject:contactController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
    }
}

-(void)handleResposeOfNotificationWithString:(NSNotification *)notification
{
    NSDictionary *dict = [notification userInfo];
    DebugLog(@"handleResposeOfNotificationWithString dict = %@",dict);
//    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
//    [dict setObject:@"nissan" forKey:@"nissan"];
    
    if ([dict objectForKey:@"aps"] != nil) {
        NSDictionary *mainDict = [dict objectForKey:@"aps"];
        DebugLog(@"type = %@",[dict objectForKey:@"title"]);
        NSString *tabType = [dict objectForKey:@"type"];
        int badgevalue = [[mainDict objectForKey:@"badge"] intValue];
        DebugLog(@"badge = %d",badgevalue);

        if(tabType != nil && [tabType isEqualToString:@"tips"])
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            int oldbadgevalue = [defaults integerForKey:TIPS_BADGE];
            DebugLog(@"tips oldbadgevalue = %d",oldbadgevalue);

            UITableViewCell *cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[sideMenuArray indexOfObject:TIPS] inSection:0]];
            MKNumberBadgeView *badgeView = (MKNumberBadgeView *)[cell viewWithTag:3];
            badgeView.value = oldbadgevalue + badgevalue;
            
            [defaults setInteger:oldbadgevalue+badgevalue forKey:TIPS_BADGE];
            [defaults synchronize];
        }
        else if(tabType != nil && [tabType isEqualToString:@"gallery"])
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            int oldbadgevalue = [defaults integerForKey:GALLERY_BADGE];
            DebugLog(@"gallery oldbadgevalue = %d",oldbadgevalue);

            UITableViewCell *cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[sideMenuArray indexOfObject:HOME] inSection:0]];
            MKNumberBadgeView *badgeView = (MKNumberBadgeView *)[cell viewWithTag:3];
            badgeView.value = oldbadgevalue + badgevalue;
            
            [defaults setInteger:oldbadgevalue+badgevalue forKey:GALLERY_BADGE];
            [defaults synchronize];
        }
    }
    else if ([dict objectForKey:@"nissan"] != nil) {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        int oldbadgevalue = [defaults integerForKey:REMINDER_BADGE];
        
        UITableViewCell *cell = (UITableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[sideMenuArray indexOfObject:REMINDERS] inSection:0]];
        MKNumberBadgeView *badgeView = (MKNumberBadgeView *)[cell viewWithTag:3];
        badgeView.value = oldbadgevalue + 1;
        
        [defaults setInteger:oldbadgevalue+1 forKey:REMINDER_BADGE];
        [defaults synchronize];
    }
}
@end
