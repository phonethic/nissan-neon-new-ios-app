//
//  animationsTestViewController.m
//  animationsTest
//
//  Created by Rishi Saxena on 28/03/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "animationsTestViewController.h"
#import "CarUtilAppDelegate.h"

// Our conversion definition
#define DEGREES_TO_RADIANS(angle) (angle / 180.0 * M_PI)

@interface animationsTestViewController ()

@end

@implementation animationsTestViewController
@synthesize micraBody,sunnyBody,xtrailBody;
@synthesize enterButton,enterButton1,enterButton2,resetButton;
@synthesize micratyre1,micratyre2,sunnytyre1,sunnytyre2,xtrailtyre1,xtrailtyre2;
@synthesize micraView,sunnyView,xtrailView;
@synthesize moviePlayer;
@synthesize audioPlayer;

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {

}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch =[touches anyObject]; 
    //CGPoint location =[touch locationInView:self.view];
    //NSLog(@"Touched view  %@",[touch.view class] );
    
    //NSLog(@"%f %f",touch.view.frame.origin.x,touch.view.bounds.origin.y);
    //NSLog(@"%f %f",touch.view.bounds.origin.x,touch.view.bounds.origin.y);
    //NSLog(@"%f %f",touch.view.bounds.size.width,touch.view.bounds.size.height);
    
    NSArray *allTouches = [touches allObjects];
    for (UITouch *touch in allTouches)
    {
        NSLog(@"%d",touch.view.tag);
    }
    
//    if([[touch view] isKindOfClass:[UIView class]])
//    {
//        if(CGRectContainsPoint([self.xtrailBody frame], location))
//        {
//            NSInteger imgTag = [touch view].tag;
//            //NSLog(@"%d",imgTag);
//        } else if(CGRectContainsPoint([self.sunnyBody frame], location)) {
//            NSInteger imgTag = [touch view].tag;
//            //NSLog(@"%d",imgTag);
//        } else if(CGRectContainsPoint([self.micraBody frame], location)) {
//            NSInteger imgTag = [touch view].tag;
//            //NSLog(@"%d",imgTag);
//        }
//    }
    
    if (touch.view == self.xtrailBody){
        [self changeAnimation:nil];
    } else if (touch.view == self.sunnyBody) {
        [self changeAnimation1:nil];
    } else if (touch.view == self.micraBody) {
        [self changeAnimation2:nil];
    }
    self.xtrailBody.userInteractionEnabled = FALSE;
    self.micraBody.userInteractionEnabled = FALSE;
    self.sunnyBody.userInteractionEnabled = FALSE;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
}

-(void)backCABasicAnimation:(UIImageView *)lview
{
    CABasicAnimation *theAnimation; 
    theAnimation=[CABasicAnimation animationWithKeyPath:@"transform.translation.x"];
    theAnimation.duration = 2;
    theAnimation.repeatCount = 1;
    theAnimation.autoreverses = NO;
    theAnimation.fromValue=[NSNumber numberWithFloat:0];
    theAnimation.toValue=[NSNumber numberWithFloat:0];
    [theAnimation setRemovedOnCompletion:NO];
    [theAnimation setFillMode:kCAFillModeBackwards];
    [lview.layer addAnimation:theAnimation forKey:@"backanimateLayer"];
}

- (IBAction)reset:(id)sender
{

    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:xtrailView cache:YES];
    [xtrailView setFrame:CGRectMake(0.0f,0.0f,320.0f,480.0f)];
    [sunnyView setFrame:CGRectMake(0.0f,-160.0f,320.0f,480.0f)];
    [micraView setFrame:CGRectMake(0.0f,-320.0f,320.0f,480.0f)];
    [UIView commitAnimations];
    
    [self backCABasicAnimation:xtrailBody];
    [self backCABasicAnimation:sunnyBody];
    [self backCABasicAnimation:micraBody];
    [self backCABasicAnimation:xtrailtyre1];
    [self backCABasicAnimation:xtrailtyre2];
    [self backCABasicAnimation:sunnytyre1];
    [self backCABasicAnimation:sunnytyre2];
    [self backCABasicAnimation:micratyre1];
    [self backCABasicAnimation:micratyre2];
    [self.view bringSubviewToFront:xtrailBody];
    [self.view bringSubviewToFront:xtrailtyre1];
    [self.view bringSubviewToFront:xtrailtyre2];
    [sunnytyre1 setFrame:CGRectMake(236.0f,253.0f,41.0f,41.0f)];
    [sunnytyre2 setFrame:CGRectMake(52.0f,253.0f,41.0f,41.0f)];
    
    
    
    //    CABasicAnimation *rotationAnimation;
    //    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    //    rotationAnimation.fromValue = [NSNumber numberWithFloat:0];
    //    rotationAnimation.toValue = [NSNumber numberWithFloat:DEGREES_TO_RADIANS(360)];                         
    //    rotationAnimation.duration = 0.20;
    //    rotationAnimation.repeatCount = 100.0; 
    //    rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    //    rotationAnimation.removedOnCompletion = NO;
    //    [self.tempView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    
    //    UIImageView *imageToMove = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"car2.png"]];
    //    imageToMove.frame = CGRectMake(10, 10, 20, 100);
    //    
    //    [self.view addSubview:imageToMove];
    //    
    //    [self rotateImage:imageToMove duration:0.15 curve:UIViewAnimationCurveEaseIn degrees:350];
    
    //    CABasicAnimation* anim = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    //    anim.removedOnCompletion = YES;
    //    [anim setToValue:[NSNumber numberWithFloat:0.0f]];
    //    [anim setFromValue:[NSNumber numberWithDouble:M_PI/16]]; // rotation angle
    //    [anim setDuration:1.0];
    //    [anim setRepeatCount:2];
    //    [anim setAutoreverses:YES];
    //    [self.tempView.layer addAnimation:anim forKey:@"iconShake"];
    
    //    CAAnimationGroup *animationGroup = [CAAnimationGroup animation];
    //    animationGroup.removedOnCompletion = YES;
    //    
    //    CABasicAnimation *fadeAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    //    fadeAnimation.fromValue = [NSNumber numberWithFloat:0.0];
    //    fadeAnimation.toValue = [NSNumber numberWithFloat:1.0];
    //    
    //    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    //    scaleAnimation.fromValue = [NSNumber numberWithFloat:0.5];
    //    scaleAnimation.toValue = [NSNumber numberWithFloat:1.00];
    //    
    //    animationGroup.animations = [NSArray arrayWithObjects:fadeAnimation, scaleAnimation, nil];
    //    animationGroup.duration = 2.0;
    //    
    //    [[self.tempView layer]addAnimation:animationGroup forKey:@"fadeAnimation"];
    //    self.tempView.layer.opacity = 1.0;

}

-(void)moveForward:(UIImageView *)lview
{
    
    CABasicAnimation *theAnimation; 
    theAnimation=[CABasicAnimation animationWithKeyPath:@"transform.translation.x"];
    theAnimation.duration = 2;
    theAnimation.repeatCount = 1;
    theAnimation.autoreverses = NO;
    theAnimation.delegate = self;
    theAnimation.fromValue=[NSNumber numberWithFloat:0];
    theAnimation.toValue=[NSNumber numberWithFloat:320];
    [theAnimation setRemovedOnCompletion:NO];
    [theAnimation setFillMode:kCAFillModeForwards];
    [lview.layer addAnimation:theAnimation forKey:@"fwdanimateLayer"];
}

-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    if(stopAnim == 0)
    {
        [(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] animationStop];
        stopAnim = 1;
    }
}

- (void)rotateImage:(UIImageView *)lview
{
    CABasicAnimation *fullRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    fullRotation.fromValue = [NSNumber numberWithFloat:0];
    fullRotation.toValue = [NSNumber numberWithFloat:((360*M_PI)/180)];
    fullRotation.duration = 1.00;
    fullRotation.repeatCount = 3;
    fullRotation.removedOnCompletion = YES;
    [lview.layer addAnimation:fullRotation forKey:@"360"];
    
}

- (IBAction)changeAnimation:(UIButton *)sender
{
   
    
    
    //    [UIView animateWithDuration:2.0 
    //                          delay:0.0 
    //                        options:UIViewAnimationCurveEaseInOut 
    //                     animations:^ {
    //                         sender.alpha = 0.0;
    //                     } 
    //                     completion:^(BOOL finished) {
    //                         sender.hidden = YES;
    //                     }];

    [audioPlayer play];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:1.5];
    [UIView setAnimationDidStopSelector:@selector(transitionDidStop:finished:context:)];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:sunnyView cache:YES];
    [sunnyView setFrame:CGRectMake(0.0f,-480.0f,320.0f,480.0f)];
    [sunnyBody setFrame:CGRectMake(2.0f,-160.0f,308.0f,135.0f)];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:micraView cache:YES];
    [micraView setFrame:CGRectMake(0.0f,-480.0f,320.0f,480.0f)];
    [micraBody setFrame:CGRectMake(23.0f,-160.0f,274.0f,115.0f)];
    [UIView commitAnimations];
    

    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.40];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:sunnytyre1 cache:YES];
    [sunnytyre1 setFrame:CGRectMake(236.0f,-41.0f,41.0f,41.0f)];
    [sunnytyre2 setFrame:CGRectMake(52.0f,-41.0f,41.0f,41.0f)];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.15];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:micratyre1 cache:YES];
    [micratyre1 setFrame:CGRectMake(229.0f,-137.0f,41.0f,41.0f)];
    [micratyre2 setFrame:CGRectMake(46.0f,-137.0f,41.0f,41.0f)];
    [UIView commitAnimations];

}

- (IBAction)changeAnimation1:(id)sender
{
 [audioPlayer play];   
    [self.view bringSubviewToFront:xtrailView];
    [self.view bringSubviewToFront:xtrailBody];
    [self.view bringSubviewToFront:xtrailtyre1];
    [self.view bringSubviewToFront:xtrailtyre2];
    [self.view bringSubviewToFront:sunnyView];
    [self.view bringSubviewToFront:sunnyBody];
    [self.view bringSubviewToFront:sunnytyre1];
    [self.view bringSubviewToFront:sunnytyre2];
    [self.view bringSubviewToFront:micraView];
    [self.view bringSubviewToFront:micraBody];
     [self.view bringSubviewToFront:micratyre1];
     [self.view bringSubviewToFront:micratyre2];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(transitionDidStop1:finished:context:)];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:sunnyView cache:YES];
    [sunnyView setFrame:CGRectMake(0.0f,0.0f,320.0f,480.0f)];
    [sunnyBody setFrame:CGRectMake(2.0f,320.0f,308.0f,135.0f)];
    [micraView setFrame:CGRectMake(0.0f,-480.0f,320.0f,480.0f)];
    [micraBody setFrame:CGRectMake(23.0f,-160.0f,274.0f,115.0f)];
    [UIView commitAnimations];
    
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.15];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:micratyre1 cache:YES];
    [micratyre1 setFrame:CGRectMake(229.0f,-137.0f,41.0f,41.0f)];
    [micratyre2 setFrame:CGRectMake(46.0f,-137.0f,41.0f,41.0f)];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:sunnytyre1 cache:YES];
    [sunnytyre1 setFrame:CGRectMake(236.0f,413.0f,41.0f,41.0f)];
    [sunnytyre2 setFrame:CGRectMake(52.0f,413.0f,41.0f,41.0f)];
    [UIView commitAnimations];

}
- (IBAction)changeAnimation2:(id)sender
{
   [audioPlayer play];
    [self.view bringSubviewToFront:xtrailView];
    [self.view bringSubviewToFront:xtrailBody];
    [self.view bringSubviewToFront:xtrailtyre1];
    [self.view bringSubviewToFront:xtrailtyre2];
    [self.view bringSubviewToFront:sunnyView];
    [self.view bringSubviewToFront:sunnyBody];
    [self.view bringSubviewToFront:sunnytyre1];
    [self.view bringSubviewToFront:sunnytyre2];
    [self.view bringSubviewToFront:micraView];
    [self.view bringSubviewToFront:micraBody];
    [self.view bringSubviewToFront:micratyre1];
    [self.view bringSubviewToFront:micratyre2];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(transitionDidStop2:finished:context:)];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:micraView cache:YES];
    [micraView setFrame:CGRectMake(0.0f,0.0f,320.0f,480.0f)];
    [micraBody setFrame:CGRectMake(23.0f,330.0f,274.0f,115.0f)];
    [sunnyView setFrame:CGRectMake(0.0f,0.0f,320.0f,480.0f)];
    [micratyre1 setFrame:CGRectMake(229.0f,408.0f,41.0f,41.0f)];
    [micratyre2 setFrame:CGRectMake(46.0f,408.0f,41.0f,41.0f)];
    [sunnyBody setFrame:CGRectMake(2.0f,320.0f,308.0f,135.0f)];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:sunnytyre1 cache:YES];
    [sunnytyre1 setFrame:CGRectMake(236.0f,413.0f,41.0f,41.0f)];
    [sunnytyre2 setFrame:CGRectMake(52.0f,413.0f,41.0f,41.0f)];
    [UIView commitAnimations];
   

}
- (void)transitionDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [self moveForward:self.xtrailBody];
    [self moveForward:self.xtrailtyre1];
    [self moveForward:self.xtrailtyre2];
    [self rotateImage:xtrailtyre1];
    [self rotateImage:xtrailtyre2];
    //[audioPlayer play];
}

- (void)transitionDidStop1:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [self moveForward:self.sunnyBody];
    [self moveForward:self.sunnytyre1];
    [self moveForward:self.sunnytyre2];
    [self rotateImage:sunnytyre1];
    [self rotateImage:sunnytyre2];
    //[audioPlayer play];
}

- (void)transitionDidStop2:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [self moveForward:self.micraBody];
    [self moveForward:self.micratyre1];
    [self moveForward:self.micratyre2];
    [self rotateImage:micratyre1];
    [self rotateImage:micratyre2];
    //[audioPlayer play];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"295231_SOUNDDOGS__au" ofType:@"mp3"]] error:&error];
    
    if (error)
    {
        NSLog(@"Error in audioPlayer: %@", [error localizedDescription]);
    } else {
        audioPlayer.delegate = self;
        [audioPlayer prepareToPlay];
    }

    
    [self.view bringSubviewToFront:enterButton];
    [self.view bringSubviewToFront:enterButton1];
    [self.view bringSubviewToFront:enterButton2];
    [self.view bringSubviewToFront:resetButton];
    self.xtrailView.tag = 0;
    self.sunnyView.tag = 1;
    self.micraView.tag = 2;
    self.xtrailBody.tag = 3;
    self.sunnyBody.tag = 4;
    self.micraBody.tag = 5;
    stopAnim = 0;
    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] 
                                         pathForResource:@"logoanimation" ofType:@"mp4"]];
    moviePlayer =  [[MPMoviePlayerController alloc] 
                    initWithContentURL:url];
    moviePlayer.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    moviePlayer.view.autoresizingMask =  
                                        UIViewAutoresizingFlexibleLeftMargin   |
                                        UIViewAutoresizingFlexibleWidth        |
                                        UIViewAutoresizingFlexibleRightMargin  |
                                        UIViewAutoresizingFlexibleTopMargin    |
                                        UIViewAutoresizingFlexibleHeight       |
                                        UIViewAutoresizingFlexibleBottomMargin ;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:moviePlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(MPMoviePlayerDidExitFullscreen:) 
                                                 name:MPMoviePlayerDidExitFullscreenNotification object:moviePlayer];

    
    moviePlayer.controlStyle = MPMovieControlStyleNone;
    moviePlayer.shouldAutoplay = YES;
    [self.view addSubview:moviePlayer.view ];
    [moviePlayer setFullscreen:YES animated:YES];
    [self.view bringSubviewToFront:moviePlayer.view];
//    [self.view sendSubviewToBack:self.tyre1];
//    [self.view sendSubviewToBack:self.tyre2];
    
    
}

- (void)MPMoviePlayerDidExitFullscreen:(NSNotification *)notification
{
     MPMoviePlayerController *player = [notification object];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerDidExitFullscreenNotification 
                                                  object:nil];
    
    [player stop];
    if ([player respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [UIView animateWithDuration: 1.0 
                              delay:0 
                            options:UIViewAnimationOptionTransitionCrossDissolve 
                         animations:^{
                             player.view.alpha = 0.0;
                         } 
                         completion:^(BOOL finished) {
                             [player.view removeFromSuperview];
                         }];
    }
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    
    MPMoviePlayerController *player = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self      
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:player];
    [player stop];
    if ([player respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [UIView animateWithDuration: 1.0 
                                delay:0 
                                options:UIViewAnimationOptionTransitionCrossDissolve 
                         animations:^{
                             player.view.alpha = 0.0;
                         } 
                         completion:^(BOOL finished) {
                            [player.view removeFromSuperview];
                         }];
    }
}

-(void)audioPlayerDidFinishPlaying:
(AVAudioPlayer *)player successfully:(BOOL)flag
{
}

-(void)audioPlayerDecodeErrorDidOccur:
(AVAudioPlayer *)player error:(NSError *)error
{
}

-(void)audioPlayerBeginInterruption:(AVAudioPlayer *)player
{
}

-(void)audioPlayerEndInterruption:(AVAudioPlayer *)player
{
}
- (void)dealloc 
{
    [audioPlayer release];
    [super dealloc];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return ((interfaceOrientation != UIInterfaceOrientationLandscapeLeft) && (interfaceOrientation != UIInterfaceOrientationLandscapeRight));

}

@end
