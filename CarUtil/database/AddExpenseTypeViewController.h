//
//  AddExpenseTypeViewController.h
//  CarUtil
//
//  Created by Rishi Saxena on 02/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface AddExpenseTypeViewController : UIViewController {
   UITextField *typeView;
    NSString *value;
    sqlite3 *expenseDB;
    NSInteger expTypeid;
}
@property(nonatomic,retain)IBOutlet UITextField *typeView;
@property(nonatomic,retain)NSString *value;
@property (readwrite, nonatomic) NSInteger expTypeid;

@end
