//
//  AddExpenseViewController.m
//  sqllite sample
//
//  Created by Rishi Saxena on 24/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "AddExpenseViewController.h"
#import "DatePickerViewController.h"
#import "OdometerPickerViewController.h"
#import "Expense.h"
#import "ExpenseViewController.h"
#import "ExpenseTypeDetailViewController.h"
#import "ExpenseType.h"
#import "CarUtilAppDelegate.h"

@interface AddExpenseViewController ()

@end

@implementation AddExpenseViewController
@synthesize  datetextField;
@synthesize  costtextField;
@synthesize  exptypetextField;
@synthesize  odometertextField;
@synthesize  dateBut;
@synthesize datelabel;
@synthesize scrollviewP;
@synthesize flipbut;
@synthesize  notestextview;
@synthesize datedoubleval;
@synthesize tblSimpleTable;
@synthesize i;
@synthesize arryData;
@synthesize carID;
@synthesize exptypepicker;
@synthesize expensedatePicker;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewWillAppear:(BOOL)animated
{
    [arryData removeAllObjects];
    [self readExpenseTypeFromDatabase];
    //[tblSimpleTable reloadData];
    [exptypepicker reloadAllComponents];
}

- (void)viewDidLoad
{
#ifdef ENABLE_BACK_BUTTON
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [backButton release]; 
    
#endif
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *saveExpButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(Savedata:)];
    self.navigationItem.rightBarButtonItem = saveExpButton;
    [saveExpButton release];
    notestextview.text = @"";
//	flag=1;
//	tblSimpleTable.hidden=YES;
//	btn.layer.cornerRadius=0;
//	tblSimpleTable.layer.cornerRadius=0;
    [costtextField setKeyboardType:UIKeyboardTypeDecimalPad];
//    [self setDatelabel];
    [self readExpenseTypeFromDatabase];
    scrollviewP.scrollEnabled = YES;
    scrollviewP.clipsToBounds = YES;
    scrollviewP.showsHorizontalScrollIndicator = NO;
    scrollviewP.showsVerticalScrollIndicator = NO;
    scrollviewP.scrollsToTop = YES;
    scrollviewP.delegate = self;
    scrollviewP.bounces = NO;
    odometertextField.keyboardType = UIKeyboardTypeDecimalPad;
    [self addPickerWithDoneButton];
    
//    UIImageView *imgView = [[UIImageView alloc]initWithFrame:notestextview.bounds];
//    imgView.image = [UIImage imageNamed: @"notes_field.png"];
//    notestextview.backgroundColor = [UIColor colorWithPatternImage:imgView.image];
    
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.scrollviewP  addGestureRecognizer:viewTap];
    [viewTap release];
}

- (void)tapDetected:(UIGestureRecognizer *)sender {
    [self.view endEditing:YES];
    [self scrollTobottom];
}

-(void) readExpenseTypeFromDatabase {
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
	    
    // Init the expense Array
    if(arryData == nil)
    {
        DebugLog(@"new add expense array created");
        arryData = [[NSMutableArray alloc] init];
	} else {
         DebugLog(@"old add expense array used");
        [arryData removeAllObjects];
    }
	// Open the database from the users filessytem
	if(sqlite3_open(dbpath, &expenseDB) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
		const char *sqlStatement = "select * from EXPTYPE";
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(expenseDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
                NSInteger primaryKey = sqlite3_column_int(compiledStatement, 0);
				NSString *typeCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
			   
                //DebugLog(@" %d - %@ ",primaryKey,typeCol);
				// Create a new expense object with the data from the database
				ExpenseType *expenseTypeObj = [[ExpenseType alloc] initWithID:primaryKey  type:typeCol] ;

				
				// Add the animal object to the animals Array
				[arryData addObject:expenseTypeObj];
				
				[expenseTypeObj release];
			}
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
	}
	sqlite3_close(expenseDB);
}

- (void) Savedata: (id) sender
{
    DebugLog(@"exptypetextField.text %@",exptypetextField.text);
    [Flurry logEvent:@"Expense_Reading_Added_Event"];
    #ifdef TESTING
    [TestFlight passCheckpoint:@"New Expense Reading Added"];
    #endif 
    
    if([costtextField.text isEqualToString:@""])
    {
        UIAlertView *message = [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                           message:@"Please fill the mandatory fields marked with star (*)"
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil] autorelease];
        
        [message show];
        return;
        
    } else if([costtextField.text doubleValue] <= 0) {
        UIAlertView *message = [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                           message:@"Cost should be greater than zero."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil] autorelease];
        
        [message show];
        return;
    }
    
    sqlite3_stmt    *statement;
    int personID = -1;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *insertSQL = [NSString stringWithFormat: 
                               @"INSERT INTO EXPENSES (date , cost, type, notes, reading, selected, carid) VALUES (\"%f\", \"%@\", \"%@\" , \"%@\", \"%@\", \"%@\", \"%d\")", 
                               datedoubleval, costtextField.text, exptypetextField.text ,notestextview.text,odometertextField.text,@"1",self.carID];
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            personID = sqlite3_last_insert_rowid(expenseDB);
            //DebugLog(@"%d",personID);
            //[self showMessage:@"Contact added"];
        } else {
            [self showMessage:@"Failed to add contact"];
        }
        sqlite3_finalize(statement);
    } 
    sqlite3_close(expenseDB);
        
    NSNumber *myDoubleNumber = [NSNumber numberWithDouble:datedoubleval];
    
    ExpenseViewController *parent = [self.navigationController.viewControllers objectAtIndex:1];
    //call the method on the parent view controller
    if(parent != nil) 
        [parent adddataInExpenseArray:personID date:[myDoubleNumber stringValue] cost:costtextField.text type:exptypetextField.text notes:notestextview.text reading:odometertextField.text];
    parent = nil;
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == exptypetextField) {
        CURRENTCASE = EXPENSE_TYPE_TAG;
        [self showtypePicker];
	} else if (textField == datetextField) {
        CURRENTCASE = EXPENSE_DATE_TAG;
        [self showtypePicker];
    } else if (textField == odometertextField) {
        [self scrollTotop:180];
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    // do not allow . at the beggining
    if (range.location == 0 && [string isEqualToString:@"."]) {
        return NO;
    }
    NSRange temprange = [textField.text rangeOfString:@"."];
    if ((temprange.location != NSNotFound) && [string isEqualToString:@"."])
    {
        return NO;
    }
    if (textField == costtextField)
    {
        return !([costtextField.text length]> 11 && [string length] > range.length);
    }
    else if (textField == odometertextField)
    {
        return !([odometertextField.text length]> 7 && [string length] > range.length);
    }
    else
    {
        return YES;
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range  replacementText:(NSString *)text
{
    
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        [self scrollTobottom];
        // Return FALSE so that the final '\n' character doesn't get added
        return NO;
    }
    // For any other character return TRUE so that the text gets added to the view
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	//if (textField == costtextField) {
		[textField resignFirstResponder];
	//}
//	else if (textField == typetextField) {
//		[typetextField resignFirstResponder];
//        [notestextview becomeFirstResponder];
//	}
//    else if (textField == notestextField) {
//		[notestextField resignFirstResponder];
//        [odometertextField becomeFirstResponder];
//	}
//    else if (textField == odometertextField) {
//		[odometertextField resignFirstResponder];
//	}
   	return YES;
}
- (IBAction)textFieldFinished:(id)sender
{
//     [sender resignFirstResponder];
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    [self scrollTotop:90];
    return YES;    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
}
-(void)scrollTobottom
{
    //DebugLog(@"contentsize = %f bound = %f",imgscrollView.contentSize.height,self.imgscrollView.bounds.size.height);
    CGPoint bottomOffset = CGPointMake(0, 0);
    [scrollviewP setContentOffset:bottomOffset animated:YES];
}

//-(void)scrollTotop
//{
//    CGPoint topOffset = CGPointMake(0, 90);
//    [scrollviewP setContentOffset:topOffset animated:YES];
//}

-(void)scrollTotop : (int) value
{
    CGPoint topOffset = CGPointMake(0, value);
    [scrollviewP setContentOffset:topOffset animated:YES];
}
- (void)viewDidUnload
{
	self.tblSimpleTable=nil;
	self.i=nil;
	self.arryData=nil;
	
    [self setScrollviewP:nil];
    [self setFlipbut:nil];
    [self setExptypetextField:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

//-(void)setDatelabel
//{
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"dd MMMM yyyy"];
//    NSString *dateString = [dateFormat stringFromDate:[NSDate date]];  
//    [dateFormat release];
//    double dateDouble  = [[NSDate date] timeIntervalSinceReferenceDate];
//    [self setDateFieldValue:dateString doubleval:dateDouble type:0];
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)showMessage:(NSString *) lmessage{
    UIAlertView *message = [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                      message:lmessage
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil] autorelease];
    
    [message show];
}

//-(void)setOdometerFieldValue:(NSString *) value
//{
//    odometertextField.text = value;
//}
//
//-(void)setDateFieldValue:(NSString *) value doubleval:(double)val type:(int)ltype
//{
//    datetextField.text = value;
//    datedoubleval = val;
//}

//- (IBAction)enterDate:(id)sender {
//    DatePickerViewController *datepickerController = [[DatePickerViewController alloc] initWithNibName:@"DatePickerViewController" bundle:nil] ;
//    datepickerController.title = @"Expense Date";
//    //to push the UIView.
//    datepickerController.hidesBottomBarWhenPushed = NO;
//    datepickerController.datePickType = 1;
//     datepickerController.datedoubleval = datedoubleval;
//    [self.navigationController pushViewController:datepickerController animated:YES];
//    [datepickerController release];
//}

//- (IBAction)enterOdometerValue:(id)sender {
//    OdometerPickerViewController *odometerpickerController = [[OdometerPickerViewController alloc] initWithNibName:@"OdometerPickerViewController" bundle:nil] ;
//    //to push the UIView.
//    odometerpickerController.title = @"Odometer";
//    odometerpickerController.parentControllerType = 1;
//    odometerpickerController.setvalue = @"0";
//    odometerpickerController.hidesBottomBarWhenPushed = NO;
//    [self.navigationController pushViewController:odometerpickerController animated:YES];
//    [odometerpickerController release];
//}


- (void)datechangedNotify:(id)sender {
    NSDate *pickerDate = [expensedatePicker date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMMM yyyy"];
    NSString *dateString = [dateFormat stringFromDate:pickerDate];
    [dateFormat release];
    datetextField.text = dateString;
    datedoubleval  = [pickerDate timeIntervalSinceReferenceDate];
 }


- (void) dealloc {
    if(arryData != nil)
        [arryData release];
    [i release];
    [datetextField release];
    [notestextview release];
    [costtextField release];
    [odometertextField release];
    [dateBut release];
    [tblSimpleTable release];
    [datelabel release];
    [scrollviewP release];
    [flipbut release];
    [exptypetextField release];
    [super dealloc];
}

-(void) addPickerWithDoneButton
{
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:nil
                                     cancelButtonTitle:nil
                                destructiveButtonTitle:nil
                                     otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    exptypepicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, 0, 0)];
    exptypepicker.showsSelectionIndicator = YES;
    exptypepicker.dataSource = self;
    exptypepicker.delegate = self;
    [actionSheet addSubview:exptypepicker];
    
    expensedatePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 40, 0, 0)];
    expensedatePicker.datePickerMode = UIDatePickerModeDate;
    [actionSheet addSubview:expensedatePicker];
    
    UISegmentedControl *doneButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
    doneButton.momentary = YES;
    doneButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    doneButton.segmentedControlStyle = UISegmentedControlStyleBar;
    doneButton.tintColor = [UIColor colorWithRed:0.945 green:0.580 blue:0.113 alpha:1.0];
    [doneButton addTarget:self action:@selector(donePickerBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:doneButton];
    [doneButton release];
    
    UISegmentedControl *cancelButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Cancel"]];
    cancelButton.momentary = YES;
    cancelButton.frame = CGRectMake(10, 7.0f, 60.0f, 30.0f);
    cancelButton.segmentedControlStyle = UISegmentedControlStyleBar;
    cancelButton.tintColor = [UIColor blackColor];
    [cancelButton addTarget:self action:@selector(cancelPickerBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:cancelButton];
    [cancelButton release];
    [self donePickerBtnClicked:nil];
    [exptypepicker reloadAllComponents];
    exptypetextField.text = ((ExpenseType *)[arryData objectAtIndex:[exptypepicker selectedRowInComponent:0]]).type;
    [expensedatePicker setDate:[NSDate date] animated:YES];
    [expensedatePicker sizeToFit];
    [self datechangedNotify:nil];
}

-(void)showtypePicker
{
    switch (CURRENTCASE) {
        case EXPENSE_TYPE_TAG:
        {
            [exptypepicker setHidden:NO];
            [expensedatePicker setHidden:YES];
        }
            break;
        case EXPENSE_DATE_TAG:
        {
            [exptypepicker setHidden:YES];
            [expensedatePicker setHidden:NO];
        }
            break;
        default:
            return;
    }
    [actionSheet showInView:self.view];
    [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
}

- (void) donePickerBtnClicked:(id)sender
{
    DebugLog(@"donePickerBtnClicked");
    switch (CURRENTCASE) {
        case EXPENSE_TYPE_TAG:
        {
            exptypetextField.text = ((ExpenseType *)[arryData objectAtIndex:[exptypepicker selectedRowInComponent:0]]).type;
        }
            break;
        case EXPENSE_DATE_TAG:
        {
            [self datechangedNotify:nil];
        }
            break;
        default:
            break;
    }
    [self cancelPickerBtnClicked:nil];
}

- (void) cancelPickerBtnClicked:(id)sender
{
    DebugLog(@"cancelPickerBtnClicked");
    [self scrollTobottom];
    [self.view endEditing:YES];
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
}

#pragma mark Pickerview methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
	
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
	
	DebugLog(@"\n carTypedata : %d\n",[arryData count]);
    return [arryData count] + 1;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString * tempStr;
    if(row == [arryData count] )
    {
    	tempStr = @"<Custom>";
    } else {
        DebugLog(@"%@",((ExpenseType *)[arryData objectAtIndex:row]).type);
        tempStr = ((ExpenseType *)[arryData objectAtIndex:row]).type;
    }
    return tempStr;
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if(row == [arryData count] )
    {
        ExpenseTypeDetailViewController *typedetailController = [[ExpenseTypeDetailViewController alloc] initWithNibName:@"ExpenseTypeDetailViewController" bundle:nil] ;
        typedetailController.title = @"Expense Type";
        //to push the UIView.
        typedetailController.hidesBottomBarWhenPushed = NO;
        [self.navigationController pushViewController:typedetailController animated:YES];
        [typedetailController release];
        [thePickerView selectRow:0 inComponent:0 animated:NO];
        [self cancelPickerBtnClicked:nil];
    } else {
        DebugLog(@"Selected Exp Type: %@ Index of selected type: %i", ((ExpenseType *)[arryData objectAtIndex:row]).type, row);
    }
        
}

@end
