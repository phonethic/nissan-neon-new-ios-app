//
//  DatePickerViewController.h
//  sqllite sample
//
//  Created by Rishi Saxena on 26/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DatePickerViewController : UIViewController {
    int datePickType;
    double datedoubleval;
}
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (retain, nonatomic) IBOutlet UILabel *pickerdatelbl;
@property (readwrite, nonatomic) double datedoubleval;
@property (nonatomic, readwrite) int datePickType;
@property (retain, nonatomic) IBOutlet UIImageView *pickerMainView;


@end
