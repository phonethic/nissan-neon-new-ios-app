//
//  AddExpenseViewController.h
//  sqllite sample
//
//  Created by Rishi Saxena on 24/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import <QuartzCore/QuartzCore.h>

#define EXPENSE_TYPE_TAG 2000
#define EXPENSE_DATE_TAG 2001

@interface EditExpenseViewController : UIViewController <UITextFieldDelegate,UITextViewDelegate,UITableViewDelegate, UITableViewDataSource,UIScrollViewDelegate,UIActionSheetDelegate,UIPickerViewDataSource,UIPickerViewDelegate> {
    UITextField *costtextField;
    UIButton *findBut;
    //NSString *databasePath;
    sqlite3 *expenseDB;
    UIButton *dateBut;
    UIButton *odometerBut;
    double datedoubleval;
    NSInteger carID;
    NSInteger rowID;
    NSInteger dbowID;
    NSInteger dbmaxrowID;
    NSInteger dbminrowID;
    //Drop down list
    IBOutlet UITableView *tblSimpleTable;
	IBOutlet UIButton *btn;
	IBOutlet UIImageView *i;
	BOOL flag;
	NSMutableArray *arryTypeData;
    UIActionSheet *actionSheet;
    int CURRENTCASE;
    
}
@property (retain, nonatomic) IBOutlet UITextField *datetextField;
@property (retain, nonatomic) IBOutlet UITextField *exptypetextField;
@property (retain, nonatomic) IBOutlet UITextField *odometertextField;

@property (nonatomic, retain) IBOutlet UITextView *notestextview;
@property (nonatomic, retain) IBOutlet UITextField *costtextField;
//@property (retain, nonatomic) IBOutlet UIButton *dateBut;
//@property (retain, nonatomic) IBOutlet UIButton *odometerBut;
//@property (retain, nonatomic) IBOutlet UIButton *backBut;
//@property (retain, nonatomic) IBOutlet UIButton *fwdBut;
//@property (retain, nonatomic) IBOutlet UILabel *datefieldlable;
//@property (retain, nonatomic) IBOutlet UILabel *odometerfieldlabel;
@property (readwrite, nonatomic) double datedoubleval;
@property (readwrite, nonatomic) NSInteger carID;
@property (readwrite, nonatomic) NSInteger rowID;
@property (readwrite, nonatomic) NSInteger dbowID;
//@property (readwrite, nonatomic) NSInteger dbmaxrowID;
//@property (readwrite, nonatomic) NSInteger dbminrowID;
@property(nonatomic,retain)NSMutableArray *arryTypeData;
@property (retain, nonatomic) IBOutlet UIScrollView *editscrollviewP;
@property (retain, nonatomic) IBOutlet UIPickerView *exptypepicker;
@property (retain, nonatomic) IBOutlet UIDatePicker *expensedatePicker;

//Drop down list
//@property(nonatomic,retain)IBOutlet UIButton *btn;
//@property(nonatomic,retain)IBOutlet UITableView *tblSimpleTable;
//@property(nonatomic,retain)IBOutlet UIImageView *i;



//-(void) readExpenseFromDatabase ;
- (void)clearData:(id)sender;
- (void)updateRow:(id)sender;
- (void)searchData:(id)sender;
- (IBAction)enterDate:(id)sender;
- (IBAction)enterOdometerValue:(id)sender;
-(IBAction)btnClicked;
- (IBAction)moveBack:(id)sender;
- (IBAction)moveFwd:(id)sender;

-(void)setOdometerFieldValue:(NSString *) value;
-(void)setDateFieldValue:(NSString *) value doubleval:(double)val type:(int)ltype;
@end
