//
//  AddExpenseViewController.m
//  sqllite sample
//
//  Created by Rishi Saxena on 24/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "EditExpenseViewController.h"
#import "DatePickerViewController.h"
#import "OdometerPickerViewController.h"
#import "Expense.h"
#import "ExpenseViewController.h"
#import "ExpenseTypeDetailViewController.h"
#import "ExpenseType.h"
#import "CarUtilAppDelegate.h"

@interface EditExpenseViewController ()

@end

@implementation EditExpenseViewController
@synthesize  costtextField;
//@synthesize  dateBut;
//@synthesize  odometerBut;
//@synthesize backBut;
//@synthesize fwdBut;
//@synthesize datefieldlable;
//@synthesize odometerfieldlabel;
@synthesize odometertextField;
@synthesize datetextField;
@synthesize  exptypetextField;
@synthesize  notestextview;
@synthesize datedoubleval;
//@synthesize btn;
//@synthesize tblSimpleTable;
//@synthesize i;
@synthesize arryTypeData;
@synthesize rowID;
@synthesize dbowID;
//@synthesize dbmaxrowID;
//@synthesize dbminrowID;
@synthesize editscrollviewP;
@synthesize carID;
@synthesize exptypepicker;
@synthesize expensedatePicker;

////Animation to flip scrllview
//- (void)flipvew:(id)sender {
//    [UIView  transitionWithView:editscrollviewP duration:0.5  options:UIViewAnimationOptionTransitionFlipFromLeft
//                     animations:^(void) {
//                         BOOL oldState = [UIView areAnimationsEnabled];
//                         [UIView setAnimationsEnabled:NO];
//                         [UIView setAnimationsEnabled:oldState];
//                     } 
//                     completion:nil];
//}
//
//-(void)transitonLeftOrRight:(int)direction
//{
//    
//    // set up an animation for the transition between the views
//    CATransition *animation = [CATransition animation];
//    [animation setDuration:0.5];
//    [animation setType:kCATransitionPush];
//    if (direction == 0) {
//        [animation setSubtype:kCATransitionFromRight];
//    } else {
//        [animation setSubtype:kCATransitionFromLeft];
//    }
//    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
//    
//    [[editscrollviewP layer] addAnimation:animation forKey:@"SwitchToView"];
//}
//
//- (IBAction)moveBack:(id)sender {
//    sqlite3_stmt    *statement;
//    
//    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
//    
//    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
//    {
//        NSString *querySQL = [NSString stringWithFormat:@"SELECT * FROM expenses WHERE id < \"%d\" order by id desc limit 1 ", self.dbowID];
//        
//        const char *query_stmt = [querySQL UTF8String];
//        
//        if (sqlite3_prepare_v2(expenseDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
//        {
//            if (sqlite3_step(statement) == SQLITE_ROW)
//            {
//               
//                NSInteger primaryKey = sqlite3_column_int(statement, 0);
//                self.dbowID = primaryKey;
//                
//                if(self.dbowID == self.dbminrowID)
//                {
//                    backBut.hidden = TRUE;
//                }  
//                
//                if(self.dbowID < self.self.dbmaxrowID)
//                {
//                    fwdBut.hidden = FALSE;
//                }
//                
//                NSString *dateField = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
//                NSDate *pickerDate = [NSDate dateWithTimeIntervalSinceReferenceDate:[dateField doubleValue] ]; 
//                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//                [dateFormat setDateFormat:@"dd MMMM yyyy"];
//                NSString *dateString = [dateFormat stringFromDate:pickerDate];  
//                datefieldlable.text = dateString;
//                [dateFormat release];
//                
//                datedoubleval = [dateField doubleValue];
//                
//                [dateField release];
//                
//                NSString *costField = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 2)];
//                costtextField.text = costField;
//                [costField release];
//                
//                NSString *typeField = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 3)];
//                //typetextField.text = typeField;
//                [btn setTitle:typeField forState:UIControlStateNormal];
//                [typeField release];
//                
//                NSString *notesField = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 4)];
//                notestextview.text = notesField;
//                [notesField release];
//                
//                NSString *readingField = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 5)];
//                odometerfieldlabel.text = readingField;
//                [readingField release];
//                
//                //DebugLog(@" %d - %@ - %@ - %@ - %@ - %@",primaryKey,dateField, costField,typeField,notesField,readingField);
//                
//                //notestextview.text = @"Match found";
//                
//            } else {
//                [self showMessage:@"Match not found"];
//            }
//            sqlite3_finalize(statement);
//        }
//        sqlite3_close(expenseDB);
//    }
//    
//    [self flipvew:nil];
//    //[self transitonLeftOrRight:0];
//}
//
//- (IBAction)moveFwd:(id)sender {
//    sqlite3_stmt    *statement;
//    
//    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
//    
//    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
//    {
//        NSString *querySQL = [NSString stringWithFormat:@"SELECT * FROM expenses WHERE id > \"%d\" order by id asc limit 1 ", self.dbowID];
//        
//        const char *query_stmt = [querySQL UTF8String];
//        
//        if (sqlite3_prepare_v2(expenseDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
//        {
//            if (sqlite3_step(statement) == SQLITE_ROW)
//            {
//                NSInteger primaryKey = sqlite3_column_int(statement, 0);
//                self.dbowID = primaryKey;
//                
//                if(self.dbowID == self.dbmaxrowID)
//                {
//                    fwdBut.hidden = TRUE;
//                } 
//                
//                if(self.dbowID > self.dbminrowID)
//                {
//                    backBut.hidden = FALSE;
//                }
//                
//                NSString *dateField = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
//                NSDate *pickerDate = [NSDate dateWithTimeIntervalSinceReferenceDate:[dateField doubleValue] ]; 
//                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//                [dateFormat setDateFormat:@"dd MMMM yyyy"];
//                NSString *dateString = [dateFormat stringFromDate:pickerDate];  
//                datefieldlable.text = dateString;
//                [dateFormat release];
//                
//                datedoubleval = [dateField doubleValue];
//                
//                [dateField release];
//                
//                NSString *costField = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 2)];
//                costtextField.text = costField;
//                [costField release];
//                
//                NSString *typeField = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 3)];
//                //typetextField.text = typeField;
//                [btn setTitle:typeField forState:UIControlStateNormal];
//                [typeField release];
//                
//                NSString *notesField = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 4)];
//                notestextview.text = notesField;
//                [notesField release];
//                
//                NSString *readingField = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 5)];
//                odometerfieldlabel.text = readingField;
//                [readingField release];
//                
//                //DebugLog(@" %d - %@ - %@ - %@ - %@ - %@",primaryKey,dateField, costField,typeField,notesField,readingField);
//                
//                //notestextview.text = @"Match found";
//                
//            } else {
//                [self showMessage:@"Match not found"];
//            }
//            sqlite3_finalize(statement);
//        }
//        sqlite3_close(expenseDB);
//    }
//    [self flipvew:nil];
//    //[self transitonLeftOrRight:1];
//
//}
//
//#pragma mark Table view methods
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 1;
//}
//
//
//// Customize the number of rows in the table view.
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return [arryTypeData count] + 1;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 30;
//}
//
//// Customize the appearance of table view cells.
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//    static NSString *CellIdentifier = @"Cell";
//    
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil) {
//        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
//    }
//    
//    // Set up the cell...
//	cell.textLabel.font=[UIFont fontWithName:@"Arial" size:16];
//    cell.textLabel.textColor = [UIColor orangeColor];
//    if(indexPath.row == [arryTypeData count] )
//    {
//    	cell.textLabel.text = @"<Custom>";
//    } else {
//        cell.textLabel.text = ((ExpenseType *)[arryTypeData objectAtIndex:indexPath.row]).type;
//    }
//    return cell;
//}
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (flag==1) {
//		flag=0;
//		tblSimpleTable.hidden=NO;
//		[i setImage:[UIImage imageNamed:@"drop-down-up-arrow.png"]];
//	}
//	else{
//		flag=1;
//		tblSimpleTable.hidden=YES;
//		[i setImage:[UIImage imageNamed:@"drop-down-arrow.png"]];
//	}
//    if(indexPath.row == [arryTypeData count])
//    {
//        ExpenseTypeDetailViewController *typedetailController = [[ExpenseTypeDetailViewController alloc] initWithNibName:@"ExpenseTypeDetailViewController" bundle:nil] ;
//        typedetailController.title = @"Expense Type";
//        //to push the UIView.
//        typedetailController.hidesBottomBarWhenPushed = NO;
//        [self.navigationController pushViewController:typedetailController animated:YES];
//        [typedetailController release];
//        [tblSimpleTable deselectRowAtIndexPath:[tblSimpleTable indexPathForSelectedRow] animated:NO];
//    } else {
//        NSString *data = ((ExpenseType *)[arryTypeData objectAtIndex:indexPath.row]).type;
//        [btn setTitle:data forState:UIControlStateNormal];
//        [tblSimpleTable deselectRowAtIndexPath:[tblSimpleTable indexPathForSelectedRow] animated:NO];
//    }
//
//}
//

//-(void)getMaxRowID
//{
//    sqlite3_stmt    *statement;
//    
//    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
//    
//    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
//    {
//        NSString *querySQL = [NSString stringWithFormat:@"SELECT max(id) FROM expenses where CARID=\"%d\"", self.carID]; 
//        
//        const char *query_stmt = [querySQL UTF8String];
//        
//        if (sqlite3_prepare_v2(expenseDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
//        {
//            if (sqlite3_step(statement) == SQLITE_ROW)
//            {
//                
//                NSInteger primaryKey = sqlite3_column_int(statement, 0);
//                self.dbmaxrowID = primaryKey;
//               
//            } else {
//                [self showMessage:@"Match not found"];
//            }
//            sqlite3_finalize(statement);
//        }
//        sqlite3_close(expenseDB);
//    }
//
//}
//
//-(void)getMinRowID
//{
//    sqlite3_stmt    *statement;
//    
//    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
//    
//    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
//    {
//        NSString *querySQL = [NSString stringWithFormat:@"SELECT min(id) FROM expenses where CARID=\"%d\"", self.carID];       
//        
//        const char *query_stmt = [querySQL UTF8String];
//        
//        if (sqlite3_prepare_v2(expenseDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
//        {
//            if (sqlite3_step(statement) == SQLITE_ROW)
//            {
//                
//                NSInteger primaryKey = sqlite3_column_int(statement, 0);
//                self.dbminrowID = primaryKey;
//                
//            } else {
//                [self showMessage:@"Match not found"];
//            }
//            sqlite3_finalize(statement);
//        }
//        sqlite3_close(expenseDB);
//    }
//}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [arryTypeData removeAllObjects];
    [self readExpenseTypeFromDatabase];
    //[tblSimpleTable reloadData];
    [exptypepicker reloadAllComponents];
}

- (void)viewDidLoad
{
#ifdef ENABLE_BACK_BUTTON
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [backButton release]; 
    
#endif
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    UIBarButtonItem *saveEditExpButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(startEditing)];
//    self.navigationItem.rightBarButtonItem = saveEditExpButton;
//    [saveEditExpButton release];
//    costtextField.enabled = FALSE;
//    notestextview.editable = FALSE;
//    findBut.enabled = FALSE;
//    dateBut.hidden = TRUE;
//    odometerBut.hidden = TRUE;
//    btn.enabled = FALSE;
//	flag=1;
//    i.hidden = TRUE;
//	tblSimpleTable.hidden=YES;
//	btn.layer.cornerRadius=0;
//	tblSimpleTable.layer.cornerRadius=0;
    odometertextField.keyboardType = UIKeyboardTypeDecimalPad;
    [costtextField setKeyboardType:UIKeyboardTypeDecimalPad];
    [self readExpenseTypeFromDatabase];
    [self searchData:nil];
    [self addPickerWithDoneButton];
    datetextField.enabled = FALSE;
    costtextField.enabled = FALSE;
    notestextview.editable = FALSE;
    exptypetextField.enabled = FALSE;
    odometertextField.enabled = FALSE;
//    [self getMaxRowID];
//    [self getMinRowID];
//    if((self.dbowID == self.dbminrowID) && (self.dbowID == self.dbmaxrowID))
//    {
//        backBut.hidden = TRUE;
//        fwdBut.hidden = TRUE;
//    } else if(self.dbowID == self.dbminrowID) {
//        backBut.hidden = TRUE;
//         fwdBut.hidden = FALSE;
//    } else if(self.dbowID == self.dbmaxrowID) {
//        fwdBut.hidden = TRUE;
//        backBut.hidden = FALSE;
//    }
    editscrollviewP.scrollEnabled = YES;
    editscrollviewP.clipsToBounds = YES;
    editscrollviewP.showsHorizontalScrollIndicator = NO;
    editscrollviewP.showsVerticalScrollIndicator = NO;
    editscrollviewP.scrollsToTop = YES;
    editscrollviewP.delegate = self;
    editscrollviewP.bounces = NO;
    
    
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.editscrollviewP  addGestureRecognizer:viewTap];
    [viewTap release];
    
    [self startEditing];

}

- (void)tapDetected:(UIGestureRecognizer *)sender {
    [self.view endEditing:YES];
    [self scrollTobottom];
}


-(void) readExpenseTypeFromDatabase {
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
	    
    // Init the expense Array
    if(arryTypeData == nil)
    {
        DebugLog(@"new edit expense array created");
        arryTypeData = [[NSMutableArray alloc] init];
	} else {
        DebugLog(@"old edit expense array used");
        [arryTypeData removeAllObjects];
    }
	
	// Open the database from the users filessytem
	if(sqlite3_open(dbpath, &expenseDB) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
		const char *sqlStatement = "select * from EXPTYPE";
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(expenseDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
                NSInteger primaryKey = sqlite3_column_int(compiledStatement, 0);
				NSString *typeCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
			   
                //DebugLog(@" %d - %@ ",primaryKey,typeCol);
				// Create a new expense object with the data from the database
				ExpenseType *expenseTypeObj = [[ExpenseType alloc] initWithID:primaryKey  type:typeCol] ;

				
				// Add the animal object to the animals Array
				[arryTypeData addObject:expenseTypeObj];
				
				[expenseTypeObj release];
			}
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
		
	}
	sqlite3_close(expenseDB);
}

- (void) startEditing {
    UIBarButtonItem *saveAvgButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemSave                              target: self action: @selector(stopEditing)] ;
	self.navigationItem.rightBarButtonItem = saveAvgButton;
    [saveAvgButton release];
    datetextField.enabled = TRUE;
    costtextField.enabled = TRUE;
    notestextview.editable = TRUE;
    exptypetextField.enabled = TRUE;
    odometertextField.enabled = TRUE;
//    findBut.enabled = TRUE;
//    dateBut.hidden = FALSE;
//    odometerBut.hidden = FALSE;
//    btn.enabled = TRUE;
//    i.hidden = FALSE;
//    backBut.hidden = TRUE;
//    fwdBut.hidden = TRUE;

}

- (void) stopEditing { 
    UIBarButtonItem *saveAvgButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemEdit
		 target: self action: @selector(startEditing)];
   	self.navigationItem.rightBarButtonItem	= saveAvgButton;
    [saveAvgButton release];
    datetextField.enabled = FALSE;
    costtextField.enabled = FALSE;
    notestextview.editable = FALSE;
    exptypetextField.enabled = FALSE;
    odometertextField.enabled = FALSE;
//    findBut.enabled = FALSE;
//    dateBut.hidden = TRUE;
//    odometerBut.hidden = TRUE;
//    i.hidden = TRUE;
//    btn.enabled = FALSE;
//    if((self.dbowID == self.dbminrowID) && (self.dbowID == self.dbmaxrowID))
//    {
//        backBut.hidden = TRUE;
//        fwdBut.hidden = TRUE;
//    } else if(self.dbowID == self.dbminrowID) {
//        backBut.hidden = TRUE;
//        fwdBut.hidden = FALSE;
//    } else if(self.dbowID == self.dbmaxrowID) {
//        fwdBut.hidden = TRUE;
//         backBut.hidden = FALSE;
//    } else {
//        fwdBut.hidden = FALSE;
//        backBut.hidden = FALSE;
//    }
    [self updateRow:nil];
    [self scrollTobottom];

//    flag = 1;
//    tblSimpleTable.hidden=YES;
//    [i setImage:[UIImage imageNamed:@"drop-down-arrow.png"]];    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == exptypetextField) {
        CURRENTCASE = EXPENSE_TYPE_TAG;
        [self showtypePicker];
	} else if (textField == datetextField) {
        CURRENTCASE = EXPENSE_DATE_TAG;
        [self showtypePicker];
    } else if (textField == odometertextField) {
        [self scrollTotop:180];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    // do not allow . at the beggining
    if (range.location == 0 && [string isEqualToString:@"."]) {
        return NO;
    }
    NSRange temprange = [textField.text rangeOfString:@"."];
    if ((temprange.location != NSNotFound) && [string isEqualToString:@"."])
    {
        return NO;
    }
    if (textField == costtextField)
    {
        return !([costtextField.text length]> 11 && [string length] > range.length);
    }
    else if (textField == odometertextField)
    {
        return !([odometertextField.text length]> 7 && [string length] > range.length);
    }
    else
    {
        return YES;
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range  replacementText:(NSString *)text
{
    
    if ([text isEqualToString:@"\n"]) {
        [self scrollTobottom];
        [textView resignFirstResponder];
        // Return FALSE so that the final '\n' character doesn't get added
        return NO;
    }
    // For any other character return TRUE so that the text gets added to the view
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//	if (textField == datetextField) {
//		[datetextField resignFirstResponder];
//		[costtextField becomeFirstResponder];
//	}
//	else if (textField == costtextField) {
//		[costtextField resignFirstResponder];
//		[typetextField becomeFirstResponder];
//	}
//	else if (textField == typetextField) {
//		[typetextField resignFirstResponder];
//        [notestextview becomeFirstResponder];
//	}
//    else if (textField == notestextField) {
//		[notestextField resignFirstResponder];
//        [odometertextField becomeFirstResponder];
//	}
//    else if (textField == odometertextField) {
//		[odometertextField resignFirstResponder];
//	}
   	return YES;
}

- (IBAction)textFieldFinished:(id)sender
{
//     /[sender resignFirstResponder];
}

- (void)viewDidUnload
{
//    self.btn = nil;
//	self.tblSimpleTable = nil;
//	self.i = nil;	
//    [self setBackBut:nil];
//    [self setFwdBut:nil];
//    [self setDatefieldlable:nil];
//    [self setOdometerfieldlabel:nil];
    self.arryTypeData = nil;
    [self setEditscrollviewP:nil];
    [self setDatetextField:nil];
    [self setExptypetextField:nil];
    [self setOdometertextField:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    [self scrollTotop:110];
    return YES;    
}

-(void)scrollTobottom
{
    CGPoint bottomOffset = CGPointMake(0, 0);
    [editscrollviewP setContentOffset:bottomOffset animated:YES];
}

-(void)scrollTotop:(int)value
{
    CGPoint topOffset = CGPointMake(0, value);
    [editscrollviewP setContentOffset:topOffset animated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)showMessage:(NSString *) lmessage{
    UIAlertView *message = [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                      message:lmessage
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil] autorelease];
    
    [message show];
}

- (void)clearData:(id)sender {
    costtextField.text = @"";
    notestextview.text = @"";
}

- (void)updateRow:(id)sender {
    
    if([costtextField.text isEqualToString:@""])
    {
        UIAlertView *message = [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                           message:@"Please fill the mandatory fields marked with star (*)"
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil] autorelease];
        
        [message show];
        return;
        
    } else if([costtextField.text doubleValue] < 1) {
        UIAlertView *message = [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                           message:@"Cost should be greater than one."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil] autorelease];
        
        [message show];
        return;
    }
    
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    DebugLog(@"date = %f , id=%d ",datedoubleval,self.rowID);
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"update expenses set date=\"%f\" , cost=\"%@\" ,  type=\"%@\" , notes=\"%@\" , reading=\"%@\"where id=\"%d\"",  datedoubleval , costtextField.text, exptypetextField.text , notestextview.text, odometertextField.text , self.dbowID];
        
        const char *update_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, update_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            //[self showMessage:@"Contact updated"];
        } else {
            [self showMessage:@"Contact updatation failed"];
        }
        sqlite3_finalize(statement);
    }         
    sqlite3_close(expenseDB);

    //NSNumber *myDoubleNumber = [NSNumber numberWithDouble:datedoubleval];
    
    ExpenseViewController *parent = [self.navigationController.viewControllers objectAtIndex:1];
    //call the method on the parent view controller
    //[parent updatedataInExpenseArray:self.dbowID date:[myDoubleNumber stringValue] cost:costtextField.text type:[btn currentTitle] notes:notestextview.text reading:odometerfieldlabel.text  rowID:self.rowID];
    if(parent != nil) 
        [parent reloadExpenseArray];
    parent = nil;

}

- (void)searchData:(id)sender {
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    DebugLog(@"edit db row id %d",self.dbowID);
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"SELECT date, cost , type , notes, reading FROM expenses WHERE id=\"%d\" and CARID=\"%d\"", self.dbowID, self.carID];
        
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(expenseDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *dateField = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)];
                NSDate *pickerDate = [NSDate dateWithTimeIntervalSinceReferenceDate:[dateField doubleValue] ]; 
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"dd MMMM yyyy"];
                NSString *dateString = [dateFormat stringFromDate:pickerDate];  
                datetextField.text = dateString;
                [dateFormat release];
                
                datedoubleval = [dateField doubleValue];
                
                [dateField release];
                
                NSString *costField = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 1)];
                costtextField.text = costField;
                [costField release];
                
                NSString *typeField = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 2)];
                exptypetextField.text = typeField;
                [typeField release];
                
                NSString *notesField = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 3)];
                notestextview.text = notesField;
                [notesField release];
                
                NSString *readingField = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 4)];
                odometertextField.text = readingField;
                [readingField release];
                
                
            } else {
                [self showMessage:@"Match not found"];
                [self clearData:nil];
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(expenseDB);
    }
}


//-(void)setOdometerFieldValue:(NSString *) value
//{
//    odometertextField.text = value;
//}
//
//-(void)setDateFieldValue:(NSString *) value doubleval:(double)val type:(int)ltype
//{
//    datetextField.text = value;
//    datedoubleval = val;
//}

//- (IBAction)enterDate:(id)sender {
//    DatePickerViewController *datepickerController = [[DatePickerViewController alloc] initWithNibName:@"DatePickerViewController" bundle:nil] ;
//    datepickerController.title = @"Edit Date";
//    //to push the UIView.
//    datepickerController.hidesBottomBarWhenPushed = NO;
//    datepickerController.datePickType = 4;
//    datepickerController.datedoubleval = datedoubleval;
//    [self.navigationController pushViewController:datepickerController animated:YES];
//    [datepickerController release];
//}


//- (IBAction)enterOdometerValue:(id)sender {
//    OdometerPickerViewController *odometerpickerController = [[OdometerPickerViewController alloc] initWithNibName:@"OdometerPickerViewController" bundle:nil] ;
//    //to push the UIView.
//    odometerpickerController.title = @"Odometer";
//    odometerpickerController.parentControllerType = 2;
//    DebugLog(@"val --> %@",odometertextField.text);
//    odometerpickerController.setvalue = odometertextField.text;
//    odometerpickerController.hidesBottomBarWhenPushed = NO;
//    [self.navigationController pushViewController:odometerpickerController animated:YES];
//    [odometerpickerController release];
//}

- (void)datechangedNotify:(id)sender {
    NSDate *pickerDate = [expensedatePicker date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMMM yyyy"];
    NSString *dateString = [dateFormat stringFromDate:pickerDate];
    [dateFormat release];
    datetextField.text = dateString;
    datedoubleval = [pickerDate timeIntervalSinceReferenceDate];
}

- (void)dealloc {
    [i release];
    [tblSimpleTable release];
    [dateBut release];
    [notestextview release];
    [costtextField release];
    [odometerBut release];
    [btn release];
    [arryTypeData release];
//    [backBut release];
//    [fwdBut release];
//    [datefieldlable release];
//    [odometerfieldlabel release];
    [editscrollviewP release];
    [datetextField release];
    [exptypetextField release];
    [odometertextField release];
    [super dealloc];
}


-(void) addPickerWithDoneButton
{
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:nil
                                     cancelButtonTitle:nil
                                destructiveButtonTitle:nil
                                     otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    exptypepicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, 0, 0)];
    exptypepicker.showsSelectionIndicator = YES;
    exptypepicker.dataSource = self;
    exptypepicker.delegate = self;
    [actionSheet addSubview:exptypepicker];
    
    expensedatePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 40, 0, 0)];
    expensedatePicker.datePickerMode = UIDatePickerModeDate;
    [actionSheet addSubview:expensedatePicker];
    
    UISegmentedControl *doneButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
    doneButton.momentary = YES;
    doneButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    doneButton.segmentedControlStyle = UISegmentedControlStyleBar;
    doneButton.tintColor = [UIColor colorWithRed:0.945 green:0.580 blue:0.113 alpha:1.0];
    [doneButton addTarget:self action:@selector(donePickerBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:doneButton];
    [doneButton release];
    
    UISegmentedControl *cancelButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Cancel"]];
    cancelButton.momentary = YES;
    cancelButton.frame = CGRectMake(10, 7.0f, 60.0f, 30.0f);
    cancelButton.segmentedControlStyle = UISegmentedControlStyleBar;
    cancelButton.tintColor = [UIColor blackColor];
    [cancelButton addTarget:self action:@selector(cancelPickerBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:cancelButton];
    [cancelButton release];
//    [self donePickerBtnClicked:nil];
    [exptypepicker reloadAllComponents];
    //exptypetextField.text = ((ExpenseType *)[arryTypeData objectAtIndex:[exptypepicker selectedRowInComponent:0]]).type;
    //[expensedatePicker setDate:[NSDate date] animated:YES];
    [expensedatePicker sizeToFit];
    //[self datechangedNotify:nil];
    
    if(0 == datedoubleval)
    {
        [expensedatePicker setDate:[NSDate date] animated:YES];
    }
    else
    {
        [expensedatePicker setDate:[NSDate dateWithTimeIntervalSinceReferenceDate:datedoubleval] animated:NO];
    }
    if ([arryTypeData indexOfObject:exptypetextField.text] == -1)
    {
        [exptypepicker selectRow:0 inComponent:0 animated:NO];
    }
    else
    {
        for (ExpenseType *tempObj in arryTypeData)
        {
            if([tempObj.type isEqualToString:exptypetextField.text])
            {
                [exptypepicker selectRow:[arryTypeData indexOfObject:tempObj] inComponent:0 animated:NO];
                break;
            }
            else
            {
                [exptypepicker selectRow:0 inComponent:0 animated:NO];
            }
        }
    }
}

-(void)showtypePicker
{
    switch (CURRENTCASE) {
        case EXPENSE_TYPE_TAG:
        {
            [exptypepicker setHidden:NO];
            [expensedatePicker setHidden:YES];
        }
            break;
        case EXPENSE_DATE_TAG:
        {
            [exptypepicker setHidden:YES];
            [expensedatePicker setHidden:NO];
        }
            break;
        default:
            return;
    }
    [actionSheet showInView:self.view];
    [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
    
}

- (void) donePickerBtnClicked:(id)sender
{
    DebugLog(@"donePickerBtnClicked");
    switch (CURRENTCASE) {
        case EXPENSE_TYPE_TAG:
        {
            exptypetextField.text = ((ExpenseType *)[arryTypeData objectAtIndex:[exptypepicker selectedRowInComponent:0]]).type;
        }
            break;
        case EXPENSE_DATE_TAG:
        {
            [self datechangedNotify:nil];
        }
            break;
        default:
            break;
    }
    [self cancelPickerBtnClicked:nil];
}

- (void) cancelPickerBtnClicked:(id)sender
{
    DebugLog(@"cancelPickerBtnClicked");
    [self scrollTobottom];
    [self.view endEditing:YES];
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
}

#pragma mark Pickerview methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
	
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
	
	DebugLog(@"\n carTypedata : %d\n",[arryTypeData count]);
    return [arryTypeData count] + 1;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString * tempStr;
    if(row == [arryTypeData count] )
    {
    	tempStr = @"<Custom>";
    } else {
        DebugLog(@"%@",((ExpenseType *)[arryTypeData objectAtIndex:row]).type);
        tempStr = ((ExpenseType *)[arryTypeData objectAtIndex:row]).type;
    }
    return tempStr;
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if(row == [arryTypeData count] )
    {
        ExpenseTypeDetailViewController *typedetailController = [[ExpenseTypeDetailViewController alloc] initWithNibName:@"ExpenseTypeDetailViewController" bundle:nil] ;
        typedetailController.title = @"Expense Type";
        //to push the UIView.
        typedetailController.hidesBottomBarWhenPushed = NO;
        [self.navigationController pushViewController:typedetailController animated:YES];
        [typedetailController release];
        [thePickerView selectRow:0 inComponent:0 animated:NO];
        [self cancelPickerBtnClicked:nil];
    } else {
        DebugLog(@"Selected Exp Type: %@ Index of selected type: %i", ((ExpenseType *)[arryTypeData objectAtIndex:row]).type, row);
    }
    
}
@end
