//
//  Expense.h
//  sqllite sample
//
//  Created by Rishi Saxena on 30/04/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Expense : NSObject {
    NSInteger expenseid;
    NSString *date;
	NSString *cost;
	NSString *type;
    NSString *notes;
    NSString *reading;
    NSString *selected;
}
@property (nonatomic, readwrite) NSInteger expenseid;
@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *cost;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *notes;
@property (nonatomic, copy) NSString *reading;
@property (nonatomic, copy) NSString *selected;


-(id)initWithID:(NSInteger)lid  date:(NSString *)ldate cost:(NSString *)lcost type:(NSString *)ltype notes:(NSString *)lnotes reading:(NSString *)lreading selected:(NSString *)lselected;

@end
