//
//  ExpenseTypeDetailViewController.m
//  CarUtil
//
//  Created by Rishi Saxena on 02/05/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import "ExpenseTypeDetailViewController.h"
#import "AddExpenseTypeViewController.h"
#import "ExpenseType.h"
#import "CarUtilAppDelegate.h"

@interface ExpenseTypeDetailViewController ()

@end

@implementation ExpenseTypeDetailViewController
@synthesize typeDetailTable;
@synthesize arryExpType;
@synthesize footerView;
@synthesize headerView;

#pragma mark Table view methods
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section   // custom view for header. will be adjusted to default or specified header height
{
    
    if(headerView == nil) {
        //allocate the view if it doesn't exist yet
        headerView  = [[UIView alloc] initWithFrame:CGRectMake(0.0f,0.0f,320.0f,35)];
        //headerView.backgroundColor = [UIColor lightGrayColor];
        
        UIImage *image = [UIImage imageNamed:@"type_header.png"];
        
        UIImageView *tempImg = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)] autorelease];
        [tempImg setImage:image];
        
        [headerView insertSubview:tempImg atIndex:0];  
                
    }
    
    //return the view for the footer
    return headerView;
}

// specify the height of your footer section
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    //differ between your sections or if you
    //have only on section return a static value
    return 34;
}

// custom view for footer. will be adjusted to default or specified footer height
// Notice: this will work only for one section within the table view
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    if(footerView == nil) {
        //allocate the view if it doesn't exist yet
        footerView  = [[UIView alloc] init];
        footerView.backgroundColor = [UIColor clearColor];
        
        UIImage *image = [UIImage imageNamed:@"footer.png"];
        
        UIImageView *tempImg = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)] autorelease];
        [tempImg setImage:image];
        
        //add the button to the view
        [footerView insertSubview:tempImg atIndex:0];
        
    }
    
    //return the view for the footer
    return footerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arryExpType count] ;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Set up the cell...
	cell.textLabel.font=[UIFont fontWithName:@"Arial" size:16];
    cell.textLabel.textColor = [UIColor orangeColor];
	cell.textLabel.text = ((ExpenseType *)[arryExpType objectAtIndex:indexPath.row]).type;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *type =  ((ExpenseType *)[arryExpType objectAtIndex:indexPath.row]).type;
    NSInteger expId =  ((ExpenseType *)[arryExpType objectAtIndex:indexPath.row]).expenseTypeid;
    AddExpenseTypeViewController *addtypedetailController = [[AddExpenseTypeViewController alloc] initWithNibName:@"AddExpenseTypeViewController" bundle:nil] ;
    addtypedetailController.title = @"Edit Expense Type";
    //to push the UIView.
    addtypedetailController.hidesBottomBarWhenPushed = NO;
    addtypedetailController.value = type;
    addtypedetailController.expTypeid = expId;
    [self.navigationController pushViewController:addtypedetailController animated:YES];
    [addtypedetailController release];
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:NO];

}

- (void)tableView:(UITableView *)tv commitEditingStyle:(UITableViewCellEditingStyle)editingStyle 
forRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if(editingStyle == UITableViewCellEditingStyleDelete) {
		
		//Get the object to delete from the array.
        ExpenseType *expenseTypeObj = (ExpenseType *)[arryExpType objectAtIndex:indexPath.row];
        [self deleteTypedata:expenseTypeObj.expenseTypeid];
		[arryExpType removeObject:expenseTypeObj];
		
		//Delete the object from the table.
		[tv deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationMiddle];
	}
}


-(void)deleteTypedata:(NSInteger)expenseTypeid
{
    sqlite3_stmt    *statement;
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
    
    if (sqlite3_open(dbpath, &expenseDB) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM exptype WHERE id=\"%d\"", expenseTypeid];
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(expenseDB, delete_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            //[self showMessage:@"Contact deleted"];
            //[self clearData:nil];
        } else {
            //[self showMessage:@"Failed to delete contact"];
        }
        sqlite3_finalize(statement);
    }         
    sqlite3_close(expenseDB);
}


-(void) reloadExpenseType
{
    [arryExpType removeAllObjects];
    [self readExpenseTypeFromDatabaseTable];
    [typeDetailTable reloadData];
    
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
#ifdef ENABLE_BACK_BUTTON
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    [backButton release]; 
    
#endif
    [super viewDidLoad];
     UIBarButtonItem *AddButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(Adddata:)];
    self.navigationItem.rightBarButtonItem = AddButton;
    [AddButton release];
    // Do any additional setup after loading the view from its nib.
    [self readExpenseTypeFromDatabaseTable];
}

-(void) readExpenseTypeFromDatabaseTable {
    
    const char *dbpath = [[(CarUtilAppDelegate *)[[UIApplication sharedApplication] delegate] databasePath] UTF8String];
	    
    // Init the expense Array
    if(arryExpType == nil)
    {
        DebugLog(@"new  expense type array created");
        arryExpType = [[NSMutableArray alloc] init];
	} else {
        DebugLog(@"old expense type array used");
        [arryExpType removeAllObjects];
    }
	
	// Open the database from the users filessytem
	if(sqlite3_open(dbpath, &expenseDB) == SQLITE_OK) {
		// Setup the SQL Statement and compile it for faster access
		const char *sqlStatement = "select * from EXPTYPE where TYPE <> '<Customize>'";
		sqlite3_stmt *compiledStatement;
		if(sqlite3_prepare_v2(expenseDB, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
			// Loop through the results and add them to the feeds array
			while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
				// Read the data from the result row
                NSInteger primaryKey = sqlite3_column_int(compiledStatement, 0);
				NSString *typeCol = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                
                //DebugLog(@" %d - %@ ",primaryKey,typeCol);
				// Create a new expense object with the data from the database
				ExpenseType *expenseTypeObj = [[ExpenseType alloc] initWithID:primaryKey  type:typeCol] ;
                
				
				// Add the animal object to the animals Array
				[arryExpType addObject:expenseTypeObj];
				
				[expenseTypeObj release];
			}
		}
		// Release the compiled statement from memory
		sqlite3_finalize(compiledStatement);
		
	}
	sqlite3_close(expenseDB);
}


- (void) Adddata: (id) sender
{
    AddExpenseTypeViewController *addtypedetailController = [[AddExpenseTypeViewController alloc] initWithNibName:@"AddExpenseTypeViewController" bundle:nil] ;
    addtypedetailController.title = @"Add Expense Type";
    //to push the UIView.
    addtypedetailController.expTypeid = -1;
    addtypedetailController.hidesBottomBarWhenPushed = NO;
    [self.navigationController pushViewController:addtypedetailController animated:YES];
    [addtypedetailController release];
}

- (void)viewDidUnload
{
    self.arryExpType = nil;
    self.headerView = nil;
    self.footerView = nil;
    self.typeDetailTable = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc {
    if(headerView != nil)
        [headerView release];
    if(footerView != nil)
        [footerView release];
    if(arryExpType != nil)
        [arryExpType release];
    [typeDetailTable release];
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
