<?xml version="1.0" encoding="UTF-8"?><stores><city name='Agra'><store id='25' name='Atmaram NISSAN' address='Near Shastri Puram Fly-over, 
Village Artoni, 
NH -2, Agra  282007' phone1='8057988666' phone2='0562-2640117' mobile1='0' mobile2='0' fax='' email='' website='' photo1='www.nissanmicra.co.in/uploads/20479447464e32aa61a6ee4.1' photo2='www.nissanmicra.co.in/uploads/16339917874e32aa61a7920.2' description='Agra
Atmaram NISSAN
Plot No. 660/2,
Near Shastri Puram Fly-over,
Village Artoni,
NH -2, Agra  282007
8057988666
0562-2640117 ' latitude='27.17667' longitude='78.00807'></store></city><city name='Ahmedabad'><store id='5' name='Petal NISSAN' address='Sun Complex,
Near Sola Flyover,
New Highcourt, S.G. Highway,
Ahmedabad - 380054	' phone1='+91 79 65123311 / 22' phone2='' mobile1='+91 98250 08914      ' mobile2='0' fax='' email='' website='' photo1='www.nissanmicra.co.in/uploads/11653192324e16fce5af8fb.jpg' photo2='www.nissanmicra.co.in/uploads/11700158884e16fce5b058a.jpg' description='Sun Complex,
Near Sola Flyover,
New Highcourt, S.G. Highway,
Ahmedabad - 380054	' latitude='23.05459' longitude='72.51974'></store><store id='50' name='Central NISSAN' address='Central NISSAN,
Panjrapole Cross Road- IIM Road,
Ahmedabad-380015.' phone1='079-40059900' phone2='' mobile1='+91-8980771000' mobile2='' fax='079- 40306131' email='info@centralnissan.co.in' website='' photo1='' photo2='' description='Central NISSAN,
Panjrapole Cross Road- IIM Road,
Ahmedabad-380015.' latitude='23.02682' longitude='72.54942'></store></city><city name='Ajmer'><store id='81' name='Raj NISSAN Ajmer' address='Opposite PATEL Stadium, 
Jaipur Road, Ajmer - 305001' phone1='0145 2633246' phone2='' mobile1='0 9414003046' mobile2='' fax='' email='' website='' photo1='' photo2='' description='Opposite PATEL Stadium, 
Jaipur Road, Ajmer - 305001' latitude='26.46765' longitude='74.63821'></store></city><city name='Ambala'><store id='57' name='Khanna NISSAN' address='13/40 KM Stone,
Tepla, Auto Hub,
Ambala - Jagadhri Highway,
AMBALA - 133104, Haryana' phone1='0171-2821333' phone2='0171-2821444' mobile1='+91-8607807100' mobile2='' fax='91-171-2821555' email='sales@khannanissan.co.in' website='' photo1='www.nissanmicra.co.in/uploads/14077735854f952052e73c0.jpg' photo2='' description='Khanna NISSAN
13/40 KM Stone,
Tepla, Auto Hub,
Ambala - Jagadhri Highway,
AMBALA - 133104, Haryana' latitude='30.30258' longitude='76.95949'></store></city><city name='Amritsar'><store id='35' name='Maharaja NISSAN' address='Near New Amritsar Gate, 
G.T. Road, 
Mohan Viahr,
Amritsar, Punjab - 143 001' phone1='+91 183 258 8188' phone2='+91 258 8288' mobile1='+91 84271 12345' mobile2='0' fax='' email='sales@mrnissan.co.in' website='' photo1='www.nissanmicra.co.in/uploads/2739972094e1707b69032c.jpg' photo2='www.nissanmicra.co.in/uploads/21312993414e1707b693bdc.jpg' description='Maharaja NISSAN' latitude='31.63398' longitude='74.87226'></store></city><city name='Aurangabad'><store id='17' name='Shirin NISSAN' address='opp, Govt. Milk Scheme, Near Kranti Chowk, Jalna Road, Aurangabad-431 001' phone1='+91 240 6649 700' phone2='+91 91589 96644' mobile1='+91 91589 96644      ' mobile2='0' fax='+ 91 240 6649 777' email='sales@shirinnissan.co.in' website='' photo1='www.nissanmicra.co.in/uploads/2113297944e17014db0976.jpg' photo2='www.nissanmicra.co.in/uploads/13304415584e17014db4a04.jpg' description='Shirin NISSAN' latitude='19.86934' longitude='75.3965'></store></city><city name='Bangalore'><store id='9' name='Shahwar NISSAN' address='No.15, Queens Road, 
Bangalore 560052.' phone1='080-41235305' phone2='22355407' mobile1='9880019438' mobile2='9008101909' fax='080-22355408' email='sales@shahwarnissan.com' website='' photo1='www.nissanmicra.co.in/uploads/5852296744e16fd0409a96.jpg' photo2='www.nissanmicra.co.in/uploads/6432519904e16fd040a77b.jpg' description='No.15, Queens Road, 
Bangalore 560052.' latitude='12.98324' longitude='77.59679'></store></city><city name='Bathinda'><store id='75' name='AVC NISSAN' address='AVC NISSAN
Near Jassi Chowk
Mansa Road
Bathinda
Punjab- 151001' phone1='0164-2430243' phone2='' mobile1='8872700005' mobile2='' fax='0164-2430043' email='sales@avcnissan.co.in' website='' photo1='' photo2='' description='AVC NISSAN
Near Jassi Chowk
Mansa Road
Bathinda
Punjab- 151001
' latitude='30.2025' longitude='74.95154'></store></city><city name='Bhubaneshwar'><store id='10' name='Ananta NISSAN' address='94 Pahal,
Cuttack Highway,
Bhubaneswar 752 101' phone1='750-499-2024, 750-499-2026' phone2='' mobile1='' mobile2='' fax='' email='' website='' photo1='www.nissanmicra.co.in/uploads/10430922944e16ff733cd24.jpg' photo2='www.nissanmicra.co.in/uploads/8577856754e16ff733f376.jpg' description='94 Pahal,
Cuttack Highway,
Bhubaneswar 752 101' latitude='20.45261' longitude='85.91619'></store></city><city name='Calicut'><store id='12' name='EVM NISSAN' address='Opp Puthur Temple,
Pavangad, P.O.Pudiyangadi,
Calicut 673021' phone1='+91 495 3014444' phone2='' mobile1='9995046666' mobile2='0' fax='' email='' website='' photo1='www.nissanmicra.co.in/uploads/19596200554e16ffeb15589.jpg' photo2='www.nissanmicra.co.in/uploads/13885865284e16ffeb16685.jpg' description='Opp Puthur Temple,
Pavangad, P.O.Pudiyangadi,
Calicut 673021' latitude='11.25875' longitude='75.78041'></store></city><city name='Chandigarh'><store id='3' name='Bhagat NISSAN' address='Plot No C-19, 
Industrial Area Phase I, 
SAS Nagar, Mohali, 
Punjab	' phone1='+91 172 3057700' phone2='' mobile1='+91 99150 45704' mobile2='0' fax='' email='' website='' photo1='' photo2='' description='Plot No C-19, 
Industrial Area Phase I, 
SAS Nagar, Mohali, 
Punjab	' latitude='30.72769' longitude='76.70623'></store><store id='54' name='Hind NISSAN' address='9, Industrial Area Phase 1,
Chandigarh - 160002' phone1='5001111' phone2='' mobile1='+91 9780035700' mobile2='+91 9814002197' fax='0172-2651805' email='sales@hindmotorsnissan.co.in' website='' photo1='www.nissanmicra.co.in/uploads/543479204f952135f2f33.jpg' photo2='' description='Hind NISSAN
9, Industrial Area Phase 1,
Chandigarh - 160002' latitude='30.70834' longitude='76.80578'></store></city><city name='Chennai'><store id='8' name='Sherif NISSAN' address='133 Velachery Main Road,
Guindy, Chennai - 600032' phone1='+91 44 22301980 / 83' phone2='' mobile1='+91 99418 11120       / 30 / 40 / 50 / 60' mobile2='0' fax='' email='' website='' photo1='www.nissanmicra.co.in/uploads/6027946614e16fcfce3b3a.jpg' photo2='www.nissanmicra.co.in/uploads/9251819864e16fcfce46b7.jpg' description='133 Velachery Main Road,
Guindy, Chennai - 600032' latitude='12.98252' longitude='80.2236'></store><store id='38' name='Jubilant NISSAN' address='No. 18/3, Raja Annamalai Building
Rukhmini Lakshmipati Road (Marshal Road)
Egmore,
Chennai  600 008' phone1='044-28587000 / 044-28581444 / 044-28581777' phone2='' mobile1='95515 70000 / 99410 28000 / 95510 22255' mobile2='' fax='' email='' website='' photo1='' photo2='' description='No. 18/3, Raja Annamalai Building
Rukhmini Lakshmipati Road (Marshal Road)
Egmore,' latitude='13.07309' longitude='80.25722'></store><store id='72' name='Avenue NISSAN' address='133 Velachery Main Road,
Guindy, Chennai - 600032' phone1=' +91 44 22301980 /81/82/83 ' phone2='' mobile1='+91 9443333336 ' mobile2='' fax='' email='info@avenuenissan.co.in ' website='' photo1='' photo2='' description='133 Velachery Main Road,
Guindy, Chennai - 600032' latitude='13.00205' longitude='80.21899'></store></city><city name='Cochin'><store id='6' name='EVM NISSAN' address='INTUC Junction,
Maradu, Nettor,
Cochin - 682040	' phone1='' phone2='' mobile1='+91 99950 66666      ' mobile2='91' fax='' email='' website='' photo1='www.nissanmicra.co.in/uploads/7253241204e16fced88e4d.jpg' photo2='www.nissanmicra.co.in/uploads/6690190084e16fced89d62.jpg' description='INTUC Junction,
Maradu, Nettor,
Cochin - 682040	' latitude='9.92874' longitude='76.31769'></store></city><city name='Coimbatore'><store id='18' name='Ramani NISSAN' address='176/2 Sathyamangalam Road,
Saravanampatty,
Coimbatore - 641035	' phone1='+91 422 2668941' phone2='2668942' mobile1='91' mobile2='91' fax='' email='' website='' photo1='www.nissanmicra.co.in/uploads/9620763924e1701ab89cc4.jpg' photo2='www.nissanmicra.co.in/uploads/7625339924e1701ab8e59b.jpg' description='Ramani NISSAN' latitude='10.999' longitude='76.9609'></store></city><city name='Dehradun'><store id='60' name='Oberai NISSAN' address='By Pass Road
Near Kargi Chowk
Dehradun - 248001
Uttarakhand' phone1='' phone2='' mobile1='+91 9756798650 ' mobile2='' fax='0135 2677075 ' email='sales@oberainissan.co.in' website='' photo1='www.nissanmicra.co.in/uploads/2981957834f9522c0c3ac8.jpg' photo2='' description='Oberai NISSAN
By Pass Road
Near Kargi Chowk
Dehradun - 248001
Uttarakhand
' latitude='30.2899' longitude='78.01057'></store></city><city name='Delhi'><store id='2' name='Nath NISSAN' address='A-30 Mohan Cooperative Industrial Estate,
Mathura Road,
New Delhi - 110044	' phone1='011-43299999' phone2='' mobile1='9582598432/36/37/39' mobile2='0' fax='01143299928' email='' website='' photo1='' photo2='' description='Delhi
Nath Nissan
A-30 Mohan Cooperative Industrial Estate,
Mathura Road,
New Delhi - 110044
Tel : 011-43299999
Mob : 9582598432/36/37/39
Fax: 01143299928. ' latitude='28.50934' longitude='77.29912'></store><store id='28' name='Sky NISSAN' address='63 Rama Marg, 
Najafgarh Road, 
New Delhi - 110015' phone1='011-40066666' phone2='011-40066667' mobile1='0' mobile2='0' fax='' email='' website='' photo1='www.nissanmicra.co.in/uploads/5916416974e17055c7b54e.jpg' photo2='www.nissanmicra.co.in/uploads/2014690534e17055c7c351.jpg' description='Sky Nissan' latitude='28.66155' longitude='77.15454'></store><store id='48' name='RC NISSAN ' address='Aggarwal auto Mall,
Plot No. 2,
A-BLK, 
Shalimar Bagh Extension, 
New Delhi- 110088' phone1='011-30289289' phone2='' mobile1='08860605434' mobile2='' fax='' email='gm@rcnissan.co.in' website='' photo1='www.nissanmicra.co.in/uploads/16135919484f9522f204b48.jpg' photo2='www.nissanmicra.co.in/uploads/5150229544f9522f2060b5.jpg' description='RC NISSAN 
Aggarwal auto Mall,
Plot No. 2,
A-BLK, 
Shalimar Bagh Extension, 
New Delhi- 110088' latitude='28.68193' longitude='77.15647'></store><store id='55' name='Zedex NISSAN' address='89 F.I.E. Patparganj Industrial Area,
Delhi  110092' phone1='+91-11-49320000' phone2='' mobile1='+91-9871733311' mobile2='' fax='+91-11-43055800' email='sales@zedexnissan.co.in' website='' photo1='www.nissanmicra.co.in/uploads/19107839054f9523fc64bb0.jpg' photo2='' description='Zedex NISSAN
89 F.I.E. Patparganj Industrial Area,
Delhi  110092' latitude='28.63942' longitude='77.3109'></store></city><city name='Dhanbad'><store id='68' name='Animesh NISSAN' address='Animesh NISSAN
G. T. Road, Barwadda, Kashitand.
Dhanbad - 826004
Jharkhand' phone1='0326-2293033' phone2='' mobile1='+91 8651555555' mobile2='' fax='' email='sales@animeshnissan.co.in' website='' photo1='' photo2='' description='Animesh NISSAN
G. T. Road, Barwadda, Kashitand.
Dhanbad - 826004
Jharkhand

' latitude='23.84667' longitude='86.42841'></store></city><city name='Faridabad'><store id='73' name='BM NISSAN' address='14/6 Milestone 
Delhi Mathura Highway
Faridabad - 121003
Haryana' phone1='0129-4260606' phone2='' mobile1='+91 8826400606' mobile2='' fax='0129-4314720 ' email='sales@bmnissan.co.in ' website='' photo1='www.nissanmicra.co.in/uploads/1098417955502c989e8263c.jpg' photo2='' description='BM NISSAN
14/6 Milestone 
Delhi Mathura Highway
Faridabad - 121003
Haryana
' latitude='28.37856' longitude='77.31354'></store></city><city name='Gandhidham'><store id='78' name='IB NISSAN' address='Plot.No.79,Opp.Cargo Motors Service Station,
GIDC,Sector-11, K.P.T Port,
Gandhidham, Kutch- 370201' phone1='02836-654001' phone2='02836-238929' mobile1='9033090209' mobile2='' fax='' email='Sales_gdm@ibnissan.co.in' website='' photo1='' photo2='' description='Plot.No.79,Opp.Cargo Motors Service Station,
GIDC,Sector-11, K.P.T Port,
Gandhidham, Kutch- 370201' latitude='23.05794' longitude='70.13775'></store></city><city name='Goa'><store id='23' name='AM NISSAN' address='Plot Nos. 49/3 &amp; 49/4,
Nagoa, Verna, Goa,' phone1='0832 6482101  07' phone2='' mobile1='0' mobile2='0' fax='0832 6482108' email='' website='' photo1='www.nissanmicra.co.in/uploads/20520943614e996adc22b4f.jpg' photo2='www.nissanmicra.co.in/uploads/19567663514e996adc245bb.jpg' description='AM NISSAN
Plot Nos. 49/3 &amp; 49/4,
Nagoa, Verna, Goa,
0832 - 2887421
0832 - 2887423' latitude='15.34733' longitude='73.92959'></store></city><city name='Gurgaon'><store id='13' name='Sterling NISSAN' address='Plot 39,
Sector 18, HUDA,
Gurgaon 122 015' phone1='0124-4953000 ' phone2='' mobile1='9650096333' mobile2='9650096329' fax='' email='sales@sterlingnissan.co.in' website='' photo1='www.nissanmicra.co.in/uploads/2690556994e1700251976b.jpg' photo2='www.nissanmicra.co.in/uploads/1970631054e1700251a910.jpg' description='Plot 39,
Sector 18, HUDA,
Gurgaon 122 015' latitude='28.43479' longitude='77.05285'></store></city><city name='Gurgaon '><store id='58' name='Sidheshwar NISSAN ' address='Ground Floor,
Universal Trade Tower,
Sohna Road,
Gurgaon - 122 001' phone1='+ 91 124 492 8888' phone2='' mobile1='+91 81306 92025' mobile2='+91 8130692012' fax='+ 91 124 492 8888' email='sales@sidheshwarnissan.co.in' website='' photo1='www.nissanmicra.co.in/uploads/6895870764f9523b9f03af.jpg' photo2='' description='Sidheshwar NISSAN 
Ground Floor,
Universal Trade Tower,
Sohna Road,
Gurgaon - 122 001' latitude='28.41624' longitude='77.04145'></store></city><city name='Guwahati'><store id='40' name='Binod NISSAN' address='NH - 37, Lalung  Gaon,
Betkuchi,
Guwahati - 781 034,' phone1='7896022168, 7896022178' phone2='' mobile1='' mobile2='' fax='' email='sales@binodnissan.co.in' website='' photo1='' photo2='' description='NH - 37, Lalung  Gaon,
Betkuchi,
Guwahati - 781 034,' latitude='26.14713' longitude='91.73555'></store></city><city name='Haldwani'><store id='80' name='Pal NISSAN' address='Plot No - 88, 
Manpur West Pargana Six Khata,
Delhi-Rampur Road, Haldwani, 
Uttarakhand- 263139' phone1='05964-234111' phone2='' mobile1='8477008200' mobile2='' fax='05946-235205' email='sales@palnissan.co.in' website='' photo1='www.nissanmicra.co.in/uploads/124581599350765f57bbf16.JPG' photo2='www.nissanmicra.co.in/uploads/14505366150765f57bcef4.JPG' description='Plot No - 88, 
Manpur West Pargana Six Khata,
Delhi-Rampur Road, Haldwani, 
Uttarakhand- 263139' latitude='29.20805' longitude='79.52137'></store></city><city name='Hissar'><store id='70' name='Sidheshwar NISSAN' address='NH-10
Delhi-Hissar by pass road
Hissar - 125001
Haryana' phone1='01662-222333' phone2='' mobile1='+91 07206018915, +91 07206018916' mobile2='+91 07206018917' fax='' email='sales.hsr@sidheshwarnissan.co.in' website='' photo1='' photo2='' description='NH-10
Delhi-Hissar by pass road
Hissar - 125001
Haryana

' latitude='28.68373' longitude='77.01424'></store></city><city name='Hubli'><store id='74' name='Swasti NISSAN' address='Bellad Chambers II
Unkal Cross, Vidya Nagar,
Hubli, Karnataka - 580031' phone1='0836-2271136' phone2='' mobile1='9916056789' mobile2='' fax='' email='sales@swastinissan.co.in' website='' photo1='' photo2='' description='Swasti NISSAN
Bellad Chambers II
Unkal Cross, Vidya Nagar,
Hubli, Karnataka - 580031
' latitude='15.36543' longitude='75.1238'></store></city><city name='Hyderabad'><store id='31' name='Lakshmi NISSAN' address='8-2-293/A/472
Sai Gallaria
Road #36,
Jubilee hills
Hyderabad  500 034.' phone1='040 - 23384180 - 81, 32580657, 64639583' phone2='' mobile1='0' mobile2='0' fax='' email='' website='' photo1='www.nissanmicra.co.in/uploads/12735898024e1706d5722c0.jpg' photo2='www.nissanmicra.co.in/uploads/11071636914e1706d5735e5.jpg' description='8-2-293/A/472
Sai Gallaria
Road #36,
Jubilee hills
Hyderabad  500 034.' latitude='17.42953' longitude='78.41266'></store></city><city name='Indore'><store id='22' name='Asean NISSAN' address='6/88,
10,Vishnupuri,Bhawarkua Square.
A.B.Road, Indore-452005' phone1='0731-4039401,2,3' phone2='' mobile1='0' mobile2='0' fax='' email='' website='' photo1='www.nissanmicra.co.in/uploads/12629924264e32a931a86db.1' photo2='www.nissanmicra.co.in/uploads/4949945064e32a931a94d7.2' description='Asean NISSAN
6/88,
10,Vishnupuri,Bhawarkua Square.
A.B.Road, Indore-452005
0731-4039401,2,3' latitude='' longitude=''></store></city><city name='Jaipur'><store id='20' name='Royal NISSAN' address='Plot No: 1&amp;2,
Durga Vihar,
Sita Bari,
Tonk Road,
Jaipur - 302 015' phone1='Tel +91 141 514 3331/3333/2000' phone2='' mobile1='+91 97850 55333' mobile2='+91 96675 53108' fax='' email='sales@royalnissan.co.in ' website='' photo1='www.nissanmicra.co.in/uploads/10151306384e1702998fbe6.jpg' photo2='www.nissanmicra.co.in/uploads/10110596594e17029992d6a.jpg' description='Plot No: 1&amp;2, Durga Vihar, Sita Bari, Tonk Road, Jaipur - 302 015' latitude='26.91242' longitude='75.78729'></store></city><city name='Jalandhar'><store id='14' name='Dada NISSAN' address='G.T Road, Adjacent to Ansal Mall,
Near Jalandhar Octroi,
Jalandhar 144005' phone1='+91 181 6600400' phone2='' mobile1='0' mobile2='0' fax='' email='' website='' photo1='www.nissanmicra.co.in/uploads/1462039924e17004b1dac8.jpg' photo2='www.nissanmicra.co.in/uploads/17663471454e17004b1eb67.jpg' description='G.T Road, Adjacent to Ansal Mall,
Near Jalandhar Octroi,
Jalandhar 144005' latitude='31.32602' longitude='75.57618'></store></city><city name='Jammu'><store id='82' name='Lahori NISSAN' address='Lahori Nissan Pvt. Ltd.
National highway, bye pass, 
Sainik Colony, Jammu. ' phone1='0191 - 469817 ' phone2='' mobile1='9419118586' mobile2='' fax='' email='sales@lahorinissan.co.in' website='' photo1='' photo2='' description='Lahori Nissan Pvt. Ltd.
National highway, bye pass, 
Sainik Colony, Jammu. 
' latitude='32.67727' longitude='74.90626'></store></city><city name='Jamshedpur'><store id='41' name='Bhalotia NISSAN' address='VIIth Phase , Adityapur Industrial Area,
Ghamharia,
Jamshedpur
For Services contact: 09204067583' phone1='0657 6510782' phone2='+91 9204067585' mobile1='+91 9204067580' mobile2='+91 9204067581' fax='0657 2227684' email='jsrnissan@gmail.com' website='' photo1='' photo2='' description='VIIth Phase , Adityapur Industrial Area,
Ghamharia,
Jamshedpur
' latitude='22.78045' longitude='86.15676'></store></city><city name='Jodhpur '><store id='45' name='Grand NISSAN' address='70/1, Basni Baghela,
NH-65, Pali Road,
Jodhpur - 342 005' phone1='0291- 2729888' phone2='' mobile1='+91 9983897000' mobile2='+91 9782001750  ' fax='' email='sales@grandnissan.co.in ' website='' photo1='' photo2='' description='70/1, Basni Baghela, NH-65, Pali Road,Jodhpur - 342 005' latitude='25.7976' longitude='73.29146'></store></city><city name='Kannur'><store id='53' name='Manumatic NISSAN' address='Thottada,
Kannur - 670007,
Kerala.
' phone1='+91 497 2837199' phone2='' mobile1='+91 86061 20000' mobile2='' fax='' email='info@manumaticnissan.co.in' website='' photo1='' photo2='' description='Thottada,
Kannur - 670007,
Kerala.

' latitude='11.84299' longitude='75.4214'></store></city><city name='Kanpur'><store id='43' name='RNG NISSAN' address='Afim Kothi Crossing,
84/105, A, G.T. Road,
KANPUR - 208003' phone1='0512-524444' phone2='' mobile1='9506222888' mobile2='' fax='' email='sales@rngnissan.co.in' website='' photo1='' photo2='' description='Afim Kothi Crossing,  84/105  A, G.T. Road,        KANPUR - 208003' latitude='26.44992' longitude='80.33187'></store></city><city name='Karnal'><store id='11' name='Malwa NISSAN' address='SCO 8, Sector 14, 
Urban Estate, 
Meerut Road,
Karnal - 132001 ' phone1='0184-2200326 ' phone2='' mobile1='0' mobile2='0' fax='' email='' website='' photo1='www.nissanmicra.co.in/uploads/15017581154e32ab53d767b.1' photo2='www.nissanmicra.co.in/uploads/21068662114e32ab53d7ec5.2' description='Malwa NISSAN
SCO 8, Sector 14,
Urban Estate,
Meerut Road,
Karnal - 132001
0184-2200326 ' latitude='29.68039' longitude='76.99385'></store></city><city name='Kolhapur'><store id='29' name='SMG NISSAN' address='R.S. no. 14, 
15 Mauje Ujalaiwadi, 
NH-4 Highway, Kolhapur ' phone1='0231- 2606365 ' phone2='2606366' mobile1='0' mobile2='0' fax='' email='' website='' photo1='www.nissanmicra.co.in/uploads/6373545634e32ab8f275b2.2' photo2='' description='SMG NISSAN
R.S. no. 14,
15 Mauje Ujalaiwadi,
NH-4 Highway, Kolhapur
0231- 2606365 / 2606366' latitude='16.69131' longitude='74.24487'></store></city><city name='Kolkata'><store id='26' name='Chandrani NISSAN' address='1/1-A, Mohindra Roy Lane,
Ground Floor, P.S. Pace Building,
Kolkata-46' phone1='033-40004444' phone2='' mobile1='0' mobile2='0' fax='' email='' website='' photo1='www.nissanmicra.co.in/uploads/9999256274e32aa9671b48.1' photo2='' description='Chandrani NISSAN
1/1-A, Mohindra Roy Lane,
Ground Floor, P.S. Pace Building,
Kolkata-46
033-40004444' latitude='22.57265' longitude='88.36389'></store></city><city name='Kota'><store id='36' name='FM NISSAN' address='252, Transport Nagar,
Jhalawar Road, Kota - 324008' phone1='0744-2428189' phone2='' mobile1='8696943004' mobile2='8696943000' fax='0744-2436823' email='sales@fmnissan.co.in' website='' photo1='www.nissanmicra.co.in/uploads/15757004574e1707ff60e8b.jpg' photo2='www.nissanmicra.co.in/uploads/5683296294e1707ff61b38.jpg' description='FM NISSAN' latitude='24.59708' longitude='76.12152'></store></city><city name='Kottayam'><store id='76' name='EVM NISSAN' address='EVM Automotive (I) Pvt. Ltd
Building No. XI/3C &amp; XI/3D.
Opp. St. Marys Chapel, M.C.Road,
Thellokam, Ettumanoor(P.O),
Kottayam dist.  686016.' phone1='0481-3041111, 0481-3240404' phone2='' mobile1='9995803804, 9567046666' mobile2='' fax='' email='sales.ktm@evmnissan.co.in, service.ktm@evmnissan.co.in' website='' photo1='' photo2='' description='EVM Automotive (I) Pvt. Ltd
Building No. XI/3C &amp; XI/3D.
Opp. St. Marys Chapel, M.C.Road,
Thellokam, Ettumanoor(P.O),
Kottayam dist.  686016.' latitude='9.59872' longitude='76.5289'></store></city><city name='Lucknow'><store id='34' name='Dream NISSAN' address='Faizabad Road, 
Lucknow,
Uttar Pradesh' phone1='+91 97210 45555' phone2='' mobile1='0' mobile2='0' fax='' email='enquiry@dreamnissan.in' website='' photo1='' photo2='' description='Dream Nissan' latitude='26.87406' longitude='81.01875'></store></city><city name='Ludhiana'><store id='33' name='Dada NISSAN' address='Dada NISSAN
G.T. Road,
Dhandari Kalan,
Ludhiana - 141 014.' phone1='+91 89680 55000' phone2='+91 89688 30000' mobile1='0' mobile2='0' fax='' email='nissan1@dadamotors.com ' website='' photo1='www.nissanmicra.co.in/uploads/16523214874e1707439bc83.jpg' photo2='www.nissanmicra.co.in/uploads/5195988124e1707439cba6.jpg' description='Dada NISSAN
G.T. Road, 
Dhandari Kalan, 
Ludhiana - 141 014.' latitude='30.85546' longitude='75.94112'></store></city><city name='Madurai'><store id='42' name='Jeevan NISSAN' address='204 GST road,
Thanakkankulam
Madurai - 625 006' phone1='+91 452 248 8177/88' phone2='' mobile1='+91 94890 88188' mobile2='' fax='' email='' website='' photo1='' photo2='' description='204 GST road,
Thanakkankulam' latitude='12.95747' longitude='80.14149'></store></city><city name='Mangalore'><store id='62' name='Excelsior NISSAN' address='S.No. 32/2B3, 2B2, National Highway - 17 (66)
Derebail, Kottarachowki,
Mangalore 575 006
Karnataka
' phone1='0824 3027777' phone2='' mobile1='+91 9972277777 ' mobile2='' fax='' email='sales@excelsiornissan.co.in' website='' photo1='www.nissanmicra.co.in/uploads/210701804f95218d95526.jpg' photo2='' description='Excelsior NISSAN
S.No. 32/2B3, 2B2, National Highway - 17 (66)
Derebail, Kottarachowki,
Mangalore 575 006
Karnataka

' latitude='12.90393' longitude='74.84805'></store></city><city name='Meerut'><store id='37' name='GS NISSAN' address='Delhi Road, 
Meerut - 250 103,' phone1='+91 121 251 1803/04' phone2='+91 73512 00300' mobile1='0' mobile2='0' fax='+91 121 251 1805 ' email='sales@gsnissan.co.in ' website='' photo1='www.nissanmicra.co.in/uploads/6532885514e170836828a6.jpg' photo2='www.nissanmicra.co.in/uploads/21330896824e17083683843.jpg' description='GS NISSAN' latitude='28.95165' longitude='77.67808'></store></city><city name='Mumbai'><store id='1' name='Ichibaan NISSAN' address='Vaswani Chambers, No-264/265 
Dr. Annie Besant Road, 
Worli, Mumbai. ' phone1='+91 22 2430 9616 - 20 ' phone2='' mobile1='+91 96194 97182      ' mobile2='0' fax='' email='' website='' photo1='www.nissanmicra.co.in/uploads/989243154e32a7a97623e.jpg' photo2='www.nissanmicra.co.in/uploads/10987941154e32a7f695959.jpg' description='Mumbai
Ichibaan NISSAN
Vaswani Chambers, No-264/265
Dr. Annie Besant Road,
Worli, Mumbai.
+91 22 2430 9616 - 20
+91 96194 97182' latitude='18.99934' longitude='72.81717'></store><store id='61' name='Torrent NISSAN' address='G 7, G 8, G 9, G 10A &amp; G 10B, Ground Floor
Shalimar Morya Park, Off Link Road,
Andheri West,
Mumbai
MAHARASHTRA' phone1='022 - 4056 0000 ' phone2='' mobile1='+91 7226 0000' mobile2='' fax='022 - 4056 0001' email='sales@torrentnissan.co.in' website='' photo1='www.nissanmicra.co.in/uploads/18979126044f9796919dbd5.jpg' photo2='www.nissanmicra.co.in/uploads/388415234f9796919f180.jpg' description='Torrent NISSAN
G 7, G 8, G 9, G 10A &amp; G 10B, Ground Floor
Shalimar Morya Park, Off Link Road,
Andheri West,
Mumbai
MAHARASHTRA
400 053
' latitude='19.14132' longitude='72.83217'></store></city><city name='Mysore'><store id='71' name='Honnassiri NISSAN' address='No.193/5,Hunsur Main Road,
Next to Shell Petrol Bunk
Hinkal
Mysore - 570017
Karnataka
' phone1='0821-4191500' phone2='0821-4191501' mobile1='+91  9686978888' mobile2='' fax=' 0821-4192777' email='sales@honnassirinissan.co.in ' website='' photo1='' photo2='' description='No.193/5,Hunsur Main Road,
Next to Shell Petrol Bunk
Hinkal
Mysore - 570017
Karnataka

' latitude='12.32659' longitude='76.61041'></store></city><city name='Nagpur'><store id='27' name='Navnit NISSAN' address='D-3, MIDC,
Hingna, Nagpur - 27' phone1='7104667111' phone2='7104667112' mobile1='0' mobile2='0' fax='' email='' website='' photo1='www.nissanmicra.co.in/uploads/20597074414e32ab72cc4d7.2' photo2='' description='Nagpur
Navnit NISSAN
D-3, MIDC,
Hingna, Nagpur - 27
7104667111
7104667112' latitude='21.1458' longitude='79.08815'></store></city><city name='Nellore'><store id='67' name='Lucky NISSAN' address='NH-5, Chemudugunta Village,
Venkatachalam Mandal,
Nellore - 524 320
Andhra Pradesh' phone1='0861-2383637' phone2='0861-2383637' mobile1='+91 7702931111, +91 7702971111' mobile2=' +91 7702891111' fax='0861-2383633' email='admin.nlr@luckynissan.co.in' website='' photo1='' photo2='' description='Lucky NISSAN
NH-5, Chemudugunta Village,
Venkatachalam Mandal,
Nellore - 524 320
Andhra Pradesh

' latitude='14.39395' longitude='79.94202'></store></city><city name='Noida'><store id='56' name='Neo NISSAN' address='H-5, SECTOR-63,
NOIDA (UP) - 201301' phone1='0120-4700000' phone2='' mobile1='+91 85 27 99 55 00' mobile2='' fax='0120-4700047' email='sales@neonissan.co.in' website='' photo1='www.nissanmicra.co.in/uploads/9264016004f95224a3dcc3.jpg' photo2='www.nissanmicra.co.in/uploads/12794771634f95224a3f3b0.jpg' description='Neo NISSAN
H-5, SECTOR-63,
NOIDA (UP) - 201301' latitude='28.62099' longitude='77.38116'></store></city><city name='Patiala'><store id='39' name='KL NISSAN' address='Bahadurgarh,
Rajpura Road,
Patiala,
Punjab,' phone1='+91 175 506 0004' phone2='+91 92170 23080' mobile1='0' mobile2='0' fax='' email='' website='' photo1='' photo2='' description='Bahadurgarh,
Rajpura Road,
Patiala,
Punjab,' latitude='30.32747' longitude='76.39755'></store></city><city name='Pune'><store id='4' name='Oxford NISSAN' address='Near Sus Over Bridge,
Off Mumbai - Bangalore Highway
Baner, Pune - 411045	' phone1='+91 20 66804848' phone2='' mobile1='+91 95525 59510/15' mobile2='0' fax='' email='' website='' photo1='www.nissanmicra.co.in/uploads/17839684134e16fcd8ddcd0.jpg' photo2='www.nissanmicra.co.in/uploads/6421307784e16fcd8deea7.jpg' description='abc' latitude='18.54838' longitude='73.77343'></store></city><city name='Raipur'><store id='32' name='AKS NISSAN' address='Raipur Bhilai Expressway, 
Near Kumhari TOll, 
RNM - Ahiwara, Tehsil - Dhamdha,
Chhattisgarh - 490 042' phone1='+91 788 211 3111' phone2='' mobile1='+91 90092 22200' mobile2='+91 90096 66600' fax='' email='dgm@aksnissan.co.in' website='' photo1='www.nissanmicra.co.in/uploads/207633024e996c253e547.jpg' photo2='www.nissanmicra.co.in/uploads/4826675234e996c253fbde.jpg' description='AKS NISSAN
Raipur Bhilai Expressway, 
Near Kumhari TOll, 
RNM - Ahiwara, Tehsil - Dhamdha,
Chhattisgarh - 490 042' latitude='21.45966' longitude='81.33271'></store></city><city name='Rajamundry'><store id='64' name='Kantipudi NISSAN' address='Kantipudi NISSAN
S.No: 202/1A, NH-5
Gandhi Prakash Nagar,
RAJAMUNDRY - 533107' phone1='0883-2556666' phone2='0883-2477755' mobile1='+91 7799977555, +91 7799977771' mobile2='+91 77999-77778' fax='0883-2477755 ' email='info.rjy@kantipudinissan.co.in' website='' photo1='' photo2='' description='Kantipudi NISSAN
S.No: 202/1A, NH-5
Gandhi Prakash Nagar,
RAJAMUNDRY - 533107' latitude='16.98883' longitude='81.77845'></store></city><city name='Rajkot'><store id='7' name='IB NISSAN' address='Gondal  Ahmedabad By-Pass Circle, Gondal Road, NH- 8-B, At: VAVDI - RAJKOT 
 360 004 (Gujarat) ' phone1='+91-9228-00-1313 ' phone2='+91-9228-00-1414.' mobile1='+91-9228-00-1414' mobile2='+91-9228-00-1313' fax='' email='sales@ibnissan.co.in' website='' photo1='www.nissanmicra.co.in/uploads/2156876164e32ab275b6f7.3' photo2='www.nissanmicra.co.in/uploads/2896285494e32ab275c05c.1' description='IB Nissan
Gondal  Ahmedabad By-Pass Circle,
 Gondal Road, NH- 8-B, At: VAVDI - RAJKOT  360 004 (Gujarat) Tel : +91-9228-00-1313 / +91-9228-00-1414.
Mob : +91-9228-00-1313 / +91-9228-00-1414
Email : sales@ibnissan.co.in' latitude='22.23546' longitude='70.79862'></store></city><city name='Salem'><store id='79' name='Marvel NISSAN' address='Opp. LPG gas Bunk,
Near RTO Office,
Kandampatty Bypass,
Salem, Tamilnadu- 636005' phone1='0427-2225583' phone2='' mobile1='8220110044' mobile2='' fax='0427-2225499' email='sales@marvelnissan.co.in' website='' photo1='' photo2='' description='Opp. LPG gas Bunk,
Near RTO Office,
Kandampatty Bypass,
Salem, Tamilnadu- 636005' latitude='11.67611' longitude='78.12378'></store></city><city name='Secunderabad'><store id='30' name='Fortune NISSAN' address='160-D, Patny Nagar,
S. P. Road, Secunderabad,
Andhra Pradesh  500003 ' phone1='4027180000' phone2='' mobile1='9666913000 ' mobile2='0' fax='' email='' website='' photo1='www.nissanmicra.co.in/uploads/7613319514e32aae8ac1b8.Secundrabad' photo2='' description='Fortune NISSAN
160-D, Patny Nagar,
S. P. Road, Secunderabad,
Andhra Pradesh  500003
4027180000 / 9666913000' latitude='17.43993' longitude='78.49827'></store></city><city name='Sonipat'><store id='52' name='Malwa NISSAN' address='N.H.- 1,  31 K.M STONE,
G.T. ROAD, KUNDLI,
SONIPAT(HR) (131028).' phone1='0130-2121731/732/733' phone2='' mobile1='08607400310/08607400313' mobile2='08607400311/08607400312' fax='' email='sales.snp@malwanissan.co.in' website='' photo1='' photo2='' description='N.H.- 1,  31 K.M STONE,
G.T. ROAD, KUNDLI,
SONIPAT(HR) (131028).' latitude='28.87181' longitude='77.11975'></store></city><city name='Surat'><store id='16' name='Silicon NISSAN' address='Near BRC Temple,
Udhna Navsari Main Road.
UDHNA.
Surat-394210
' phone1='0261-2890290' phone2='' mobile1='+91 9925033000' mobile2='' fax='' email='sales@siliconnissan.co.in' website='' photo1='www.nissanmicra.co.in/uploads/7065586244e1701111522a.jpg' photo2='www.nissanmicra.co.in/uploads/11834670994e1701111632d.jpg' description='Near BRC Temple,
Udhna Navsari Main Road.
UDHNA.
Surat-394210
' latitude='21.195' longitude='72.81944'></store><store id='47' name='Pramukh NISSAN' address='Pramukh Automobiles Pvt. Ltd.,
Opp. Central Mall,Near Valentine Cinema,
Surat Dumas Road, Surat(GUJARAT-INDIA)
Pin Code: 395008.
' phone1='+91 7800098556' phone2='' mobile1='' mobile2='' fax='0261-2915915' email='sales@pramukhnissan.co.in' website='' photo1='' photo2='' description='Pramukh Automobiles Pvt. Ltd.,
Opp. Central Mall,Near Valentine Cinema,
Surat Dumas Road, Surat(GUJARAT-INDIA)
Pin Code: 395008.
' latitude='21.17586' longitude='72.7958'></store></city><city name='Thane '><store id='65' name='Ritu NISSAN' address='Ground Floor, Vibgyor Building
Village Kolshet, Patli Pada,
Near Hiranandani Estate, Ghodbunder Road,
Thane - 400607
Maharashtra' phone1='022 - 39994999' phone2='' mobile1='+91 8691800000' mobile2='' fax='022 - 39994900' email='sales@ritunissan.co.in' website='' photo1='www.nissanmicra.co.in/uploads/1789461234f95232b92f6a.jpg' photo2='' description='Ritu NISSAN
Ground Floor, Vibgyor Building
Village Kolshet, Patli Pada,
Near Hiranandani Estate, Ghodbunder Road,
Thane - 400607
Maharashtra

' latitude='19.23361' longitude='72.99128'></store></city><city name='Trichy'><store id='83' name='SJB NISSAN' address='SJB Automobiles (P) Ltd
133A/1A, Trichy - Madurai NH,
Next to Saranathan Engineering College,
Panjapur, Trichy,
Tamil Nadu- 620012' phone1='0431-3015111' phone2='' mobile1='8883066399' mobile2='' fax='0431- 3015101' email='sales@sjbnissan.co.in' website='' photo1='' photo2='' description='SJB Automobiles (P) Ltd
133A/1A, Trichy - Madurai NH,
Next to Saranathan Engineering College,
Panjapur, Trichy,
Tamil Nadu- 620012' latitude='10.79048' longitude='78.70467'></store></city><city name='Trivandrum'><store id='44' name='Marikar NISSAN' address='N.H.BY PASS,
VENPALAVATTOM,
ANAYARA P.O,
 TRIVANDRUM-695029' phone1='0471-2740505 / 2740506' phone2='' mobile1='9562290001' mobile2='' fax='0471-2740509' email='sales@marikarnissan.co.in ' website='' photo1='' photo2='' description='N.H.BY    PASS,VENPALAVATTOM,ANAYARA P.O, TRIVANDRUM' latitude='8.48749' longitude='76.94862'></store></city><city name='Udaipur'><store id='63' name='Nidhi Kamal NISSAN' address='126,
ABC,Hiran Magri,Sec.11,
Near Shahi Bagh Garden,
Udaipur - 313001
Rajasthan' phone1='0294-2481327' phone2='' mobile1='8094111189' mobile2='' fax='0294-2481328' email='sales@nidhikamalnissan.co.in' website='' photo1='www.nissanmicra.co.in/uploads/1468009134f952285a4612.jpg' photo2='' description='Nidhi Kamal NISSAN
126,
ABC,Hiran Magri,Sec.11,
Near Shahi Bagh Garden,
Udaipur - 313001
Rajasthan

' latitude='24.55657' longitude='73.69457'></store></city><city name='Vadodara'><store id='24' name='Aster NISSAN' address='Plot No. 986/32 GIDC,
Makarpura Main Road,
Vadodara, Gujarat' phone1=' 97-1234-7000 / 8000' phone2='97-1234-9000' mobile1='0' mobile2='0' fax='0265-2680822' email='' website='' photo1='www.nissanmicra.co.in/uploads/20543498034e996cf4730da.jpg' photo2='www.nissanmicra.co.in/uploads/3542472974e996cf474794.jpg' description='Aster NISSAN
Plot No. 986/32 GIDC,
Makarpura Main Road,
Vadodara, Gujarat' latitude='22.24785' longitude='73.19696'></store></city><city name='Vapi'><store id='66' name='Pramukh NISSAN' address='Survey No. 134,
Ravenue Survey No.312, 
Vapi Town,N.H. No. 8, 
Near Jalaram Temple
Vapi - 396 191
Gujarat' phone1='' phone2='' mobile1='+91 8469077711' mobile2='' fax='' email='salesvapi@pramukhnissan.co.in' website='' photo1='www.nissanmicra.co.in/uploads/6777581574fa0c71630da9.jpg' photo2='www.nissanmicra.co.in/uploads/10477332074fa0c71632178.jpg' description='Pramukh NISSAN
Survey No. 134,Ravenue Survey No.312, Vapi Town,N.H. No. 8, Near Jalaram Temple' latitude='20.43804' longitude='73.03928'></store></city><city name='Varanasi'><store id='69' name='Avantika NISSAN' address='Avantika NISSAN
Shukla Niwas,B.12/8, Gauriganj, Bhelupur,
Varanasi - 221010
U.P.' phone1='0542-2275003' phone2='' mobile1='+91 8933944444' mobile2='' fax='0542-2275006' email='sales@avantikanissan.co.in' website='' photo1='' photo2='' description='Avantika NISSAN
Shukla Niwas,B.12/8, Gauriganj, Bhelupur,
Varanasi - 221010
U.P.' latitude='25.3019' longitude='82.99596'></store></city><city name='Vellore'><store id='84' name='Jubilant NISSAN' address='S.No.374, 
Alamelumangapuram,
Vellore - 632 009
Tamil Nadu' phone1='0416-2255061' phone2='' mobile1='+91 96777 77241' mobile2='' fax='' email='sales.vlr@jubilantnissan.co.in' website='' photo1='' photo2='' description='S.No.374, 
Alamelumangapuram,
Vellore - 632 009
Tamil Nadu
' latitude='12.94267' longitude='79.18584'></store></city><city name='Vijaywada'><store id='21' name='Lucky NISSAN' address='6/88,
Opp Tankasala Kalyana Mandapam,
Vijaywada 521108' phone1='' phone2='' mobile1='+91 8662845888 ' mobile2='+91 8662844111' fax='' email='' website='' photo1='www.nissanmicra.co.in/uploads/21170188474e1702d71e821.jpg' photo2='www.nissanmicra.co.in/uploads/9458327934e1702d720845.jpg' description='Lucky NISSAN' latitude='16.50617' longitude='80.64802'></store></city><city name='Vishakhapatnam'><store id='51' name='Kantipudi NISSAN' address='Kantipudi NISSAN,
S.No:122/3, NH-5,
Sheela Nagar, 
opp. Ayyappa Swamy Temple, 
Adjacent to Vikas College, 
Vishakhapatnam - 530012' phone1='0891-2545545' phone2='+91 7799977777' mobile1='+91 77999-77111, +91 7799977333' mobile2='+91 7799977112, +91 7799977339' fax='' email='info.vsp@kantipudinissan.co.in' website='' photo1='www.nissanmicra.co.in/uploads/11793352134f95222291da3.jpg' photo2='' description='Kantipudi NISSAN,
S.No:122/3, NH-5,
Sheela Nagar, 
opp. Ayyappa Swamy Temple, 
Adjacent to Vikas College, 
Vishakhapatnam - 530012' latitude='17.71713' longitude='83.19921'></store></city></stores>