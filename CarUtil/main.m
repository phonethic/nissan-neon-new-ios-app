//
//  main.m
//  CarUtil
//
//  Created by Rishi Saxena on 29/03/12.
//  Copyright (c) 2012 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CarUtilAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CarUtilAppDelegate class]));
    }
}
