//
//  CarUtilTips.h
//  CarUtil
//
//  Created by Sagar Mody on 20/01/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarUtilTips : NSObject {
    
}
@property (nonatomic, readwrite) int tipsid;
@property (nonatomic, copy) NSString *ilink;
@property (nonatomic, copy) NSString *vlink;

-(id)initWithID:(int)ltipsid image:(NSString *)lilink  video:(NSString *)lvlink;
@end
